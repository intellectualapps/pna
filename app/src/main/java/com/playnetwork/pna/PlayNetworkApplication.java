package com.playnetwork.pna;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.multidex.MultiDexApplication;

import com.cloudinary.android.MediaManager;
import com.google.firebase.database.FirebaseDatabase;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.LaunchActivity;
import com.playnetwork.pna.utils.AppLifecycleHandler;
import com.playnetwork.pna.utils.AutoRefreshExecutor;
import com.playnetwork.pna.utils.FirebaseUtils;
import com.playnetwork.pna.utils.ListUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class PlayNetworkApplication extends MultiDexApplication {
    static File cacheFile;
    private static PlayNetworkApplication playNetworkApplication;

    public static PlayNetworkApplication get(Context context) {
        return (PlayNetworkApplication) context.getApplicationContext();
    }

    public static Resources getApplicationResources() {
        return getAppInstance().getResources();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        cacheFile = new File(getCacheDir(), "responses");
        playNetworkApplication = this;
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        FirebaseUtils.initializeFirebase();
        initUncaughtExceptionHandler();
        initializeAutoRefreshExecutor();
        MediaManager.init(this, getCloudinaryConfig());
        registerActivityLifecycleCallbacks(new AppLifecycleHandler());
    }

    public static Map getCloudinaryConfig() {
        Map config = new HashMap();
        config.put("cloud_name", BuildConfig.CLOUDINARY_NAME);
        config.put("api_key", BuildConfig.CLOUDINARY_API_KEY);
        config.put("api_secret", BuildConfig.CLOUDINARY_API_SECRET);
        return config;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }

    public static File getCacheFile() {
        return cacheFile;
    }

    public static PlayNetworkApplication getAppInstance() {
        return playNetworkApplication;
    }

    private void initializeAutoRefreshExecutor() {
        final Timer timerExecutor = new Timer();
        final TimerTask timerTask;
        timerTask = new TimerTask() {
            @Override
            public void run() {
                AutoRefreshExecutor autoRefreshExecutor = new AutoRefreshExecutor();
                autoRefreshExecutor.execute(null);
            }
        };
        timerExecutor.schedule(timerTask, checkIfLookUpDataAlreadyFetched() ? 60 * 1000 * 10 : 0, 60 * 1000 * 60 * 48); // Update Lookup Data every 48 hours
    }

    private boolean checkIfLookUpDataAlreadyFetched() {
        PlayNetworkDatabase playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        boolean flag = ListUtils.isNotEmpty(playNetworkDatabase.industryDao().getAllIndustries()) && ListUtils.isNotEmpty(playNetworkDatabase.countryDao().getAllCountries());
        PlayNetworkDatabase.destroyDatabaseInstance();
        return flag;
    }

    private void initUncaughtExceptionHandler() {
        final ScheduledThreadPoolExecutor c = new ScheduledThreadPoolExecutor(1);
        c.schedule(new Runnable() {
            @Override
            public void run() {
                final Thread.UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
                Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    @Override
                    public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                        Intent intent = new Intent(getApplicationContext(), LaunchActivity.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        PendingIntent pendingIntent = PendingIntent.getActivity(getAppInstance().getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);

                        AlarmManager mgr = (AlarmManager) getAppInstance().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                        if (mgr != null) {
                            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, pendingIntent);
                        }

                        defaultHandler.uncaughtException(paramThread, paramThrowable);
                    }
                });
            }
        }, 5, TimeUnit.SECONDS);
    }
}
