package com.playnetwork.pna.data.api.interceptors;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.utils.NetworkUtils;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HttpHeaderInterceptor implements Interceptor {
    private Map<String, String> headers;

    public HttpHeaderInterceptor(Map<String, String> headers) {
        this.headers = headers;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder();
        requestBuilder.header("Content-Type", "application/json")
                .removeHeader("Pragma")
                .build();
        if (headers != null && headers.size() > 0) {
            for (String key : headers.keySet()) {
                if (headers.get(key) != null) {
                    requestBuilder.header(key, headers.get(key));
                }
            }
        }
        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
