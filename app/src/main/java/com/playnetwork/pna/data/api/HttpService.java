package com.playnetwork.pna.data.api;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.api.responses.ChatSessionsResponse;
import com.playnetwork.pna.data.api.responses.CommentResponse;
import com.playnetwork.pna.data.api.responses.DeviceResponse;
import com.playnetwork.pna.data.api.responses.EventResponse;
import com.playnetwork.pna.data.api.responses.ExperienceResponse;
import com.playnetwork.pna.data.api.responses.FeedCommentResponse;
import com.playnetwork.pna.data.api.responses.GenericFeedResponse;
import com.playnetwork.pna.data.api.responses.HomeScreenResponse;
import com.playnetwork.pna.data.api.responses.LikeFeedResponse;
import com.playnetwork.pna.data.api.responses.LookupCountriesResponse;
import com.playnetwork.pna.data.api.responses.LookupIndustriesResponse;
import com.playnetwork.pna.data.api.responses.LookupStatesResponse;
import com.playnetwork.pna.data.api.responses.MemberResponse;
import com.playnetwork.pna.data.api.responses.NetworkResponse;
import com.playnetwork.pna.data.api.responses.OfferResponse;
import com.playnetwork.pna.data.api.responses.PNADailyResponse;
import com.playnetwork.pna.data.api.responses.PasswordResetResponse;
import com.playnetwork.pna.data.api.responses.PublicKeyResponse;
import com.playnetwork.pna.data.api.responses.SkillResponse;
import com.playnetwork.pna.data.api.responses.SlimMemberResponse;
import com.playnetwork.pna.data.api.responses.SubscriptionAmountResponse;
import com.playnetwork.pna.data.api.responses.TransactionResponse;
import com.playnetwork.pna.data.api.responses.UpdateNetworkInvitationResponse;
import com.playnetwork.pna.data.api.responses.YellowPageResponse;
import com.playnetwork.pna.data.models.Feed;
import com.playnetwork.pna.utils.ApiUtils;

import org.parceler.apache.commons.collections.MapUtils;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class HttpService {
    private static final String TAG = "HttpService";

    public HttpService() {

    }

    private static ApiServiceEndpoints getApiServiceEndpoints(Map<String, String> headers) {
        if (apiServiceEndpoints == null || MapUtils.isNotEmpty(headers)) {
            apiServiceEndpoints = ApiModule.getApiModuleInstance(PlayNetworkApplication.getCacheFile(), ApiUtils.buildHeaders(headers)).getApiService();
        }

        return apiServiceEndpoints;
    }

    private static ApiServiceEndpoints apiServiceEndpoints = null;


    public static void resetApiService() {
        apiServiceEndpoints = null;
    }

    public void authenticateUser(Map<String, String> requestMap, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiServiceEndpoints(null).authenticateAccount(requestMap);
        call.enqueue(callback);
    }

    public void authenticateSocialAccount(Map<String, String> requestMap, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiServiceEndpoints(null).authenticateSocialAccount(requestMap);
        call.enqueue(callback);
    }

    public void updateSocialAccount(Map<String, String> requestMap, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiServiceEndpoints(null).updateSocialAccount(requestMap);
        call.enqueue(callback);
    }

    public void createAccount(Map<String, String> requestMap, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiServiceEndpoints(null).createAccount(requestMap);
        call.enqueue(callback);
    }

    public void createMemberProfile(Map<String, String> requestMap, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiServiceEndpoints(null).createMemberProfile(requestMap);
        call.enqueue(callback);
    }

    public void updateMemberProfile(Map<String, String> requestMap, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiServiceEndpoints(null).updateMemberProfile(requestMap);
        call.enqueue(callback);
    }

    public void fetchFeed(Map<String, String> requestMap, Callback<GenericFeedResponse<List<Feed>>> callback) {
        Call<GenericFeedResponse<List<Feed>>> call = getApiServiceEndpoints(null).fetchFeed(requestMap);
        call.enqueue(callback);
    }

    public void fetchNetworkSuggestions(Map<String, String> requestMap, Callback<NetworkResponse> callback) {
        Call<NetworkResponse> call = getApiServiceEndpoints(null).fetchNetworkSuggestions(requestMap);
        call.enqueue(callback);
    }

    public void fetchMemberNetwork(String memberId, Map<String, String> requestMap, Callback<NetworkResponse> callback) {
        Call<NetworkResponse> call = getApiServiceEndpoints(null).fetchMemberNetwork(memberId, requestMap);
        call.enqueue(callback);
    }

    public void addMembersToNetwork(Map<String, String> requestMap, Callback<NetworkResponse> callback) {
        Call<NetworkResponse> call = getApiServiceEndpoints(null).addMembersToNetwork(requestMap);
        call.enqueue(callback);
    }

    public void fetchFeedComments(String feedId, Map<String, String> requestMap, Callback<FeedCommentResponse> callback) {
        Call<FeedCommentResponse> call = getApiServiceEndpoints(null).fetchFeedComments(feedId, requestMap);
        call.enqueue(callback);
    }

    public void createComment(String feedId, Map<String, String> requestMap, Callback<CommentResponse> callback) {
        Call<CommentResponse> call = getApiServiceEndpoints(null).createComment(feedId, requestMap);
        call.enqueue(callback);
    }

    public void updateFeedLikeStatus(String feedId, String email, Callback<LikeFeedResponse> callback) {
        Call<LikeFeedResponse> call = getApiServiceEndpoints(null).updateFeedLikeStatus(feedId, email);
        call.enqueue(callback);
    }

    public void resetPassword(Map<String, String> requestMap, Callback<PasswordResetResponse> callback) {
        Call<PasswordResetResponse> call = getApiServiceEndpoints(null).resetPassword(requestMap);
        call.enqueue(callback);
    }

    public void fetchIndustries(Callback<LookupIndustriesResponse> callback) {
        Call<LookupIndustriesResponse> call = getApiServiceEndpoints(null).fetchIndustries();
        call.enqueue(callback);
    }

    public void fetchCountries(Callback<LookupCountriesResponse> callback) {
        Call<LookupCountriesResponse> call = getApiServiceEndpoints(null).fetchCountries();
        call.enqueue(callback);
    }

    public void fetchStates(String countryId, Callback<LookupStatesResponse> callback) {
        Call<LookupStatesResponse> call = getApiServiceEndpoints(null).fetchStates(countryId);
        call.enqueue(callback);
    }

    public void fetchMemberProfile(String memberId, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiServiceEndpoints(null).fetchMemberProfile(memberId);
        call.enqueue(callback);
    }

    public void fetchMemberProfile(String memberId, String otherMemberId, Callback<MemberResponse> callback) {
        Call<MemberResponse> call = getApiServiceEndpoints(null).fetchOtherMemberProfile(memberId, otherMemberId);
        call.enqueue(callback);
    }

    public void fetchSlimMemberProfile(String id, Callback<SlimMemberResponse> callback) {
        Call<SlimMemberResponse> call = getApiServiceEndpoints(null).fetchSlimMemberProfile(id);
        call.enqueue(callback);
    }

    public void storeDeviceToken(Map<String, String> requestMap, Callback<DeviceResponse> callback) {
        Call<DeviceResponse> call = getApiServiceEndpoints(null).storeDeviceToken(requestMap);
        call.enqueue(callback);
    }

    public void updateNetworkInvitation(String networkId, String option, String memberId, Callback<UpdateNetworkInvitationResponse> callback) {
        Call<UpdateNetworkInvitationResponse> call = getApiServiceEndpoints(null).updateNetworkInvitation(networkId, option, memberId);
        call.enqueue(callback);
    }

    public void fetchMemberNetworkRequests(String memberId, Map<String, String> requestMap, Callback<NetworkResponse> callback) {
        Call<NetworkResponse> call = getApiServiceEndpoints(null).fetchMemberNetworkRequests(memberId, requestMap);
        call.enqueue(callback);
    }

    public void fetchDiscountOffers(Map<String, String> requestMap, Callback<OfferResponse> callback) {
        Call<OfferResponse> call = getApiServiceEndpoints(null).fetchDiscountOffers(requestMap);
        call.enqueue(callback);
    }

    public void redeemOffer(String offerId, Map<String, String> requestMap, Callback<OfferResponse> callback) {
        Call<OfferResponse> call = getApiServiceEndpoints(null).redeemOffer(offerId, requestMap);
        call.enqueue(callback);
    }

    public void fetchOffer(String offerId, String memberId, Callback<OfferResponse> callback) {
        Call<OfferResponse> call = getApiServiceEndpoints(null).fetchOffer(offerId, memberId);
        call.enqueue(callback);
    }

    public void fetchPNADaily(Callback<PNADailyResponse> callback) {
        Call<PNADailyResponse> call = getApiServiceEndpoints(null).fetchPNADaily();
        call.enqueue(callback);
    }

    public void fetchSubscriptionAmount(Callback<SubscriptionAmountResponse> callback) {
        Call<SubscriptionAmountResponse> call = getApiServiceEndpoints(null).fetchSubscriptionAmount();
        call.enqueue(callback);
    }

    public void fetchPublicKey(Callback<PublicKeyResponse> callback) {
        Call<PublicKeyResponse> call = getApiServiceEndpoints(null).fetchPublicKey();
        call.enqueue(callback);
    }

    public void initializeTransaction(Map<String, String> dataMap, Callback<TransactionResponse> callback) {
        Call<TransactionResponse> call = getApiServiceEndpoints(null).initializeTransaction(dataMap);
        call.enqueue(callback);
    }

    public void initializeTicketTransaction(Map<String, String> dataMap, Callback<TransactionResponse> callback) {
        Call<TransactionResponse> call = getApiServiceEndpoints(null).initializeTicketTransaction(dataMap);
        call.enqueue(callback);
    }

    public void initializeTableTransaction(Map<String, String> dataMap, Callback<TransactionResponse> callback) {
        Call<TransactionResponse> call = getApiServiceEndpoints(null).initializeTableTransaction(dataMap);
        call.enqueue(callback);
    }

    public void verifyTransaction(String referenceCode, String emailAddress, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiServiceEndpoints(null).verifyTransaction(referenceCode, emailAddress);
        call.enqueue(callback);
    }

    public void verifyTicketTransaction(String referenceCode, String emailAddress, Callback<EventResponse> callback) {
        Call<EventResponse> call = getApiServiceEndpoints(null).verifyTicketTransaction(referenceCode, emailAddress);
        call.enqueue(callback);
    }

    public void verifyTableTransaction(String referenceCode, String emailAddress, Callback<EventResponse> callback) {
        Call<EventResponse> call = getApiServiceEndpoints(null).verifyTableTransaction(referenceCode, emailAddress);
        call.enqueue(callback);
    }

    public void fetchEvents(Map<String, String> requestMap, Callback<EventResponse> callback) {
        Call<EventResponse> call = getApiServiceEndpoints(null).fetchEvents(requestMap);
        call.enqueue(callback);
    }

    public void fetchEvent(String eventId, String memberId, Callback<EventResponse> callback) {
        Call<EventResponse> call = getApiServiceEndpoints(null).fetchEvent(eventId, memberId);
        call.enqueue(callback);
    }

    public void confirmEventRSVP(Map<String, String> requestMap, Callback<EventResponse> callback) {
        Call<EventResponse> call = getApiServiceEndpoints(null).RSVPEvent(requestMap);
        call.enqueue(callback);
    }

    public void fetchSearchResults(String memberId, String searchTerm, Map<String, String> requestMap, Callback<NetworkResponse> callback) {
        Call<NetworkResponse> call = getApiServiceEndpoints(null).fetchSearchResults(memberId, searchTerm, requestMap);
        call.enqueue(callback);
    }

    public void createPost(String memberId, Map<String, String> requestMap, Callback<GenericFeedResponse<Feed>> callback) {
        Call<GenericFeedResponse<Feed>> call = getApiServiceEndpoints(null).createPost(memberId, requestMap);
        call.enqueue(callback);
    }

    public void fetchYellowPages(Map<String, String> requestMap, boolean inFilterMode, Callback<YellowPageResponse> callback) {
        Call<YellowPageResponse> call = inFilterMode ? getApiServiceEndpoints(null).fetchFilteredYellowPages(requestMap) : getApiServiceEndpoints(null).fetchYellowPages(requestMap);
        call.enqueue(callback);
    }

    public void searchYellowPages(String searchTerm, Map<String, String> requestMap, Callback<YellowPageResponse> callback) {
        Call<YellowPageResponse> call = getApiServiceEndpoints(null).searchYellowPages(searchTerm, requestMap);
        call.enqueue(callback);
    }

    public void fetchYellowPageById(String listingId, Callback<YellowPageResponse> callback) {
        Call<YellowPageResponse> call = getApiServiceEndpoints(null).fetchYellowPageById(listingId);
        call.enqueue(callback);
    }

    public void initializeChatSession(Map<String, String> requestMap, Callback<ChatSessionsResponse> callback) {
        Call<ChatSessionsResponse> call = getApiServiceEndpoints(null).initializeChatSession(requestMap);
        call.enqueue(callback);
    }

    public void addWorkExperience(String memberId, Map<String, String> requestMap, Callback<ExperienceResponse> callback) {
        Call<ExperienceResponse> call = getApiServiceEndpoints(null).addWorkExperience(memberId, requestMap);
        call.enqueue(callback);
    }

    public void addSkills(String memberId, Map<String, String> requestMap, Callback<SkillResponse> callback) {
        Call<SkillResponse> call = getApiServiceEndpoints(null).addSkills(memberId, requestMap);
        call.enqueue(callback);
    }

    public void fetchHomeScreenData(String memberId, Callback<HomeScreenResponse> callback) {
        Call<HomeScreenResponse> call = getApiServiceEndpoints(null).fetchHomeScreenData(memberId);
        call.enqueue(callback);
    }
}
