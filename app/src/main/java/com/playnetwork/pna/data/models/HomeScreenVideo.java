package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;

@Entity
public class HomeScreenVideo extends HomeScreenItem {
    private String id;
    private String videoUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
