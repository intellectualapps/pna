package com.playnetwork.pna.data.enums;

public enum RedemptionOption {
    QR,
    CARD,
    ANY;

    RedemptionOption() {
    }

    public static RedemptionOption fromString(String redemptionOptionName) {
        for (RedemptionOption redemptionOption : RedemptionOption.values()) {
            if (redemptionOption.name().equalsIgnoreCase(redemptionOptionName)) {
                return redemptionOption;
            }
        }
        return null;
    }
}
