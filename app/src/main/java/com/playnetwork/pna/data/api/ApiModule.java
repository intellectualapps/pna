package com.playnetwork.pna.data.api;

import android.util.Log;

import com.playnetwork.pna.BuildConfig;
import com.playnetwork.pna.data.api.interceptors.HttpHeaderInterceptor;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiModule {
    private File cacheFile;
    private static ApiServiceEndpoints apiServiceEndpoints;
    private Map<String, String> headers;
    private static ApiModule apiModuleInstance;
    private static Retrofit.Builder retrofitBuilder;
    private static OkHttpClient.Builder okHttpClientBuilder;
    private static Retrofit retrofit;
    private static Dispatcher mDispatcher;

    public ApiModule(File cacheFile, final Map<String, String> headers, Boolean flag) {
        this.cacheFile = cacheFile;
        this.headers = headers;
        apiServiceEndpoints = getApiModuleInstance(cacheFile, headers).getApiService();
        apiModuleInstance = this;
    }

    private ApiModule(File cacheFile, final Map<String, String> headers) {
        this.cacheFile = cacheFile;
        this.headers = headers;
        retrofit = buildRetrofitAdapter(cacheFile, headers);
        if (retrofit != null) {
            apiServiceEndpoints = retrofit.create(ApiServiceEndpoints.class);
        }
        apiModuleInstance = this;
    }

    public static void resetApiClient() {
        retrofit = null;
        retrofitBuilder = null;
        okHttpClientBuilder = null;
        apiModuleInstance = null;
        apiServiceEndpoints = null;
    }

    ApiServiceEndpoints getApiService() {
        return apiServiceEndpoints;
    }


    static ApiModule getApiModuleInstance(File cacheFile, final Map<String, String> headers) {
        if (apiModuleInstance == null) {
            apiModuleInstance = new ApiModule(cacheFile, headers);
        }

        return apiModuleInstance;
    }

    private static OkHttpClient buildOkHttpClient(File cacheFile, final Map<String, String> headers) {
        if (okHttpClientBuilder == null) {
            okHttpClientBuilder = new OkHttpClient.Builder();

            Cache cache = null;
            try {
                cache = new Cache(cacheFile, 10 * 1024 * 1024);

                if (BuildConfig.DEBUG) {
                    HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                     //okHttpClientBuilder.addInterceptor(logging);
                }

                okHttpClientBuilder.connectTimeout(60 * 1000, TimeUnit.MILLISECONDS)
                        .readTimeout(60 * 1000, TimeUnit.MILLISECONDS)
                        .writeTimeout(60 * 1000, TimeUnit.MILLISECONDS);

                okHttpClientBuilder.cache(cache);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return okHttpClientBuilder.build();
    }

    public static void cancelAllRequests() {
        if (mDispatcher != null) {
            mDispatcher.executorService().shutdown();
        }
    }

    public static String getBaseUrl() {
        return BuildConfig.DEBUG ? "http://api.bulamayusuf.com" : "http://api.bulamayusuf.com";
    }

    private static Retrofit.Builder buildRetrofitBuilder(OkHttpClient okHttpClient) {
        if (retrofitBuilder == null) {
            retrofitBuilder = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create());
        }

        return retrofitBuilder;
    }

    public static Retrofit buildRetrofitAdapter(File cacheFile, Map<String, String> headers) {
        if (retrofit == null) {
            retrofit = buildRetrofitBuilder(buildOkHttpClient(cacheFile, headers)).build();
        }

        return retrofit;
    }
}
