package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;

@Entity
public class HomeScreenPNADaily extends HomeScreenItem {
    private String contentUrl;

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }
}
