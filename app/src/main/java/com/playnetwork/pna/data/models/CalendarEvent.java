package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class CalendarEvent implements Comparable<CalendarEvent>{
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String eventId;
    private long calendarEventId;

    public CalendarEvent(String eventId, long calendarEventId) {
        this.eventId = eventId;
        this.calendarEventId = calendarEventId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public long getCalendarEventId() {
        return calendarEventId;
    }

    public void setCalendarEventId(long calendarEventId) {
        this.calendarEventId = calendarEventId;
    }

    @Override
    public int compareTo(@NonNull CalendarEvent o) {
        if (getEventId() == null || o.getEventId() == null)
            return 0;
        return getEventId().compareTo(o.getEventId());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((eventId == null) ? 0 : eventId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof CalendarEvent && ((CalendarEvent) o).getEventId().equals(eventId);
    }

    @Override
    public String toString() {
        return "CalendarEvent{" +
                "id=" + id +
                ", eventId='" + eventId + '\'' +
                ", calendarEventId=" + calendarEventId +
                '}';
    }
}
