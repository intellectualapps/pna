package com.playnetwork.pna.data.enums;

public enum PaymentType {
    REGULAR_TICKET("Regular Ticket", "regular"),
    VIP_TICKET("VIP Ticket", "vip"),
    REGULAR_TABLE("Regular Table", "regular"),
    VIP_TABLE("VIP Table", "vip"),
    PREMIUM_SUBSCRIPTION("Premium Subscription", "premium");

    private String value;
    private String type;

    PaymentType(String value, String type) {
        this.value = value;
        this.type = type;
    }

    public static PaymentType fromString(String paymentTypeName) {
        for (PaymentType paymentType : PaymentType.values()) {
            if (paymentType.name().equalsIgnoreCase(paymentTypeName)) {
                return paymentType;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
