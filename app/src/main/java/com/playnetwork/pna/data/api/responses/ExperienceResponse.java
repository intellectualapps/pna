package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.Experience;

public class ExperienceResponse extends DefaultResponse {
    @SerializedName("experience")
    @Expose
    private Experience experience;

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }
}
