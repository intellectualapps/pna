package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.Industry;

import java.util.List;

public class LookupIndustriesResponse extends DefaultResponse {
    @SerializedName("industries")
    @Expose
    private List<Industry> industries;

    public List<Industry> getIndustries() {
        return industries;
    }

    public void setIndustries(List<Industry> industries) {
        this.industries = industries;
    }
}
