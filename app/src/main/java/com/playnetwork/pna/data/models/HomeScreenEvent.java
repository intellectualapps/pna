package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;

@Entity
public class HomeScreenEvent extends HomeScreenItem{
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
