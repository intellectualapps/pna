package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.State;

import java.util.List;

public class LookupStatesResponse extends DefaultResponse {
    @SerializedName("states")
    @Expose
    private List<State> states;

    public List<State> getStates() {
        return states;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }
}
