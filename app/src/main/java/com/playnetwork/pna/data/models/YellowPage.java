package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity
public class YellowPage implements Parcelable {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("memberId")
    @Expose
    private int memberId;
    @SerializedName("businessName")
    @Expose
    private String businessName;
    @SerializedName("businessUrl")
    @Expose
    private String businessUrl;
    @SerializedName("businessId")
    @Expose
    private String businessId;
    @SerializedName("businessCategory")
    @Expose
    private String businessCategory;
    @SerializedName("businessPhone")
    @Expose
    private String businessPhone;
    @SerializedName("otherPhones")
    @Expose
    private String otherPhones;
    @SerializedName("businessEmail")
    @Expose
    private String businessEmail;
    @SerializedName("businessAddress")
    @Expose
    private String businessAddress;
    @SerializedName("businessState")
    @Expose
    private String businessState;
    @SerializedName("businessDetails")
    @Expose
    private String businessDetails;
    @SerializedName("businessOffers")
    @Expose
    private List<String> businessOffers = null;
    @SerializedName("businessWebsite")
    @Expose
    private String businessWebsite;
    @SerializedName("businessTags")
    @Expose
    private List<String> businessTags = null;
    @SerializedName("businessLogo")
    @Expose
    private String businessLogo;
    @SerializedName("businessHours")
    @Expose
    private List<String> businessHours = null;
    @SerializedName("methodsOfPayment")
    @Expose
    private List<String> methodsOfPayment = null;
    @SerializedName("dateAdded")
    @Expose
    private String dateAdded;
    @SerializedName("views")
    @Expose
    private int views;

    public YellowPage() {

    }

    protected YellowPage(Parcel in) {
        id = in.readInt();
        memberId = in.readInt();
        businessName = in.readString();
        businessUrl = in.readString();
        businessId = in.readString();
        businessCategory = in.readString();
        businessPhone = in.readString();
        otherPhones = in.readString();
        businessEmail = in.readString();
        businessAddress = in.readString();
        businessState = in.readString();
        businessDetails = in.readString();
        businessOffers = in.createStringArrayList();
        businessWebsite = in.readString();
        businessTags = in.createStringArrayList();
        businessLogo = in.readString();
        businessHours = in.createStringArrayList();
        methodsOfPayment = in.createStringArrayList();
        dateAdded = in.readString();
        views = in.readInt();
    }

    public static final Creator<YellowPage> CREATOR = new Creator<YellowPage>() {
        @Override
        public YellowPage createFromParcel(Parcel in) {
            return new YellowPage(in);
        }

        @Override
        public YellowPage[] newArray(int size) {
            return new YellowPage[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessUrl() {
        return businessUrl;
    }

    public void setBusinessUrl(String businessUrl) {
        this.businessUrl = businessUrl;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getBusinessCategory() {
        return businessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        this.businessCategory = businessCategory;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getOtherPhones() {
        return otherPhones;
    }

    public void setOtherPhones(String otherPhones) {
        this.otherPhones = otherPhones;
    }

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getBusinessState() {
        return businessState;
    }

    public void setBusinessState(String businessState) {
        this.businessState = businessState;
    }

    public String getBusinessDetails() {
        return businessDetails;
    }

    public void setBusinessDetails(String businessDetails) {
        this.businessDetails = businessDetails;
    }

    public List<String> getBusinessOffers() {
        return businessOffers;
    }

    public void setBusinessOffers(List<String> businessOffers) {
        this.businessOffers = businessOffers;
    }

    public String getBusinessWebsite() {
        return businessWebsite;
    }

    public void setBusinessWebsite(String businessWebsite) {
        this.businessWebsite = businessWebsite;
    }

    public List<String> getBusinessTags() {
        return businessTags;
    }

    public void setBusinessTags(List<String> businessTags) {
        this.businessTags = businessTags;
    }

    public String getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        this.businessLogo = businessLogo;
    }

    public List<String> getBusinessHours() {
        return businessHours;
    }

    public void setBusinessHours(List<String> businessHours) {
        this.businessHours = businessHours;
    }

    public List<String> getMethodsOfPayment() {
        return methodsOfPayment;
    }

    public void setMethodsOfPayment(List<String> methodsOfPayment) {
        this.methodsOfPayment = methodsOfPayment;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(memberId);
        dest.writeString(businessName);
        dest.writeString(businessUrl);
        dest.writeString(businessId);
        dest.writeString(businessCategory);
        dest.writeString(businessPhone);
        dest.writeString(otherPhones);
        dest.writeString(businessEmail);
        dest.writeString(businessAddress);
        dest.writeString(businessState);
        dest.writeString(businessDetails);
        dest.writeStringList(businessOffers);
        dest.writeString(businessWebsite);
        dest.writeStringList(businessTags);
        dest.writeString(businessLogo);
        dest.writeStringList(businessHours);
        dest.writeStringList(methodsOfPayment);
        dest.writeString(dateAdded);
        dest.writeInt(views);
    }
}
