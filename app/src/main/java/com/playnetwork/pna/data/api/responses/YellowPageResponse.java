package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.YellowPage;

import java.util.List;

public class YellowPageResponse extends DefaultResponse {
    @SerializedName("yellowPages")
    @Expose
    private List<YellowPage> yellowPages;
    @SerializedName("yellowPage")
    @Expose
    private YellowPage yellowPage;

    public List<YellowPage> getYellowPages() {
        return yellowPages;
    }

    public void setYellowPages(List<YellowPage> yellowPages) {
        this.yellowPages = yellowPages;
    }

    public YellowPage getYellowPage() {
        return yellowPage;
    }

    public void setYellowPage(YellowPage yellowPage) {
        this.yellowPage = yellowPage;
    }
}
