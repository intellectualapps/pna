package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlimMemberResponse extends DefaultResponse {
    @SerializedName("memberId")
    @Expose
    private int memberId;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("jobTitle")
    @Expose
    private String jobTitle;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("profilePhoto")
    @Expose
    private String profilePhoto;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }
}
