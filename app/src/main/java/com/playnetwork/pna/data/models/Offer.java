package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity
public class Offer implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int primaryId;
    private String id;
    private String title;
    private String businessName;
    private String address;
    private String type;
    private String photo;
    private String details;
    private String status;
    private String offerLevel;
    private String createdAt;
    private String startsAt;
    private String stopsAt;
    private String redemptionOption;
    private int numberOfTimesRedeemed;

    public Offer() {

    }

    protected Offer(Parcel in) {
        primaryId = in.readInt();
        id = in.readString();
        title = in.readString();
        businessName = in.readString();
        address = in.readString();
        type = in.readString();
        photo = in.readString();
        details = in.readString();
        status = in.readString();
        offerLevel = in.readString();
        createdAt = in.readString();
        startsAt = in.readString();
        stopsAt = in.readString();
        numberOfTimesRedeemed = in.readInt();
        redemptionOption = in.readString();
    }

    public static final Creator<Offer> CREATOR = new Creator<Offer>() {
        @Override
        public Offer createFromParcel(Parcel in) {
            return new Offer(in);
        }

        @Override
        public Offer[] newArray(int size) {
            return new Offer[size];
        }
    };

    public int getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(int primaryId) {
        this.primaryId = primaryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOfferLevel() {
        return offerLevel;
    }

    public void setOfferLevel(String offerLevel) {
        this.offerLevel = offerLevel;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(String startsAt) {
        this.startsAt = startsAt;
    }

    public String getStopsAt() {
        return stopsAt;
    }

    public void setStopsAt(String stopsAt) {
        this.stopsAt = stopsAt;
    }

    public int getNumberOfTimesRedeemed() {
        return numberOfTimesRedeemed;
    }

    public void setNumberOfTimesRedeemed(int numberOfTimesRedeemed) {
        this.numberOfTimesRedeemed = numberOfTimesRedeemed;
    }

    public String getRedemptionOption() {
        return redemptionOption;
    }

    public void setRedemptionOption(String redemptionOption) {
        this.redemptionOption = redemptionOption;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(primaryId);
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(businessName);
        dest.writeString(address);
        dest.writeString(type);
        dest.writeString(photo);
        dest.writeString(details);
        dest.writeString(status);
        dest.writeString(offerLevel);
        dest.writeString(createdAt);
        dest.writeString(startsAt);
        dest.writeString(stopsAt);
        dest.writeInt(numberOfTimesRedeemed);
        dest.writeString(redemptionOption);
    }
}
