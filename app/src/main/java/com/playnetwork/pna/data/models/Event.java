package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Event implements Parcelable, Comparable<Event> {
    @PrimaryKey(autoGenerate = true)
    private int primaryId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("parsedDate")
    @Expose
    private String parsedDate;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("venue")
    @Expose
    private String venue;
    @SerializedName("organizer")
    @Expose
    private String organizer;
    @SerializedName("picUrl")
    @Expose
    private String picUrl;
    @SerializedName("dressCode")
    @Expose
    private String dressCode;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("hasRSVP")
    @Expose
    private boolean hasRSVP;
    @SerializedName("isPaidEvent")
    @Expose
    private boolean isPaidEvent;
    @SerializedName("hasTableReservation")
    @Expose
    private boolean hasTableReservation;
    @SerializedName("hasReservedTable")
    @Expose
    private boolean hasReservedTable;
    @SerializedName("numberOfRSVPs")
    @Expose
    private int numberOfRSVPs;
    @SerializedName("tickets")
    @Expose
    private Ticket tickets;
    @SerializedName("tables")
    @Expose
    private Table tables;
    @SerializedName("eventGroup")
    @Expose
    private String eventGroup;

    private long calendarEventId;

    public Event() {

    }

    public Event(String id, String date) {
        this.id = id;
        this.date = date;
    }

    private Event(Parcel in) {
        primaryId = in.readInt();
        id = in.readString();
        title = in.readString();
        address = in.readString();
        parsedDate = in.readString();
        date = in.readString();
        time = in.readString();
        venue = in.readString();
        organizer = in.readString();
        picUrl = in.readString();
        dressCode = in.readString();
        info = in.readString();
        hasRSVP = in.readByte() != 0;
        isPaidEvent = in.readByte() != 0;
        hasTableReservation = in.readByte() != 0;
        hasReservedTable = in.readByte() != 0;
        numberOfRSVPs = in.readInt();
        tickets = in.readParcelable(Ticket.class.getClassLoader());
        tables = in.readParcelable(Table.class.getClassLoader());
        calendarEventId = in.readLong();
        eventGroup = in.readString();
    }

    @Override
    public int compareTo(Event o) {
        if (getId() == null || o.getId() == null) {
            return 0;
        }
        return getId().compareTo(o.getId());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && o instanceof Event && ((Event) o).getId() != null && ((Event) o).getId().equals(id);
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(primaryId);
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(address);
        dest.writeString(parsedDate);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(venue);
        dest.writeString(organizer);
        dest.writeString(picUrl);
        dest.writeString(dressCode);
        dest.writeString(info);
        dest.writeByte((byte) (hasRSVP ? 1 : 0));
        dest.writeByte((byte) (isPaidEvent ? 1 : 0));
        dest.writeByte((byte) (hasTableReservation ? 1 : 0));
        dest.writeByte((byte) (hasReservedTable ? 1 : 0));
        dest.writeInt(numberOfRSVPs);
        dest.writeParcelable(tickets, flags);
        dest.writeParcelable(tables, flags);
        dest.writeLong(calendarEventId);
        dest.writeString(eventGroup);
    }

    public int getPrimaryId() {
        return primaryId;
    }

    public void setPrimaryId(int primaryId) {
        this.primaryId = primaryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getParsedDate() {
        return parsedDate;
    }

    public void setParsedDate(String parsedDate) {
        this.parsedDate = parsedDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getDressCode() {
        return dressCode;
    }

    public void setDressCode(String dressCode) {
        this.dressCode = dressCode;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean getHasRSVP() {
        return hasRSVP;
    }

    public void setHasRSVP(boolean hasRSVP) {
        this.hasRSVP = hasRSVP;
    }

    public boolean isPaidEvent() {
        return isPaidEvent;
    }

    public void setPaidEvent(boolean paidEvent) {
        isPaidEvent = paidEvent;
    }

    public boolean isHasTableReservation() {
        return hasTableReservation;
    }

    public void setHasTableReservation(boolean hasTableReservation) {
        this.hasTableReservation = hasTableReservation;
    }

    public boolean isHasReservedTable() {
        return hasReservedTable;
    }

    public void setHasReservedTable(boolean hasReservedTable) {
        this.hasReservedTable = hasReservedTable;
    }

    public int getNumberOfRSVPs() {
        return numberOfRSVPs;
    }

    public void setNumberOfRSVPs(int numberOfRSVPs) {
        this.numberOfRSVPs = numberOfRSVPs;
    }

    public Ticket getTickets() {
        return tickets;
    }

    public void setTickets(Ticket tickets) {
        this.tickets = tickets;
    }

    public Table getTables() {
        return tables;
    }

    public void setTables(Table tables) {
        this.tables = tables;
    }

    public long getCalendarEventId() {
        return calendarEventId;
    }

    public void setCalendarEventId(long calendarEventId) {
        this.calendarEventId = calendarEventId;
    }

    public String getEventGroup() {
        return eventGroup;
    }

    public void setEventGroup(String eventGroup) {
        this.eventGroup = eventGroup;
    }
}
