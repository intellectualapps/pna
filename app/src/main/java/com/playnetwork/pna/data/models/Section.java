package com.playnetwork.pna.data.models;

public class Section {
    private int firstPosition;
    private int sectionedPosition;
    private CharSequence title;

    public Section(int firstPosition, CharSequence title) {
        this.firstPosition = firstPosition;
        this.title = title;
    }

    public int getFirstPosition() {
        return firstPosition;
    }

    public void setFirstPosition(int firstPosition) {
        this.firstPosition = firstPosition;
    }

    public int getSectionedPosition() {
        return sectionedPosition;
    }

    public void setSectionedPosition(int sectionedPosition) {
        this.sectionedPosition = sectionedPosition;
    }

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    public CharSequence getTitle() {
        return title;
    }
}
