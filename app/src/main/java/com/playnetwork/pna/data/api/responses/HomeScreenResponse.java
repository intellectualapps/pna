package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.HomeScreenEvent;
import com.playnetwork.pna.data.models.HomeScreenOffer;
import com.playnetwork.pna.data.models.HomeScreenPNADaily;
import com.playnetwork.pna.data.models.HomeScreenVideo;
import com.playnetwork.pna.data.models.MemberPhoto;

import java.util.List;

public class HomeScreenResponse extends DefaultResponse {

    @SerializedName("pnaDaily")
    @Expose
    private HomeScreenPNADaily homeScreenPNADaily;
    @SerializedName("event")
    @Expose
    private HomeScreenEvent homeScreenEvent;
    @SerializedName("offer")
    @Expose
    private HomeScreenOffer homeScreenOffer;
    @SerializedName("video")
    @Expose
    private HomeScreenVideo homeScreenVideo;
    @SerializedName("memberPhotos")
    @Expose
    private List<MemberPhoto> memberPhotos;

    public HomeScreenPNADaily getHomeScreenPNADaily() {
        return homeScreenPNADaily;
    }

    public void setHomeScreenPNADaily(HomeScreenPNADaily homeScreenPNADaily) {
        this.homeScreenPNADaily = homeScreenPNADaily;
    }

    public HomeScreenEvent getHomeScreenEvent() {
        return homeScreenEvent;
    }

    public void setHomeScreenEvent(HomeScreenEvent homeScreenEvent) {
        this.homeScreenEvent = homeScreenEvent;
    }

    public HomeScreenOffer getHomeScreenOffer() {
        return homeScreenOffer;
    }

    public void setHomeScreenOffer(HomeScreenOffer homeScreenOffer) {
        this.homeScreenOffer = homeScreenOffer;
    }

    public HomeScreenVideo getHomeScreenVideo() {
        return homeScreenVideo;
    }

    public void setHomeScreenVideo(HomeScreenVideo homeScreenVideo) {
        this.homeScreenVideo = homeScreenVideo;
    }

    public List<MemberPhoto> getMemberPhotos() {
        return memberPhotos;
    }

    public void setMemberPhotos(List<MemberPhoto> memberPhotos) {
        this.memberPhotos = memberPhotos;
    }
}
