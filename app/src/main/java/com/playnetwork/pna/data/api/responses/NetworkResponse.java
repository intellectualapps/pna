package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;

import java.util.List;

public class NetworkResponse extends DefaultResponse {
    @SerializedName("network_suggestions")
    @Expose
    private List<User> networkSuggestions;

    @SerializedName("members")
    @Expose
    private List<User> members;

    @SerializedName("networkInvitation")
    @Expose
    private List<User> networkInvitation;

    @SerializedName("network")
    @Expose
    private List<Network> network;

    public List<User> getNetworkSuggestions() {
        return networkSuggestions;
    }

    public void setNetworkSuggestions(List<User> networkSuggestions) {
        this.networkSuggestions = networkSuggestions;
    }

    public List<User> getNetworkInvitations() {
        return networkInvitation;
    }

    public void setNetworkInvitations(List<User> networkInvitation) {
        this.networkInvitation = networkInvitation;
    }

    public List<Network> getNetwork() {
        return network;
    }

    public void setNetwork(List<Network> network) {
        this.network = network;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
