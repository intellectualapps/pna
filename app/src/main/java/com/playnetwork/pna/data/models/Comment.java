package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Comment {
    @PrimaryKey
    @SerializedName("commentId")
    @Expose
    private int commentId;
    @SerializedName("memberId")
    @Expose
    private int memberId;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("commentAuthorPicture")
    @Expose
    private String commentAuthorPicture;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("commentDate")
    @Expose
    private String commentDate;
    @SerializedName("networkState")
    @Expose
    private String networkState;


    public Comment() {

    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCommentAuthorPicture() {
        return commentAuthorPicture;
    }

    public void setCommentAuthorPicture(String commentAuthorPicture) {
        this.commentAuthorPicture = commentAuthorPicture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }
    public String getNetworkState() {
        return networkState;
    }

    public void setNetworkState(String networkState) {
        this.networkState = networkState;
    }
}
