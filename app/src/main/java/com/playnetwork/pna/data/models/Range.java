package com.playnetwork.pna.data.models;

import android.support.annotation.Keep;

@Keep
public class Range {
    private int high;
    private int low;

    public Range(int high, int low) {
        this.high = high;
        this.low = low;
    }

    public int getHigh() {
        return this.high;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    public int getLow() {
        return this.low;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public String toString() {
        return String.format("%s - %s", this.low, this.high);
    }
}
