package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.Feed;

import java.util.List;

public class FeedResponse extends DefaultResponse {
    @SerializedName("feed")
    @Expose
    private List<Feed> feed;

    public List<Feed> getFeed() {
        return feed;
    }

    public void setFeeds(List<Feed> feed) {
        this.feed = feed;
    }
}
