package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;

import java.util.List;

@Entity
public class HomeScreenActiveMembers extends HomeScreenItem {
    public HomeScreenActiveMembers(){

    }
    public HomeScreenActiveMembers(List<MemberPhoto> memberPhotos) {
        this.memberPhotos = memberPhotos;
    }

    private List<MemberPhoto> memberPhotos;

    public List<MemberPhoto> getMemberPhotos() {
        return memberPhotos;
    }

    public void setMemberPhotos(List<MemberPhoto> memberPhotos) {
        this.memberPhotos = memberPhotos;
    }
}
