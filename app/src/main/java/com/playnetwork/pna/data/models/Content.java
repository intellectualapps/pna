package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;

@Entity
public class Content {
    private String logo;
    private String headLine;
    private String body;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getHeadLine() {
        return headLine;
    }

    public void setHeadLine(String headLine) {
        this.headLine = headLine;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
