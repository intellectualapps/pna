package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.ChatSession;

import java.util.List;

public class ChatSessionsResponse extends DefaultResponse {
    @SerializedName("chatSessions")
    @Expose
    private List<ChatSession> chatSessions = null;
    @SerializedName("chatSession")
    @Expose
    private ChatSession chatSession;

    public List<ChatSession> getChatSessions() {
        return chatSessions;
    }

    public void setChatSessions(List<ChatSession> chatSessions) {
        this.chatSessions = chatSessions;
    }

    public ChatSession getChatSession() {
        return chatSession;
    }

    public void setChatSession(ChatSession chatSession) {
        this.chatSession = chatSession;
    }
}
