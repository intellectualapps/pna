package com.playnetwork.pna.data.api;

import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.api.responses.LookupCountriesResponse;
import com.playnetwork.pna.data.api.responses.LookupIndustriesResponse;
import com.playnetwork.pna.data.api.responses.LookupStatesResponse;
import com.playnetwork.pna.data.api.responses.PasswordResetResponse;
import com.playnetwork.pna.data.api.responses.PublicKeyResponse;
import com.playnetwork.pna.data.api.responses.SubscriptionAmountResponse;

public class ApiClientCallbacks {
    public interface AccountRegistrationListener {
        void onAccountRegistered(AuthenticationResponse authenticationResponse);
    }

    public interface AuthenticationListener {
        void onAccountAuthenticated(AuthenticationResponse authenticationResponse);
    }

    public interface PasswordResetListener {
        void onRequestSubmitted(PasswordResetResponse passwordResetResponse);
    }

    public interface LookupIndustriesListener {
        void onIndustriesFetched(LookupIndustriesResponse lookupIndustriesResponse);
    }

    public interface LookupCountriesListener {
        void onCountriesFetched(LookupCountriesResponse lookupCountriesResponse);
    }

    public interface SubscriptionAmountListener {
        void onSubscriptionAmountFetched(SubscriptionAmountResponse subscriptionAmountResponse);
    }

    public interface PublicKeyListener {
        void onPublicKeyFetched(PublicKeyResponse publicKeyResponse);
    }

    public interface LookupStatesListener {
        void onStatesFetched(LookupStatesResponse lookupStatesResponse);
    }
}
