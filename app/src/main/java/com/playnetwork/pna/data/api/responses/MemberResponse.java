package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.User;

public class MemberResponse extends DefaultResponse {
    @SerializedName("otherMemberProfile")
    @Expose
    private User user;
    @SerializedName("updatedMemberProfile")
    @Expose
    private User updatedMemberProfile;
    @SerializedName("profile")
    @Expose
    private User profile;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getProfile() {
        return profile;
    }

    public void setProfile(User profile) {
        this.profile = profile;
    }

    public User getUpdatedMemberProfile() {
        return updatedMemberProfile;
    }

    public void setUpdatedMemberProfile(User updatedMemberProfile) {
        this.updatedMemberProfile = updatedMemberProfile;
    }
}
