package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.Comment;

import java.util.List;

public class FeedCommentResponse extends DefaultResponse {
    @SerializedName("feedComments")
    @Expose
    private List<Comment> feedComments;

    public List<Comment> getFeedComments() {
        return feedComments;
    }

    public void setFeedComments(List<Comment> feedComments) {
        this.feedComments = feedComments;
    }
}
