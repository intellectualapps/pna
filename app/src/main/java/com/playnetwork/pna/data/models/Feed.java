package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Feed implements Parcelable {

    @PrimaryKey
    @SerializedName("feedId")
    @Expose
    private int feedId;
    @SerializedName("memberId")
    @Expose
    private int memberId;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("feedAuthorPicture")
    @Expose
    private String feedAuthorPicture;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("altContent")
    @Expose
    private String altContent;
    @SerializedName("likes")
    @Expose
    private int likes;
    @SerializedName("hasUserLiked")
    @Expose
    private boolean hasUserLiked;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("feedDate")
    @Expose
    private String feedDate;
    @SerializedName("networkState")
    @Expose
    private String networkState;
    @SerializedName("shareType")
    @Expose
    private String shareType;

    public Feed() {

    }

    public int getFeedId() {
        return feedId;
    }

    public void setFeedId(int feedId) {
        this.feedId = feedId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFeedAuthorPicture() {
        return feedAuthorPicture;
    }

    public void setFeedAuthorPicture(String feedAuthorPicture) {
        this.feedAuthorPicture = feedAuthorPicture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isHasUserLiked() {
        return hasUserLiked;
    }

    public void setHasUserLiked(boolean hasUserLiked) {
        this.hasUserLiked = hasUserLiked;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFeedDate() {
        return feedDate;
    }

    public void setFeedDate(String feedDate) {
        this.feedDate = feedDate;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getNetworkState() {
        return networkState;
    }

    public void setNetworkState(String networkState) {
        this.networkState = networkState;
    }

    public String getShareType() {
        return shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getAltContent() {
        return altContent;
    }

    public void setAltContent(String altContent) {
        this.altContent = altContent;
    }

    protected Feed(Parcel in) {
        feedId = in.readInt();
        memberId = in.readInt();
        author = in.readString();
        description = in.readString();
        feedAuthorPicture = in.readString();
        content = in.readString();
        altContent = in.readString();
        likes = in.readInt();
        hasUserLiked = in.readByte() != 0;
        type = in.readString();
        feedDate = in.readString();
        networkState = in.readString();
        shareType = in.readString();
    }

    public static final Creator<Feed> CREATOR = new Creator<Feed>() {
        @Override
        public Feed createFromParcel(Parcel in) {
            return new Feed(in);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(feedId);
        parcel.writeInt(memberId);
        parcel.writeString(author);
        parcel.writeString(description);
        parcel.writeString(feedAuthorPicture);
        parcel.writeString(content);
        parcel.writeString(altContent);
        parcel.writeInt(likes);
        parcel.writeByte((byte) (hasUserLiked ? 1 : 0));
        parcel.writeString(type);
        parcel.writeString(feedDate);
        parcel.writeString(networkState);
        parcel.writeString(shareType);
    }
}
