package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Table implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    public Table(String regular, String vip) {
        this.regular = regular;
        this.vip = vip;
    }

    @SerializedName("regular")
    @Expose
    private String regular;
    @SerializedName("vip")
    @Expose
    private String vip;


    protected Table(Parcel in) {
        id = in.readInt();
        regular = in.readString();
        vip = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(regular);
        dest.writeString(vip);
    }

    public static final Creator<Table> CREATOR = new Creator<Table>() {
        @Override
        public Table createFromParcel(Parcel in) {
            return new Table(in);
        }

        @Override
        public Table[] newArray(int size) {
            return new Table[size];
        }
    };

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
