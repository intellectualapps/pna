package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.User;

public class AuthenticationResponse extends DefaultResponse {
    @SerializedName("member")
    @Expose
    private User user;
    @SerializedName("updatedMemberProfile")
    @Expose
    private User updatedMemberProfile;
    @SerializedName("profile")
    @Expose
    private User profile;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUpdatedMemberProfile() {
        return updatedMemberProfile;
    }

    public void setUpdatedMemberProfile(User updatedMemberProfile) {
        this.updatedMemberProfile = updatedMemberProfile;
    }

    public User getProfile() {
        return profile;
    }

    public void setProfile(User profile) {
        this.profile = profile;
    }
}
