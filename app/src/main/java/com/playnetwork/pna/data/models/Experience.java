package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Experience implements Parcelable{
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("employer")
    @Expose
    private String employer;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("period")
    @Expose
    private String period;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("dateadded")
    @Expose
    private String dateAdded;
    @SerializedName("date")
    @Expose
    private String date;

    protected Experience(Parcel in) {
        id = in.readInt();
        userId = in.readString();
        employer = in.readString();
        position = in.readString();
        period = in.readString();
        location = in.readString();
        dateAdded = in.readString();
        date = in.readString();
    }

    public static final Creator<Experience> CREATOR = new Creator<Experience>() {
        @Override
        public Experience createFromParcel(Parcel in) {
            return new Experience(in);
        }

        @Override
        public Experience[] newArray(int size) {
            return new Experience[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(userId);
        dest.writeString(employer);
        dest.writeString(position);
        dest.writeString(period);
        dest.writeString(location);
        dest.writeString(dateAdded);
        dest.writeString(date);
    }
}
