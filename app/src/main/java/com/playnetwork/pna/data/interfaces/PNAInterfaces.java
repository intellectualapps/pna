package com.playnetwork.pna.data.interfaces;

import android.net.Uri;

import com.playnetwork.pna.data.api.responses.ChatSessionsResponse;
import com.playnetwork.pna.data.api.responses.DefaultResponse;
import com.playnetwork.pna.data.models.ChatSession;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.data.models.User;

import java.util.List;
import java.util.Map;

public class PNAInterfaces {
    public interface SocialAuthenticationListener {
        void onUserAuthenticated(Map<String, String> profileData);
    }

    public interface PaginationAdapterCallback {
        void loadMoreItems();
    }

    public interface SuggestionSelectionListener {
        void onSuggestionSelected(int position, int memberId);
    }

    public interface PaymentCompletionListener {
        abstract void onPaymentCompleted(String reference, User user);
        abstract void onEventPaymentCompleted(String reference, Event event);
    }

    public interface FragmentOnBackPressedListener {
        void onBackPressed();
    }

    public interface MessagePaginationListener {
        void onLoadMore(int position, String messageKey);
    }

    public interface SessionInitializationListener {
        void onSessionInitialized(ChatSession chatSession);
    }

    public interface NotifyOfflineUserListener {
        void onNotificationSent(DefaultResponse defaultResponse);
    }

    public interface InitializeChatSessionsListener {
        void onChatSessionInitialized(ChatSessionsResponse chatSessionsResponse);
    }

    public interface ImageOptimizationListener {
        void onImageOptimized(String postContentText, Uri mFileUri, byte[] bytes);
    }

    public interface UserProfileUpdateListener {
        void onProfileUpdated(User user);
    }

    public interface FilterSelectionListener {
        void onSelectionComplete(int mode);

        void onSelectionComplete(List<Integer> stateIds, List<Integer> industryIds);
    }

    public interface EventPaymentCompletionListener{
        void onPaymentCompleted();
    }

    public interface Filter<T, E> {
        boolean isMatched(T object, E text);
    }
}
