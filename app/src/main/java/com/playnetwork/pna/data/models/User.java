package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.room.utils.Converters;

import java.util.List;

@Entity
public class User implements Parcelable {
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("flname")
    @Expose
    private String flname;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("pic")
    @Expose
    private String pic;
    @SerializedName("picId")
    @Expose
    private String picId;
    @SerializedName("picLikes")
    @Expose
    private int picLikes;
    @SerializedName("jobTitle")
    @Expose
    private String jobTitle;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("skills")
    @Expose
    private String skills;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("instagram")
    @Expose
    private String instagram;
    @SerializedName("views")
    @Expose
    private int views;
    @SerializedName("lastactive")
    @Expose
    private String lastactive;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("privacy")
    @Expose
    private String privacy;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("joined")
    @Expose
    private String joined;
    @SerializedName("profilePics")
    @Expose
    private String profilePics;
    @SerializedName("experience")
    @Expose
    @TypeConverters(Converters.class)
    private List<Experience> experience;
    @SerializedName("membershipType")
    @Expose
    private String membershipType;
    @SerializedName("networkState")
    @Expose
    private String networkState;
    private boolean loggedIn;
    private String password;
    private boolean hasCreatedProfile;
    private boolean hasViewedNetworkSuggestions;
    private long timestamp;

    public User() {

    }

    protected User(Parcel in) {
        id = in.readInt();
        email = in.readString();
        fname = in.readString();
        lname = in.readString();
        flname = in.readString();
        sex = in.readString();
        phone = in.readString();
        address = in.readString();
        state = in.readString();
        country = in.readString();
        url = in.readString();
        pic = in.readString();
        picId = in.readString();
        picLikes = in.readInt();
        jobTitle = in.readString();
        company = in.readString();
        industry = in.readString();
        skills = in.readString();
        website = in.readString();
        facebook = in.readString();
        twitter = in.readString();
        instagram = in.readString();
        views = in.readInt();
        lastactive = in.readString();
        status = in.readString();
        privacy = in.readString();
        date = in.readString();
        joined = in.readString();
        experience = in.readArrayList(Experience.class.getClassLoader());
        membershipType = in.readString();
        networkState = in.readString();
        profilePics = in.readString();
        loggedIn = in.readByte() != 0;
        password = in.readString();
        hasCreatedProfile = in.readByte() != 0;
        hasViewedNetworkSuggestions = in.readByte() != 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFlname() {
        return flname;
    }

    public void setFlname(String flname) {
        this.flname = flname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPicId() {
        return picId;
    }

    public void setPicId(String picId) {
        this.picId = picId;
    }

    public int getPicLikes() {
        return picLikes;
    }

    public void setPicLikes(int picLikes) {
        this.picLikes = picLikes;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getLastactive() {
        return lastactive;
    }

    public void setLastactive(String lastactive) {
        this.lastactive = lastactive;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getJoined() {
        return joined;
    }

    public void setJoined(String joined) {
        this.joined = joined;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getHasCreatedProfile() {
        return hasCreatedProfile;
    }

    public void setHasCreatedProfile(boolean hasCreatedProfile) {
        this.hasCreatedProfile = hasCreatedProfile;
    }

    public boolean getHasViewedNetworkSuggestions() {
        return hasViewedNetworkSuggestions;
    }

    public void setHasViewedNetworkSuggestions(boolean hasViewNetworkSuggestions) {
        this.hasViewedNetworkSuggestions = hasViewNetworkSuggestions;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfilePics() {
        return profilePics;
    }

    public void setProfilePics(String profilePics) {
        this.profilePics = profilePics;
    }

    public List<Experience> getExperience() {
        return experience;
    }

    public void setExperience(List<Experience> experience) {
        this.experience = experience;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public String getNetworkState() {
        return networkState;
    }

    public void setNetworkState(String networkState) {
        this.networkState = networkState;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(email);
        dest.writeString(fname);
        dest.writeString(lname);
        dest.writeString(flname);
        dest.writeString(sex);
        dest.writeString(phone);
        dest.writeString(address);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(url);
        dest.writeString(pic);
        dest.writeString(picId);
        dest.writeInt(picLikes);
        dest.writeString(jobTitle);
        dest.writeString(company);
        dest.writeString(industry);
        dest.writeString(skills);
        dest.writeString(website);
        dest.writeString(facebook);
        dest.writeString(twitter);
        dest.writeString(instagram);
        dest.writeInt(views);
        dest.writeString(lastactive);
        dest.writeString(status);
        dest.writeString(privacy);
        dest.writeString(date);
        dest.writeString(joined);
        dest.writeList(experience);
        dest.writeString(membershipType);
        dest.writeString(networkState);
        dest.writeString(profilePics);
        dest.writeByte((byte) (loggedIn ? 1 : 0));
        dest.writeString(password);
        dest.writeByte((byte) (hasCreatedProfile ? 1 : 0));
        dest.writeByte((byte) (hasViewedNetworkSuggestions ? 1 : 0));
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
