package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.Offer;
import com.playnetwork.pna.data.models.RedeemedOffer;

import java.util.List;

public class OfferResponse extends DefaultResponse {
    @SerializedName("offers")
    @Expose
    private List<Offer> offers;
    @SerializedName("redeemedOffer")
    @Expose
    private RedeemedOffer redeemedOffer;

    @SerializedName("offer")
    @Expose
    private Offer offer;

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public RedeemedOffer getRedeemedOffer() {
        return redeemedOffer;
    }

    public void setRedeemedOffer(RedeemedOffer redeemedOffer) {
        this.redeemedOffer = redeemedOffer;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }
}
