package com.playnetwork.pna.data.api.responses;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class CommentResponse extends DefaultResponse {
    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @SerializedName("commentId")
    @Expose
    private String commentId;
    @SerializedName("feedId")
    @Expose
    private String feedId;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("comment")
    @Expose
    private String comment;
}
