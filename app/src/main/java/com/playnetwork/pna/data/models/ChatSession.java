package com.playnetwork.pna.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class ChatSession implements Parcelable {
    @SerializedName("chatId")
    @Expose
    private String chatId;
    @SerializedName("otherMemberId")
    @Expose
    private int otherMemberId;
    @SerializedName("memberId")
    @Expose
    private int memberId;
    @SerializedName("participants")
    @Expose
    private Map<String, Object> participantData;
    @SerializedName("currentStatus")
    @Expose
    private String currentStatus;
    @SerializedName("dateStarted")
    @Expose
    private String dateStarted;

    public ChatSession() {

    }

    protected ChatSession(Parcel in) {
        chatId = in.readString();
        otherMemberId = in.readInt();
        memberId = in.readInt();
    }

    public static final Creator<ChatSession> CREATOR = new Creator<ChatSession>() {
        @Override
        public ChatSession createFromParcel(Parcel in) {
            return new ChatSession(in);
        }

        @Override
        public ChatSession[] newArray(int size) {
            return new ChatSession[size];
        }
    };

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public int getOtherMemberId() {
        return otherMemberId;
    }

    public void setOtherMemberId(int otherMemberId) {
        this.otherMemberId = otherMemberId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public Map<String, Object> getParticipantData() {
        return participantData;
    }

    public void setParticipantData(Map<String, Object> participantData) {
        this.participantData = participantData;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(String dateStarted) {
        this.dateStarted = dateStarted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(chatId);
        dest.writeInt(otherMemberId);
        dest.writeInt(memberId);
    }
}
