package com.playnetwork.pna.data.api.responses;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class LikeFeedResponse extends DefaultResponse {
    @SerializedName("feedId")
    @Expose
    private int feedId;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("feedAuthorPicture")
    @Expose
    private String feedAuthorPicture;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("likes")
    @Expose
    private int likes;
    @SerializedName("hasUserLiked")
    @Expose
    private boolean hasUserLiked;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("feedDate")
    @Expose
    private String feedDate;

    public LikeFeedResponse() {

    }

    public int getFeedId() {
        return feedId;
    }

    public void setFeedId(int feedId) {
        this.feedId = feedId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFeedAuthorPicture() {
        return feedAuthorPicture;
    }

    public void setFeedAuthorPicture(String feedAuthorPicture) {
        this.feedAuthorPicture = feedAuthorPicture;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isHasUserLiked() {
        return hasUserLiked;
    }

    public void setHasUserLiked(boolean hasUserLiked) {
        this.hasUserLiked = hasUserLiked;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFeedDate() {
        return feedDate;
    }

    public void setFeedDate(String feedDate) {
        this.feedDate = feedDate;
    }
}
