package com.playnetwork.pna.data.api;

import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.api.responses.ChatSessionsResponse;
import com.playnetwork.pna.data.api.responses.CommentResponse;
import com.playnetwork.pna.data.api.responses.DeviceResponse;
import com.playnetwork.pna.data.api.responses.EventResponse;
import com.playnetwork.pna.data.api.responses.ExperienceResponse;
import com.playnetwork.pna.data.api.responses.FeedCommentResponse;
import com.playnetwork.pna.data.api.responses.GenericFeedResponse;
import com.playnetwork.pna.data.api.responses.HomeScreenResponse;
import com.playnetwork.pna.data.api.responses.LikeFeedResponse;
import com.playnetwork.pna.data.api.responses.LookupCountriesResponse;
import com.playnetwork.pna.data.api.responses.LookupIndustriesResponse;
import com.playnetwork.pna.data.api.responses.LookupStatesResponse;
import com.playnetwork.pna.data.api.responses.MemberResponse;
import com.playnetwork.pna.data.api.responses.NetworkResponse;
import com.playnetwork.pna.data.api.responses.OfferResponse;
import com.playnetwork.pna.data.api.responses.PNADailyResponse;
import com.playnetwork.pna.data.api.responses.PasswordResetResponse;
import com.playnetwork.pna.data.api.responses.PublicKeyResponse;
import com.playnetwork.pna.data.api.responses.SkillResponse;
import com.playnetwork.pna.data.api.responses.SlimMemberResponse;
import com.playnetwork.pna.data.api.responses.SubscriptionAmountResponse;
import com.playnetwork.pna.data.api.responses.TransactionResponse;
import com.playnetwork.pna.data.api.responses.UpdateNetworkInvitationResponse;
import com.playnetwork.pna.data.api.responses.YellowPageResponse;
import com.playnetwork.pna.data.models.Feed;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


interface ApiServiceEndpoints {
    @FormUrlEncoded
    @POST("/api/v1/member")
    Call<AuthenticationResponse> createAccount(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @PUT("/api/v1/member")
    Call<AuthenticationResponse> createMemberProfile(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @PUT("/api/v1/member/profile")
    Call<AuthenticationResponse> updateMemberProfile(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/authenticate")
    Call<AuthenticationResponse> authenticateAccount(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/auth/social")
    Call<AuthenticationResponse> authenticateSocialAccount(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @PUT("/api/v1/auth/social")
    Call<AuthenticationResponse> updateSocialAccount(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/reset-password")
    Call<PasswordResetResponse> resetPassword(@FieldMap Map<String, String> dataMap);

    @GET("/api/v1/member/feed/stripped-content")
    Call<GenericFeedResponse<List<Feed>>> fetchFeed(@QueryMap Map<String, String> dataMap);

    @GET("/api/v1/member/network/suggestion")
    Call<NetworkResponse> fetchNetworkSuggestions(@QueryMap Map<String, String> dataMap);

    @GET("/api/v1/member/{memberId}/network")
    Call<NetworkResponse> fetchMemberNetwork(@Path("memberId") String memberId, @QueryMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/network")
    Call<NetworkResponse> addMembersToNetwork(@FieldMap Map<String, String> dataMap);

    @GET("/api/v1/member/feed/{feedId}/comment/all")
    Call<FeedCommentResponse> fetchFeedComments(@Path("feedId") String feedId, @QueryMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/feed/{feedId}/comment")
    Call<CommentResponse> createComment(@Path("feedId") String feedId, @FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/feed/{feedId}/like")
    Call<LikeFeedResponse> updateFeedLikeStatus(@Path("feedId") String feedId, @Field("email") String emailAddress);

    @GET("/api/v1/lookup/industry/all")
    Call<LookupIndustriesResponse> fetchIndustries();

    @GET("/api/v1/lookup/country/all")
    Call<LookupCountriesResponse> fetchCountries();

    @GET("/api/v1/lookup/states/{country-id}")
    Call<LookupStatesResponse> fetchStates(@Path("country-id") String countryId);

    @GET("/api/v1/member/{memberId}/network/profile/full/{otherMemberId}")
    Call<MemberResponse> fetchOtherMemberProfile(@Path("memberId") String memberId, @Path("otherMemberId") String otherMemberId);

    @GET("api/v1/member/{memberId}/profile")
    Call<AuthenticationResponse> fetchMemberProfile(@Path("memberId") String memberId);

    @FormUrlEncoded
    @POST("/api/v1/member/device-token")
    Call<DeviceResponse> storeDeviceToken(@FieldMap Map<String, String> dataMap);

    @PUT("/api/v1/member/network/request/{network-id}/{option}")
    Call<UpdateNetworkInvitationResponse> updateNetworkInvitation(@Path("network-id") String networkId, @Path("option") String updateFlag, @Query("member-id") String memberId);

    @GET("/api/v1/member/{memberId}/network/request")
    Call<NetworkResponse> fetchMemberNetworkRequests(@Path("memberId") String memberId, @QueryMap Map<String, String> dataMap);

    @GET("/api/v1/offer")
    Call<OfferResponse> fetchDiscountOffers(@QueryMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/offer/{offerId}/redeem")
    Call<OfferResponse> redeemOffer(@Path("offerId") String offerId, @FieldMap Map<String, String> requestMap);

    @GET("/api/v1/offer/{offerId}")
    Call<OfferResponse> fetchOffer(@Path("offerId") String offerId, @Query("member-id") String memberId);

    @GET("/api/v1/pna-daily")
    Call<PNADailyResponse> fetchPNADaily();

    @GET("/api/v1/premium-subscription/amount")
    Call<SubscriptionAmountResponse> fetchSubscriptionAmount();

    @FormUrlEncoded
    @POST("/api/v1/member/transaction/premium-membership-subscription")
    Call<TransactionResponse> initializeTransaction(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/transaction/event-ticket")
    Call<TransactionResponse> initializeTicketTransaction(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/transaction/event-table")
    Call<TransactionResponse> initializeTableTransaction(@FieldMap Map<String, String> dataMap);

    @GET("/api/v1/payments/pk")
    Call<PublicKeyResponse> fetchPublicKey();

    @GET("/api/v1/member/transaction/premium-membership-subscription/{referenceCode}")
    Call<AuthenticationResponse> verifyTransaction(@Path("referenceCode") String referenceCode, @Query("email") String email);

    @GET("/api/v1/member/transaction/event-ticket/{referenceCode}")
    Call<EventResponse> verifyTicketTransaction(@Path("referenceCode") String referenceCode, @Query("email") String email);

    @GET("/api/v1/member/transaction/event-table/{referenceCode}")
    Call<EventResponse> verifyTableTransaction(@Path("referenceCode") String referenceCode, @Query("email") String email);

    @GET("/api/v1/member/slim")
    Call<SlimMemberResponse> fetchSlimMemberProfile(@Query("id") String id);

    @GET("/api/v1/event")
    Call<EventResponse> fetchEvents(@QueryMap Map<String, String> dataMap);

    @GET("/api/v1/event/{eventId}")
    Call<EventResponse> fetchEvent(@Path("eventId") String eventId, @Query("member-id") String memberId);

    @FormUrlEncoded
    @POST("/api/v1/event/rsvp")
    Call<EventResponse> RSVPEvent(@FieldMap Map<String, String> dataMap);

    @GET("/api/v1/member/{memberId}/search/{searchTerm}")
    Call<NetworkResponse> fetchSearchResults(@Path("memberId") String memberId, @Path("searchTerm") String searchTerm, @QueryMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/{memberId}/feed")
    Call<GenericFeedResponse<Feed>> createPost(@Path("memberId") String memberId, @FieldMap Map<String, String> dataMap);

    @GET("/api/v1/yellow-pages")
    Call<YellowPageResponse> fetchYellowPages(@QueryMap Map<String, String> dataMap);

    @GET("/api/v1/yellow-pages/filter")
    Call<YellowPageResponse> fetchFilteredYellowPages(@QueryMap Map<String, String> dataMap);

    @GET("/api/v1/yellow-pages/search/{searchTerm}")
    Call<YellowPageResponse> searchYellowPages(@Path("searchTerm") String searchTerm, @QueryMap Map<String, String> dataMap);

    @GET("/api/v1/yellow-pages/{listingId}")
    Call<YellowPageResponse> fetchYellowPageById(@Path("listingId") String listingId);

    @FormUrlEncoded
    @POST("/api/v1/member/chat")
    Call<ChatSessionsResponse> initializeChatSession(@FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/{memberId}/experience")
    Call<ExperienceResponse> addWorkExperience(@Path("memberId") String memberId, @FieldMap Map<String, String> dataMap);

    @FormUrlEncoded
    @POST("/api/v1/member/{memberId}/skill")
    Call<SkillResponse> addSkills(@Path("memberId") String memberId, @FieldMap Map<String, String> dataMap);

    @GET("/api/v1/home-screen-content")
    Call<HomeScreenResponse> fetchHomeScreenData(@Query("member-id") String id);
}
