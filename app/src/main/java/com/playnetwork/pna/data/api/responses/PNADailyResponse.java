package com.playnetwork.pna.data.api.responses;

public class PNADailyResponse extends DefaultResponse {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
