package com.playnetwork.pna.data.api.responses;

import android.support.annotation.Keep;

@Keep
public class GenericFeedResponse<T> extends DefaultResponse {
    private T feed;

    public T getFeed() {
        return feed;
    }

    public void setFeed(T feed) {
        this.feed = feed;
    }

    public GenericFeedResponse() {
    }
}