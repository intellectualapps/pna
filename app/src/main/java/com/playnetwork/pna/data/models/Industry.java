package com.playnetwork.pna.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Industry implements Comparable<Industry>{
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("industry")
    @Expose
    private String industry;

    public Industry(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    @Override
    public int compareTo(@NonNull Industry o) {
        return Double.compare(getId(), o.getId());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Industry && id == ((Industry) o).getId();
    }
}
