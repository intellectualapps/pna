package com.playnetwork.pna.data.models;

public class CustomField {
    private String fieldLabel;
    private String fieldValue;
    private boolean isAutoLink;

    public CustomField(String fieldLabel, String fieldValue) {
        this.fieldLabel = fieldLabel;
        this.fieldValue = fieldValue;
    }

    public CustomField(String fieldValue, boolean isAutoLink){
        this.fieldValue = fieldValue;
        this.isAutoLink = isAutoLink;
    }

    public String getFieldLabel() {
        return fieldLabel;
    }

    public void setFieldLabel(String fieldLabel) {
        this.fieldLabel = fieldLabel;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public boolean isAutoLink() {
        return isAutoLink;
    }

    public void setAutoLink(boolean autoLink) {
        isAutoLink = autoLink;
    }
}
