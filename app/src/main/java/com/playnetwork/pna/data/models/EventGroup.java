package com.playnetwork.pna.data.models;

import android.content.res.Resources;
import android.text.TextUtils;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.enums.PaymentType;

public enum EventGroup {
    THIS_WEEK("thisWeek", getLabel(R.string.this_week_label)),
    THIS_MONTH("thisMonth", getLabel(R.string.this_month_label)),
    UPCOMING("upcoming", getLabel(R.string.upcoming_label));

    private String id;
    private String value;

    EventGroup(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public static EventGroup getEventGroup(String eventGroupId) {
        for (EventGroup eventGroup : EventGroup.values()) {
            if (eventGroup.id.equalsIgnoreCase(eventGroupId)) {
                return eventGroup;
            }
        }
        return null;
    }

    private static String getLabel(int id) {
        Resources resources = PlayNetworkApplication.getApplicationResources();
        return resources.getString(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
