package com.playnetwork.pna.data.models;

import android.os.Parcel;
import android.os.Parcelable;

public class EventConfirmation implements Parcelable {
    private String eventId;
    private String eventName;
    private String selectedPurchaseOption;
    private double price;
    private int quantity;

    public EventConfirmation() {

    }

    public EventConfirmation(String eventId, String eventName, String selectedPurchaseOption, Double price) {
        this.eventId = eventId;
        this.eventName = eventName;
        this.selectedPurchaseOption = selectedPurchaseOption;
        this.price = price;
    }

    protected EventConfirmation(Parcel in) {
        eventId = in.readString();
        eventName = in.readString();
        selectedPurchaseOption = in.readString();
        price = in.readDouble();
        quantity = in.readInt();
    }

    public static final Creator<EventConfirmation> CREATOR = new Creator<EventConfirmation>() {
        @Override
        public EventConfirmation createFromParcel(Parcel in) {
            return new EventConfirmation(in);
        }

        @Override
        public EventConfirmation[] newArray(int size) {
            return new EventConfirmation[size];
        }
    };

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getSelectedPurchaseOption() {
        return selectedPurchaseOption;
    }

    public void setSelectedPurchaseOption(String selectedPurchaseOption) {
        this.selectedPurchaseOption = selectedPurchaseOption;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(eventId);
        dest.writeString(eventName);
        dest.writeString(selectedPurchaseOption);
        dest.writeDouble(price);
        dest.writeInt(quantity);
    }
}
