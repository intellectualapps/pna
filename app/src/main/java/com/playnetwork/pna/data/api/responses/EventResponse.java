package com.playnetwork.pna.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.playnetwork.pna.data.models.Event;

import java.util.List;

public class EventResponse extends DefaultResponse {
    @SerializedName("events")
    @Expose
    private List<Event> events;
    @SerializedName("event")
    @Expose
    private Event event;

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
