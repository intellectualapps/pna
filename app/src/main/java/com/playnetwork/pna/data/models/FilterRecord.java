package com.playnetwork.pna.data.models;

import android.support.annotation.NonNull;

public class FilterRecord implements Comparable<FilterRecord> {
    private int id;
    private String value;

    public FilterRecord(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int compareTo(@NonNull FilterRecord o) {
        return Double.compare(getId(), o.getId());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof FilterRecord && id == ((FilterRecord) o).getId();
    }
}
