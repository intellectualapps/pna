package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.PasswordResetResponse;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.NetworkUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener {
    private User user;
    private EditText emailAddressInput;
    private Button continueButton, loginButton;
    private Toolbar toolbar;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new ForgotPasswordFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public ForgotPasswordFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        emailAddressInput = (EditText) view.findViewById(R.id.email_address);
        continueButton = (Button) view.findViewById(R.id.continue_button);
        loginButton = (Button) view.findViewById(R.id.login_button);
        continueButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.password_reset_progress_label);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.continue_button:
                if (validateEmailInput(emailAddressInput)) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        getHttpService().resetPassword(getUserInput(), buildPasswordResetCallback());
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.login_button:
                replaceFragment(LoginFragment.newInstance(null));
                break;
        }
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String emailAddress = emailAddressInput.getText().toString().trim();
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        return map;
    }

    private Callback<PasswordResetResponse> buildPasswordResetCallback() {
        return new Callback<PasswordResetResponse>() {
            @Override
            public void onResponse(Call<PasswordResetResponse> call, Response<PasswordResetResponse> response) {
                PasswordResetResponse passwordResetResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    showSuccessPopupMessage(passwordResetResponse.getMessage());
                    replaceFragment(LoginFragment.newInstance(null));
                }
            }

            @Override
            public void onFailure(Call<PasswordResetResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }
}
