package com.playnetwork.pna.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.Spinner;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.State;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.room.daos.UserDao;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.AppUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PreferenceStorageManager;
import com.playnetwork.pna.utils.ToastUtil;
import com.playnetwork.pna.utils.Validator;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;

public class BaseFragment extends Fragment {
    public View mSnackBarView;

    public CallbackManager callbackManager;
    public FacebookCallback<LoginResult> callback;
    public static final int MEDIA_PERMISSION_REQUEST_CODE = 103;
    public static final int CONTACT_PERMISSION_REQUEST_CODE = 104;
    public static final int CALENDAR_PERMISSION_REQUEST_CODE = 105;
    public static final int CALL_PERMISSION_REQUEST_CODE = 106;

    public static boolean hasMediaPermissionRequestBeenMade = false;
    public static boolean hasCalendarPermissionRequestBeenMade = false;
    public static boolean hasCallPermissionRequestBeenMade = false;

    private HttpService httpService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public HttpService getHttpService() {
        if (httpService == null) {
            httpService = new HttpService();
        }

        return httpService;
    }

    public void closeFragment() {
        if (getActivity() != null) {
            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                hideKeyboard();
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
                hideKeyboard();
                getActivity().finish();
            }
        }
    }

    public void finishActivity(Activity activity) {
        if (activity == null) {
            activity = getActivity();
        }
        if (activity != null) {
            activity.finish();
        }
    }

    public void showLoadingIndicator(String message) {

        try {
            if (isAdded() && getActivity() != null) {
                ((BaseActivity) getActivity()).showLoadingIndicator(message);
            }
        } catch (Exception ignored) {
        }
    }

    public void hideLoadingIndicator() {
        try {
            if (isAdded() && getActivity() != null) {
                ((BaseActivity) getActivity()).hideLoadingIndicator();
            }
        } catch (Exception ignored) {
        }
    }

    public String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getPlayNetworkApplicationContext().getString(R.string.error_msg_unknown);
        if (!NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            errorMsg = getResources().getString(R.string.network_connection_label);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    public void endSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        if (swipeRefreshLayout != null && (swipeRefreshLayout.isRefreshing() || swipeRefreshLayout.isShown())
                && isVisible()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void animateSwipeRefreshLayout(final SwipeRefreshLayout mSwipeRefreshLayout) {
        mSwipeRefreshLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mSwipeRefreshLayout
                        .getViewTreeObserver()
                        .removeOnGlobalLayoutListener(this);
                mSwipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    public void startFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    public void switchFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    private void clearBackStack(FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                while (fragmentManager.getBackStackEntryCount() > 0) {
                    fragmentManager.popBackStackImmediate();
                }
            }
        }
        fragmentManager.getBackStackEntryCount();
    }

    public void startFragment(Fragment frag, boolean shouldClearBackstack) {
        if (getActivity() != null && frag != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            if (shouldClearBackstack) {
                clearBackStack(fragmentManager);
            }
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    public void startMetaFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) {
            FragmentTransaction fragmentTransaction = manager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, fragment, backStateName);
            fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commit();
        }
    }

    public void replaceFragmentWithoutBackStack(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        clearBackStack(fragmentManager);
        if (!fragmentPopped && fragmentManager.findFragmentByTag(backStateName) == null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, fragment, backStateName);
            //fragmentTransaction.addToBackStack(backStateName);
            fragmentTransaction.commit();
        }
    }

    public FragmentManager getSupportFragmentManager() {
        if (getActivity() != null) {
            return getActivity().getSupportFragmentManager();
        }

        return null;
    }

    public boolean checkFileAccessPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.storage_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MEDIA_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MEDIA_PERMISSION_REQUEST_CODE);
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public boolean checkMediaAccessPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.CAMERA)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.media_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, MEDIA_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.dismiss_label, null);
                    builder.show();

                    return false;

                } else {
                    if (hasMediaPermissionRequestBeenMade) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setTitle(getString(R.string.permission_request_dialog_title));
                        builder.setMessage(getString(R.string.media_permission_rationale_after_permission_denial));
                        builder.setCancelable(true);
                        builder.setPositiveButton(R.string.enable_manually, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                intent.addCategory(Intent.CATEGORY_DEFAULT);
                                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                activity.startActivity(intent);
                            }
                        });
                        builder.setNegativeButton(R.string.dismiss_label, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    } else {
                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                MEDIA_PERMISSION_REQUEST_CODE);
                    }
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public boolean checkContactAccessPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_CONTACTS)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.contact_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, CONTACT_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                            CONTACT_PERMISSION_REQUEST_CODE);
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public boolean checkCalendarPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.WRITE_CALENDAR) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_CALENDAR)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.calendar_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR}, CALENDAR_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.dismiss_label, null);
                    builder.show();

                } else {
                    if (hasCalendarPermissionRequestBeenMade) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setTitle(getString(R.string.permission_request_dialog_title));
                        builder.setMessage(getString(R.string.calendar_permission_rationale_after_permission_denial));
                        builder.setCancelable(true);
                        builder.setPositiveButton(R.string.enable_manually, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                intent.addCategory(Intent.CATEGORY_DEFAULT);
                                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                activity.startActivity(intent);
                            }
                        });
                        builder.setNegativeButton(R.string.dismiss_label, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    } else {
                        requestPermissions(new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR},
                                CALENDAR_PERMISSION_REQUEST_CODE);
                    }
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public boolean checkCallPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.CALL_PHONE)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.call_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.show();

                } else {
                    if (hasCallPermissionRequestBeenMade) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                        builder.setTitle(getString(R.string.permission_request_dialog_title));
                        builder.setMessage(getString(R.string.call_permission_rationale_after_permission_denial));
                        builder.setCancelable(true);
                        builder.setPositiveButton(R.string.enable_manually, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                intent.addCategory(Intent.CATEGORY_DEFAULT);
                                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                activity.startActivity(intent);
                            }
                        });
                        builder.setNegativeButton(R.string.dismiss_label, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    } else {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                                CALL_PERMISSION_REQUEST_CODE);
                    }
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    protected void hideKeyboard() {
        if (getActivity() != null) {
            AppUtils.hideKeyboard(getActivity());
        }
    }

    public User saveUserData(PlayNetworkDatabase playNetworkDatabase, AuthenticationResponse authenticationResponse, String password, String emailAddress) {
        User user = syncLocalFields(playNetworkDatabase, emailAddress, authenticationResponse);

        if (!TextUtils.isEmpty(user.getFname()) && !TextUtils.isEmpty(user.getLname()) && !TextUtils.isEmpty(user.getState()) && !TextUtils.isEmpty(user.getCountry())) {
            user.setHasCreatedProfile(true);
            user.setLoggedIn(true);
        } else {
            user.setHasCreatedProfile(false);
        }

        if (!TextUtils.isEmpty(password)) {
            user.setPassword(password);
        }

        user.setTimestamp(System.currentTimeMillis());
        playNetworkDatabase.userDao().insertUser(user);
        return user;
    }

    public void clearUserData(PlayNetworkDatabase playNetworkDatabase) {
        UserDao userDao = playNetworkDatabase.userDao();
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).clearCachedData(playNetworkDatabase);
        }
        userDao.deleteAllUsers(ListUtils.convertUserListToArray(userDao.getAll()));
    }

    public Map<String, String> buildParameterMap(int start, int pageSize, User user) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
        return requestMap;
    }

    public User syncLocalFields(PlayNetworkDatabase playNetworkDatabase, String emailAddress, AuthenticationResponse authenticationResponse) {
        User user = authenticationResponse.getUser() != null ? authenticationResponse.getUser() : authenticationResponse.getUpdatedMemberProfile() != null ? authenticationResponse.getUpdatedMemberProfile() : authenticationResponse.getProfile();
        if (!TextUtils.isEmpty(emailAddress)) {
            User cachedUser = playNetworkDatabase.userDao().findByEmail(emailAddress);
            if (user != null && cachedUser != null) {
                user.setHasViewedNetworkSuggestions(cachedUser.getHasViewedNetworkSuggestions());
                user.setHasCreatedProfile(cachedUser.getHasCreatedProfile());
                user.setPassword(cachedUser.getPassword());
            }
        }
        return user;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void manageFacebookAuthentication(final PNAInterfaces.SocialAuthenticationListener socialAuthenticationListener) {
        callbackManager = CallbackManager.Factory.create();
        final String message = getString(R.string.registration_facebook_progress_label);
        final String error = getString(R.string.facebook_signup_error);

        final String PERMISSION_EMAIL = "email";
        final String PERMISSION_PUBLIC_PROFILE = "public_profile";
        final String PERMISSION_MOBILE_NUMBER = "user_mobile_phone";

        ArrayList<String> permissions = new ArrayList<String>();
        permissions.add(PERMISSION_EMAIL);
        permissions.add(PERMISSION_PUBLIC_PROFILE);
        //permissions.add(PERMISSION_MOBILE_NUMBER);

        callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult != null) {
                    final AccessToken accessToken = loginResult.getAccessToken();
                    if (accessToken != null) {
                        Set<String> declinedPermissions = accessToken.getDeclinedPermissions();
                        if (declinedPermissions.isEmpty()) {
                            GraphRequest req = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                                    if (jsonObject != null) {
                                        try {
                                            Map<String, String> accountMap = new HashMap<String, String>();
                                            Map<String, String> profileData = new HashMap<String, String>();
                                            Map<String, String> facebookAuthParamsMap = new HashMap<String, String>();

                                            String fbId = "";
                                            String email = "";
                                            String gender = "";
                                            String name = "";
                                            String profilePhoto = "";
                                            String phoneNumber = "";
                                            String firstName = "", lastName = "";
                                            String accessTokenValue = "";


                                            if (jsonObject.has("id")) {
                                                fbId = jsonObject.getString("id");
                                            }
                                            if (jsonObject.has("email")) {
                                                email = jsonObject.getString("email");
                                            }
                                            if (jsonObject.has("gender")) {
                                                gender = jsonObject.getString("gender");
                                            }
                                            if (jsonObject.has("name")) {
                                                name = jsonObject.getString("name");
                                            }
                                            if (jsonObject.has("picture")) {
                                                profilePhoto = jsonObject.getJSONObject("picture").getJSONObject("data").getString("url");
                                                profileData.put(Constants.PROFILE_PHOTO, profilePhoto);
                                            }

                                            if (jsonObject.has("verified_mobile_phone")) {
                                                phoneNumber = jsonObject.getString("verified_mobile_phone");
                                                profileData.put(Constants.PHONE_NUMBER, phoneNumber);
                                            }

                                            if (name.contains(" ")) {
                                                String[] split_names = name.split(" ");
                                                firstName = split_names[0];
                                                lastName = split_names[1];
                                            } else {
                                                firstName = name;
                                                lastName = "";
                                            }

                                            accessTokenValue = accessToken.getToken();

                                            facebookAuthParamsMap.put(Constants.FACEBOOK_ID, fbId);
                                            facebookAuthParamsMap.put(Constants.FACEBOOK_ACCESS_TOKEN, accessTokenValue);

                                            String gender_short = null;
                                            if (gender != null) {
                                                if (gender.equalsIgnoreCase("male")) {
                                                    gender_short = "m";
                                                } else if (gender.equalsIgnoreCase("female")) {
                                                    gender_short = "f";
                                                }
                                            }

                                            profileData.put(Constants.FIRST_NAME, firstName);
                                            profileData.put(Constants.LAST_NAME, lastName);
                                            profileData.put(Constants.FULL_NAME, name);
                                            profileData.put(Constants.EMAIL_ADDRESS, email);
                                            profileData.putAll(facebookAuthParamsMap);

                                            PreferenceStorageManager.saveFacebookAuthParams(getPlayNetworkApplicationContext(), facebookAuthParamsMap);
                                            Log.w("Facebook User data: ", email + " : " + gender + " : " + name + " : " + fbId + " : " + accessToken.getToken());
                                            showLoadingIndicator(message);
                                            if (socialAuthenticationListener != null) {
                                                socialAuthenticationListener.onUserAuthenticated(profileData);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            });

                            String fields = "id,email,gender,name,picture.type(large)";
                            Bundle params = new Bundle();
                            params.putString("fields", fields);
                            req.setParameters(params);
                            req.executeAsync();
                        }
                    }
                } else {
                    Log.e("Facebook Login", "Null Login Result");
                    hideLoadingIndicator();
                    showErrorPopupMessage(error);
                }
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                exception.printStackTrace();
                hideLoadingIndicator();
                showErrorPopupMessage(error);
            }
        };
        LoginManager.getInstance().logInWithReadPermissions(this, permissions);
        LoginManager.getInstance().registerCallback(callbackManager, callback);
    }

    public List<String> getIndustryList(List<Industry> industries) {
        List<String> spinnerValues = new ArrayList<String>();
        if (ListUtils.isNotEmpty(industries)) {
            spinnerValues.addAll(ListUtils.getIndustryLabels(industries));
        }
        spinnerValues.add(0, getString(R.string.select_industry_spinner_label));
        return spinnerValues;
    }

    public List<String> getCountryList(List<Country> countries) {
        List<String> spinnerValues = new ArrayList<String>();
        if (ListUtils.isNotEmpty(countries)) {
            spinnerValues.addAll(ListUtils.getCountryLabels(countries));
        }
        spinnerValues.add(0, getString(R.string.select_country_spinner_label));
        return spinnerValues;
    }

    public List<String> getStateList(List<State> states) {
        List<String> spinnerValues = new ArrayList<String>();
        if (ListUtils.isNotEmpty(states)) {
            spinnerValues.addAll(ListUtils.getStateLabels(states));
        }
        spinnerValues.add(0, getString(R.string.select_state_spinner_label));
        return spinnerValues;
    }

    public void showPopupMessage(Object message) {
        if (isAdded() && getActivity() != null) {
            ToastUtil.showToast(getActivity(), (String) message);
        }
    }

    public void showSuccessPopupMessage(Object message) {
        if (isAdded() && getActivity() != null) {
            ToastUtil.showSuccessToast(getActivity(), (String) message);
        }
    }

    public void showErrorPopupMessage(Object message) {
        if (isAdded() && getActivity() != null) {
            ToastUtil.showErrorToast(getActivity(), (String) message);
        }
    }

    public void unregisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        try {
            context.unregisterReceiver(broadcastReceiver);
        } catch (Exception ignored) {
        }
    }

    public void registerReceiver(Context context, BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        try {
            context.registerReceiver(broadcastReceiver, intentFilter);
        } catch (Exception ignored) {
        }
    }

    public Context getPlayNetworkApplicationContext() {
        return PlayNetworkApplication.getAppInstance().getApplicationContext();
    }

    public Resources getPlayNetworkResources() {
        return PlayNetworkApplication.getApplicationResources();
    }

    public boolean validateFields(EditText[] views) {
        return Validator.validateInputViewsNotEmpty(views);
    }

    public boolean validateEmailInput(EditText view) {
        return Validator.validateEmailAddress(view);
    }

    public boolean validateTextInput(EditText view) {
        return Validator.validateInputNotEmpty(view);
    }

    public boolean validatePasswordViews(EditText passwordInput, EditText confirmPasswordInput) {
        return Validator.validatePasswordsEqual(passwordInput, confirmPasswordInput);
    }

    public boolean validateInputLength(EditText editText, int length) {
        return Validator.validateInputLength(editText, length);
    }

    public boolean validatePhoneNumber(EditText view) {
        return Validator.validatePhoneNumber(view);
    }

    public boolean validateAmountView(EditText view) {
        return false;
    }

    public boolean validateSpinnerControls(Spinner[] spinners) {
        return Validator.validateSpinnerControls(spinners);
    }
}
