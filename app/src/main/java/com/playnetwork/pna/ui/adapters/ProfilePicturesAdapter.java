package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ProfilePicturesAdapter extends BaseRecyclerAdapter {

    private List<String> profilePictureList;
    private ClickListener clickListener;

    public ProfilePicturesAdapter(Context context, ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
        this.profilePictureList = new ArrayList<String>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        view = mInflater.inflate(R.layout.profile_picture_item_layout, parent, false);
        viewHolder = new ProfilePicturesAdapter.ViewHolder(view, this.clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        String profilePictureUrl = profilePictureList.get(position);
        final ProfilePicturesAdapter.ViewHolder viewHolder = (ProfilePicturesAdapter.ViewHolder) holder;
        Picasso.with(context).load(profilePictureUrl).placeholder(R.color.colorGray).into(viewHolder.profilePhotoView);
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(profilePictureList) ? 0 : profilePictureList.size();
    }

    /*
    Helpers
    */

    public void setItems(List<String> items) {
        this.profilePictureList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (profilePictureList != null) {
            this.profilePictureList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(String profilePictureUrl) {
        if (!TextUtils.isEmpty(profilePictureUrl)) {
            this.profilePictureList.add(profilePictureUrl);
            notifyDataSetChanged();
        }
    }


    public void removeItem(String profilePictureUrl) {
        int position = profilePictureList.indexOf(profilePictureUrl);
        if (position > -1) {
            profilePictureList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private String getItem(int position) {
        return profilePictureList.get(position);
    }


    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        ImageView profilePhotoView;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            profilePhotoView = (ImageView) itemView.findViewById(R.id.profile_photo_view);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            profilePhotoView.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }
}