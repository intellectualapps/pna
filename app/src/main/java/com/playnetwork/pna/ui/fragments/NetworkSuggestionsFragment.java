package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.NetworkResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.NetworkSuggestionsAdapter;
import com.playnetwork.pna.ui.customviews.AutoFitGridRecyclerView;
import com.playnetwork.pna.ui.dialogs.SuggestionDetailViewDialog;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkSuggestionsFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, PNAInterfaces.SuggestionSelectionListener, PNAInterfaces.PaginationAdapterCallback {
    private User user;
    private Button addToNetworkButton, skipButton;
    private NetworkSuggestionsAdapter networkSuggestionsAdapter;
    private AutoFitGridRecyclerView networkSuggestionsRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private View mEmptyView;
    private Toolbar toolbar;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private int start = 0, pageSize = 10;
    private int currentPage = start;

    private List<User> networkSuggestionsList;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new NetworkSuggestionsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public NetworkSuggestionsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_network_suggestions, container, false);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);

        addToNetworkButton =  view.findViewById(R.id.add_to_network_button);
        skipButton =  view.findViewById(R.id.skip_button);
        networkSuggestionsRecyclerView =  view.findViewById(R.id.network_suggestions_recyclerview);
        swipeRefreshLayout =  view.findViewById(R.id.swipe_refresh_layout);

        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);

        mEmptyViewLabel.setText(R.string.network_suggestions_empty_label);

        networkSuggestionsRecyclerView.setEmptyView(mEmptyView);
        networkSuggestionsAdapter = new NetworkSuggestionsAdapter(getContext(), this, this, Constants.NETWORK_SUGGESTION_MODE);
        networkSuggestionsRecyclerView.setAdapter(networkSuggestionsAdapter);
        networkSuggestionsRecyclerView.addOnScrollListener(new PaginationScrollListener(networkSuggestionsRecyclerView.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (networkSuggestionsList != null && networkSuggestionsList.size() >= pageSize) {
                            fetchNetworkSuggestions(start, pageSize, true);
                        }
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        fetchNetworkSuggestions(0, pageSize, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (NetworkUtils.isConnected(getContext())) {
            animateSwipeRefreshLayout(swipeRefreshLayout);
            fetchNetworkSuggestions(0, pageSize, false);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }

        addToNetworkButton.setOnClickListener(this);
        skipButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.request_processing_label);
        switch (v.getId()) {
            case R.id.add_to_network_button:
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    showLoadingIndicator(message);
                    String memberIds = android.text.TextUtils.join(",", networkSuggestionsAdapter.getSelectedMemberIds());
                    Map<String, String> requestMap = new HashMap<String, String>();
                    requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
                    requestMap.put(Constants.MEMBER_IDS, memberIds);
                    getHttpService().addMembersToNetwork(requestMap, buildAddToNetworkCallbackCallback(networkSuggestionsAdapter.getSelectedItemCount()));
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
            case R.id.skip_button:
                completeOnBoarding();
                break;
        }
    }

    private void completeOnBoarding() {
        if (user != null && !user.getHasViewedNetworkSuggestions()) {
            user.setHasViewedNetworkSuggestions(true);
            playNetworkDatabase.userDao().insertUser(user);
            if (getActivity() != null) {
                ((BaseActivity) getActivity()).showMainActivity(getActivity());
            }
        } else {
            if (getActivity() != null) {
                getActivity().finish();
            }
        }
    }

    private void fetchNetworkSuggestions(int start, int pageSize, boolean inLoadMoreMode) {
        if (!inLoadMoreMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }

        getHttpService().fetchNetworkSuggestions(buildParameterMap(start, pageSize), buildFetchNetworkSuggestionsCallback(inLoadMoreMode));
    }

    private Map<String, String> buildParameterMap(int start, int pageSize) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
        return requestMap;
    }

    private void updateNetworkSuggestionList(List<User> networkSuggestionsListParam) {
        if (networkSuggestionsList == null) {
            networkSuggestionsList = new ArrayList<User>();
        }
        if (networkSuggestionsListParam != null && networkSuggestionsListParam.size() > 0) {
            networkSuggestionsList.addAll(networkSuggestionsListParam);
        }
    }

    private void loadAdapter(List<User> networkSuggestionsList) {
        if (isAdded() && isVisible()) {
            networkSuggestionsAdapter.addAll(networkSuggestionsList);
        }
    }

    private Callback<NetworkResponse> buildFetchNetworkSuggestionsCallback(final boolean inLoadMoreMode) {
        return new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                NetworkResponse networkResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                isLoading = false;
                if (inLoadMoreMode && isVisible()) {
                    networkSuggestionsAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    List<User> networkSuggestionListParam = networkResponse.getNetworkSuggestions();

                    if (networkSuggestionListParam == null || networkSuggestionListParam.size() == 0 || networkSuggestionListParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (networkSuggestionsList != null) {
                            networkSuggestionsList.clear();
                        }
                        networkSuggestionsAdapter.clearItems();
                    }

                    updateNetworkSuggestionList(networkSuggestionListParam);

                    loadAdapter(networkSuggestionListParam);

                    if (!hasListEnded) {
                        networkSuggestionsAdapter.addLoadingFooter();
                    }
                } else {
                    if (isVisible()) {
                        loadAdapter(networkSuggestionsList);
                    }
                }
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    networkSuggestionsAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    private Callback<NetworkResponse> buildAddToNetworkCallbackCallback(final int count) {
        return new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                NetworkResponse networkResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (isVisible()) {
                        showSuccessPopupMessage(getString(count == 1 ? R.string.network_invitation_sent_label : R.string.network_invitations_sent_label));
                        addToNetworkButton.setEnabled(false);
                        networkSuggestionsRecyclerView.setEnabled(false);
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            completeOnBoarding();
                        }
                    }, 1000);
                }
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private void toggleSelection(int position, int memberId) {
        networkSuggestionsAdapter.toggleSelection(position, memberId);
        addToNetworkButton.setEnabled(networkSuggestionsAdapter.getSelectedItemCount() > 0);
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1) {
            User suggestion = networkSuggestionsList.get(position);
            switch (view.getId()) {
                case R.id.suggestion_full_name:
                case R.id.suggestion_job_title:
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.SUGGESTION, suggestion);
                    bundle.putInt(Constants.POSITION, position);
                    bundle.putBoolean(Constants.IS_SELECTED, networkSuggestionsAdapter.isSelected(position));
                    SuggestionDetailViewDialog suggestionDetailViewDialog = new SuggestionDetailViewDialog();
                    suggestionDetailViewDialog.setSuggestionSelectedListener(this);
                    suggestionDetailViewDialog.setArguments(bundle);
                    if (getActivity() != null) {
                        ((BaseActivity) getActivity()).launchFragmentDialog(suggestionDetailViewDialog);
                    }
                    break;
                case R.id.suggestion_photo:
                    toggleSelection(position, suggestion.getId());
                    break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (user != null && playNetworkDatabase != null && playNetworkDatabase.isOpen()) {
                user.setHasViewedNetworkSuggestions(true);
                playNetworkDatabase.userDao().insertUser(user);
            }
        } catch (Exception ignored) {
        }
        PlayNetworkDatabase.destroyDatabaseInstance();
    }

    @Override
    public void onSuggestionSelected(int position, int memberId) {
        networkSuggestionsAdapter.toggleSelection(position, memberId);
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            start = currentPage * pageSize;
            fetchNetworkSuggestions(start, pageSize, true);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }
}
