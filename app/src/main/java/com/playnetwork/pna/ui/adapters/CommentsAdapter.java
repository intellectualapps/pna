package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Comment;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CommentsAdapter extends BaseRecyclerAdapter {
    private List<Comment> commentList;
    private ClickListener clickListener;
    private final int TEXT_COMMENT = 4;

    public CommentsAdapter(Context context, ClickListener clickListener, PNAInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.commentList = new ArrayList<Comment>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;

        switch (viewType) {
            default:
                view = mInflater.inflate(R.layout.comment_layout, parent, false);
                viewHolder = new CommentsAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Comment comment = commentList.get(position);
        switch (getItemViewType(position)) {
            case LOADING:
               initializeLoadingViewHolder(holder);
                break;
            default:
                final CommentsAdapter.ViewHolder viewHolder = (CommentsAdapter.ViewHolder) holder;
                if (comment != null) {
                    viewHolder.commentAuthorFullName.setText(comment.getAuthor());
                    viewHolder.commentContentView.setText(comment.getContent());
                    viewHolder.commentDate.setText(DateUtils.timeSincePosted(comment.getCommentDate()));
                    if (comment.getCommentAuthorPicture() != null) {
                        Picasso.with(context).load(comment.getCommentAuthorPicture()).placeholder(R.color.colorGray).into(viewHolder.commentAuthorPhoto);
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(commentList) ? 0 : commentList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == commentList.size() - 1 && isLoadingAdded) {
            return LOADING;
        }

        return TEXT_COMMENT;
    }

    /*
    Helpers
    */

    public void setItems(List<Comment> items) {
        this.commentList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (commentList != null) {
            this.commentList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Comment comment) {
        if (comment != null) {
            this.commentList.add(comment);
            notifyDataSetChanged();
        }
    }

    public void add(Comment comment) {
        commentList.add(comment);
        notifyItemInserted(commentList.size() - 1);
    }

    public void addAll(List<Comment> commentList) {
        if (commentList != null && commentList.size() > 0) {
            for (Comment comment : commentList) {
                add(comment);
            }
        }
        notifyDataSetChanged();
    }

    public void remove(Comment comment) {
        int position = commentList.indexOf(comment);
        if (position > -1) {
            commentList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Comment getItem(int position) {
        return commentList.get(position);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Comment());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = commentList.size() - 1;
        if (position > -1) {
            Comment comment = getItem(position);

            if (comment != null) {
                if (TextUtils.isEmpty(comment.getContent())) {
                    commentList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }


    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView commentAuthorFullName, commentDate, commentContentView;
        private ImageView commentAuthorPhoto;
        private ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            commentAuthorFullName = (TextView) itemView.findViewById(R.id.comment_author_full_name);
            commentDate = (TextView) itemView.findViewById(R.id.comment_date);
            commentContentView = (TextView) itemView.findViewById(R.id.comment_content_view);
            commentAuthorPhoto = (ImageView) itemView.findViewById(R.id.comment_author_photo);
            itemView.setOnClickListener(this);
            commentAuthorFullName.setOnClickListener(this);
            commentAuthorPhoto.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(commentList.size() - 1);
        if (errorMsg != null) this.errorMsg = errorMsg;
    }
}