package com.playnetwork.pna.ui.customviews;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextWatcher;
import android.util.AttributeSet;

import com.playnetwork.pna.utils.CreditCardBaseTextWatcher;

/**
 * Created by yekmer
 */
public class CreditCardEditText extends AppCompatEditText {

    private CreditCardBaseTextWatcher mTextWatcher;

    public CreditCardEditText(Context context) {
        super(context);
    }

    public CreditCardEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CreditCardEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CreditCardBaseTextWatcher getTextWatcher() {
        return mTextWatcher;
    }

    public void setTextWatcher(CreditCardBaseTextWatcher textWatcher) {
        this.mTextWatcher = textWatcher;
    }

    public void setCopyPastedText(CharSequence text) {
        mTextWatcher.setIsCopyPasted(true);
        setText(text);
        mTextWatcher.setIsCopyPasted(false);
    }

    @Override
    public void addTextChangedListener(TextWatcher watcher) {
        super.addTextChangedListener(watcher);
        if (watcher instanceof CreditCardBaseTextWatcher) {
            CreditCardBaseTextWatcher creditCardBaseTextWatcher = (CreditCardBaseTextWatcher) watcher;
            setTextWatcher(creditCardBaseTextWatcher);
        }
    }
}
