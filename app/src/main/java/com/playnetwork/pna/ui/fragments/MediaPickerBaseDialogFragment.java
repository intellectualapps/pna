package com.playnetwork.pna.ui.fragments;

import android.content.Context;
import android.net.Uri;

import com.playnetwork.pna.ui.dialogs.BaseDialogFragment;
import com.playnetwork.pna.ui.dialogs.MediaPickerDialog;

public abstract class MediaPickerBaseDialogFragment extends BaseDialogFragment {

    MediaPickerDialog.MediaPickedListener mMediaPickedListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mMediaPickedListener = (MediaPickerDialog.MediaPickedListener) getActivity();
        } catch (ClassCastException cce) {
            throw new ClassCastException("The activity must implement " + MediaPickerDialog.MediaPickedListener.class.getSimpleName());
        }
    }

    public abstract void onMediaPickerSuccess(Uri fileUri, int mediaType, String fragmentTag);

    public abstract void onMediaPickerError(String message, String fragmentTag);
}
