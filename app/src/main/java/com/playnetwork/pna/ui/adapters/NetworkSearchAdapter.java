package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NetworkSearchAdapter extends BaseRecyclerAdapter {
    private List<User> userList;
    private ClickListener clickListener;
    private final int SEARCH_RESULT = 0;
    private int adapterMode = 1;

    public NetworkSearchAdapter(Context context, ClickListener clickListener, PNAInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.userList = new ArrayList<User>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;

        switch (viewType) {
            default:
            case SEARCH_RESULT:
                view = mInflater.inflate(R.layout.network_search_item_layout, parent, false);
                viewHolder = new NetworkSearchAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        User user = userList.get(position);
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            default:
                final NetworkSearchAdapter.ViewHolder viewHolder = (NetworkSearchAdapter.ViewHolder) holder;
                if (user != null) {
                    viewHolder.memberFullName.setText(String.format("%s %s", user.getFname(), user.getLname()));
                    viewHolder.memberJobTitle.setText(user.getJobTitle());
                    if (!TextUtils.isEmpty(user.getPic())) {
                        String photoUrl = user.getPic().contains("http") ? user.getPic() : user.getUrl() + "/" + user.getPic();
                        Picasso.with(context).load(photoUrl).placeholder(R.color.colorGray).into(viewHolder.memberProfilePhoto);
                    }

                    if (!TextUtils.isEmpty(user.getNetworkState())) {
                        switch (user.getNetworkState()) {
                            case Constants.PENDING_NETWORK_STATE:
                                viewHolder.addToNetworkButton.setVisibility(View.GONE);
                                break;
                            case Constants.ACCEPTED_NETWORK_STATE:
                                viewHolder.addToNetworkButton.setVisibility(View.GONE);
                                break;
                            case Constants.NONE_NETWORK_STATE:
                            default:
                                viewHolder.addToNetworkButton.setVisibility(View.VISIBLE);
                                break;
                        }
                    } else {
                        viewHolder.addToNetworkButton.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(userList) ? 0 : userList.size();
    }

    @Override
    public int getItemViewType(int position) {
        User user = userList.get(position);
        if (position == userList.size() - 1 && isLoadingAdded && (user != null && TextUtils.isEmpty(user.getEmail()))) {
            return LOADING;
        }

        return SEARCH_RESULT;
    }

    public List<User> getUserList() {
        return userList;
    }

    /*
    Helpers
    */

    public void setItems(List<User> items) {
        this.userList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (userList != null) {
            this.userList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(User user) {
        if (user != null) {
            this.userList.add(user);
            notifyDataSetChanged();
        }
    }

    public void add(User user) {
        if (userList == null) {
            userList = new ArrayList<User>();
        }
        userList.add(user);
        notifyItemInserted(userList.size() - 1);
    }

    public void addAll(List<User> userList) {
        try {
            if (userList != null && userList.size() > 0) {
                for (User user : userList) {
                    add(user);
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void remove(User user) {
        int position = userList.indexOf(user);
        if (position > -1) {
            userList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(int position) {
        if (position > -1) {
            userList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private User getItem(int position) {
        return userList.get(position);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new User());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = userList.size() - 1;
        if (position > -1) {
            User user = getItem(position);

            if (user != null) {
                if (user.getId() < 1 && TextUtils.isEmpty(user.getEmail())) {
                    userList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView memberFullName, memberJobTitle;
        ImageView memberProfilePhoto, addToNetworkButton;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            memberFullName = (TextView) itemView.findViewById(R.id.member_full_name);
            memberJobTitle = (TextView) itemView.findViewById(R.id.member_job_title);
            addToNetworkButton = (ImageView) itemView.findViewById(R.id.add_to_network_button);
            memberProfilePhoto = (ImageView) itemView.findViewById(R.id.member_profile_photo);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            memberFullName.setOnClickListener(this);
            memberJobTitle.setOnClickListener(this);
            memberProfilePhoto.setOnClickListener(this);
            addToNetworkButton.setOnClickListener(this);
            itemView.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(userList.size() - 1);
        if (errorMsg != null) this.errorMsg = errorMsg;
    }
}