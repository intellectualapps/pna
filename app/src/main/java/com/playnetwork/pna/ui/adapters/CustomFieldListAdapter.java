package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.CustomField;
import com.playnetwork.pna.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomFieldListAdapter extends BaseRecyclerAdapter {
    private List<CustomField> customFieldList;
    private ClickListener clickListener;
    private final int SINGLE_FIELD = 1, DOUBLE_FIELD = 2;
    private boolean inDoubleFieldMode = false;

    public CustomFieldListAdapter(Context context, ClickListener clickListener, boolean inDoubleFieldMode) {
        super(context);
        this.clickListener = clickListener;
        this.customFieldList = new ArrayList<CustomField>();
        this.inDoubleFieldMode = inDoubleFieldMode;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        switch (viewType) {
            default:
            case DOUBLE_FIELD:
                view = mInflater.inflate(R.layout.custom_field_layout, parent, false);
                viewHolder = new CustomFieldListAdapter.ViewHolder(view, this.clickListener);
                break;
            case SINGLE_FIELD:
                view = mInflater.inflate(R.layout.custom_single_field_layout, parent, false);
                viewHolder = new CustomFieldListAdapter.ViewHolder(view, this.clickListener);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CustomField customField = customFieldList.get(position);
        final CustomFieldListAdapter.ViewHolder viewHolder = (CustomFieldListAdapter.ViewHolder) holder;

        if (customField != null) {
            switch (getItemViewType(position)) {
                case DOUBLE_FIELD:
                default:
                    viewHolder.fieldLabelView.setText(customField.getFieldLabel());
                    viewHolder.fieldValueView.setText(customField.getFieldValue());
                    break;
                case SINGLE_FIELD:
                    if (customField.isAutoLink()) {
                        boolean isEmail = Patterns.EMAIL_ADDRESS.matcher(customField.getFieldValue()).matches();
                        viewHolder.fieldValueView.setLinkTextColor(ContextCompat.getColor(context, R.color.colorPrimaryBlue));
                        viewHolder.fieldValueView.setAutoLinkMask(isEmail ? Linkify.EMAIL_ADDRESSES : Linkify.WEB_URLS);
                        viewHolder.fieldValueView.setMovementMethod(LinkMovementMethod.getInstance());
                    }
                    viewHolder.fieldValueView.setText(customField.getFieldValue());
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(customFieldList) ? 0 : customFieldList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return inDoubleFieldMode ? DOUBLE_FIELD : SINGLE_FIELD;
    }

    /*
    Helpers
    */

    public void setItems(List<CustomField> items) {
        this.customFieldList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (customFieldList != null) {
            this.customFieldList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(CustomField customField) {
        if (customField != null) {
            this.customFieldList.add(customField);
            notifyDataSetChanged();
        }
    }

    public void remove(CustomField customField) {
        int position = customFieldList.indexOf(customField);
        if (position > -1) {
            customFieldList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private CustomField getItem(int position) {
        return customFieldList.get(position);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView fieldLabelView, fieldValueView;
        private ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            fieldLabelView = itemView.findViewById(R.id.field_label);
            fieldValueView = itemView.findViewById(R.id.field_value);
            itemView.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (clickListener != null) {
                clickListener.onClick(view, position, false);
            }
        }
    }
}