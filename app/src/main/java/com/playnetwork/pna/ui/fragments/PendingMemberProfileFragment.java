package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.Constants;
import com.squareup.picasso.Picasso;

public class PendingMemberProfileFragment extends BaseFragment implements View.OnClickListener {

    private TextView memberFullNameView;
    private TextView memberJobTitleView;
    private TextView memberIndustryLocationView;
    private ImageView memberPhotoView;
    private Network network;
    private int position;
    private int memberId, networkId;
    private User user, member;

    public static Fragment newInstance(Bundle args) {
        PendingMemberProfileFragment frag = new PendingMemberProfileFragment();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(Constants.NETWORK)) {
            network = getArguments().getParcelable(Constants.NETWORK);
            position = getArguments().getInt(Constants.POSITION, 0);
        }

        if (getArguments() != null && getArguments().containsKey(Constants.MEMBER)) {
            member = getArguments().getParcelable(Constants.MEMBER);
            position = getArguments().getInt(Constants.POSITION, 0);
            memberId = member.getId();
        }

        if (getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.member_pending_network_layout, container, false);
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.member_details_toolbar_label));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        memberFullNameView = view.findViewById(R.id.member_full_name);
        memberJobTitleView = view.findViewById(R.id.member_job_title);
        memberIndustryLocationView = view.findViewById(R.id.member_industry_location);
        memberPhotoView = view.findViewById(R.id.member_photo);
        if (network != null && !TextUtils.isEmpty(network.getMemberId())) {
            loadNetworkData(network);
        } else {
            loadMemberData(member);
        }
    }

    private void loadNetworkData(Network network) {
        if (network != null) {
            memberFullNameView.setText(String.format("%s %s", network.getFirstName(), network.getLastName()));
            memberJobTitleView.setText(network.getJobTitle());
            memberId = Integer.parseInt(network.getMemberId());
            memberIndustryLocationView.setText(String.format("%s | %s", network.getCompany(), network.getState()));
            if (!TextUtils.isEmpty(network.getProfilePhotoUrl())) {
                Picasso.with(getContext()).load(network.getProfilePhotoUrl()).placeholder(R.color.colorGray).into(memberPhotoView);
            } else {
                Picasso.with(getContext()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(memberPhotoView);
            }
        }
    }

    private void loadMemberData(User member) {
        if (member != null) {
            memberFullNameView.setText(String.format("%s %s", member.getFname(), member.getLname()));
            memberJobTitleView.setText(member.getJobTitle());
            if (!TextUtils.isEmpty(member.getState())) {
                memberIndustryLocationView.setText(String.format("%s | %s", member.getCompany(), member.getState()));
            } else {
                memberIndustryLocationView.setText(member.getCompany());
            }
            if (!TextUtils.isEmpty(member.getPic())) {
                String photoUrl = member.getPic().contains("http") ? member.getPic() : member.getUrl() + "/" + member.getPic();
                Picasso.with(getContext()).load(photoUrl).placeholder(R.color.colorGray).into(memberPhotoView);
            } else {
                Picasso.with(getContext()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(memberPhotoView);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.remove_from_network_button:
                break;
        }
    }
}
