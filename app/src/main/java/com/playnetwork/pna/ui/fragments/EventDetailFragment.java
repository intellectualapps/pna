package com.playnetwork.pna.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.EventResponse;
import com.playnetwork.pna.data.enums.PaymentType;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.CalendarEvent;
import com.playnetwork.pna.data.models.CustomField;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.data.models.EventConfirmation;
import com.playnetwork.pna.data.models.Table;
import com.playnetwork.pna.data.models.Ticket;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.room.daos.CalendarEventDao;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.CustomFieldListAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.ui.customviews.CustomTypefaceSpan;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.CalendarUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.CurrencyUtils;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.FontUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailFragment extends BaseFragment implements View.OnClickListener, PNAInterfaces.FragmentOnBackPressedListener, RadioGroup.OnCheckedChangeListener, PNAInterfaces.EventPaymentCompletionListener {

    private TextView eventTitleView;
    private Button proceedButton;
    private ImageView eventBannerView;
    private Event event;
    private ViewFlipper viewFlipper;
    private CustomRecyclerView customFieldsRecyclerView;
    private CustomFieldListAdapter customFieldListAdapter;
    private List<CustomField> customFieldList;
    private User user;
    private int position;
    private boolean hasCalendarPermissionBeenGranted = false;
    private PlayNetworkDatabase playNetworkDatabase;
    private String eventId;
    private long calendarEventId = 0;
    private Double purchaseCost = 0.0;
    private PaymentType paymentType;
    private RadioGroup purchaseOptionsRadioGroup, ticketRadioGroup, tableRadioGroup;
    private RadioButton ticketRadioButton, tableRadioButton, regularTicketRadioButton, vipTicketRadioButton, regularTableRadioButton, vipTableRadioButton;
    private CustomTypefaceSpan mediumTypefaceSpan;
    private SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
    private boolean isPaymentContext = false;

    public EventDetailFragment() {
    }

    public static Fragment newInstance(Bundle args) {
        EventDetailFragment frag = new EventDetailFragment();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(Constants.EVENT)) {
            event = getArguments().getParcelable(Constants.EVENT);
            position = getArguments().getInt(Constants.POSITION, -1);
            eventId = event.getId();
        }

        if (getArguments() != null && getArguments().containsKey(Constants.EVENT_ID)) {
            eventId = getArguments().getString(Constants.EVENT_ID);
        }

        if (getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        CalendarEventDao calendarEventDao = playNetworkDatabase.calendarEventDao();
        CalendarEvent calendarEvent = calendarEventDao.getEventWithEventId(eventId);
        if (calendarEvent != null && event != null) {
            event.setCalendarEventId(calendarEvent.getCalendarEventId());
        }
        registerReceiver(getContext(), calendarEventsReceiver, new IntentFilter(Constants.CALENDAR_EVENT_INTENT_FILTER));
        mediumTypefaceSpan = new CustomTypefaceSpan(FontUtils.selectTypeface(getContext(), FontUtils.STYLE_MEDIUM));

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_detail, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (hasCalendarPermissionBeenGranted) {
            addEventToCalendar(event);
            hasCalendarPermissionBeenGranted = false;
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.event_details_toolbar_label));
        eventTitleView = view.findViewById(R.id.event_title);
        eventBannerView = view.findViewById(R.id.event_banner);
        proceedButton = view.findViewById(R.id.proceed_button);
        viewFlipper = view.findViewById(R.id.view_flipper);
        customFieldsRecyclerView = view.findViewById(R.id.custom_fields_recyclerview);
        purchaseOptionsRadioGroup = view.findViewById(R.id.purchase_options_radio_group);
        ticketRadioGroup = view.findViewById(R.id.ticket_options_radio_group);
        tableRadioGroup = view.findViewById(R.id.table_options_radio_group);
        ticketRadioButton = view.findViewById(R.id.ticket_radio_button);
        tableRadioButton = view.findViewById(R.id.table_radio_button);
        regularTicketRadioButton = view.findViewById(R.id.regular_ticket_radio_button);
        vipTicketRadioButton = view.findViewById(R.id.vip_ticket_radio_button);
        regularTableRadioButton = view.findViewById(R.id.regular_table_radio_button);
        vipTableRadioButton = view.findViewById(R.id.vip_table_radio_button);

        purchaseOptionsRadioGroup.setOnCheckedChangeListener(this);
        ticketRadioGroup.setOnCheckedChangeListener(this);
        tableRadioGroup.setOnCheckedChangeListener(this);

        customFieldListAdapter = new CustomFieldListAdapter(getContext(), null, true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        customFieldsRecyclerView.setLayoutManager(linearLayoutManager);
        customFieldsRecyclerView.setAdapter(customFieldListAdapter);

        proceedButton.setOnClickListener(this);
        proceedButton.setEnabled(false);
        loadEventData(event);
        fetchEventDetails(eventId, String.valueOf(user.getId()));
    }

    private void loadEventData(Event event) {
        if (event != null && !TextUtils.isEmpty(event.getId()) && getContext() != null) {
            proceedButton.setEnabled(true);
            eventTitleView.setText(event.getTitle());
            if (!TextUtils.isEmpty(event.getPicUrl())) {
                Picasso.with(getContext()).load(event.getPicUrl()).placeholder(R.color.colorGray).into(eventBannerView);
                eventBannerView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
                Picasso.with(getContext()).load(R.drawable.play_africa_logo).placeholder(R.color.colorGray).into(eventBannerView);
                eventBannerView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            }

            proceedButton.setBackground(ContextCompat.getDrawable(getContext(), event.getHasRSVP() ? R.drawable.gray_button_ripple_resource : R.drawable.black_button_ripple_resource));
            proceedButton.setText(getActionButtonLabel(event));
            customFieldListAdapter.setItems(getFieldDetails(event));
        }
    }

    private String getActionButtonLabel(Event event) {
        return getString(event.getHasRSVP() ? R.string.remove_from_calendar_button : event.isPaidEvent() ? R.string.purchase_label : R.string.add_to_calendar_label);
    }

    private List<CustomField> getFieldDetails(Event event) {
        List<CustomField> list = new ArrayList<CustomField>();
        String ordinalDate = DateUtils.getOrdinalDate(event.getDate());
        list.add(new CustomField(Constants.EVENT_VENUE, event.getVenue()));
        list.add(new CustomField(Constants.EVENT_ADDRESS, event.getAddress()));
        list.add(new CustomField(Constants.EVENT_DATE, String.format("%s %s", ordinalDate, DateUtils.getSimpleDateFormat(event.getDate(), "MMMM, yyyy"))));
        list.add(new CustomField(Constants.EVENT_TIME, event.getTime()));
        list.add(new CustomField(Constants.EVENT_DRESS_CODE, event.getDressCode()));
        list.add(new CustomField(Constants.EVENT_INFO, event.getInfo()));
        if (event.isPaidEvent()) {
            purchaseOptionsRadioGroup.setVisibility(View.VISIBLE);
            if (event.getTickets() != null) {
                list.addAll(getTicketDetails(event.getTickets()));
            } else {
                ticketRadioButton.setVisibility(View.GONE);
                ticketRadioGroup.setVisibility(View.GONE);
            }
            if (event.getTables() != null) {
                list.addAll(getTableDetails(event.getTables()));
            } else {
                tableRadioButton.setVisibility(View.GONE);
                tableRadioGroup.setVisibility(View.GONE);
            }
        }

        return list;
    }

    private List<CustomField> getTicketDetails(Ticket ticket) {
        List<CustomField> customFieldList = new ArrayList<CustomField>();
        if (ticket != null) {
            String formattedAmount = null, content = null;
            ticketRadioButton.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(ticket.getRegular())) {
                formattedAmount = formatAmount(ticket.getRegular());
                content = getString(R.string.regular_placeholder, formattedAmount);
                regularTicketRadioButton.setVisibility(View.VISIBLE);
                regularTicketRadioButton.setText(getFormattedContent(content, formattedAmount));
                customFieldList.add(new CustomField(Constants.REGULAR_EVENT_TICKET, formattedAmount));
            }

            if (!TextUtils.isEmpty(ticket.getVip())) {
                formattedAmount = formatAmount(ticket.getVip());
                content = getString(R.string.vip_placeholder, formattedAmount);
                vipTicketRadioButton.setVisibility(View.VISIBLE);
                vipTicketRadioButton.setText(getFormattedContent(content, formattedAmount));
                customFieldList.add(new CustomField(Constants.VIP_EVENT_TICKET, formattedAmount));
            }
        } else {
            ticketRadioButton.setVisibility(View.GONE);
            ticketRadioGroup.setVisibility(View.GONE);
        }
        return customFieldList;
    }

    private SpannableStringBuilder getFormattedContent(String content, String formattedAmount) {
        spannableStringBuilder.clear();
        spannableStringBuilder.append(content);
        int length = content.length();
        spannableStringBuilder.setSpan(mediumTypefaceSpan, length - formattedAmount.length(), length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableStringBuilder;
    }

    private List<CustomField> getTableDetails(Table table) {
        List<CustomField> customFieldList = new ArrayList<CustomField>();
        if (table != null) {
            String formattedAmount = null, content = null;
            tableRadioButton.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(table.getRegular())) {
                formattedAmount = formatAmount(table.getRegular());
                content = getString(R.string.regular_placeholder, formattedAmount);
                regularTableRadioButton.setVisibility(View.VISIBLE);
                regularTableRadioButton.setText(getFormattedContent(content, formattedAmount));
                customFieldList.add(new CustomField(Constants.REGULAR_EVENT_TABLE, formattedAmount));
            }
            if (!TextUtils.isEmpty(table.getVip())) {
                formattedAmount = formatAmount(table.getVip());
                content = getString(R.string.vip_placeholder, formattedAmount);
                vipTableRadioButton.setVisibility(View.VISIBLE);
                vipTableRadioButton.setText(getFormattedContent(content, formattedAmount));
                customFieldList.add(new CustomField(Constants.VIP_EVENT_TABLE, formattedAmount));
            }
        } else {
            tableRadioButton.setVisibility(View.GONE);
            tableRadioGroup.setVisibility(View.GONE);
        }
        return customFieldList;
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.purchase_options_radio_group:
                if (checkedId == R.id.ticket_radio_button) {
                    toggleRadioGroupChildren(ticketRadioGroup, true);
                    toggleRadioGroupChildren(tableRadioGroup, false);
                } else if (checkedId == R.id.table_radio_button) {
                    toggleRadioGroupChildren(tableRadioGroup, true);
                    toggleRadioGroupChildren(ticketRadioGroup, false);
                }
                break;
            case R.id.ticket_options_radio_group:
                ticketRadioButton.setChecked(true);
                if (checkedId == R.id.regular_ticket_radio_button) {
                    purchaseCost = Double.valueOf(event.getTickets().getRegular());
                    paymentType = PaymentType.REGULAR_TICKET;
                } else {
                    purchaseCost = Double.valueOf(event.getTickets().getVip());
                    paymentType = PaymentType.VIP_TICKET;
                }
                break;
            case R.id.table_options_radio_group:
                tableRadioButton.setChecked(true);
                if (checkedId == R.id.regular_table_radio_button) {
                    purchaseCost = Double.valueOf(event.getTables().getRegular());
                    paymentType = PaymentType.REGULAR_TABLE;
                } else {
                    purchaseCost = Double.valueOf(event.getTables().getVip());
                    paymentType = PaymentType.VIP_TABLE;
                }
                break;
        }
    }

    private void toggleRadioGroupChildren(RadioGroup radioGroup, boolean enabled) {
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            radioGroup.getChildAt(i).setEnabled(enabled);
        }
    }

    private String formatAmount(String value) {
        return CurrencyUtils.formatToCurrencyWithSymbol(Double.valueOf(value), Constants.CURRENCY_SYMBOL);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.proceed_button:
                if (!event.isPaidEvent() || event.getHasRSVP()) {
                    manageEventRSVP();
                } else {
                    if (viewFlipper.getDisplayedChild() == 1) {
                        if (getSelectedPurchaseOption(event) <= 0.0) {
                            showErrorPopupMessage(getString(R.string.select_purchase_option_error));
                            return;
                        }

                        Bundle bundle = new Bundle();
                        EventConfirmation eventConfirmation = new EventConfirmation(event.getId(), event.getTitle(), paymentType.name(), purchaseCost);
                        bundle.putParcelable(Constants.EVENT_CONFIRMATION, eventConfirmation);
                        bundle.putParcelable(Constants.USER, user);
                        EventCheckoutFragment eventCheckoutFragment = new EventCheckoutFragment();
                        eventCheckoutFragment.setArguments(bundle);
                        eventCheckoutFragment.setPaymentCompletedListener(this);
                        switchFragment(eventCheckoutFragment);
                    } else {
                        viewFlipper.setDisplayedChild(1);
                    }
                }
                break;
        }
    }

    private Double getSelectedPurchaseOption(Event event) {
        if (event == null || (event.getTickets() == null && event.getTables() == null) || (!ticketRadioButton.isChecked() && !tableRadioButton.isChecked())) {
            return 0.0;
        }

        return purchaseCost;
    }

    private void fetchEventDetails(String eventId, String memberId) {
        if (NetworkUtils.isConnected(getContext())) {
            getHttpService().fetchEvent(eventId, memberId, buildFetchEventCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void manageEventRSVP() {
        if (NetworkUtils.isConnected(getContext())) {
            if (checkCalendarPermission(getActivity())) {
                Map<String, String> requestMap = new HashMap<String, String>();
                requestMap.put(Constants.EVENT_ID, event.getId());
                requestMap.put(Constants.MEMBER_ID_PARAM, String.valueOf(user.getId()));
                if (event.getHasRSVP()) {
                    confirmDelete(requestMap);
                } else {
                    showLoadingIndicator(getString(R.string.rsvp_confirmation_message));
                    getHttpService().confirmEventRSVP(requestMap, buildRSVPEventCallback());
                }
            }
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void addEventToCalendar(Event event) {
        if (getActivity() != null && checkCalendarPermission(getActivity())) {
            showLoadingIndicator(getString(R.string.request_processing_label));
            CalendarUtils.saveEventToCalendar(event);
        }
    }

    private void removeEventFromCalendar(Event event) {
        if (getActivity() != null && checkCalendarPermission(getActivity())) {
            showLoadingIndicator(getString(R.string.request_processing_label));
            CalendarUtils.deleteEvent(event);
        }
    }

    private BroadcastReceiver calendarEventsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra(Constants.ACTION);
            long calendarEventId = intent.getLongExtra(Constants.EVENT_ID, -1);

            hideLoadingIndicator();
            switch (action) {
                case Constants.EVENT_ERROR:
                    showErrorPopupMessage(getString(R.string.event_error_message));
                    break;
                case Constants.EVENT_ADDED:
                    playNetworkDatabase.calendarEventDao().insertCalendarEvent(new CalendarEvent(event.getId(), calendarEventId));
                    event.setCalendarEventId(calendarEventId);
                    event.setHasRSVP(true);
                    updateEventList(true, calendarEventId);
                    showSuccessPopupMessage(getString(R.string.event_added_message));
                    if (!isPaymentContext) {
                        closeFragment();
                    }
                    break;
                case Constants.EVENT_DELETED:
                    updateEventList(false, calendarEventId);
                    showSuccessPopupMessage(getString(R.string.event_deleted_message));
                    closeFragment();
                    break;
            }
        }
    };

    private void confirmDelete(final Map<String, String> requestMap) {
        if (getContext() != null) {
            new AlertDialog.Builder(getContext())
                    .setMessage(event.isPaidEvent() ? R.string.confirm_leave_paid_event : R.string.confirm_leave_event)
                    .setNegativeButton(android.R.string.cancel, null)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showLoadingIndicator(getString(R.string.rsvp_confirmation_message));
                            getHttpService().confirmEventRSVP(requestMap, buildRSVPEventCallback());
                            dialog.dismiss();
                        }
                    })
                    .create()
                    .show();
        }
    }

    private void updateEventList(boolean hasRSVPed, long calendarEventId) {
        Intent intent = new Intent();
        intent.setAction(Constants.UPDATE_EVENTS_INTENT_FILTER);
        intent.putExtra(Constants.POSITION, position);
        intent.putExtra(Constants.CALENDAR_EVENT_ID, calendarEventId);
        intent.putExtra(Constants.HAS_RSVP, hasRSVPed);
        if (getContext() != null) {
            getContext().sendBroadcast(intent);
        }
    }

    private Callback<EventResponse> buildRSVPEventCallback() {
        return new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                EventResponse eventResponse = response.body();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (eventResponse != null && eventResponse.getEvent() != null) {
                        long calendarEventId = event.getCalendarEventId();
                        event = eventResponse.getEvent();
                        event.setCalendarEventId(calendarEventId);
                        if (event.getHasRSVP()) {
                            addEventToCalendar(event);
                        } else {
                            removeEventFromCalendar(event);
                        }
                    } else {
                        hideLoadingIndicator();
                        showErrorPopupMessage(getString(R.string.add_event_error_message));
                    }
                } else {
                    hideLoadingIndicator();
                }
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<EventResponse> buildFetchEventCallback() {
        return new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                EventResponse eventResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (eventResponse != null && eventResponse.getEvent() != null) {
                        if (event != null) {
                            calendarEventId = event.getCalendarEventId();
                        }
                        event = eventResponse.getEvent();
                        event.setCalendarEventId(calendarEventId);

                        if (isVisible()) {
                            loadEventData(event);
                        }

                        if (isPaymentContext) {
                            if (event.getHasRSVP()) {
                                addEventToCalendar(event);
                            }
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.fetch_event_error_message));
                    }
                }
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CALENDAR_PERMISSION_REQUEST_CODE:
                hasCalendarPermissionRequestBeenMade = true;
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            hasCalendarPermissionBeenGranted = false;
                            checkCalendarPermission(getActivity());
                            return;
                        }
                        hasCalendarPermissionBeenGranted = true;
                    }
                } else {
                    checkCalendarPermission(getActivity());
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
        unregisterReceiver(getContext(), calendarEventsReceiver);
    }

    @Override
    public void onBackPressed() {
        if (viewFlipper != null && viewFlipper.getDisplayedChild() == 1) {
            viewFlipper.setDisplayedChild(0);
        } else {
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
        }
    }

    @Override
    public void onPaymentCompleted() {
        event.setHasRSVP(true);
        loadEventData(event);
        fetchEventDetails(eventId, String.valueOf(user.getId()));
        isPaymentContext = true;
    }
}
