package com.playnetwork.pna.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import com.playnetwork.pna.R;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.fragments.MyNetworkFragment;
import com.playnetwork.pna.ui.fragments.NetworkRequestFragment;
import com.playnetwork.pna.ui.fragments.NetworkRequestsFragment;
import com.playnetwork.pna.ui.fragments.NetworkSuggestionsFragment;
import com.playnetwork.pna.utils.Constants;

public class NetworkActivity extends BaseActivity {
    private Bundle fragmentBundle;
    private String viewType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_suggestions);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);

            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.NETWORK_SUGGESTIONS_VIEW_TAG:
                        frag = NetworkSuggestionsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.NETWORK_REQUESTS_VIEW_TAG:
                        frag = NetworkRequestsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.MY_NETWORK_VIEW_TAG:
                        frag = MyNetworkFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.NETWORK_REQUEST_VIEW_TAG:
                        frag = NetworkRequestFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = NetworkSuggestionsFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = NetworkSuggestionsFragment.newInstance(null);
            }

            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }
}
