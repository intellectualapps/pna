package com.playnetwork.pna.ui.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.utils.Constants;

public class LaunchActivity extends BaseActivity {
    private static final int LAUNCH_DURATION = 100;
    private static final String TAG = LaunchActivity.class.getSimpleName();
    Animation animation;
    private ImageView splashIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_launch);
        splashIcon = (ImageView) findViewById(R.id.app_icon);

        animation = AnimationUtils.loadAnimation(this, R.anim.image_splash_translation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new LaunchTask().execute();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        splashIcon.startAnimation(animation);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class LaunchTask extends AsyncTask<Void, Void, User> {
        Intent intent;
        User user;

        @Override
        protected User doInBackground(Void... voids) {
            try {
                Thread.sleep(LAUNCH_DURATION);
                user = PlayNetworkDatabase.getPlayNetworkDatabase().userDao().findLastLoggedInUser();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return user;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(User user) {
            super.onPostExecute(user);
            if (user != null && user.isLoggedIn()) {
                showMainActivity(LaunchActivity.this);
            } else {
                intent = new Intent(LaunchActivity.this, TempActivity.class);
                String viewType = Constants.LOGIN_VIEW_TAG;
                if (user != null) {
                    if (!user.getHasCreatedProfile()) {
                        if (TextUtils.isEmpty(user.getFname()) || TextUtils.isEmpty(user.getLname())) {
                            viewType = Constants.SOCIAL_PROFILE_VIEW_TAG;
                        } else {
                            viewType = Constants.CREATE_PROFILE_VIEW_TAG;
                        }
                    } else {
                        if (!user.getHasViewedNetworkSuggestions()) {
                            intent = new Intent(LaunchActivity.this, NetworkActivity.class);
                            viewType = Constants.NETWORK_SUGGESTIONS_VIEW_TAG;
                        }
                    }

                    intent.putExtra(Constants.VIEW_TYPE, viewType);
                }

                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        PlayNetworkDatabase.destroyDatabaseInstance();
        super.onDestroy();
    }
}
