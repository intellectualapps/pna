package com.playnetwork.pna.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.ui.fragments.EventDetailFragment;
import com.playnetwork.pna.ui.fragments.EventsFragment;
import com.playnetwork.pna.utils.Constants;

public class EventsActivity extends BaseActivity {
    private Bundle fragmentBundle;
    private String viewType;
    Fragment frag = null;
    final static String eventDetailTag = EventDetailFragment.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        toolbar = findViewById(R.id.toolbar);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);


            if (viewType != null) {
                switch (viewType) {
                    default:
                    case Constants.EVENTS_VIEW_TAG:
                        frag = EventsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.EVENT_DETAIL_VIEW_TAG:
                        frag = EventDetailFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = EventsFragment.newInstance(null);
            }

            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
