package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Feed;
import com.playnetwork.pna.ui.customviews.likebutton.LikeButton;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FeedListAdapter extends BaseRecyclerAdapter {
    private List<Feed> feedList;
    private final int TEXT_FEED = 2, IMAGE_FEED = 3;
    private ClickListener clickListener;

    public FeedListAdapter(Context context, ClickListener clickListener, PNAInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.feedList = new ArrayList<Feed>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;

        switch (viewType) {
            default:
            case TEXT_FEED:
                view = mInflater.inflate(R.layout.feed_item_text_layout, parent, false);
                viewHolder = new FeedListAdapter.ViewHolder(view, this.clickListener);
                break;
            case IMAGE_FEED:
                view = mInflater.inflate(R.layout.feed_item_image_layout, parent, false);
                viewHolder = new FeedListAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            case TEXT_FEED:
            case IMAGE_FEED:
                Feed feed = feedList.get(position);
                final FeedListAdapter.ViewHolder viewHolder = (FeedListAdapter.ViewHolder) holder;
                if (feed != null) {
                    viewHolder.feedDate.setText(DateUtils.timeSincePosted(feed.getFeedDate()));
                    viewHolder.feedDescription.setText(feed.getDescription());
                    viewHolder.authorFullName.setText(feed.getAuthor());

                    if (feed.getLikes() > 0) {
                        viewHolder.feedLikesCount.setText(String.valueOf(feed.getLikes()));
                        adjustLikeButtonIcon(feed.isHasUserLiked(), viewHolder.likeButton);
                        adjustLikeButtonIcon(feed.isHasUserLiked(), viewHolder.favouriteButton);
                    } else {
                        viewHolder.feedLikesCount.setText("");
                        adjustLikeButtonIcon(false, viewHolder.likeButton);
                        adjustLikeButtonIcon(false, viewHolder.favouriteButton);
                    }

                    if (!TextUtils.isEmpty(feed.getFeedAuthorPicture())) {
                        Picasso.with(context).load(feed.getFeedAuthorPicture()).placeholder(R.color.colorGray).into(viewHolder.authorPhotoThumbnail);
                    } else {
                        viewHolder.authorPhotoThumbnail.setImageDrawable(null);
                    }

                    switch (feed.getType()) {
                        case "pic":
                            viewHolder.feedContent.setVisibility(View.GONE);
                            viewHolder.feedContentBanner.setVisibility(View.VISIBLE);
                            viewHolder.feedContent.setText(feed.getAltContent());
                            Picasso.with(context).load(feed.getContent()).placeholder(R.color.colorGray).into(viewHolder.feedContentBanner);
                            break;
                        case "share":
                            switch (feed.getShareType()) {
                                case "text":
                                    viewHolder.feedContent.setText(feed.getContent());
                                    viewHolder.feedContent.setVisibility(View.VISIBLE);
                                    viewHolder.feedContentBanner.setVisibility(View.GONE);
                                    break;
                                case "picture":
                                    viewHolder.feedContent.setVisibility(View.GONE);
                                    viewHolder.feedContentBanner.setVisibility(View.VISIBLE);
                                    Picasso.with(context).load(feed.getContent()).placeholder(R.color.colorGray).into(viewHolder.feedContentBanner);
                                    break;
                                case "both":
                                    viewHolder.feedContent.setVisibility(View.VISIBLE);
                                    viewHolder.feedContentBanner.setVisibility(View.VISIBLE);
                                    Picasso.with(context).load(feed.getContent()).placeholder(R.color.colorGray).into(viewHolder.feedContentBanner);
                                    viewHolder.feedContent.setText(feed.getAltContent());
                                    break;
                            }
                            break;
                        case "job":
                        case "info":
                        case "text":
                            viewHolder.feedContent.setText(feed.getContent());
                            break;
                    }
                } else {
                    viewHolder.authorPhotoThumbnail.setImageDrawable(null);
                }
                break;
        }
    }

    private void adjustLikeButtonIcon(boolean stateFlag, View likeButton) {
        if (likeButton instanceof ImageView) {
            ((ImageView) likeButton).setImageDrawable(ContextCompat.getDrawable(context, stateFlag ? R.drawable.ic_liked_state : R.drawable.ic_unliked_state));
        } else {
            ((LikeButton) likeButton).setLiked(stateFlag);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == feedList.size() - 1 && isLoadingAdded) {
            return LOADING;
        } else {
            Feed feed = feedList.get(position);
            if (!TextUtils.isEmpty(feed.getType())) {
                switch (feed.getType()) {
                    case "text":
                    case "job":
                    case "info":
                        return TEXT_FEED;
                    case "pic":
                    case "share":
                        return IMAGE_FEED;
                }
            }
            return TEXT_FEED;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(feedList) ? 0 : feedList.size();
    }


    /*
    View Holders
    */

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView authorFullName, feedDate, feedDescription, feedContent, feedLikesCount;
        ImageView authorPhotoThumbnail, feedContentBanner, likeButton;
        LikeButton favouriteButton;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            authorFullName = itemView.findViewById(R.id.author_full_name);
            authorPhotoThumbnail = itemView.findViewById(R.id.author_photo_thumbnail);
            feedContentBanner = itemView.findViewById(R.id.feed_content_banner);
            likeButton = itemView.findViewById(R.id.like_button);
            feedDate = itemView.findViewById(R.id.feed_date);
            feedDescription = itemView.findViewById(R.id.feed_description);
            feedContent = itemView.findViewById(R.id.feed_content_view);
            feedLikesCount = itemView.findViewById(R.id.feed_likes_count);
            favouriteButton = itemView.findViewById(R.id.favourite_button);

            itemView.setOnClickListener(this);
            likeButton.setOnClickListener(this);
            feedLikesCount.setOnClickListener(this);
            authorFullName.setOnClickListener(this);
            favouriteButton.setOnClickListener(this);
            authorPhotoThumbnail.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }
    }

    /*
    Helpers
    */

    public void setItems(List<Feed> items) {
        this.feedList = items;
        notifyDataSetChanged();
    }

    public void add(Feed feed) {
        feedList.add(feed);
        notifyItemInserted(feedList.size() - 1);
    }

    public void addAll(List<Feed> feedList) {
        if (feedList != null && feedList.size() > 0) {
            for (Feed feed : feedList) {
                add(feed);
            }
        }
        notifyDataSetChanged();
    }

    public void remove(Feed feed) {
        int position = feedList.indexOf(feed);
        if (position > -1) {
            feedList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clearItems() {
        if (feedList != null) {
            this.feedList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Feed feed) {
        if (feed != null) {
            this.feedList.add(feed);
            notifyDataSetChanged();
        }
    }

    private Feed getItem(int position) {
        return feedList.get(position);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Feed());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = feedList.size() - 1;
        if (position > -1) {
            Feed feed = getItem(position);

            if (feed != null) {
                if (TextUtils.isEmpty(feed.getAuthor())) {
                    feedList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(feedList.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }
}