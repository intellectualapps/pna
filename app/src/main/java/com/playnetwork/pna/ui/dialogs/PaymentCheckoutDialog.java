package com.playnetwork.pna.ui.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.api.responses.EventResponse;
import com.playnetwork.pna.data.api.responses.PublicKeyResponse;
import com.playnetwork.pna.data.api.responses.TransactionResponse;
import com.playnetwork.pna.data.enums.PaymentType;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.customviews.CreditCardEditText;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.CurrencyUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.OtherCardTextWatcher;
import com.playnetwork.pna.utils.ToastUtil;
import com.playnetwork.pna.utils.TwoDigitsCardTextWatcher;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentCheckoutDialog extends BaseDialogFragment implements View.OnClickListener, TextView.OnEditorActionListener {
    private Context context;
    private Button confirmPaymentButton;
    private User user;
    private CreditCardEditText creditCardEditText;
    private EditText expiryDateEditText, cvvEditText;
    private PNAInterfaces.PaymentCompletionListener listener;
    private TextView subscriptionAmountView;
    private Toolbar toolbar;
    private Double amount;
    private String memberFullName, memberEmailAddress;
    private Card card;
    private com.playnetwork.pna.data.models.Transaction transaction;
    private PlayNetworkDatabase playNetworkDatabase;
    private PaymentType paymentType;
    private int quantity = 0;
    private String eventId = "";

    public static PaymentCheckoutDialog create() {
        return new PaymentCheckoutDialog();
    }

    public static PaymentCheckoutDialog getInstance(Bundle args) {
        PaymentCheckoutDialog frag = new PaymentCheckoutDialog();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialogStyle);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        context = getActivity();
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
            amount = getArguments().getDouble(Constants.AMOUNT, 0.00);
            memberFullName = getArguments().getString(Constants.FULL_NAME, "");
            memberEmailAddress = getArguments().getString(Constants.EMAIL_ADDRESS, "");
            if (getArguments().containsKey(Constants.PAYMENT_TYPE)) {
                paymentType = PaymentType.fromString(getArguments().getString(Constants.PAYMENT_TYPE));
            }
            quantity = getArguments().getInt(Constants.QUANTITY, 0);
            eventId = getArguments().getString(Constants.EVENT_ID, "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = getActivity().getLayoutInflater().inflate(R.layout.dialog_card_details_input, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(getDialog().getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes(layoutParams);
        getDialog().getWindow().setWindowAnimations(R.style.dialog_animation_fade);
        init(rootView, savedInstanceState);

        return rootView;
    }

    private void init(View rootView, Bundle savedInstanceState) {
        subscriptionAmountView = rootView.findViewById(R.id.subscription_amount);

        String formattedAmount = CurrencyUtils.formatToCurrencyWithSymbol(amount, Constants.CURRENCY_SYMBOL);
        subscriptionAmountView.setText(formattedAmount);

        confirmPaymentButton = rootView.findViewById(R.id.pay_button);
        creditCardEditText = rootView.findViewById(R.id.card_number_input);
        expiryDateEditText = rootView.findViewById(R.id.expiry_date_input);
        cvvEditText = rootView.findViewById(R.id.cvv_input);
        toolbar = rootView.findViewById(R.id.toolbar);

        confirmPaymentButton.setOnClickListener(this);
        cvvEditText.setOnEditorActionListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissAllowingStateLoss();
            }
        });

        creditCardEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(25)});

        creditCardEditText.addTextChangedListener(new OtherCardTextWatcher(creditCardEditText));
        expiryDateEditText.addTextChangedListener(new TwoDigitsCardTextWatcher(expiryDateEditText));
        setCancelable(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() == null || getDialog().getWindow() == null) {
            return;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pay_button:
                if (validateCardInputView(creditCardEditText, getString(R.string.invalid_card_number)) && validateExpiryDateInput(expiryDateEditText, getString(R.string.valid_date_error)) && validateCVV(cvvEditText, getString(R.string.invalid_cvv_error_message))) {
                    card = buildCardObject(creditCardEditText, cvvEditText, expiryDateEditText);
                    initializeTransaction();
                }
                break;
        }
    }


    public void setPaymentCompletionListener(PNAInterfaces.PaymentCompletionListener paymentCompletionListener) {
        this.listener = paymentCompletionListener;
    }

    public Card buildCardObject(CreditCardEditText creditCardEditText, EditText cvvEditText, EditText expiryDateEditText) {
        Card card = new Card.Builder(creditCardEditText.getText().toString().replaceAll(" ", ""), 0, 0, "").build();
        card.setCvc(cvvEditText.getText().toString());
        String expiryDate = expiryDateEditText.getText().toString();
        int expiryMonth = Integer.parseInt(expiryDate.split("/")[0]);
        int expiryYear = Integer.parseInt(expiryDate.split("/")[1]);
        card.setExpiryMonth(expiryMonth);
        card.setExpiryYear(expiryYear);

        return card;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            if (validateCardInputView(creditCardEditText, getString(R.string.invalid_card_number)) && validateExpiryDateInput(expiryDateEditText, getString(R.string.valid_date_error)) && validateCVV(cvvEditText, getString(R.string.invalid_cvv_error_message))) {
                card = buildCardObject(creditCardEditText, cvvEditText, expiryDateEditText);
                initializeTransaction();
                return false;
            }
        }

        return false;
    }

    private void fetchPublicKey() {
        if (NetworkUtils.isConnected(getContext())) {
            getHttpService().fetchPublicKey(buildPublicKeyCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void initializeTransaction() {
        hideKeyboard();
        if (NetworkUtils.isConnected(getContext())) {
            showLoadingIndicator(getString(R.string.transaction_loading_message));
            Map<String, String> dataMap = new HashMap<String, String>();
            String AmountValue = String.valueOf(amount);

            switch (paymentType) {
                case PREMIUM_SUBSCRIPTION:
                    dataMap.put(Constants.EMAIL_ADDRESS, memberEmailAddress);
                    dataMap.put(Constants.AMOUNT, AmountValue.substring(0, AmountValue.indexOf(".")));
                    getHttpService().initializeTransaction(dataMap, buildInitializeTransactionCallback());
                    break;
                case REGULAR_TICKET:
                case VIP_TICKET:
                    dataMap.put(Constants.EMAIL_ADDRESS, memberEmailAddress);
                    dataMap.put(Constants.EVENT_ID, eventId);
                    dataMap.put(Constants.TICKET_TYPE, paymentType.getType());
                    dataMap.put(Constants.QUANTITY, String.valueOf(quantity));
                    dataMap.put(Constants.AMOUNT, AmountValue.substring(0, AmountValue.indexOf(".")));
                    getHttpService().initializeTicketTransaction(dataMap, buildInitializeTransactionCallback());
                    break;
                case REGULAR_TABLE:
                case VIP_TABLE:
                    dataMap.put(Constants.EMAIL_ADDRESS, memberEmailAddress);
                    dataMap.put(Constants.EVENT_ID, eventId);
                    dataMap.put(Constants.TABLE_TYPE, paymentType.getType());
                    dataMap.put(Constants.QUANTITY, String.valueOf(quantity));
                    dataMap.put(Constants.AMOUNT, AmountValue.substring(0, AmountValue.indexOf(".")));
                    getHttpService().initializeTableTransaction(dataMap, buildInitializeTransactionCallback());
                    break;
            }

        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void initializePayStack(String key) {
        if (!TextUtils.isEmpty(key)) {
            hideKeyboard();
            PaystackSdk.setPublicKey(key);
            PaystackSdk.initialize(PlayNetworkApplication.getAppInstance().getApplicationContext());
            if (NetworkUtils.isConnected(getContext())) {
                showLoadingIndicator(getString(R.string.transaction_loading_message));
                performCharge();
            } else {
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        } else {
            showErrorPopupMessage(getString(R.string.generic_error_message));
        }
    }

    public void performCharge() {
        Charge charge = new Charge();
        charge.setCard(card);
        charge.setAccessCode(transaction.getAccessCode());
        charge.setAmount((int) (amount * 100));
        charge.setEmail(memberEmailAddress);
        charge.setReference(transaction.getReferenceCode());
        charge.addParameter("Name", memberFullName);

        PaystackSdk.chargeCard(getActivity(), charge, new Paystack.TransactionCallback() {
            @Override
            public void onSuccess(Transaction transaction) {
                hideLoadingIndicator();
                completeTransaction(transaction);
            }

            @Override
            public void beforeValidate(Transaction transaction) {

            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
                hideLoadingIndicator();
                showErrorPopupMessage(error.getMessage());
            }
        });
    }

    private void completeTransaction(Transaction transaction) {
        if (!TextUtils.isEmpty(transaction.getReference()) && this.transaction.getReferenceCode().equalsIgnoreCase(transaction.getReference())) {
            if (NetworkUtils.isConnected(getContext())) {
                AppEventsLogger.newLogger(getContext()).logPurchase(BigDecimal.valueOf(amount), Currency.getInstance("NGN"));
                showLoadingIndicator(getString(R.string.confirm_transaction_loading_message));
                switch (paymentType) {
                    case PREMIUM_SUBSCRIPTION:
                        getHttpService().verifyTransaction(transaction.getReference(), user.getEmail(), buildVerifyTransactionCallback(transaction.getReference()));
                        break;
                    default:
                    case REGULAR_TABLE:
                    case VIP_TABLE:
                        getHttpService().verifyTableTransaction(transaction.getReference(), user.getEmail(), buildVerifyEventTransactionCallback(transaction.getReference()));
                        break;
                    case VIP_TICKET:
                    case REGULAR_TICKET:
                        getHttpService().verifyTicketTransaction(transaction.getReference(), user.getEmail(), buildVerifyEventTransactionCallback(transaction.getReference()));
                        break;
                }
            } else {
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        }
    }


    private Callback<PublicKeyResponse> buildPublicKeyCallback() {
        return new Callback<PublicKeyResponse>() {
            @Override
            public void onResponse(Call<PublicKeyResponse> call, Response<PublicKeyResponse> response) {
                PublicKeyResponse publicKeyResponse = response.body();
                if (ApiUtils.isSuccessResponse(response) && publicKeyResponse != null) {
                    initializePayStack(publicKeyResponse.getPk());
                } else {
                    hideLoadingIndicator();
                    showErrorPopupMessage(getString(R.string.payment_error));
                }
            }

            @Override
            public void onFailure(Call<PublicKeyResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<TransactionResponse> buildInitializeTransactionCallback() {
        return new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                TransactionResponse transactionResponse = response.body();
                if (ApiUtils.isSuccessResponse(response) && transactionResponse != null) {
                    transaction = transactionResponse.getTransaction();
                    fetchPublicKey();
                } else {
                    hideLoadingIndicator();
                    showErrorPopupMessage(getString(R.string.payment_error));
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<AuthenticationResponse> buildVerifyTransactionCallback(final String reference) {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                hideLoadingIndicator();
                AuthenticationResponse authenticationResponse = response.body();
                if (ApiUtils.isSuccessResponse(response) && authenticationResponse != null && authenticationResponse.getUpdatedMemberProfile() != null) {
                    User updatedMemberProfile = authenticationResponse.getUpdatedMemberProfile();
                    user.setMembershipType(updatedMemberProfile.getMembershipType());
                    playNetworkDatabase.userDao().updateUser(user);
                    if (isVisible()) {
                        ToastUtil.showToast(getString(R.string.successful_payment_message), ToastUtil.SUCCESS);
                    }
                    if (listener != null) {
                        listener.onPaymentCompleted(reference, user);
                    }

                    dismissAllowingStateLoss();
                } else {
                    showErrorPopupMessage(getString(R.string.confirm_payment_error_message));
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<EventResponse> buildVerifyEventTransactionCallback(final String reference) {
        return new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                EventResponse eventResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (eventResponse != null && eventResponse.getEvent() != null) {
                        if (isVisible()) {
                            ToastUtil.showToast(getString(R.string.event_successful_payment_message), ToastUtil.SUCCESS);
                        }
                        if (listener != null) {
                            listener.onEventPaymentCompleted(reference, eventResponse.getEvent());
                        }

                        dismissAllowingStateLoss();
                    } else {
                        showErrorPopupMessage(getString(R.string.fetch_event_error_message));
                    }
                }
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        playNetworkDatabase = null;
        PlayNetworkDatabase.destroyDatabaseInstance();
    }
}
