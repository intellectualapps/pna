package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.enums.PaymentType;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.data.models.EventConfirmation;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.dialogs.PaymentCheckoutDialog;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.CurrencyUtils;


public class PaymentConfirmationFragment extends BaseFragment implements View.OnClickListener, PNAInterfaces.PaymentCompletionListener {
    private User user;
    private Button payButton;
    private TextView amountView;
    private Double amount;
    private String memberFullName, memberEmailAddress;
    private String confirmationText;
    private PaymentType paymentType;
    private EventConfirmation eventConfirmation;
    private TextView memberFullNameView, memberEmailAddressView, confirmationLabelView;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new PaymentConfirmationFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public PaymentConfirmationFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
            amount = getArguments().getDouble(Constants.AMOUNT, 0.00);
            memberFullName = getArguments().getString(Constants.FULL_NAME, "");
            memberEmailAddress = getArguments().getString(Constants.EMAIL_ADDRESS, "");
            confirmationText = getArguments().getString(Constants.CONFIRMATION_CONTENT, "");
            if (getArguments().containsKey(Constants.PAYMENT_TYPE)) {
                paymentType = PaymentType.fromString(getArguments().getString(Constants.PAYMENT_TYPE));
            }

            if (getArguments().containsKey(Constants.EVENT_CONFIRMATION)) {
                eventConfirmation = getArguments().getParcelable(Constants.EVENT_CONFIRMATION);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_confirmation, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.payment_confirmation_label));
        payButton = view.findViewById(R.id.pay_button);
        amountView = view.findViewById(R.id.amount);
        memberEmailAddressView = view.findViewById(R.id.member_email_address);
        memberFullNameView = view.findViewById(R.id.member_full_name);
        confirmationLabelView = view.findViewById(R.id.confirmation_label);

        String formattedAmount = CurrencyUtils.formatToCurrencyWithSymbol(amount, Constants.CURRENCY_SYMBOL);
        amountView.setText(formattedAmount);
        payButton.setOnClickListener(this);

        loadUserData();
    }

    private void loadUserData() {
        if (!TextUtils.isEmpty(memberFullName)) {
            memberFullNameView.setText(memberFullName);
        }

        if (!TextUtils.isEmpty(memberEmailAddress)) {
            memberEmailAddressView.setText(memberEmailAddress);
        }

        if (!TextUtils.isEmpty(confirmationText)) {
            confirmationLabelView.setVisibility(View.VISIBLE);
            confirmationLabelView.setText(confirmationText);
        } else {
            confirmationLabelView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pay_button:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.USER, user);
                bundle.putDouble(Constants.AMOUNT, amount);
                bundle.putString(Constants.FULL_NAME, memberFullName);
                if (paymentType != null) {
                    bundle.putString(Constants.PAYMENT_TYPE, paymentType.name());
                }
                bundle.putString(Constants.EMAIL_ADDRESS, memberEmailAddress);
                if (eventConfirmation != null) {
                    bundle.putString(Constants.EVENT_ID, eventConfirmation.getEventId());
                    bundle.putInt(Constants.QUANTITY, eventConfirmation.getQuantity());
                }
                PaymentCheckoutDialog paymentCheckoutDialog = new PaymentCheckoutDialog();
                paymentCheckoutDialog.setPaymentCompletionListener(this);
                paymentCheckoutDialog.setArguments(bundle);

                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).launchFragmentDialog(paymentCheckoutDialog);
                }
                break;
        }
    }

    @Override
    public void onPaymentCompleted(String reference, User user) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.USER, user);
        if (!TextUtils.isEmpty(reference)) {
            replaceFragmentWithoutBackStack(PaymentStatusFragment.newInstance(bundle));
        }
    }

    @Override
    public void onEventPaymentCompleted(String reference, Event event) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.USER, user);
        bundle.putParcelable(Constants.EVENT, event);
        if (!TextUtils.isEmpty(reference)) {
            replaceFragmentWithoutBackStack(PaymentStatusFragment.newInstance(bundle));
        }
    }
}
