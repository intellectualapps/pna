package com.playnetwork.pna.ui.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.PNADailyResponse;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.NetworkUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PNADailyDialog extends BaseDialogFragment {
    private WebView webView;
    private Context context;
    private String cachedPNADaily;
    private PlayNetworkDatabase playNetworkDatabase;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static PNADailyDialog create() {
        return new PNADailyDialog();
    }

    public static PNADailyDialog getInstance(Bundle args) {
        PNADailyDialog frag = new PNADailyDialog();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        loadCachedPNADailyContent();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View customView;
        try {
            customView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_web_view, null);
        } catch (InflateException e) {
            Log.e("PNADailyDialog", e.getMessage());
            return new AlertDialog.Builder(context)
                    .setTitle(R.string.pna_daily_dialog_title)
                    .setMessage(R.string.pna_daily_dialog_webview_error)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dismiss();
                        }
                    })
                    .show();
        }
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.pna_daily_dialog_title)
                .setView(customView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .show();

        webView = customView.findViewById(R.id.web_view);
        swipeRefreshLayout = customView.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(context, R.color.green),
                ContextCompat.getColor(context, R.color.blue),
                ContextCompat.getColor(context, R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(context)) {
                        fetchPNADaily(false);
                    } else {
                        if (getActivity() != null) {
                            ((BaseActivity) getActivity()).endSwipeRefreshLayout(swipeRefreshLayout);
                        }
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            webView.loadData(cachedPNADaily, "text/html", "UTF-8");
            if (NetworkUtils.isConnected(context)) {
                fetchPNADaily(true);
            } else {
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        } catch (Throwable e) {
            webView.loadData(!TextUtils.isEmpty(cachedPNADaily) ? cachedPNADaily : getString(R.string.unable_to_load_content_error) + e.getLocalizedMessage() + "</p>", "text/html", "UTF-8");
        }
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setAttributes(layoutParams);
        dialog.getWindow().setWindowAnimations(R.style.dialog_animation_fade);
        return dialog;
    }

    private void fetchPNADaily(boolean showLoader) {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).animateSwipeRefreshLayout(swipeRefreshLayout);
        }
        getHttpService().fetchPNADaily(buildFetchPNADailyCallback());
    }

    private void loadCachedPNADailyContent() {
        Device device = playNetworkDatabase.deviceDao().getDevice();
        cachedPNADaily = device != null ? device.getCachedPNADaily() : "";
    }

    private Callback<PNADailyResponse> buildFetchPNADailyCallback() {
        return new Callback<PNADailyResponse>() {
            @Override
            public void onResponse(Call<PNADailyResponse> call, Response<PNADailyResponse> response) {
                PNADailyResponse pnaDailyResponse = response.body();
                if (ApiUtils.isSuccessResponse(response) && pnaDailyResponse != null) {
                    String PNADailyContent = stripHtml(pnaDailyResponse.getContent());
                    Device device = playNetworkDatabase.deviceDao().getDevice();
                    device.setCachedPNADaily(PNADailyContent);
                    playNetworkDatabase.deviceDao().insertDevice(device);

                    if (getActivity() != null) {
                        ((BaseActivity) getActivity()).endSwipeRefreshLayout(swipeRefreshLayout);
                        webView.loadData(PNADailyContent, "text/html", "US-ASCII");
                    }
                }
            }

            @Override
            public void onFailure(Call<PNADailyResponse> call, Throwable t) {
                Log.e("PNADailyDialog", t.getMessage());
                if (getActivity() != null) {
                    ((BaseActivity) getActivity()).endSwipeRefreshLayout(swipeRefreshLayout);
                }
            }
        };
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    @Override
    public void onDestroy() {
        PlayNetworkDatabase.destroyDatabaseInstance();
        super.onDestroy();
    }
}
