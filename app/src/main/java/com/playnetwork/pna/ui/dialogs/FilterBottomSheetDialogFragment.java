package com.playnetwork.pna.ui.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.FilterRecord;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.State;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.YellowPagesFilterAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class FilterBottomSheetDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, PNAInterfaces.FilterSelectionListener {
    private TextView industryFilterSelector, stateFilterSelector;
    private Button applyButton, clearSelectionButton;
    private View industryContainer, statesContainer;
    private PlayNetworkDatabase playNetworkDatabase;
    private List<State> stateList;
    private List<Industry> industryList;
    private List<Country> countryList;
    private List<FilterRecord> filterRecordList, stateFilterRecordList, industryFilterRecordList;
    private YellowPagesFilterAdapter yellowPagesFilterAdapter;
    private List<Integer> selectedStates, selectedIndustries;
    private PNAInterfaces.FilterSelectionListener filterSelectionListener;

    public FilterBottomSheetDialogFragment() {

    }

    public static FilterBottomSheetDialogFragment newInstance() {
        FilterBottomSheetDialogFragment fragment = new FilterBottomSheetDialogFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.STATES)) {
                stateList = (List<State>) getArguments().getSerializable(Constants.STATES);
            }
            if (getArguments().containsKey(Constants.STATE_IDS)) {
                selectedStates = (List<Integer>) getArguments().getSerializable(Constants.STATE_IDS);
            }
            if (getArguments().containsKey(Constants.INDUSTRY_IDS)) {
                selectedIndustries = (List<Integer>) getArguments().getSerializable(Constants.INDUSTRY_IDS);
            }
        }
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        industryList = playNetworkDatabase.industryDao().getAllIndustries();
        countryList = playNetworkDatabase.countryDao().getAllCountries();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_filter_bottom_sheet, container, false);
        init(rootView, savedInstanceState);
        return rootView;
    }

    public PNAInterfaces.FilterSelectionListener getFilterSelectionListener() {
        return filterSelectionListener;
    }

    public void setFilterSelectionListener(PNAInterfaces.FilterSelectionListener filterSelectionListener) {
        this.filterSelectionListener = filterSelectionListener;
    }

    private void init(View view, Bundle savedInstanceState) {
        stateFilterSelector = view.findViewById(R.id.state_filter_selector);
        industryFilterSelector = view.findViewById(R.id.industry_filter_selector);
        industryContainer = view.findViewById(R.id.industry_container);
        statesContainer = view.findViewById(R.id.states_container);
        applyButton = view.findViewById(R.id.apply_button);
        clearSelectionButton = view.findViewById(R.id.clear_selection_button);
        yellowPagesFilterAdapter = new YellowPagesFilterAdapter(getContext(), this, -1);

        industryContainer.setOnClickListener(this);
        statesContainer.setOnClickListener(this);
        stateFilterSelector.setOnClickListener(this);
        industryFilterSelector.setOnClickListener(this);
        applyButton.setOnClickListener(this);
        clearSelectionButton.setOnClickListener(this);

        stateFilterSelector.setText(displaySelectedFilters(selectedStates, buildFilterRecordsListFromStates(stateList), YellowPagesFilterAdapter.STATE_MODE));
        industryFilterSelector.setText(displaySelectedFilters(selectedIndustries, buildFilterRecordsListFromIndustries(industryList), YellowPagesFilterAdapter.INDUSTRY_MODE));
    }

    private void showSelectionDialog(final Context context, YellowPagesFilterAdapter yellowPagesFilterAdapter, List<FilterRecord> filterRecords, final int mode, final PNAInterfaces.FilterSelectionListener filterSelectionListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.dialog_filter_list, null);
        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.setTitle(mode == YellowPagesFilterAdapter.STATE_MODE ? getString(R.string.select_state_label) : getString(R.string.select_industry_label));
        final CustomRecyclerView customRecyclerView = dialogView.findViewById(R.id.filter_recyclerview);
        customRecyclerView.setAdapter(yellowPagesFilterAdapter);
        customRecyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        customRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        yellowPagesFilterAdapter.setItems(filterRecords);
        yellowPagesFilterAdapter.clearSelection();
        switch (mode) {
            case YellowPagesFilterAdapter.STATE_MODE:
                if (ListUtils.isNotEmpty(selectedStates)) {
                    for (Integer id : selectedStates) {
                        if (filterRecords.contains(new FilterRecord(id, ""))) {
                            toggleSelection(filterRecords.indexOf(new FilterRecord(id, "")), id);
                        }
                    }
                }
                break;
            case YellowPagesFilterAdapter.INDUSTRY_MODE:
                if (ListUtils.isNotEmpty(selectedIndustries)) {
                    for (Integer id : selectedIndustries) {
                        if (filterRecords.contains(new FilterRecord(id, ""))) {
                            toggleSelection(filterRecords.indexOf(new FilterRecord(id, "")), id);
                        }
                    }
                }
                break;
        }
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.save_selection), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (filterSelectionListener != null) {
                    filterSelectionListener.onSelectionComplete(mode);
                }
            }
        });
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.industry_container:
            case R.id.industry_filter_selector:
                yellowPagesFilterAdapter.clearSelection();
                filterRecordList = buildFilterRecordsListFromIndustries(industryList);
                showSelectionDialog(requireContext(), yellowPagesFilterAdapter, filterRecordList, YellowPagesFilterAdapter.INDUSTRY_MODE, FilterBottomSheetDialogFragment.this);
                break;
            case R.id.states_container:
            case R.id.state_filter_selector:
                yellowPagesFilterAdapter.clearSelection();
                filterRecordList = buildFilterRecordsListFromStates(stateList);
                showSelectionDialog(requireContext(), yellowPagesFilterAdapter, filterRecordList, YellowPagesFilterAdapter.STATE_MODE, FilterBottomSheetDialogFragment.this);
                break;
            case R.id.apply_button:
                if (filterSelectionListener != null) {
                    filterSelectionListener.onSelectionComplete(selectedStates, selectedIndustries);
                }
                dismissAllowingStateLoss();
                break;
            case R.id.clear_selection_button:
                yellowPagesFilterAdapter.clearSelection();
                if (selectedIndustries != null) {
                    selectedIndustries.clear();
                }
                if (selectedStates != null) {
                    selectedStates.clear();
                }
                stateFilterSelector.setText(displaySelectedFilters(selectedStates, filterRecordList, YellowPagesFilterAdapter.STATE_MODE));
                industryFilterSelector.setText(displaySelectedFilters(selectedIndustries, filterRecordList, YellowPagesFilterAdapter.INDUSTRY_MODE));
                break;
        }
    }

    private List<FilterRecord> buildFilterRecordsListFromStates(List<State> states) {
        if (ListUtils.isEmpty(stateFilterRecordList)) {
            stateFilterRecordList = new ArrayList<FilterRecord>();
            if (ListUtils.isNotEmpty(states)) {
                for (State state : states) {
                    stateFilterRecordList.add(new FilterRecord(state.getId(), state.getName()));
                }
            }
        }
        return stateFilterRecordList;
    }

    private List<FilterRecord> buildFilterRecordsListFromIndustries(List<Industry> industries) {
        if (ListUtils.isEmpty(industryFilterRecordList)) {
            industryFilterRecordList = new ArrayList<FilterRecord>();
            if (ListUtils.isNotEmpty(industries)) {
                for (Industry industry : industries) {
                    industryFilterRecordList.add(new FilterRecord(industry.getId(), industry.getIndustry()));
                }
            }
        }
        return industryFilterRecordList;
    }

    private List<FilterRecord> buildFilterRecordsFromSelectedItems
            (List<State> states, List<Integer> stateIds, List<Industry> industries, List<Integer> industryIds,
             int mode) {
        List<FilterRecord> filterRecords = new ArrayList<FilterRecord>();
        switch (mode) {
            case YellowPagesFilterAdapter.STATE_MODE:
                if (ListUtils.isNotEmpty(stateIds)) {
                    for (Integer stateId : stateIds) {
                        filterRecords.add(new FilterRecord(stateId, states.get(states.indexOf(new State(stateId))).getName()));
                    }
                }
                break;
            case YellowPagesFilterAdapter.INDUSTRY_MODE:
                if (ListUtils.isNotEmpty(industryIds)) {
                    for (Integer industryId : industryIds) {
                        filterRecords.add(new FilterRecord(industryId, industries.get(industries.indexOf(new Industry(industryId))).getIndustry()));
                    }
                }
                break;
        }
        return filterRecords;
    }

    private void toggleSelection(int position, int memberId) {
        yellowPagesFilterAdapter.toggleSelection(position, memberId);
    }

    private boolean checkIfMaximumLimitReached(int mode) {
        switch (mode) {
            case YellowPagesFilterAdapter.STATE_MODE:
                return yellowPagesFilterAdapter.getSelectedItemCount() <= 6;
            case YellowPagesFilterAdapter.INDUSTRY_MODE:
                return yellowPagesFilterAdapter.getSelectedItemCount() <= 6;
        }

        return false;
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1) {
            FilterRecord filterRecord = filterRecordList.get(position);
            switch (v.getId()) {
                default:
                    toggleSelection(position, filterRecord.getId());
                    break;
            }
        }
    }

    @Override
    public void onSelectionComplete(int mode) {
        switch (mode) {
            case YellowPagesFilterAdapter.STATE_MODE:
                selectedStates = yellowPagesFilterAdapter.getSelectedMemberIds();
                stateFilterSelector.setText(displaySelectedFilters(selectedStates, filterRecordList, mode));
                break;
            case YellowPagesFilterAdapter.INDUSTRY_MODE:
                selectedIndustries = yellowPagesFilterAdapter.getSelectedMemberIds();
                industryFilterSelector.setText(displaySelectedFilters(selectedIndustries, filterRecordList, mode));
                break;
        }
    }

    @Override
    public void onSelectionComplete(List<Integer> stateIds, List<Integer> industryIds) {

    }

    private String displaySelectedFilters
            (List<Integer> selectedItems, List<FilterRecord> filterRecordList, int mode) {
        List<String> values = new ArrayList<String>();
        if (ListUtils.isNotEmpty(selectedItems) && ListUtils.isNotEmpty(filterRecordList)) {
            for (Integer item : selectedItems) {
                if (filterRecordList.contains(new FilterRecord(item, ""))) {
                    int index = filterRecordList.indexOf(new FilterRecord(item, ""));
                    values.add(filterRecordList.get(index).getValue());
                }
            }
        }
        return ListUtils.isNotEmpty(values) ? android.text.TextUtils.join(", ", values) : mode == YellowPagesFilterAdapter.STATE_MODE ? getString(R.string.select_state_label) : getString(R.string.select_industry_label);
    }
}
