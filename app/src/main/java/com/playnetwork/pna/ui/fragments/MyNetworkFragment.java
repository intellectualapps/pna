package com.playnetwork.pna.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.NetworkResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.ProfileActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.NetworkAdapter;
import com.playnetwork.pna.ui.customviews.AutoFitGridRecyclerView;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyNetworkFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, PNAInterfaces.PaginationAdapterCallback, Toolbar.OnMenuItemClickListener {
    private User user;
    private NetworkAdapter networkAdapter;
    private AutoFitGridRecyclerView memberNetworkRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private View mEmptyView;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private boolean hasPageInitialized = false;
    private int start = 0, pageSize = 10;
    private int currentPage = start;

    private List<Network> memberNetworkList;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new MyNetworkFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public MyNetworkFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        memberNetworkList = playNetworkDatabase.networkDao().getMemberNetwork();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_network, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.my_network_menu, menu);
    }

    private void initializeToolbar(final Activity activity) {
        if (activity != null) {
            Toolbar toolbar = ((BaseActivity) activity).initializeToolbar(getString(R.string.my_network_toolbar_label));
            toolbar.setTitle(getString(R.string.my_network_toolbar_label));
            ((BaseActivity) activity).setSupportActionBar(toolbar);
            toolbar.setOnMenuItemClickListener(this);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) activity).navigateUp();
                }
            });
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        initializeToolbar(getActivity());
        memberNetworkRecyclerView = view.findViewById(R.id.my_network_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);

        mEmptyViewLabel.setText(R.string.my_network_empty_label);

        memberNetworkRecyclerView.setEmptyView(mEmptyView);
        networkAdapter = new NetworkAdapter(getContext(), this, this);
        memberNetworkRecyclerView.setAdapter(networkAdapter);

        memberNetworkRecyclerView.addOnScrollListener(new PaginationScrollListener(memberNetworkRecyclerView.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (memberNetworkList != null && memberNetworkList.size() >= pageSize) {
                            fetchMemberNetwork(start, pageSize, true);
                        }
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        fetchMemberNetwork(0, pageSize, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (ListUtils.isNotEmpty(memberNetworkList)) {
            loadAdapter(memberNetworkList);
            if (!hasPageInitialized) {
                hasPageInitialized = true;
                if (NetworkUtils.isConnected(getContext())) {
                    fetchMemberNetwork(0, pageSize, false);
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }
        } else {
            if (NetworkUtils.isConnected(getContext())) {
                fetchMemberNetwork(0, pageSize, false);
            } else {
                networkAdapter.clearItems();
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        }
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.request_processing_label);
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1) {
            Network network = memberNetworkList.get(position);
            switch (view.getId()) {
                default:
                    Intent intent = new Intent(getContext(), ProfileActivity.class);
                    intent.putExtra(Constants.NETWORK, network);
                    intent.putExtra(Constants.USER, user);
                    intent.putExtra(Constants.POSITION, position);
                    if (network.getHasAccepted()) {
                        intent.putExtra(Constants.VIEW_TYPE, Constants.MEMBER_PROFILE_VIEW_TAG);
                    } else {
                        intent.putExtra(Constants.VIEW_TYPE, Constants.PENDING_PROFILE_VIEW_TAG);
                    }
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            start = currentPage * pageSize;
            fetchMemberNetwork(start, pageSize, true);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onDestroy() {
        PlayNetworkDatabase.destroyDatabaseInstance();
        super.onDestroy();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Bundle bundle = new Bundle();
        switch (item.getItemId()) {
            case R.id.action_view_network_requests:
                bundle.putParcelable(Constants.USER, user);
                switchFragment(NetworkRequestsFragment.newInstance(bundle));
                return true;
            case R.id.action_add_network_suggestion:
                bundle.putParcelable(Constants.USER, user);
                switchFragment(NetworkSearchFragment.newInstance(bundle));
                return true;
            default:
                return false;
        }
    }

    private void fetchMemberNetwork(int start, int pageSize, boolean inLoadMoreMode) {
        if (!inLoadMoreMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }

        getHttpService().fetchMemberNetwork(String.valueOf(user.getId()), buildParameterMap(start, pageSize), buildFetchNetworkSuggestionsCallback(inLoadMoreMode));
    }

    private Map<String, String> buildParameterMap(int start, int pageSize) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
        return requestMap;
    }

    private void updateNetworkList(List<Network> networkListParam) {
        if (memberNetworkList == null) {
            memberNetworkList = new ArrayList<Network>();
        }
        if (networkListParam != null && networkListParam.size() > 0) {
            memberNetworkList.addAll(networkListParam);
        }
    }

    private void loadAdapter(List<Network> networkList) {
        networkAdapter.addAll(networkList);
    }

    private void updateNetworkListInCache(List<Network> networkList) {
        playNetworkDatabase.networkDao().deleteCachedNetworks();
        for (Network network : networkList) {
            network.setMode(Constants.NETWORK_MODE);
        }
        playNetworkDatabase.networkDao().insertAll(ListUtils.convertNetworkListToArray(networkList));
    }

    private Callback<NetworkResponse> buildFetchNetworkSuggestionsCallback(final boolean inLoadMoreMode) {
        return new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                NetworkResponse networkResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                isLoading = false;
                if (inLoadMoreMode && isVisible()) {
                    networkAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    List<Network> networkListParam = networkResponse.getNetwork();

                    if (networkListParam == null || networkListParam.size() == 0 || networkListParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (memberNetworkList != null) {
                            memberNetworkList.clear();
                        }
                        networkAdapter.clearItems();
                        updateNetworkListInCache(networkListParam);
                    }

                    updateNetworkList(networkListParam);
                    if (isAdded() && isVisible()) {
                        loadAdapter(networkListParam);
                    }

                    if (!hasListEnded) {
                        networkAdapter.addLoadingFooter();
                    }
                } else {
                    if (isVisible()) {
                        loadAdapter(memberNetworkList);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NetworkResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    networkAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }
}
