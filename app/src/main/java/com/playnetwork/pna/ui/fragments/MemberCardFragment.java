package com.playnetwork.pna.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.OfferResponse;
import com.playnetwork.pna.data.models.CustomField;
import com.playnetwork.pna.data.models.Offer;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.QRCodeScannerActivity;
import com.playnetwork.pna.ui.adapters.CustomFieldListAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.Transformers;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberCardFragment extends BaseFragment implements View.OnClickListener {
    private Button closeButton;
    private ImageView userBanner;
    private Offer offer;
    private TextView memberIdView, memberNameView, membershipTypeView;
    private CustomRecyclerView offerFieldsRecyclerView;
    private CustomFieldListAdapter customFieldListAdapter;
    private List<CustomField> customFieldList;
    private User user;
    private boolean shouldDisplayFetchDataError = false;

    public static Fragment newInstance(Bundle args) {
        MemberCardFragment frag = new MemberCardFragment();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(Constants.OFFER)) {
            offer = getArguments().getParcelable(Constants.OFFER);
        }

        if (getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_card, container, false);
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.member_card_toolbar_label));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        closeButton = view.findViewById(R.id.close_button);
        userBanner = view.findViewById(R.id.user_banner);
        memberIdView = view.findViewById(R.id.member_id);
        memberNameView = view.findViewById(R.id.member_name);
        membershipTypeView = view.findViewById(R.id.membership_type);
        offerFieldsRecyclerView = view.findViewById(R.id.member_card_details_recyclerview);

        customFieldListAdapter = new CustomFieldListAdapter(getContext(), null, true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        offerFieldsRecyclerView.setLayoutManager(linearLayoutManager);
        offerFieldsRecyclerView.setAdapter(customFieldListAdapter);

        closeButton.setOnClickListener(this);
        loadData(offer, user);
    }

    private void loadData(Offer offer, User user) {
        if (user != null) {
            if (!TextUtils.isEmpty(user.getPic())) {
                userBanner.setVisibility(View.VISIBLE);
                String profilePictureUrl = user.getPic().contains("http") ? user.getPic() : user.getUrl().concat("/").concat(user.getPic()).replace("///", "//");
                Picasso.with(getContext()).load(profilePictureUrl).placeholder(R.color.colorGray).into(userBanner);
            } else {
                userBanner.setVisibility(View.GONE);
            }
            memberIdView.setText(String.valueOf(user.getId()));
            memberNameView.setText(user.getFlname());
            membershipTypeView.setText(String.format("%s %s", Transformers.capitalizeTag(user.getMembershipType()), getString(R.string.member_label)));
        }

        if (offer != null) {
            customFieldListAdapter.setItems(getFieldDetails(offer));
        }
    }

    private List<CustomField> getFieldDetails(Offer offer) {
        List<CustomField> list = new ArrayList<CustomField>();
        String ordinalDate = DateUtils.getOrdinalDate(offer.getStopsAt());
        list.add(new CustomField(Constants.BUSINESS_NAME, offer.getBusinessName()));
        list.add(new CustomField(Constants.DISCOUNT_TYPE, offer.getType()));
        list.add(new CustomField(Constants.OFFER_LEVEL, offer.getOfferLevel()));
        list.add(new CustomField(Constants.OFFER_ENDS, String.format("%s %s", ordinalDate, DateUtils.getSimpleDateFormat(offer.getStopsAt(), "MMMM, yyyy"))));
        list.add(new CustomField(Constants.OFFER_REDEEMED_AT, offer.getAddress()));

        return list;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_button:
                closeFragment();
                break;
        }
    }
}
