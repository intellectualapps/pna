package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.ui.customviews.CustomTypefaceSpan;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.FontUtils;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class EventsAdapter extends BaseRecyclerAdapter {
    private List<Event> eventList;
    private ClickListener clickListener;
    private final int EVENT = 2;
    private SpannableStringBuilder spannableStringBuilder;

    public EventsAdapter(Context context, ClickListener clickListener, PNAInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.eventList = new ArrayList<Event>();
    }

    /*
    Helpers
    */

    public void setItems(List<Event> items) {
        this.eventList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (eventList != null) {
            this.eventList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Event event) {
        if (event != null) {
            this.eventList.add(event);
            notifyDataSetChanged();
        }
    }

    public void add(Event event) {
        eventList.add(event);
        notifyItemInserted(eventList.size() - 1);
    }

    public void addAll(List<Event> eventList) {
        if (eventList != null && eventList.size() > 0) {
            for (Event event : eventList) {
                add(event);
            }
        }
        notifyDataSetChanged();
    }

    public void remove(Event event) {
        int position = eventList.indexOf(event);
        if (position > -1) {
            eventList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Event getItem(int position) {
        return eventList.get(position);
    }

    public List<Event> getEventList() {
        return eventList;
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(eventList) ? 0 : eventList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Event event = eventList.get(position);
        if (position == eventList.size() - 1 && isLoadingAdded && event != null && TextUtils.isEmpty(event.getId())) {
            return LOADING;
        }

        return EVENT;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        int count = 0;
        for (int i = 0; i < eventList.size(); i++) {
            if (eventList.get(i) == null || TextUtils.isEmpty(eventList.get(i).getId())) {
                eventList.remove(i);
                count++;
                notifyItemRemoved(i);
            }
        }
        if (count == 0) {
            add(new Event());
        }
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = eventList.size() - 1;
        if (position > -1) {
            Event event = getItem(position);

            if (event != null) {
                if (TextUtils.isEmpty(event.getId())) {
                    eventList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    public void initializeEventCategoryViewHolder(RecyclerView.ViewHolder holder, int position) {
        EventCategoryViewHolder eventCategoryViewHolder = (EventCategoryViewHolder) holder;
        Event event = eventList.get(position);
        if (event != null && !TextUtils.isEmpty(event.getDate())) {
            eventCategoryViewHolder.eventCategoryView.setText(event.getDate());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View view;

        switch (viewType) {
            case EVENT:
                view = mInflater.inflate(R.layout.event_item_modified_layout, parent, false);
                viewHolder = new EventsAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            case EVENT:
                final EventsAdapter.ViewHolder viewHolder = (EventsAdapter.ViewHolder) holder;
                Event event = eventList.get(position);
                if (event != null && !TextUtils.isEmpty(event.getId())) {
                    viewHolder.itemView.setVisibility(View.VISIBLE);
                    viewHolder.eventName.setText(event.getTitle());
                    viewHolder.daysTillEvent.setText(event.getParsedDate());
                    viewHolder.eventTime.setText(event.getTime());
                    viewHolder.eventDate.setText(getFormattedEventDate(event.getDate()));
                    viewHolder.eventName.setCompoundDrawablesWithIntrinsicBounds(null, null, event.isPaidEvent() ? ContextCompat.getDrawable(context, R.drawable.ic_lock_black_18dp) : null, null);
                    if (!TextUtils.isEmpty(event.getPicUrl())) {
                        Picasso.with(context).load(event.getPicUrl()).placeholder(R.color.colorGray).into(viewHolder.eventBanner);
                        viewHolder.eventBanner.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    } else {
                        Picasso.with(context).load(R.drawable.play_africa_logo).placeholder(R.color.colorGray).into(viewHolder.eventBanner);
                        viewHolder.eventBanner.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    }
                } else {
                    viewHolder.itemView.setVisibility(View.GONE);
                }
                break;
        }
    }

    private SpannableStringBuilder getFormattedEventDate(String eventDateValue) {
        String eventDate = DateUtils.getSimpleDateFormat(eventDateValue, "dd");
        String eventMonth = DateUtils.getSimpleDateFormat(eventDateValue, "MMM.");
        String separator = "\n";

        if (TextUtils.isEmpty(eventDate) || TextUtils.isEmpty(eventMonth)) {
            return null;
        }

        int eventDateSize = context.getResources().getDimensionPixelSize(R.dimen.label_text_size_xxlarge);
        int eventMonthSize = context.getResources().getDimensionPixelSize(R.dimen.label_text_size_xlarge);

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        CustomTypefaceSpan boldTypefaceSpan, mediumTypefaceSpan, regularTypefaceSpan;
        boldTypefaceSpan = new CustomTypefaceSpan(FontUtils.selectTypeface(context, FontUtils.STYLE_BOLD));
        mediumTypefaceSpan = new CustomTypefaceSpan(FontUtils.selectTypeface(context, FontUtils.STYLE_MEDIUM));
        regularTypefaceSpan = new CustomTypefaceSpan(FontUtils.selectTypeface(context, FontUtils.STYLE_REGULAR));
        spannableStringBuilder.append(eventDate).append(separator).append(eventMonth);

        int length = eventDate.length() + separator.length() + eventMonth.length();
        spannableStringBuilder.setSpan(boldTypefaceSpan, 0, eventDate.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(eventDateSize), 0, eventDate.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(regularTypefaceSpan, length - eventMonth.length(), length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(eventMonthSize), length - eventMonth.length(), length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableStringBuilder;
    }

    public class EventCategoryViewHolder extends RecyclerView.ViewHolder {
        public TextView eventCategoryView;

        public EventCategoryViewHolder(View itemView) {
            super(itemView);
            eventCategoryView = itemView.findViewById(R.id.event_category);
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView eventName, daysTillEvent, eventTime, eventDate;
        ImageView eventBanner;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            eventName = itemView.findViewById(R.id.event_name);
            daysTillEvent = itemView.findViewById(R.id.days_till_event);
            eventTime = itemView.findViewById(R.id.event_time);
            eventDate = itemView.findViewById(R.id.event_date);
            eventBanner = itemView.findViewById(R.id.event_banner);
            eventDate.setOnClickListener(this);
            itemView.setOnClickListener(this);

            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (clickListener != null) {
                clickListener.onClick(view, position, false);
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(eventList.size() - 1);
        if (errorMsg != null) {
            this.errorMsg = errorMsg;
        }
    }
}