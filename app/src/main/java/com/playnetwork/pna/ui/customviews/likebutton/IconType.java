package com.playnetwork.pna.ui.customviews.likebutton;

public enum IconType {
    Heart,
    Thumb,
    Star
}
