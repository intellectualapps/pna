package com.playnetwork.pna.ui.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.playnetwork.pna.R;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.dialogs.MediaPickerDialog;
import com.playnetwork.pna.ui.fragments.CreatePostFragment;
import com.playnetwork.pna.ui.fragments.FeedDetailsFragment;
import com.playnetwork.pna.ui.fragments.FeedListFragment;
import com.playnetwork.pna.ui.fragments.MediaPickerBaseFragment;
import com.playnetwork.pna.utils.Constants;

public class FeedActivity extends BaseActivity implements MediaPickerDialog.MediaPickedListener {
    private Bundle fragmentBundle;
    private String viewType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        toolbar = findViewById(R.id.toolbar);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.FEED_DETAILS_VIEW_TAG:
                        frag = FeedDetailsFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.FEED_LIST_VIEW_TAG:
                        frag = FeedListFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.CREATE_POST_VIEW_TAG:
                        frag = CreatePostFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = FeedDetailsFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = FeedDetailsFragment.newInstance(null);
            }

            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }

    @Override
    public void onMediaItemPicked(Uri fileUri, String filePath, int mediaType, String fragmentTag) {
        Fragment frag = findActiveFragmentByTag(fragmentTag, getSupportFragmentManager());
        if (frag != null && frag instanceof MediaPickerBaseFragment) {
            ((MediaPickerBaseFragment) frag).onMediaPickerSuccess(fileUri, filePath, mediaType, fragmentTag);
        }
    }

    @Override
    public void onCancel(String message, String fragmentTag) {
        Fragment frag = findActiveFragmentByTag(fragmentTag, getSupportFragmentManager());
        if (frag != null && frag instanceof MediaPickerBaseFragment) {
            ((MediaPickerBaseFragment) frag).onMediaPickerError(message, fragmentTag);
        }
    }
}
