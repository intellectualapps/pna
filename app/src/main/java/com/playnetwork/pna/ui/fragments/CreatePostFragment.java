package com.playnetwork.pna.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.UploadRequest;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.preprocess.BitmapEncoder;
import com.cloudinary.android.preprocess.DimensionsValidator;
import com.cloudinary.android.preprocess.ImagePreprocessChain;
import com.cloudinary.android.preprocess.Limit;
import com.playnetwork.pna.BuildConfig;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.GenericFeedResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Feed;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.dialogs.MediaPickerDialog;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.FileUtils;
import com.playnetwork.pna.utils.ImageOptimizerTask;
import com.playnetwork.pna.utils.ImageUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.Validator;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePostFragment extends MediaPickerBaseFragment implements View.OnClickListener, Toolbar.OnMenuItemClickListener, TextWatcher, PNAInterfaces.ImageOptimizationListener {
    private ImageView imagePreview, removeImageButton;
    private View attachImageView;
    private View previewContainer;
    private EditText postContentInput;
    private User user;
    private Uri mFileUri = null;
    private String uploadedImageUrl = null, postContentText;
    private boolean hasMediaPermissionBeenGranted = false;
    private MenuItem publishPostMenuButton;

    public CreatePostFragment() {

    }

    public static CreatePostFragment newInstance(Bundle args) {
        CreatePostFragment frag = new CreatePostFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_post, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.create_post_menu, menu);
        publishPostMenuButton = menu.findItem(R.id.action_publish_post);
        publishPostMenuButton.setEnabled(false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void initializeToolbar(final Activity activity) {
        if (activity != null) {
            Toolbar toolbar = ((BaseActivity) activity).initializeToolbar(getString(R.string.create_post_toolbar_label));
            ((BaseActivity) activity).setSupportActionBar(toolbar);
            toolbar.setOnMenuItemClickListener(this);
            toolbar.setNavigationIcon(R.drawable.ic_action_close);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) activity).navigateUp();
                }
            });
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        initializeToolbar(getActivity());
        attachImageView = view.findViewById(R.id.attach_image_view);
        postContentInput = view.findViewById(R.id.create_post_input);
        imagePreview = view.findViewById(R.id.image_preview);
        removeImageButton = view.findViewById(R.id.remove_image_button);
        previewContainer = view.findViewById(R.id.preview_container);

        postContentInput.addTextChangedListener(this);
        attachImageView.setOnClickListener(this);
        removeImageButton.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (hasMediaPermissionBeenGranted) {
            showPhotoUploadOptions();
            hasMediaPermissionBeenGranted = false;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.attach_image_view:
                if (checkMediaAccessPermission(getActivity())) {
                    showPhotoUploadOptions();
                }
                break;
            case R.id.remove_image_button:
                mFileUri = null;
                imagePreview.setImageDrawable(null);
                previewContainer.setVisibility(View.GONE);
                publishPostMenuButton.setTitle(getString(R.string.upload_image_label));
                if (TextUtils.isEmpty(postContentInput.getText().toString().trim())) {
                    publishPostMenuButton.setEnabled(false);
                }
                break;
        }
    }

    private void showPhotoUploadOptions() {
        DialogFragment mediaPickerDialog = MediaPickerDialog.newInstance(getString(R.string.select_image_dialog_title), MediaPickerDialog.TYPE_IMAGE, CreatePostFragment.class.getSimpleName());
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).launchFragmentDialog(mediaPickerDialog);
        }
    }

    private void savePost() {
        hideKeyboard();
        if (NetworkUtils.isConnected(getContext())) {
            showLoadingIndicator(getString(R.string.post_creation_loading_message));
            final String postContent = postContentInput.getText().toString().trim();
            if (mFileUri != null && !TextUtils.isEmpty(mFileUri.toString())) {
                ImageOptimizerTask imageOptimizerTask = new ImageOptimizerTask(postContentText, mFileUri, getContext(), this);
                imageOptimizerTask.execute();
            } else {
                submitDataToServer(postContent, null);
            }
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void submitDataToServer(String postContent, String imageUrl) {
        hideKeyboard();
        if (NetworkUtils.isConnected(getContext())) {
            Map<String, String> map = new HashMap<String, String>();
            map.put(Constants.TEXT, postContent);
            if (!TextUtils.isEmpty(imageUrl)) {
                map.put(Constants.PIC_URL, imageUrl);
            }
            getHttpService().createPost(String.valueOf(user.getId()), map, buildCreatePostCallback());
            showLoadingIndicator(getString(R.string.post_creation_loading_message));
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void reloadFeedList() {
        Intent intent = new Intent();
        intent.setAction(Constants.REFRESH_FEED_INTENT_FILTER);
        intent.putExtra(Constants.REFRESH_FLAG, true);
        if (getContext() != null) {
            getContext().sendBroadcast(intent);
        }
    }

    private void setImagePreview(Uri fileUri) {
        this.mFileUri = fileUri;
        previewContainer.setVisibility(View.VISIBLE);
        publishPostMenuButton.setEnabled(true);
        publishPostMenuButton.setTitle(getString(R.string.replace_image_label));
        ImageUtils.loadImageUri(imagePreview, getContext(), fileUri);
    }

    @Override
    public void onMediaPickerSuccess(Uri fileUri, String filePath, int mediaType, String fragmentTag) {
        try {
            if (TextUtils.isEmpty(filePath)) {
                if (!fileUri.toString().contains("content://")) {
                    setImagePreview(fileUri);
                } else {
                    Uri resolvedUri = Uri.parse("file://" + FileUtils.getRealPathFromURI(getContext(), fileUri));
                    setImagePreview(resolvedUri);
                }
            } else {
                Uri resolvedUri = Uri.parse(filePath);
                setImagePreview(resolvedUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMediaPickerError(String message, String fragmentTag) {
        if (getActivity() != null) {
            showErrorPopupMessage(message);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MEDIA_PERMISSION_REQUEST_CODE:
                hasMediaPermissionRequestBeenMade = true;
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            hasMediaPermissionBeenGranted = false;
                            checkMediaAccessPermission(getActivity());
                            return;
                        }
                        hasMediaPermissionBeenGranted = true;
                    }
                } else {
                    checkMediaAccessPermission(getActivity());
                }
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_publish_post:
                if ((mFileUri != null && !TextUtils.isEmpty(mFileUri.toString())) || Validator.validateInputNotEmpty(postContentInput)) {
                    savePost();
                }
                break;
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        if (publishPostMenuButton != null) {
            if (charSequence != null && !TextUtils.isEmpty(charSequence.toString().trim())) {
                publishPostMenuButton.setEnabled(true);
            } else {
                publishPostMenuButton.setEnabled(false);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private Callback<GenericFeedResponse<Feed>> buildCreatePostCallback() {
        return new Callback<GenericFeedResponse<Feed>>() {
            @Override
            public void onResponse(Call<GenericFeedResponse<Feed>> call, Response<GenericFeedResponse<Feed>> response) {
                GenericFeedResponse<Feed> feedResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response) && feedResponse != null && isVisible()) {
                    showSuccessPopupMessage(getString(R.string.post_saved_success_message));
                    reloadFeedList();
                    closeFragment();
                }
            }

            @Override
            public void onFailure(Call<GenericFeedResponse<Feed>> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private void uploadImageToCloudinary(final String postContentText, Uri uri, byte[] imageByteArray) {
        MediaManager mediaManager = MediaManager.get();
        UploadRequest uploadRequest = null;
        if (imageByteArray != null && imageByteArray.length > 0) {
            uploadRequest = mediaManager.upload(imageByteArray);
        } else {
            uploadRequest = mediaManager.upload(uri);
        }

        String requestId = uploadRequest.callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {

            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {

            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                uploadedImageUrl = String.valueOf(resultData.get("secure_url"));
                submitDataToServer(postContentText, uploadedImageUrl);
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                hideLoadingIndicator();
                uploadedImageUrl = "";
                showErrorPopupMessage(error.getDescription());
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {

            }
        }).unsigned(BuildConfig.CLOUDINARY_PRESET)
                .option("angle", "ignore")
                .option("resource_type", "image")
                .preprocess(ImagePreprocessChain.limitDimensionsChain(1000, 1000)
                        .addStep(new DimensionsValidator(10, 10, 1000, 1000))
                        .addStep(new Limit(1000, 1000))
                        .saveWith(new BitmapEncoder(BitmapEncoder.Format.JPEG, 80)))
                .dispatch(getContext());
    }

    @Override
    public void onImageOptimized(String postContentText, Uri mFileUri, byte[] bytes) {
        if (bytes == null || bytes.length < 1) {
            uploadImageToCloudinary(postContentText, mFileUri, null);
        } else {
            uploadImageToCloudinary(postContentText, mFileUri, bytes);

        }
    }
}
