package com.playnetwork.pna.ui.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.ApiClientCallbacks;
import com.playnetwork.pna.data.api.responses.EventResponse;
import com.playnetwork.pna.data.api.responses.SubscriptionAmountResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.CalendarEvent;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.data.models.EventGroup;
import com.playnetwork.pna.data.models.Section;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.room.daos.CalendarEventDao;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.ProfileActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.EventsAdapter;
import com.playnetwork.pna.ui.adapters.EventGroupAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.AutoRefreshUtils;
import com.playnetwork.pna.utils.CalendarEventsUpdaterTask;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.CurrencyUtils;
import com.playnetwork.pna.utils.FilterList;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventsFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, PNAInterfaces.PaginationAdapterCallback, CompoundButton.OnCheckedChangeListener, ApiClientCallbacks.SubscriptionAmountListener {
    private User user;
    private EventsAdapter eventsAdapter;
    private EventGroupAdapter eventGroupAdapter;
    private CustomRecyclerView eventsRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private View mEmptyView;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private boolean hasPageInitialized = false;
    private int start = 0, pageSize = 5;
    private int currentPage = start;
    private EventGroup currentGroup = null;
    private SwitchCompat userEventsToggleButton;
    private boolean isUserEventToggleOn = false;
    private Double subscriptionAmount = 0.0;
    private Device device;

    private List<Event> eventList = new ArrayList<Event>(), masterEventList = new ArrayList<Event>();

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new EventsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public EventsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        initializeEventList(playNetworkDatabase.eventDao().getAll());
        device = playNetworkDatabase.deviceDao().getDevice();
        registerReceiver(getContext(), refreshEventsReceiver, new IntentFilter(Constants.UPDATE_EVENTS_INTENT_FILTER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.events_toolbar_label));

        eventsRecyclerView = view.findViewById(R.id.events_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        mEmptyView = view.findViewById(R.id.empty_view);
        userEventsToggleButton = view.findViewById(R.id.user_events_switch);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mEmptyViewLabel.setText(R.string.events_empty_label);

        eventsRecyclerView.setEmptyView(mEmptyView);
        eventsAdapter = new EventsAdapter(getContext(), this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        eventsRecyclerView.setLayoutManager(linearLayoutManager);
        eventGroupAdapter = new EventGroupAdapter(getContext(), R.layout.layout_event_category_separator, R.id.event_category, eventsAdapter);
        eventsRecyclerView.setAdapter(eventGroupAdapter);
        userEventsToggleButton.setOnCheckedChangeListener(this);

        eventsRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (eventList != null && eventList.size() >= pageSize) {
                            fetchEvents(start, pageSize, true);
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        isUserEventToggleOn = false;
                        userEventsToggleButton.setChecked(false);
                        fetchEvents(0, pageSize, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (eventList != null && eventList.size() > 0) {
            loadAdapter(eventList, false);
            if (!hasPageInitialized) {
                hasPageInitialized = true;
                if (NetworkUtils.isConnected(getContext())) {
                    fetchEvents(0, pageSize, false);
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }
        } else {
            if (NetworkUtils.isConnected(getContext())) {
                fetchEvents(0, pageSize, false);
            } else {
                eventsAdapter.clearItems();
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        }

        if (device == null || device.getSubscriptionAmount() == null || device.getSubscriptionAmount() < 1) {
            AutoRefreshUtils.fetchSubscriptionAmount(getHttpService(), this);
        } else {
            subscriptionAmount = device.getSubscriptionAmount();
        }
    }

    private void initializeEventList(List<Event> events) {
        eventList = events;
        masterEventList = eventList;
    }

    private void fetchEvents(int start, int pageSize, boolean inLoadMoreMode) {
        if (!inLoadMoreMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }

        getHttpService().fetchEvents(buildParameterMap(start, pageSize), buildFetchEventsCallback(inLoadMoreMode));
    }

    private Map<String, String> buildParameterMap(int start, int pageSize) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.MEMBER_ID_PARAM, String.valueOf(user.getId()));
        return requestMap;
    }

    private void loadAdapter(final List<Event> eventListParam, boolean inLoadMoreMode) {
        if (eventList == null) {
            eventList = new ArrayList<Event>();
        }

        if (eventListParam != null && eventListParam.size() > 0) {
            List<Event> cleanedEventList = new ArrayList<Event>();
            for (Event event : eventListParam) {
                if (eventList.contains(event)) {
                    eventList.set(eventList.indexOf(event), event);
                } else {
                    cleanedEventList.add(event);
                }
            }

            int size = eventList.size();
            eventList.addAll(cleanedEventList);
            updateMasterList(eventListParam);

            eventList = applyUserEventsFilter(eventList, isUserEventToggleOn);

            if (inLoadMoreMode) {
                cleanedEventList = applyUserEventsFilter(cleanedEventList, isUserEventToggleOn);
                eventsAdapter.notifyItemRangeInserted(size, cleanedEventList.size());
            } else {
                eventsAdapter.setItems(eventList);
            }
            eventGroupAdapter.setSections(getSectionLabels(eventList));
            new CalendarEventsUpdaterTask(cleanedEventList).execute();
        }
    }

    private void updateEventListInCache(List<Event> eventList) {
        playNetworkDatabase.eventDao().deleteCachedEvents();
        playNetworkDatabase.eventDao().insertAll(ListUtils.convertEventListToArray(eventList));
    }

    private List<Event> updateCalendarEvents(List<Event> eventListParam) {
        for (Event event : eventListParam) {
            CalendarEventDao calendarEventDao = playNetworkDatabase.calendarEventDao();
            List<CalendarEvent> calendarEvents = calendarEventDao.getAll();
            CalendarEvent calendarEvent = new CalendarEvent(event.getId(), 1);
            if (calendarEvents.contains(calendarEvent)) {
                event.setCalendarEventId(calendarEvents.get(calendarEvents.indexOf(calendarEvent)).getCalendarEventId());
            }
        }
        return eventListParam;
    }

    private List<Event> applyUserEventsFilter(List<Event> eventList, boolean isUserEventToggleOn) {
        List<Event> toggledEvents = new ArrayList<Event>();
        if (ListUtils.isNotEmpty(eventList)) {
            for (Event event : eventList) {
                if (isUserEventToggleOn) {
                    if (!TextUtils.isEmpty(event.getId()) && event.getHasRSVP()) {
                        toggledEvents.add(event);
                    }
                } else {
                    toggledEvents.add(event);
                }
            }
        }

        return toggledEvents;
    }

    private void updateMasterList(List<Event> paramList) {
        for (Event event : paramList) {
            if (event != null && !TextUtils.isEmpty(event.getId())) {
                if (masterEventList.contains(event)) {
                    masterEventList.set(masterEventList.indexOf(event), event);
                } else {
                    masterEventList.add(event);
                }
            }
        }
    }

    private void updateEventInMasterList(Event event) {
        if (event != null && ListUtils.isNotEmpty(masterEventList) && masterEventList.contains(event)) {
            masterEventList.set(masterEventList.indexOf(event), event);
        }
    }

    private Section[] getSectionLabels(List<Event> events) {
        int thisWeekEvents, thisMonthEvents, upcomingEvents;
        List<Section> sections = new ArrayList<Section>();

        thisWeekEvents = getEventsForGroup(events, EventGroup.THIS_WEEK).size();
        thisMonthEvents = getEventsForGroup(events, EventGroup.THIS_MONTH).size();
        upcomingEvents = getEventsForGroup(events, EventGroup.UPCOMING).size();

        if (thisWeekEvents > 0) {
            sections.add(new Section(0, EventGroup.THIS_WEEK.getValue()));
        }

        if (thisMonthEvents > 0) {
            sections.add(new Section(thisWeekEvents, EventGroup.THIS_MONTH.getValue()));
        }

        if (upcomingEvents > 0) {
            sections.add(new Section(thisWeekEvents + thisMonthEvents, EventGroup.UPCOMING.getValue()));
        }

        return sections.toArray(new Section[sections.size()]);
    }

    private List<Event> getEventsForGroup(List<Event> events, EventGroup eventGroup) {

        if (ListUtils.isNotEmpty(events) && eventGroup != null) {
            PNAInterfaces.Filter<Event, String> filter = new PNAInterfaces.Filter<Event, String>() {
                public boolean isMatched(Event event, String text) {
                    return event != null && !TextUtils.isEmpty(event.getId()) && !TextUtils.isEmpty(event.getEventGroup()) && event.getEventGroup().equalsIgnoreCase(String.valueOf(text));
                }
            };

            return new FilterList().filterList(events, filter, eventGroup.getId());
        } else {
            return new ArrayList<Event>();
        }
    }

    private Callback<EventResponse> buildFetchEventsCallback(final boolean inLoadMoreMode) {
        return new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                EventResponse eventResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                isLoading = false;
                if (inLoadMoreMode && isVisible()) {
                    eventsAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response)) {
                    List<Event> eventListParam = null;

                    if (eventResponse != null) {
                        eventListParam = eventResponse.getEvents();
                    }

                    if (eventListParam == null || eventListParam.size() == 0 || eventListParam.size() < pageSize) {
                        hasListEnded = true;
                    }
                    if (ListUtils.isNotEmpty(eventListParam)) {
                        eventListParam = updateCalendarEvents(eventListParam);
                    }

                    if (!inLoadMoreMode) {
                        if (eventList != null) {
                            eventList.clear();
                        }
                        if (masterEventList != null) {
                            masterEventList.clear();
                        }

                        eventsAdapter.clearItems();
                        updateEventListInCache(eventListParam);
                        loadAdapter(eventListParam, false);
                    } else {
                        loadAdapter(eventListParam, true);
                    }

                    if (!hasListEnded) {
                        if (isVisible()) {
                            eventsAdapter.addLoadingFooter();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    eventsAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        position = eventGroupAdapter.sectionedPositionToPosition(position);
        if (position > -1 && eventsAdapter.getEventList().size() > 0) {
            Event event = eventsAdapter.getEventList().get(position);
            switch (view.getId()) {
                default:
                    if (!event.isPaidEvent() || user.getMembershipType().equalsIgnoreCase(Constants.PREMIUM_OFFER)) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.USER, user);
                        bundle.putParcelable(Constants.EVENT, event);
                        bundle.putInt(Constants.POSITION, position);
                        switchFragment(EventDetailFragment.newInstance(bundle));
                    } else {
                        showPremiumMembershipPrompt(getActivity(), event);
                    }
                    break;
            }
        }
    }

    private void showPremiumMembershipPrompt(final Activity activity, Event event) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.become_premium_member_label));
        String formattedAmount = CurrencyUtils.formatToCurrencyWithSymbol(subscriptionAmount, Constants.CURRENCY_SYMBOL);
        builder.setMessage(getString(R.string.premium_membership_prompt_content, event.getTitle(), formattedAmount, getString(R.string.default_subscription_tenor)));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.USER, user);
                bundle.putString(Constants.VIEW_TYPE, Constants.PREMIUM_SUBSCRIPTION_VIEW_TAG);
                bundle.putDouble(Constants.SUBSCRIPTION_AMOUNT, subscriptionAmount);
                intent.putExtras(bundle);
                startActivityForResult(intent, Constants.PREMIUM_SUBSCRIPTION_REQUEST_CODE);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            start = currentPage * pageSize;
            fetchEvents(start, pageSize, true);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(getContext(), refreshEventsReceiver);
        PlayNetworkDatabase.destroyDatabaseInstance();
        super.onDestroy();
    }

    private BroadcastReceiver refreshEventsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra(Constants.POSITION, -1);
            long calendarEventId = intent.getLongExtra(Constants.CALENDAR_EVENT_ID, -1);
            boolean hasRSVPed = intent.getBooleanExtra(Constants.HAS_RSVP, false);
            if (position > -1 && ListUtils.isNotEmpty(eventList)) {
                Event event = eventList.get(position);
                event.setHasRSVP(hasRSVPed);
                event.setCalendarEventId(calendarEventId);
                eventsAdapter.getEventList().set(position, event);
                eventsAdapter.notifyItemChanged(position);
                updateEventInMasterList(event);
            }
        }
    };

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.user_events_switch:
                isUserEventToggleOn = isChecked;
                currentGroup = null;
                if (isChecked) {
                    mEmptyViewLabel.setText(R.string.user_events_empty_label);
                } else {
                    mEmptyViewLabel.setText(R.string.events_empty_label);
                }
                eventList = applyUserEventsFilter(masterEventList, isUserEventToggleOn);
                eventGroupAdapter.setSections(getSectionLabels(eventList));
                eventsAdapter.setItems(eventList);
                break;
        }
    }

    @Override
    public void onSubscriptionAmountFetched(SubscriptionAmountResponse subscriptionAmountResponse) {
        if (subscriptionAmountResponse != null && subscriptionAmountResponse.getAmount() > 0) {
            subscriptionAmount = subscriptionAmountResponse.getAmount();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.PREMIUM_SUBSCRIPTION_REQUEST_CODE) {
            user = playNetworkDatabase.userDao().findLastLoggedInUser();
        }
    }
}
