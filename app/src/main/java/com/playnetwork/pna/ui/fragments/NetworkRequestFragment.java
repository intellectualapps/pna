package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.SlimMemberResponse;
import com.playnetwork.pna.data.api.responses.UpdateNetworkInvitationResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkRequestFragment extends BaseFragment implements View.OnClickListener {

    private TextView memberFullNameView;
    private TextView memberJobTitleView;
    private TextView memberIndustryLocationView;
    private ImageView memberPhotoView;
    private Network network;
    private int position;
    private int memberId;
    private int networkId;
    private String loggedInMemberId;
    private Button acceptInvitationButton, declineInvitationButton;
    private PNAInterfaces.SuggestionSelectionListener listener;
    private User user;
    private String memberFullName;

    public static Fragment newInstance(Bundle args) {
        NetworkRequestFragment frag = new NetworkRequestFragment();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.NETWORK)) {
                network = getArguments().getParcelable(Constants.NETWORK);
                position = getArguments().getInt(Constants.POSITION, 0);
            }

            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
                loggedInMemberId = String.valueOf(user.getId());
            }

            if (getArguments().getBoolean(Constants.NOTIFICATION_FLAG, false)) {
                memberFullName = getArguments().getString(Constants.FULL_NAME, "");
                memberId = Integer.parseInt(getArguments().getString(Constants.MEMBER_ID, ""));
                networkId = Integer.parseInt(getArguments().getString(Constants.NETWORK_ID, ""));
                user = getArguments().getParcelable(Constants.USER);
                loggedInMemberId = String.valueOf(user.getId());
            } else {
                if (getArguments().containsKey(Constants.MEMBER_ID)) {
                    memberId = getArguments().getInt(Constants.MEMBER_ID, 0);
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.network_request_fragment, container, false);
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.network_invitation_toolbar_label));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        memberFullNameView = view.findViewById(R.id.member_full_name);
        memberJobTitleView = view.findViewById(R.id.member_job_title);
        memberIndustryLocationView = view.findViewById(R.id.member_industry_location);
        memberPhotoView = view.findViewById(R.id.member_photo);
        acceptInvitationButton = view.findViewById(R.id.accept_invitation_button);
        declineInvitationButton = view.findViewById(R.id.decline_invitation_button);

        acceptInvitationButton.setOnClickListener(this);
        declineInvitationButton.setOnClickListener(this);
        loadTempMemberData();
        loadNetworkData(network);
        fetchMemberSlimProfile();
    }

    private void loadTempMemberData() {
        if (!TextUtils.isEmpty(memberFullName)) {
            memberFullNameView.setText(memberFullName);
        }
    }

    private void fetchMemberSlimProfile() {
        if (NetworkUtils.isConnected(getContext())) {
            getHttpService().fetchSlimMemberProfile(String.valueOf(memberId), buildFetchMemberSlimProfileCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void loadNetworkData(Network network) {
        if (network != null) {
            memberFullNameView.setText(String.format("%s %s", network.getFirstName(), network.getLastName()));
            memberJobTitleView.setText(network.getJobTitle());
            memberId = Integer.parseInt(network.getMemberId());
            networkId = network.getNetworkId();
            memberIndustryLocationView.setText(String.format("%s | %s", network.getCompany(), network.getState()));
            if (!TextUtils.isEmpty(network.getProfilePhotoUrl())) {
                Picasso.with(getContext()).load(network.getProfilePhotoUrl()).placeholder(R.color.colorGray).into(memberPhotoView);
            } else {
                Picasso.with(getContext()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(memberPhotoView);
            }
        }
    }

    private void loadMemberData(User member) {
        if (member != null) {
            memberFullNameView.setText(member.getFullName());
            memberJobTitleView.setText(member.getJobTitle());
            if (!TextUtils.isEmpty(member.getFullName())) {
                memberFullNameView.setText(member.getFullName());
            }

            memberIndustryLocationView.setText(String.format("%s | %s", member.getCompany(), member.getState()));
            if (!TextUtils.isEmpty(member.getPic())) {
                String profilePictureUrl = member.getPic().contains("http") ? member.getPic() : member.getUrl().concat("/").concat(member.getPic()).replace("///", "//");
                Picasso.with(getContext()).load(profilePictureUrl).placeholder(R.color.colorGray).into(memberPhotoView);
            } else {
                Picasso.with(getContext()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(memberPhotoView);
            }
        }
    }

    @Override
    public void onClick(View v) {
        String networkIdParam = String.valueOf(networkId);
        String memberIdParam = String.valueOf(memberId);
        String option = String.valueOf(false);
        String message = getString(R.string.request_progress_label);

        switch (v.getId()) {
            case R.id.accept_invitation_button:
                option = String.valueOf(true);
                if (NetworkUtils.isConnected(getContext())) {
                    showLoadingIndicator(message);
                    getHttpService().updateNetworkInvitation(networkIdParam, option, loggedInMemberId, changeNetworkInvitationStateCallback());
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
            case R.id.decline_invitation_button:
                option = String.valueOf(false);
                if (NetworkUtils.isConnected(getContext())) {
                    showLoadingIndicator(message);
                    getHttpService().updateNetworkInvitation(networkIdParam, option, loggedInMemberId, changeNetworkInvitationStateCallback());
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
        }
    }

    private Callback<SlimMemberResponse> buildFetchMemberSlimProfileCallback() {
        return new Callback<SlimMemberResponse>() {
            @Override
            public void onResponse(Call<SlimMemberResponse> call, Response<SlimMemberResponse> response) {
                SlimMemberResponse slimMemberResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    if (slimMemberResponse != null) {
                        User member = new User();
                        member.setFullName(slimMemberResponse.getFullName());
                        member.setJobTitle(slimMemberResponse.getJobTitle());
                        member.setCompany(slimMemberResponse.getCompany());
                        member.setPic(slimMemberResponse.getProfilePhoto());
                        member.setIndustry(slimMemberResponse.getIndustry());
                        member.setState(slimMemberResponse.getState());
                        loadMemberData(member);
                    } else {
                        showErrorPopupMessage(getString(R.string.fetch_member_profile_error));
                    }
                }
            }

            @Override
            public void onFailure(Call<SlimMemberResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<UpdateNetworkInvitationResponse> changeNetworkInvitationStateCallback() {
        return new Callback<UpdateNetworkInvitationResponse>() {
            @Override
            public void onResponse(Call<UpdateNetworkInvitationResponse> call, Response<UpdateNetworkInvitationResponse> response) {
                hideLoadingIndicator();
                UpdateNetworkInvitationResponse updateNetworkInvitationResponse = response.body();
                if (ApiUtils.isSuccessResponse(response)) {
                    Network network = updateNetworkInvitationResponse.getNetwork();
                    showSuccessPopupMessage(getString(R.string.successful_request_message));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            closeFragment();
                        }
                    }, 1000);
                    if (getActivity() != null && isVisible()) {
                        ((BaseActivity) getActivity()).sendBroadcastIntent(Constants.REFRESH_NETWORK_REQUESTS_INTENT_FILTER);
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateNetworkInvitationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }
}
