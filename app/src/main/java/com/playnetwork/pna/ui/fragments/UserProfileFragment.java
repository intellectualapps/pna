package com.playnetwork.pna.ui.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.UploadRequest;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.preprocess.BitmapEncoder;
import com.cloudinary.android.preprocess.DimensionsValidator;
import com.cloudinary.android.preprocess.ImagePreprocessChain;
import com.cloudinary.android.preprocess.Limit;
import com.playnetwork.pna.BuildConfig;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.api.responses.ExperienceResponse;
import com.playnetwork.pna.data.api.responses.LookupStatesResponse;
import com.playnetwork.pna.data.api.responses.SkillResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.Experience;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.Skill;
import com.playnetwork.pna.data.models.State;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.MainActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.SpinnerArrayAdapter;
import com.playnetwork.pna.ui.adapters.UserExperienceAdapter;
import com.playnetwork.pna.ui.adapters.UserSkillsAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.ui.customviews.SwipeToDeleteCallback;
import com.playnetwork.pna.ui.dialogs.MediaPickerDialog;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.FileUtils;
import com.playnetwork.pna.utils.ImageOptimizerTask;
import com.playnetwork.pna.utils.ImageUtils;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UserProfileFragment extends MediaPickerBaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener, BaseRecyclerAdapter.ClickListener, PNAInterfaces.ImageOptimizationListener {
    private User user;
    private Button updateProfileButton, changeProfilePhotoButton;
    private ImageView profilePhotoView;
    private EditText companyNameInput, jobTitleInput, firstNameInput, lastNameInput, phoneNumberInput, addressInput, twitterUsernameInput, facebookUsernameInput, instagramUsernameInput, websiteInput;
    private Spinner industrySpinner, countrySpinner, stateSpinner;
    private List<Country> countries;
    private List<Industry> industries;
    private List<State> states;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean hasMediaPermissionBeenGranted = false;
    private ImageView addExperienceButton, addSkillButton;
    private CustomRecyclerView userExperienceRecyclerView, userSkillsRecyclerView;
    private UserSkillsAdapter userSkillsAdapter;
    private UserExperienceAdapter userExperienceAdapter;
    private TextView userExperienceSummary;
    private Uri mFileUri = null;
    private String uploadedImageUrl = null;

    private SpinnerArrayAdapter statesAdapter, countryAdapter, industryAdapter;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new UserProfileFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public UserProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();

        industries = PlayNetworkDatabase.getPlayNetworkDatabase().industryDao().getAllIndustries();
        countries = PlayNetworkDatabase.getPlayNetworkDatabase().countryDao().getAllCountries();
        hideKeyboard();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.user_profile_toolbar_label));
        firstNameInput = view.findViewById(R.id.first_name);
        lastNameInput = view.findViewById(R.id.last_name);
        phoneNumberInput = view.findViewById(R.id.phone_number);
        jobTitleInput = view.findViewById(R.id.job_title);
        companyNameInput = view.findViewById(R.id.company_name);
        addressInput = view.findViewById(R.id.address);
        twitterUsernameInput = view.findViewById(R.id.twitter_username_input);
        facebookUsernameInput = view.findViewById(R.id.facebook_username_input);
        instagramUsernameInput = view.findViewById(R.id.instagram_username_input);
        websiteInput = view.findViewById(R.id.website_input);
        industrySpinner = view.findViewById(R.id.industry_spinner);
        countrySpinner = view.findViewById(R.id.country_spinner);
        stateSpinner = view.findViewById(R.id.state_spinner);
        profilePhotoView = view.findViewById(R.id.profile_photo_view);
        profilePhotoView = view.findViewById(R.id.profile_photo_view);
        addExperienceButton = view.findViewById(R.id.add_experience_button);
        addSkillButton = view.findViewById(R.id.add_skill_button);
        userExperienceRecyclerView = view.findViewById(R.id.user_experience_recyclerview);
        userSkillsRecyclerView = view.findViewById(R.id.user_skills_recyclerview);
        userExperienceSummary = view.findViewById(R.id.experience_summary);

        userExperienceAdapter = new UserExperienceAdapter(getActivity(), this);
        userExperienceRecyclerView.setAdapter(userExperienceAdapter);
        userExperienceRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        userExperienceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        SwipeToDeleteCallback experienceSwipeToDeleteCallback = new SwipeToDeleteCallback(getActivity()) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                userExperienceAdapter.removeItem(viewHolder.getAdapterPosition());
            }
        };
        ItemTouchHelper experienceItemTouchHelper = new ItemTouchHelper(experienceSwipeToDeleteCallback);
        experienceItemTouchHelper.attachToRecyclerView(userExperienceRecyclerView);

        userSkillsAdapter = new UserSkillsAdapter(getActivity(), this);
        userSkillsRecyclerView.setAdapter(userSkillsAdapter);
        userSkillsRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        userSkillsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        SwipeToDeleteCallback skillSwipeToDeleteCallback = new SwipeToDeleteCallback(getActivity()) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                userSkillsAdapter.removeItem(viewHolder.getAdapterPosition());
            }
        };
        ItemTouchHelper skillItemTouchHelper = new ItemTouchHelper(skillSwipeToDeleteCallback);
        skillItemTouchHelper.attachToRecyclerView(userSkillsRecyclerView);

        updateProfileButton = view.findViewById(R.id.update_profile_button);
        changeProfilePhotoButton = view.findViewById(R.id.change_profile_photo_button);
        updateProfileButton.setOnClickListener(this);
        changeProfilePhotoButton.setOnClickListener(this);
        addExperienceButton.setOnClickListener(this);
        addSkillButton.setOnClickListener(this);

        statesAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());
        countryAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());
        industryAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());

        industryAdapter.addAll(getIndustryList(industries).toArray(new String[getIndustryList(industries).size()]));
        countryAdapter.addAll(getCountryList(countries).toArray(new String[getCountryList(countries).size()]));
        statesAdapter.addAll(getStateList(null).toArray(new String[getStateList(null).size()]));

        stateSpinner.setAdapter(statesAdapter);
        countrySpinner.setAdapter(countryAdapter);
        industrySpinner.setAdapter(industryAdapter);

        countrySpinner.setOnItemSelectedListener(this);
        industrySpinner.setOnItemSelectedListener(this);
        stateSpinner.setOnItemSelectedListener(this);
        loadUserData(user);
        fetchProfileData(String.valueOf(user.getId()));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (hasMediaPermissionBeenGranted) {
            showPhotoUploadOptions();
            hasMediaPermissionBeenGranted = false;
        }
    }

    private void fetchProfileData(String userId) {
        if (NetworkUtils.isConnected(getActivity())) {
            getHttpService().fetchMemberProfile(userId, buildFetchProfileCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void loadUserData(User user) {
        if (user != null) {
            firstNameInput.setText(user.getFname());
            lastNameInput.setText(user.getLname());
            phoneNumberInput.setText(user.getPhone());
            jobTitleInput.setText(user.getJobTitle());
            companyNameInput.setText(user.getCompany());
            addressInput.setText(user.getAddress());
            facebookUsernameInput.setText(user.getFacebook());
            twitterUsernameInput.setText(user.getTwitter());
            instagramUsernameInput.setText(user.getInstagram());
            websiteInput.setText(user.getWebsite());

            if (!TextUtils.isEmpty(user.getIndustry())) {
                for (int i = 0; i < industries.size(); i++) {
                    if (industries.get(i).getId() == Integer.valueOf(user.getIndustry())) {
                        industrySpinner.setSelection(i + 1, true);
                    }
                }
            }

            if (!TextUtils.isEmpty(user.getCountry())) {
                for (int i = 0; i < countries.size(); i++) {
                    if (countries.get(i).getSortName().equalsIgnoreCase(user.getCountry())) {
                        countrySpinner.setSelection(i + 1, true);
                    }
                }
            }

            if (!TextUtils.isEmpty(user.getSkills())) {
                userSkillsAdapter.setItems(getUserSkillsList(user));
            }

            if (ListUtils.isNotEmpty(user.getExperience())) {
                List<Experience> experienceList = user.getExperience();
                userExperienceAdapter.setItems(experienceList);
                userExperienceSummary.setVisibility(View.GONE);
            } else {
                userExperienceSummary.setVisibility(View.VISIBLE);
                userExperienceSummary.setText(getString(R.string.no_experience_label));
            }

            if (!TextUtils.isEmpty(user.getPic())) {
                String profilePictureUrl = user.getPic().contains("http") ? user.getPic() : user.getUrl().concat("/").concat(user.getPic()).replace("///", "//");
                Picasso.with(getActivity()).load(profilePictureUrl).placeholder(R.color.colorGray).into(profilePhotoView);
            } else {
                Picasso.with(getActivity()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(profilePhotoView);
            }
        }
    }

    private List<Skill> getUserSkillsList(User user) {
        List<String> skills = Arrays.asList(user.getSkills().replace(",,", ",").split("\\s*,\\s*"));
        List<Skill> skillList = new ArrayList<Skill>();
        for (String skill : skills) {
            skillList.add(new Skill(skill.trim()));
        }

        return skillList;
    }

    private void showPhotoUploadOptions() {
        DialogFragment mediaPickerDialog = MediaPickerDialog.newInstance(getString(R.string.select_image_dialog_title), MediaPickerDialog.TYPE_IMAGE, UserProfileFragment.class.getSimpleName());
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).launchFragmentDialog(mediaPickerDialog);
        }
    }

    @Override
    public void onClick(View view) {
        String message = getString(R.string.update_profile_progress_label);
        switch (view.getId()) {
            case R.id.change_profile_photo_button:
                if (checkMediaAccessPermission(getActivity())) {
                    showPhotoUploadOptions();
                }
                break;
            case R.id.update_profile_button:
                if (validateFields(new EditText[]{firstNameInput, lastNameInput}) && validatePhoneNumber(phoneNumberInput) && validateFields(new EditText[]{jobTitleInput, companyNameInput}) && validateSpinnerControls(new Spinner[]{industrySpinner, countrySpinner, stateSpinner})) {
                    if (NetworkUtils.isConnected(getActivity())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        if (mFileUri != null && !TextUtils.isEmpty(mFileUri.toString())) {
                            ImageOptimizerTask imageOptimizerTask = new ImageOptimizerTask(mFileUri, getContext(), this);
                            imageOptimizerTask.execute();
                        } else {
                            sendUpdateProfileRequest();
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.add_experience_button:
                if (getActivity() != null) {
                    showAddExperienceDialog(getActivity());
                }
                break;
            case R.id.add_skill_button:
                if (getActivity() != null) {
                    showAddSkillDialog(getActivity());
                }
                break;
        }
    }

    private void showAddExperienceDialog(final Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.dialog_add_experience, null);

        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setWindowAnimations(R.style.dialog_animation_fade);

        final Button addExperienceButton = dialogView.findViewById(R.id.add_experience_button);
        final ImageView closeDialogButton = dialogView.findViewById(R.id.close_dialog);
        final EditText companyInput = dialogView.findViewById(R.id.company_name);
        final EditText positionInput = dialogView.findViewById(R.id.position);
        final EditText startDatePicker = dialogView.findViewById(R.id.start_date);
        final EditText endDatePicker = dialogView.findViewById(R.id.end_date);
        final EditText periodInput = dialogView.findViewById(R.id.period);
        final TextInputLayout startDateWrapper = dialogView.findViewById(R.id.start_date_wrapper);
        final TextInputLayout endDateWrapper = dialogView.findViewById(R.id.end_date_wrapper);

        final Spinner countrySpinner = dialogView.findViewById(R.id.country_spinner);
        final Spinner stateSpinner = dialogView.findViewById(R.id.state_spinner);
        final Spinner industrySpinner = dialogView.findViewById(R.id.industry_spinner);

        final SpinnerArrayAdapter statesAdapter = new SpinnerArrayAdapter(context, R.layout.spinner_item, new ArrayList<String>());
        final SpinnerArrayAdapter countryAdapter = new SpinnerArrayAdapter(context, R.layout.spinner_item, new ArrayList<String>());
        final SpinnerArrayAdapter industryAdapter = new SpinnerArrayAdapter(context, R.layout.spinner_item, new ArrayList<String>());

        industryAdapter.addAll(getIndustryList(industries).toArray(new String[getIndustryList(industries).size()]));
        countryAdapter.addAll(getCountryList(countries).toArray(new String[getCountryList(countries).size()]));
        statesAdapter.addAll(getStateList(null).toArray(new String[getStateList(null).size()]));

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.start_date_wrapper:
                    case R.id.start_date:
                        showDatePickerDialog(v, "Pick Start Date", context);
                        break;
                    case R.id.end_date_wrapper:
                    case R.id.end_date:
                        showDatePickerDialog(v, "Pick End Date", context);
                        break;
                    case R.id.add_experience_button:
                        if (NetworkUtils.isNetworkConnected(getActivity())) {
                            if (validateFields(new EditText[]{companyInput, positionInput}) && validateSpinnerControls(new Spinner[]{countrySpinner, stateSpinner}) && validateTextInput(periodInput)) {
                                showLoadingIndicator(getString(R.string.request_processing_label));
                                Map<String, String> requestMap = new HashMap<String, String>();
                                requestMap.put(Constants.POSITION, positionInput.getText().toString().trim());
                                requestMap.put(Constants.EMPLOYER, companyInput.getText().toString().trim());
                                String industryId = String.valueOf(industries.get(industrySpinner.getSelectedItemPosition() - 1).getId());
                                String countryName = String.valueOf(countries.get(countrySpinner.getSelectedItemPosition() - 1).getName());
                                String state = states.get(stateSpinner.getSelectedItemPosition() - 1).getName();
                                requestMap.put(Constants.LOCATION, state.concat(", ").concat(countryName));
                                requestMap.put(Constants.INDUSTRY, industryId);
                                requestMap.put(Constants.PERIOD, periodInput.getText().toString());
                                getHttpService().addWorkExperience(String.valueOf(user.getId()), requestMap, new Callback<ExperienceResponse>() {
                                    @Override
                                    public void onResponse(Call<ExperienceResponse> call, Response<ExperienceResponse> response) {
                                        ExperienceResponse experienceResponse = response.body();
                                        hideLoadingIndicator();
                                        if (ApiUtils.isSuccessResponse(response)) {
                                            showSuccessPopupMessage(getString(R.string.work_experience_saved_message));
                                            if (experienceResponse != null) {
                                                user.getExperience().add(experienceResponse.getExperience());
                                                playNetworkDatabase.userDao().insertUser(user);
                                                userExperienceAdapter.setItems(user.getExperience());
                                            }
                                            if (isVisible()) {
                                                alertDialog.dismiss();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ExperienceResponse> call, Throwable t) {
                                        hideLoadingIndicator();
                                        showErrorPopupMessage(t.getMessage());
                                    }
                                });
                            }
                        } else {
                            showErrorPopupMessage(getString(R.string.network_connection_label));
                        }
                        break;
                    case R.id.close_dialog:
                        alertDialog.dismiss();
                        break;
                }
            }
        };

        startDateWrapper.setOnClickListener(clickListener);
        endDateWrapper.setOnClickListener(clickListener);
        startDatePicker.setOnClickListener(clickListener);
        endDatePicker.setOnClickListener(clickListener);
        addExperienceButton.setOnClickListener(clickListener);
        closeDialogButton.setOnClickListener(clickListener);

        industrySpinner.setAdapter(industryAdapter);
        stateSpinner.setAdapter(statesAdapter);
        countrySpinner.setAdapter(countryAdapter);

        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    Country country = countries.get(position - 1);
                    statesAdapter.clear();
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        getHttpService().fetchStates(String.valueOf(country.getId()), new Callback<LookupStatesResponse>() {
                            @Override
                            public void onResponse(Call<LookupStatesResponse> call, Response<LookupStatesResponse> response) {
                                LookupStatesResponse lookupStatesResponse = response.body();
                                hideLoadingIndicator();
                                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                                    if (lookupStatesResponse != null) {
                                        states = lookupStatesResponse.getStates();
                                    }

                                    if (ListUtils.isNotEmpty(states)) {
                                        statesAdapter.clear();
                                        statesAdapter.addAll(getStateList(states));
                                        statesAdapter.notifyDataSetChanged();
                                        if (!TextUtils.isEmpty(user.getState())) {
                                            for (int i = 0; i < states.size(); i++) {
                                                if (states.get(i).getName().equalsIgnoreCase(user.getState())) {
                                                    stateSpinner.setSelection(i + 1, true);
                                                }
                                            }
                                        }
                                    } else {
                                        showErrorPopupMessage(getString(R.string.empty_states_label));
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<LookupStatesResponse> call, Throwable t) {
                                hideLoadingIndicator();
                                showErrorPopupMessage(t.getMessage());
                            }
                        });

                    } else {
                        showErrorPopupMessage(getPlayNetworkResources().getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        alertDialog.show();
    }

    private void showAddSkillDialog(Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.dialog_add_skill, null);

        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.getWindow().setWindowAnimations(R.style.dialog_animation_fade);

        final Button addSkillButton = dialogView.findViewById(R.id.add_skill_button);
        final ImageView closeDialogButton = dialogView.findViewById(R.id.close_dialog);
        final EditText skillInput = dialogView.findViewById(R.id.skill_input);

        addSkillButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkConnected(getActivity())) {
                    if (validateTextInput(skillInput)) {
                        showLoadingIndicator(getString(R.string.request_processing_label));
                        Map<String, String> requestMap = new HashMap<String, String>();
                        requestMap.put(Constants.SKILLS, skillInput.getText().toString().trim());
                        getHttpService().addSkills(String.valueOf(user.getId()), requestMap, new Callback<SkillResponse>() {
                            @Override
                            public void onResponse(Call<SkillResponse> call, Response<SkillResponse> response) {
                                SkillResponse skillResponse = response.body();
                                hideLoadingIndicator();
                                if (ApiUtils.isSuccessResponse(response) && skillResponse != null) {
                                    showSuccessPopupMessage(getString(R.string.skill_saved_message));
                                    String userSkills = user.getSkills();
                                    user.setSkills(userSkills.concat(",").concat(skillResponse.getSkills().get(0)));
                                    userSkillsAdapter.setItems(getUserSkillsList(user));
                                    playNetworkDatabase.userDao().insertUser(user);
                                    if (isVisible()) {
                                        alertDialog.dismiss();
                                    }
                                } else {
                                    showErrorPopupMessage(getString(R.string.skill_error_message));
                                }
                            }

                            @Override
                            public void onFailure(Call<SkillResponse> call, Throwable t) {
                                hideLoadingIndicator();
                                showErrorPopupMessage(t.getMessage());
                            }
                        });
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }
        });

        closeDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void showDatePickerDialog(final View datePickerView, String title, Context context) {
        Calendar calendarDate = GregorianCalendar.getInstance();
        Calendar selectedDate = GregorianCalendar.getInstance();
        Calendar minimumDate = GregorianCalendar.getInstance();
        int year = 0, month = 0, day = 0;


        minimumDate.setTimeInMillis(calendarDate.getTimeInMillis());
        calendarDate.setTimeInMillis(selectedDate.getTimeInMillis());

        year = calendarDate.get(Calendar.YEAR);
        month = calendarDate.get(Calendar.MONTH);
        day = calendarDate.get(Calendar.DAY_OF_MONTH);

        int theme = 0;

        if (Build.VERSION.SDK_INT < 21) {
            theme = R.style.HoloDialog;
        } else {
            theme = android.R.style.Theme_Material_Light_Dialog_NoActionBar;
        }

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, theme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar = GregorianCalendar.getInstance();
                calendar.set(year, month, dayOfMonth);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);

                ((EditText) datePickerView).setText(DateUtils.getSimpleDateFormat(calendar.getTimeInMillis(), "MMMM dd, yyyy"));
            }
        }, year, month, day);
        datePickerDialog.getDatePicker().setTag(title);
        datePickerDialog.setTitle(title);

        datePickerDialog.show();
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String jobTitle = jobTitleInput.getText().toString().trim();
        String company = companyNameInput.getText().toString().trim();
        String address = addressInput.getText().toString().trim();
        String industryId = String.valueOf(industries.get(industrySpinner.getSelectedItemPosition() - 1).getId());
        String countryCode = String.valueOf(countries.get(countrySpinner.getSelectedItemPosition() - 1).getSortName());
        String state = states.get(stateSpinner.getSelectedItemPosition() - 1).getName();
        String firstName = firstNameInput.getText().toString().trim();
        String lastName = lastNameInput.getText().toString().trim();
        String phoneNumber = phoneNumberInput.getText().toString().trim();
        String instagramUsername = instagramUsernameInput.getText().toString().trim();
        String facebookUsername = facebookUsernameInput.getText().toString().trim();
        String twitterUsername = twitterUsernameInput.getText().toString().trim();
        String website = websiteInput.getText().toString().trim();

        if (!TextUtils.isEmpty(uploadedImageUrl)) {
            map.put(Constants.PHOTO_URL, uploadedImageUrl);
        }

        map.put(Constants.FIRST_NAME, firstName);
        map.put(Constants.LAST_NAME, lastName);
        map.put(Constants.PHONE_NUMBER, phoneNumber);
        map.put(Constants.AUTH_TYPE, Constants.FACEBOOK_AUTH_TYPE);
        map.put(Constants.EMAIL_ADDRESS, user.getEmail());
        map.put(Constants.JOB_TITLE, jobTitle);
        map.put(Constants.COMPANY, company);
        map.put(Constants.ADDRESS, address);
        map.put(Constants.INDUSTRY, industryId);
        map.put(Constants.COUNTRY_CODE, countryCode);
        map.put(Constants.STATE, state);
        map.put(Constants.INSTAGRAM, instagramUsername);
        map.put(Constants.FACEBOOK, facebookUsername);
        map.put(Constants.TWITTER, twitterUsername);
        map.put(Constants.WEBSITE, website);
        return map;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.country_spinner:
                if (position > 0) {
                    Country country = countries.get(position - 1);
                    statesAdapter.clear();
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        getStatesForCountry(String.valueOf(country.getId()));
                    } else {
                        showErrorPopupMessage(getPlayNetworkResources().getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.industry_spinner:
                break;
            case R.id.state_spinner:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MEDIA_PERMISSION_REQUEST_CODE:
                hasMediaPermissionRequestBeenMade = true;
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            hasMediaPermissionBeenGranted = false;
                            checkCalendarPermission(getActivity());
                            return;
                        }
                        hasMediaPermissionBeenGranted = true;
                    }
                } else {
                    checkCalendarPermission(getActivity());
                }
                break;
        }
    }

    private void setImagePreview(Uri fileUri) {
        this.mFileUri = fileUri;
        ImageUtils.loadImageUri(profilePhotoView, getContext(), fileUri);
    }

    @Override
    public void onMediaPickerSuccess(Uri fileUri, String filePath, int mediaType, String fragmentTag) {
        try {
            if (TextUtils.isEmpty(filePath)) {
                if (!fileUri.toString().contains("content://")) {
                    setImagePreview(fileUri);
                } else {
                    Uri resolvedUri = Uri.parse("file://" + FileUtils.getRealPathFromURI(getContext(), fileUri));
                    setImagePreview(resolvedUri);
                }
            } else {
                Uri resolvedUri = Uri.parse(filePath);
                setImagePreview(resolvedUri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMediaPickerError(String message, String fragmentTag) {
        if (getActivity() != null) {
            showErrorPopupMessage(message);
        }
    }

    private void getStatesForCountry(String countryId) {
        getHttpService().fetchStates(countryId, buildFetchStatesCallback());
    }

    private void sendUpdateProfileRequest() {
        if (NetworkUtils.isConnected(getContext())) {
            hideKeyboard();
            getHttpService().updateMemberProfile(getUserInput(), buildUpdateProfileCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {

    }

    private void uploadImageToCloudinary(Uri uri, byte[] imageByteArray) {
        MediaManager mediaManager = MediaManager.get();
        UploadRequest uploadRequest = null;
        if (imageByteArray != null && imageByteArray.length > 0) {
            uploadRequest = mediaManager.upload(imageByteArray);
        } else {
            uploadRequest = mediaManager.upload(uri);
        }

        String requestId = uploadRequest.callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {

            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {

            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                uploadedImageUrl = String.valueOf(resultData.get("secure_url"));
                sendUpdateProfileRequest();
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                hideLoadingIndicator();
                uploadedImageUrl = "";
                showErrorPopupMessage(error.getDescription());
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {

            }
        }).unsigned(BuildConfig.CLOUDINARY_PRESET)
                .option("angle", "ignore")
                .option("resource_type", "image")
                .preprocess(ImagePreprocessChain.limitDimensionsChain(1000, 1000)
                        .addStep(new DimensionsValidator(10, 10, 1000, 1000))
                        .addStep(new Limit(1000, 1000))
                        .saveWith(new BitmapEncoder(BitmapEncoder.Format.JPEG, 80)))
                .dispatch(getContext());
    }

    @Override
    public void onImageOptimized(String postContentText, Uri mFileUri, byte[] bytes) {
        if (bytes == null || bytes.length < 1) {
            uploadImageToCloudinary(mFileUri, null);
        } else {
            uploadImageToCloudinary(mFileUri, bytes);
        }
    }

    private Callback<LookupStatesResponse> buildFetchStatesCallback() {
        return new Callback<LookupStatesResponse>() {
            @Override
            public void onResponse(Call<LookupStatesResponse> call, Response<LookupStatesResponse> response) {
                LookupStatesResponse lookupStatesResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    if (lookupStatesResponse != null) {
                        states = lookupStatesResponse.getStates();
                    }

                    if (ListUtils.isNotEmpty(states)) {
                        statesAdapter.clear();
                        if (isAdded() && !isDetached()) {
                            statesAdapter.addAll(getStateList(states));
                            statesAdapter.notifyDataSetChanged();
                            if (!TextUtils.isEmpty(user.getState())) {
                                for (int i = 0; i < states.size(); i++) {
                                    if (states.get(i).getName().equalsIgnoreCase(user.getState())) {
                                        stateSpinner.setSelection(i + 1, true);
                                    }
                                }
                            }
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.empty_states_label));
                    }
                }
            }

            @Override
            public void onFailure(Call<LookupStatesResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<AuthenticationResponse> buildFetchProfileCallback() {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (authenticationResponse != null) {
                        user = saveUserData(playNetworkDatabase, authenticationResponse, user.getPassword(), user.getEmail());
                        if (isVisible()) {
                            loadUserData(user);
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.fetch_profile_error));
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }


    private Callback<AuthenticationResponse> buildUpdateProfileCallback() {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    user = saveUserData(playNetworkDatabase, authenticationResponse, null, user.getEmail());
                    if (isVisible()) {
                        showSuccessPopupMessage(getString(R.string.profile_updated_message));
                    }
                    if (MainActivity.userProfileUpdateListener != null) {
                        MainActivity.userProfileUpdateListener.onProfileUpdated(user);
                    }
                    if (getActivity() != null) {
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }
}
