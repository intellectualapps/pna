package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.util.SparseBooleanArray;

import java.util.ArrayList;
import java.util.List;

public abstract class MultiSelectableBaseAdapter extends BaseRecyclerAdapter {
    private static final String TAG = MultiSelectableBaseAdapter.class.getSimpleName();

    private SparseBooleanArray selectedItems;
    private SparseBooleanArray selectedMemberIds;

    public MultiSelectableBaseAdapter(Context context) {
        super(context);
        selectedItems = new SparseBooleanArray();
        selectedMemberIds = new SparseBooleanArray();
    }

    public MultiSelectableBaseAdapter() {
        selectedItems = new SparseBooleanArray();
    }

    public boolean isSelected(int position) {
        return getSelectedItems().contains(position);
    }

    public void toggleSelection(int position, int memberId) {
        if (selectedItems.get(position, false)) {
            selectedItems.delete(position);
        } else {
            selectedItems.put(position, true);
        }

        if (selectedMemberIds.get(memberId, false)) {
            selectedMemberIds.delete(memberId);
        } else {
            selectedMemberIds.put(memberId, true);
        }
        notifyItemChanged(position);
    }

    public void clearSelection() {
        List<Integer> selection = getSelectedItems();
        selectedItems.clear();
        selectedMemberIds.clear();
        for (Integer i : selection) {
            notifyItemChanged(i);
        }
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); ++i) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public List<Integer> getSelectedMemberIds() {
        List<Integer> items = new ArrayList<Integer>(selectedMemberIds.size());
        for (int i = 0; i < selectedMemberIds.size(); ++i) {
            items.add(selectedMemberIds.keyAt(i));
        }
        return items;
    }

    public void setSelectedItems(List<Integer> selectedItems){

    }
}
