package com.playnetwork.pna.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.OfferResponse;
import com.playnetwork.pna.data.enums.RedemptionOption;
import com.playnetwork.pna.data.models.CustomField;
import com.playnetwork.pna.data.models.Offer;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.QRCodeScannerActivity;
import com.playnetwork.pna.ui.adapters.CustomFieldListAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferDetailFragment extends BaseFragment implements View.OnClickListener {

    private TextView storeName;
    private TextView offerDescriptionView;
    private Button scanOfferButton, revealMemberCardButton;
    private ImageView storeNameDrawable, offerDescriptionBanner;
    private Offer offer;
    private CustomRecyclerView offerFieldsRecyclerView;
    private CustomFieldListAdapter customFieldListAdapter;
    private List<CustomField> customFieldList;
    private User user;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;
    private String offerId;
    private boolean shouldDisplayFetchDataError = false;

    public static Fragment newInstance(Bundle args) {
        OfferDetailFragment frag = new OfferDetailFragment();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(Constants.OFFER)) {
            offer = getArguments().getParcelable(Constants.OFFER);
            offerId = offer.getId();
        }

        if (getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }

        if (getArguments() != null && getArguments().containsKey(Constants.OFFER_ID)) {
            offerId = getArguments().getString(Constants.OFFER_ID);
            shouldDisplayFetchDataError = true;
        }

        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .round();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offer_detail, container, false);
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.offer_details_toolbar_label));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        storeName = view.findViewById(R.id.store_name);
        offerDescriptionView = view.findViewById(R.id.offer_details_description);
        scanOfferButton = view.findViewById(R.id.redeem_offer_button);
        revealMemberCardButton = view.findViewById(R.id.reveal_member_card_button);
        storeNameDrawable = view.findViewById(R.id.store_photo);
        offerDescriptionBanner = view.findViewById(R.id.offer_details_banner);
        offerFieldsRecyclerView = view.findViewById(R.id.offer_details_recyclerview);

        customFieldListAdapter = new CustomFieldListAdapter(getContext(), null, true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        offerFieldsRecyclerView.setLayoutManager(linearLayoutManager);
        offerFieldsRecyclerView.setAdapter(customFieldListAdapter);

        scanOfferButton.setOnClickListener(this);
        revealMemberCardButton.setOnClickListener(this);
        loadOfferData(offer);
        fetchOfferDetails(offerId, String.valueOf(user.getId()));
    }

    private void loadOfferData(Offer offer) {
        if (offer != null) {
            scanOfferButton.setEnabled(true);
            revealMemberCardButton.setEnabled(true);
            storeName.setText(offer.getBusinessName());
            String storeInitial = offer.getBusinessName().substring(0, 1);
            storeNameDrawable.setImageDrawable(mDrawableBuilder.build(storeInitial, mColorGenerator.getColor(offer.getId())));
            if (!TextUtils.isEmpty(offer.getPhoto())) {
                offerDescriptionBanner.setVisibility(View.VISIBLE);
                offerDescriptionView.setVisibility(View.GONE);
                Picasso.with(getContext()).load(offer.getPhoto()).placeholder(R.color.colorGray).into(offerDescriptionBanner);
            } else {
                offerDescriptionBanner.setVisibility(View.GONE);
                offerDescriptionView.setVisibility(View.VISIBLE);
                offerDescriptionView.setText(offer.getDetails());
            }
            String offerLevel = offer.getOfferLevel();
            storeName.setCompoundDrawablesWithIntrinsicBounds(null, null, !TextUtils.isEmpty(offerLevel) && offerLevel.equalsIgnoreCase(Constants.PREMIUM_OFFER) ? ContextCompat.getDrawable(getContext(), R.drawable.ic_lock_black_18dp) : null, null);

            customFieldListAdapter.setItems(getFieldDetails(offer));
            RedemptionOption redemptionOption = RedemptionOption.fromString(offer.getRedemptionOption());

            if (redemptionOption != null) {
                switch (redemptionOption) {
                    case QR:
                        scanOfferButton.setVisibility(View.VISIBLE);
                        revealMemberCardButton.setVisibility(View.GONE);
                        break;
                    case CARD:
                        revealMemberCardButton.setVisibility(View.VISIBLE);
                        scanOfferButton.setVisibility(View.GONE);
                        break;
                    case ANY:
                        scanOfferButton.setVisibility(View.VISIBLE);
                        revealMemberCardButton.setVisibility(View.VISIBLE);
                        break;
                }
            } else {
                scanOfferButton.setVisibility(View.VISIBLE);
                revealMemberCardButton.setVisibility(View.VISIBLE);
            }
        }
    }

    private void fetchOfferDetails(String offerId, String memberId) {
        if (NetworkUtils.isConnected(getContext())) {
            getHttpService().fetchOffer(offerId, memberId, buildFetchOfferCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private List<CustomField> getFieldDetails(Offer offer) {
        List<CustomField> list = new ArrayList<CustomField>();
        String ordinalDate = DateUtils.getOrdinalDate(offer.getStopsAt());
        list.add(new CustomField(Constants.DISCOUNT_TYPE, offer.getType()));
        list.add(new CustomField(Constants.OFFER_LEVEL, offer.getOfferLevel()));
        list.add(new CustomField(Constants.OFFER_ENDS, String.format("%s %s", ordinalDate, DateUtils.getSimpleDateFormat(offer.getStopsAt(), "MMMM, yyyy"))));
        list.add(new CustomField(Constants.OFFER_REDEEMED_AT, offer.getAddress()));

        return list;
    }

    private void completeOfferRedemption(String token) {
        if (!TextUtils.isEmpty(token)) {
            if (NetworkUtils.isConnected(getContext())) {
                showLoadingIndicator(getString(R.string.redeem_offer_progress_message));
                Map<String, String> requestMap = new HashMap<String, String>();
                requestMap.put(Constants.MEMBER_ID_PARAM, String.valueOf(user.getId()));
                requestMap.put(Constants.TOKEN_PARAM, token);
                getHttpService().redeemOffer(offer.getId(), requestMap, buildRedeemOfferCallback());
            } else {
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        } else {
            showErrorPopupMessage(getString(R.string.qr_code_invalid_error_message));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.redeem_offer_button:
                IntentIntegrator.forSupportFragment(this).setPrompt("").setBarcodeImageEnabled(true).setCaptureActivity(QRCodeScannerActivity.class).initiateScan();
                break;
            case R.id.reveal_member_card_button:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.OFFER, offer);
                bundle.putParcelable(Constants.USER, user);
                switchFragment(MemberCardFragment.newInstance(bundle));
                break;
        }
    }

    private Callback<OfferResponse> buildRedeemOfferCallback() {
        return new Callback<OfferResponse>() {
            @Override
            public void onResponse(Call<OfferResponse> call, Response<OfferResponse> response) {
                OfferResponse offerResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (offerResponse != null && isVisible()) {
                        showSuccessPopupMessage(getString(R.string.successful_offer_redemption_message));
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.OFFER, offer);
                        bundle.putParcelable(Constants.USER, user);
                        switchFragment(MemberCardFragment.newInstance(bundle));
                    } else {
                        showErrorPopupMessage(getString(R.string.qr_code_error_message));
                    }
                }
            }

            @Override
            public void onFailure(Call<OfferResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<OfferResponse> buildFetchOfferCallback() {
        return new Callback<OfferResponse>() {
            @Override
            public void onResponse(Call<OfferResponse> call, Response<OfferResponse> response) {
                OfferResponse offerResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (offerResponse != null && offerResponse.getOffer() != null) {
                        offer = offerResponse.getOffer();
                        if (isAdded() && isVisible()) {
                            loadOfferData(offer);
                        }
                    } else {
                        if (shouldDisplayFetchDataError) {
                            showErrorPopupMessage(getString(R.string.offer_load_failure_message));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<OfferResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        String token = null;
        if (result != null && !TextUtils.isEmpty(result.getContents())) {
            token = result.getContents();
            completeOfferRedemption(token.trim());
        } else {
            showErrorPopupMessage(getString(R.string.qr_code_error_message));
        }
    }
}
