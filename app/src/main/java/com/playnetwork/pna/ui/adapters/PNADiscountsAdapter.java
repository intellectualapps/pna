package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Offer;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PNADiscountsAdapter extends BaseRecyclerAdapter {

    private List<Offer> offerList;
    private ClickListener clickListener;
    private final int OFFER = 0;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    public PNADiscountsAdapter(Context context, ClickListener clickListener, PNAInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.offerList = new ArrayList<Offer>();
        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .round();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;

        switch (viewType) {
            default:
            case OFFER:
                view = mInflater.inflate(R.layout.discount_offer_item_layout, parent, false);
                viewHolder = new PNADiscountsAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            default:
                final PNADiscountsAdapter.ViewHolder viewHolder = (PNADiscountsAdapter.ViewHolder) holder;
                Offer offer = offerList.get(position);
                if (offer != null && !TextUtils.isEmpty(offer.getId())) {
                    viewHolder.storeName.setText(offer.getBusinessName());
                    String storeInitial = offer.getBusinessName().substring(0, 1);
                    viewHolder.storeNameDrawable.setImageDrawable(mDrawableBuilder.build(storeInitial, mColorGenerator.getColor(offer.getId())));
                    if (!TextUtils.isEmpty(offer.getPhoto())) {
                        viewHolder.offerDetailsBanner.setVisibility(View.VISIBLE);
                        viewHolder.offerDescription.setVisibility(View.GONE);
                        Picasso.with(context).load(offer.getPhoto()).placeholder(R.color.colorGray).into(viewHolder.offerDetailsBanner);
                    } else {
                        viewHolder.offerDetailsBanner.setVisibility(View.GONE);
                        viewHolder.offerDescription.setVisibility(View.VISIBLE);
                        viewHolder.offerDescription.setText(offer.getDetails());
                    }
                    String offerLevel = offer.getOfferLevel();
                    viewHolder.storeName.setCompoundDrawablesWithIntrinsicBounds(null, null, !TextUtils.isEmpty(offerLevel) && offerLevel.equalsIgnoreCase(Constants.PREMIUM_OFFER) ? ContextCompat.getDrawable(context, R.drawable.ic_lock_black_18dp) : null, null);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(offerList) ? 0 : offerList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == offerList.size() - 1 && isLoadingAdded) {
            return LOADING;
        }

        return OFFER;
    }

    /*
    Helpers
    */

    public void setItems(List<Offer> items) {
        this.offerList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (offerList != null) {
            this.offerList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Offer offer) {
        if (offer != null) {
            this.offerList.add(offer);
            notifyDataSetChanged();
        }
    }

    public void add(Offer offer) {
        offerList.add(offer);
        notifyItemInserted(offerList.size() - 1);
    }

    public void addAll(List<Offer> offerList) {
        if (offerList != null && offerList.size() > 0) {
            for (Offer offer : offerList) {
                add(offer);
            }
        }
        notifyDataSetChanged();
    }

    public void remove(Offer offer) {
        int position = offerList.indexOf(offer);
        if (position > -1) {
            offerList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Offer getItem(int position) {
        return offerList.get(position);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Offer());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = offerList.size() - 1;
        if (position > -1) {
            Offer offer = getItem(position);

            if (offer != null) {
                if (TextUtils.isEmpty(offer.getId())) {
                    offerList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView storeName, offerDescription;
        ImageView storeNameDrawable, offerDetailsBanner;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            storeName = itemView.findViewById(R.id.store_name);
            offerDescription = itemView.findViewById(R.id.offer_details_description);
            storeNameDrawable = itemView.findViewById(R.id.store_photo);
            offerDetailsBanner = itemView.findViewById(R.id.offer_details_banner);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(offerList.size() - 1);
        if (errorMsg != null) this.errorMsg = errorMsg;
    }
}