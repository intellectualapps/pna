package com.playnetwork.pna.ui.customviews.likebutton;

public interface OnLikeListener {
    void liked(LikeButton likeButton);
    void unLiked(LikeButton likeButton);
}
