package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.HomeScreenActiveMembers;
import com.playnetwork.pna.data.models.HomeScreenItem;
import com.playnetwork.pna.data.models.HomeScreenVideo;
import com.playnetwork.pna.data.models.MemberPhoto;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.customviews.AutoFitGridRecyclerView;
import com.playnetwork.pna.utils.ImageUtils;
import com.playnetwork.pna.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class HomeScreenAdapter extends BaseRecyclerAdapter {

    private List<HomeScreenItem> homeScreenItems;
    private ClickListener clickListener;
    private final int VIDEO = 1, OTHERS = 2, MEMBERS = 3;
    private static User user;

    public HomeScreenAdapter(Context context, ClickListener clickListener, User user) {
        super(context);
        this.clickListener = clickListener;
        this.user = user;
        this.homeScreenItems = new ArrayList<HomeScreenItem>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        switch (viewType) {
            default:
                view = mInflater.inflate(R.layout.homescreen_item_layout, parent, false);
                viewHolder = new HomeScreenAdapter.ViewHolder(view, this.clickListener);
                break;
            case VIDEO:
                view = mInflater.inflate(R.layout.homescreen_video_item_layout, parent, false);
                viewHolder = new HomeScreenAdapter.VideoViewHolder(view, this.clickListener);
                break;
            case MEMBERS:
                view = mInflater.inflate(R.layout.homescreen_members_layout, parent, false);
                viewHolder = new HomeScreenAdapter.ActiveMembersViewHolder(view);
                break;
        }
        return viewHolder;
    }

    private int computeSpanCount(List<MemberPhoto> memberPhotos) {
        return memberPhotos.size() % 5 == 0 ? 5 : 4;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HomeScreenItem homeScreenItem = homeScreenItems.get(position);
        switch (getItemViewType(position)) {
            case VIDEO:
                final HomeScreenAdapter.VideoViewHolder videoViewHolder = (HomeScreenAdapter.VideoViewHolder) holder;
                if (homeScreenItem != null) {
                    videoViewHolder.titleView.setText(homeScreenItem.getTitle());
                    ImageUtils.loadImageUrl(videoViewHolder.banner, context, homeScreenItem.getImageUrl());
                }
                break;
            case MEMBERS:
                final HomeScreenAdapter.ActiveMembersViewHolder activeMembersViewHolder = (HomeScreenAdapter.ActiveMembersViewHolder) holder;
                if (homeScreenItem != null) {
                    HomeScreenActiveMembers homeScreenActiveMembers = (HomeScreenActiveMembers) homeScreenItem;
                    List<MemberPhoto> memberPhotos = homeScreenActiveMembers.getMemberPhotos();
                    activeMembersViewHolder.titleView.setText(homeScreenActiveMembers.getTitle());
                    if (ListUtils.isNotEmpty(memberPhotos)) {
                        activeMembersViewHolder.activeMembersRecyclerView.setSpanCount(computeSpanCount(memberPhotos));
                        activeMembersViewHolder.memberPhotosAdapter.setItems(memberPhotos);
                    }
                }
                break;
            default:
                final HomeScreenAdapter.ViewHolder viewHolder = (HomeScreenAdapter.ViewHolder) holder;
                if (homeScreenItem != null) {
                    viewHolder.titleView.setText(homeScreenItem.getTitle());
                    ImageUtils.loadImageUrl(viewHolder.banner, context, homeScreenItem.getImageUrl());
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (homeScreenItems.get(position) instanceof HomeScreenVideo) {
            return VIDEO;
        }

        if (homeScreenItems.get(position) instanceof HomeScreenActiveMembers) {
            return MEMBERS;
        }

        return OTHERS;
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(homeScreenItems) ? 0 : homeScreenItems.size();
    }

    /*
    Helpers
    */

    public void setItems(List<HomeScreenItem> items) {
        this.homeScreenItems = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (homeScreenItems != null) {
            this.homeScreenItems.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(HomeScreenItem homeScreenItem) {
        if (homeScreenItem != null) {
            this.homeScreenItems.add(homeScreenItem);
            notifyDataSetChanged();
        }
    }


    public void removeItem(HomeScreenItem homeScreenItem) {
        int position = homeScreenItems.indexOf(homeScreenItem);
        if (position > -1) {
            homeScreenItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    private HomeScreenItem getItem(int position) {
        return homeScreenItems.get(position);
    }

    public List<HomeScreenItem> getHomeScreenItems() {
        return homeScreenItems;
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView titleView;
        ImageView banner;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            titleView = itemView.findViewById(R.id.item_title);
            banner = itemView.findViewById(R.id.item_banner);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }

    private static class VideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView titleView;
        ImageView banner;
        ImageView youtubeButton;

        ClickListener clickListener;

        VideoViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            titleView = itemView.findViewById(R.id.item_title);
            banner = itemView.findViewById(R.id.item_banner);

            youtubeButton = itemView.findViewById(R.id.youtube_player_button);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            youtubeButton.setOnClickListener(this);

            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }

    private static class ActiveMembersViewHolder extends RecyclerView.ViewHolder {
        private TextView titleView;
        private AutoFitGridRecyclerView activeMembersRecyclerView;
        private MemberPhotosAdapter memberPhotosAdapter;

        ActiveMembersViewHolder(View itemView) {
            super(itemView);
            titleView = itemView.findViewById(R.id.item_title);
            activeMembersRecyclerView = itemView.findViewById(R.id.active_members_recyclerview);
            memberPhotosAdapter = new MemberPhotosAdapter(context, user);
            memberPhotosAdapter.setItems(null);
            activeMembersRecyclerView.setAdapter(memberPhotosAdapter);
        }
    }
}