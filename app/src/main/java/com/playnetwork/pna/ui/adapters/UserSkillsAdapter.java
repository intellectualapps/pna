package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.Skill;
import com.playnetwork.pna.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class UserSkillsAdapter extends BaseRecyclerAdapter {

    private List<Skill> skillList;
    private ClickListener clickListener;

    public UserSkillsAdapter(Context context, ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
        this.skillList = new ArrayList<Skill>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        view = mInflater.inflate(R.layout.skill_item_layout, parent, false);
        viewHolder = new UserSkillsAdapter.ViewHolder(view, this.clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Skill skill = skillList.get(position);
        final UserSkillsAdapter.ViewHolder viewHolder = (UserSkillsAdapter.ViewHolder) holder;
        if (skill != null) {
            viewHolder.skillView.setText(skill.getSkill());
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(skillList) ? 0 : skillList.size();
    }

    /*
    Helpers
    */

    public void setItems(List<Skill> items) {
        this.skillList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (skillList != null) {
            this.skillList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Skill skill) {
        if (skill != null) {
            this.skillList.add(skill);
            notifyDataSetChanged();
        }
    }


    public void removeItem(Skill skill) {
        int position = skillList.indexOf(skill);
        if (position > -1) {
            skillList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeItem(int position) {
        if (position > -1) {
            skillList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Skill getItem(int position) {
        return skillList.get(position);
    }


    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView skillView;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            skillView = itemView.findViewById(R.id.skill);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }
}