package com.playnetwork.pna.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.GenericFeedResponse;
import com.playnetwork.pna.data.api.responses.LikeFeedResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Feed;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.FeedActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.FeedListAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.ui.customviews.likebutton.LikeButton;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.AutoRefreshUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.FCMTokenUtils;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedListFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, View.OnClickListener, PNAInterfaces.PaginationAdapterCallback {

    private FeedListAdapter feedListAdapter;
    private CustomRecyclerView feedRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private View mEmptyView;

    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private int start = 0, pageSize = 10;
    private int currentPage = start;

    private List<Feed> feedList = new ArrayList<Feed>();
    private User user;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean hasPageInitialized = false;
    private FloatingActionButton createPostFloatingActionButton;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new FeedListFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public FeedListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hasPageInitialized = true;

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        feedList = playNetworkDatabase.feedDao().getFeedItems();

        registerReceiver(getContext(), refreshFeedReceiver, new IntentFilter(Constants.REFRESH_FEED_INTENT_FILTER));
        AutoRefreshUtils.fetchSubscriptionAmount(getHttpService(), null);
        new FCMTokenUtils().execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_list, container, false);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.feed_list_toolbar_label));
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        createPostFloatingActionButton = view.findViewById(R.id.create_post_button);
        feedRecyclerView = view.findViewById(R.id.feed_list_recyclerview);
        mEmptyView = view.findViewById(R.id.empty_view);

        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mRetryButton = mEmptyView.findViewById(R.id.retry_button);

        mEmptyViewLabel.setText(getString(R.string.no_feed_label));

        feedListAdapter = new FeedListAdapter(getContext(), this, this);
        createPostFloatingActionButton.setOnClickListener(this);
        mRetryButton.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        feedRecyclerView.setAdapter(feedListAdapter);
        feedRecyclerView.setEmptyView(mEmptyView);
        feedRecyclerView.setLayoutManager(linearLayoutManager);
        feedRecyclerView.setItemAnimator(new DefaultItemAnimator());
        feedRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        feedRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    createPostFloatingActionButton.hide();
                    return;
                }
                if (dy < 0) {
                    createPostFloatingActionButton.show();
                }
            }
        });

        feedRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (feedList != null && feedList.size() >= pageSize) {
                            fetchFeed(start, pageSize, true);
                        }
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                    feedListAdapter.showRetry(true, getString(R.string.network_connection_label));
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.green),
                ContextCompat.getColor(getContext(), R.color.blue),
                ContextCompat.getColor(getContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchFeed(0, pageSize, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (feedList != null && feedList.size() > 0) {
            loadAdapter(feedList);
            if (NetworkUtils.isConnected(getContext())) {
                animateSwipeRefreshLayout(swipeRefreshLayout);
                fetchFeed(0, pageSize, false);
            } else {
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        } else {
            if (NetworkUtils.isConnected(getContext())) {
                animateSwipeRefreshLayout(swipeRefreshLayout);
                fetchFeed(start, pageSize, false);
            } else {
                feedListAdapter.clearItems();
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        if (feedList != null && feedList.size() > 0) {
            feedListAdapter.notifyDataSetChanged();
        }
    }

    private void manageEmptyViewState(boolean showRetryButton, String emptyViewLabel) {
        mEmptyViewLabel.setText(emptyViewLabel);
        mRetryButton.setVisibility(showRetryButton ? View.VISIBLE : View.GONE);
    }

    private void showRequestDeclinedDialog(Context context, final String notificationMessage) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.dialog_request_declined, null);

        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final TextView mDialogTitle = (TextView) dialogView.findViewById(R.id.dialog_title);
        final TextView mDialogContent = (TextView) dialogView.findViewById(R.id.dialog_content);
        final Button mDismissDialog = (Button) dialogView.findViewById(R.id.dismiss_button);

        if (notificationMessage != null) {
            mDialogContent.setText(notificationMessage);
        }

        mDismissDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private Map<String, String> buildParameterMap(int start, int pageSize) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
        return requestMap;
    }

    private void fetchFeed(int start, int pageSize, boolean inLoadMoreMode) {
        if (!inLoadMoreMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }
        getHttpService().fetchFeed(buildParameterMap(start, pageSize), buildFetchFeedCallback(inLoadMoreMode));
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1) {
            Feed feed = feedList.get(position);
            switch (view.getId()) {
                case R.id.like_button:
                    if (NetworkUtils.isConnected(getContext())) {
                        String feedId = String.valueOf(feed.getFeedId());
                        String email = user.getEmail();

                        toggleLikeButtonState(!feed.isHasUserLiked(), view, false);
                        updateFeedItem(feed, position);
                        getHttpService().updateFeedLikeStatus(feedId, email, buildLikeFeedCallback(feed, position, view));
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                    break;
                case R.id.favourite_button:
                    if (NetworkUtils.isConnected(getContext())) {
                        String feedId = String.valueOf(feed.getFeedId());
                        String email = user.getEmail();
                        toggleLikeButtonState(!feed.isHasUserLiked(), view, true);
                        updateFeedItem(feed, position);
                        getHttpService().updateFeedLikeStatus(feedId, email, buildLikeFeedCallback(feed, position, view));
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                    break;
                default:
                    Intent intent = new Intent(getContext(), FeedActivity.class);
                    intent.putExtra(Constants.FEED, feed);
                    intent.putExtra(Constants.VIEW_TYPE, Constants.FEED_DETAILS_VIEW_TAG);
                    startActivity(intent);
                    break;
            }
        }
    }

    private void toggleLikeButtonState(boolean stateFlag, View likeButton, boolean playAnimation) {
        if (likeButton instanceof ImageView) {
            ((ImageView) likeButton).setImageDrawable(ContextCompat.getDrawable(getContext(), stateFlag ? R.drawable.ic_liked_state : R.drawable.ic_unliked_state));
        } else {
            ((LikeButton) likeButton).setLiked(stateFlag);
            if (stateFlag) {
                ((LikeButton) likeButton).playAnimation();
            }
        }
    }

    private void updateFeedList(List<Feed> feedListParam) {
        if (feedList == null) {
            feedList = new ArrayList<Feed>();
        }
        if (feedListParam != null && feedListParam.size() > 0) {
            feedList.addAll(feedListParam);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.create_post_button:
                Intent intent = new Intent(getContext(), FeedActivity.class);
                intent.putExtra(Constants.USER, user);
                intent.putExtra(Constants.VIEW_TYPE, Constants.CREATE_POST_VIEW_TAG);
                startActivity(intent);
                break;
            case R.id.retry_button:
                if (NetworkUtils.isConnected(getContext())) {
                    mEmptyView.setVisibility(View.GONE);
                    manageEmptyViewState(false, getString(R.string.no_feed_label));
                    animateSwipeRefreshLayout(swipeRefreshLayout);
                    fetchFeed(0, pageSize, false);
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
        }
    }

    private void updateFeedItem(Feed feed, int position) {
        feed.setLikes(feed.isHasUserLiked() ? feed.getLikes() - 1 : feed.getLikes() + 1);
        feed.setHasUserLiked(!feed.isHasUserLiked());
        feedList.set(position, feed);
        feedListAdapter.notifyItemChanged(position, feed);
    }

    private Callback<LikeFeedResponse> buildLikeFeedCallback(final Feed feed, final int position, final View view) {
        return new Callback<LikeFeedResponse>() {
            @Override
            public void onResponse(Call<LikeFeedResponse> call, Response<LikeFeedResponse> response) {
                LikeFeedResponse likeFeedResponse = response.body();
                endSwipeRefreshLayout(swipeRefreshLayout);

                if (isVisible()) {
                    if (ApiUtils.isSuccessResponse(response) && likeFeedResponse != null) {
                        feed.setHasUserLiked(likeFeedResponse.isHasUserLiked());
                        feed.setLikes(likeFeedResponse.getLikes());
                        feedList.set(position, feed);
                        feedListAdapter.notifyItemChanged(position, feed);
                    } else {
                        toggleLikeButtonState(feed.isHasUserLiked(), view, false);
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeFeedResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
                toggleLikeButtonState(feed.isHasUserLiked(), view, false);
            }
        };
    }


    private void loadAdapter(List<Feed> feedList) {
        feedListAdapter.addAll(feedList);
    }

    private void updateFeedInCache(List<Feed> feedList) {
        playNetworkDatabase.feedDao().deleteCachedFeed();
        playNetworkDatabase.feedDao().insertFeedItems(ListUtils.convertFeedListToArray(feedList));
    }

    private Callback<GenericFeedResponse<List<Feed>>> buildFetchFeedCallback(final boolean inLoadMoreMode) {
        return new Callback<GenericFeedResponse<List<Feed>>>() {
            @Override
            public void onResponse(Call<GenericFeedResponse<List<Feed>>> call, Response<GenericFeedResponse<List<Feed>>> response) {
                GenericFeedResponse<List<Feed>> feedResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                isLoading = false;
                if (inLoadMoreMode && isVisible()) {
                    feedListAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response)) {
                    List<Feed> feedListParam = feedResponse.getFeed();

                    if (feedListParam == null || feedListParam.size() == 0 || feedListParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (feedList != null) {
                            feedList.clear();
                        }
                        feedListAdapter.clearItems();
                        updateFeedInCache(feedListParam);
                    }

                    if (isVisible()) {
                        updateFeedList(feedListParam);
                        loadAdapter(feedListParam);
                    }

                    if (!hasListEnded && isVisible()) {
                        feedListAdapter.addLoadingFooter();
                    }
                }
            }

            @Override
            public void onFailure(Call<GenericFeedResponse<List<Feed>>> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (ListUtils.isEmpty(feedList)) {
                    feedListAdapter.clearItems();
                    manageEmptyViewState(true, getString(R.string.feed_list_error));
                }

                if (inLoadMoreMode) {
                    feedListAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
        unregisterReceiver(getContext(), refreshFeedReceiver);
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getContext())) {
            start = currentPage * pageSize;
            fetchFeed(start, pageSize, true);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private BroadcastReceiver refreshFeedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (NetworkUtils.isConnected(getContext())) {
                fetchFeed(0, pageSize, false);
            }
        }
    };
}
