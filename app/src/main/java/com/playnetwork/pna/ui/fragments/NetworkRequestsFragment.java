package com.playnetwork.pna.ui.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.NetworkResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.NetworkRequestsAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkRequestsFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, PNAInterfaces.PaginationAdapterCallback {
    private User user;
    private NetworkRequestsAdapter networkRequestsAdapter;
    private CustomRecyclerView networkRequestsRecylcerview;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private View mEmptyView;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private boolean hasPageInitialized = false;
    private int start = 0, pageSize = 10;
    private int currentPage = start;

    private List<Network> networkRequestList;
    private boolean pushNotificationReceived = false, hasNotificationBeenShown = false;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new NetworkRequestsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public NetworkRequestsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        hasPageInitialized = true;
        networkRequestList = playNetworkDatabase.networkDao().getRequests();

        if (getArguments() != null) {
            pushNotificationReceived = getArguments().getBoolean(Constants.NOTIFICATION_FLAG, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_network_requests, container, false);
        mSnackBarView = view;

        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.network_request_toolbar_label));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        networkRequestsRecylcerview = view.findViewById(R.id.network_requests_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);

        mEmptyViewLabel.setText(R.string.requests_empty_label);

        networkRequestsRecylcerview.setEmptyView(mEmptyView);
        networkRequestsAdapter = new NetworkRequestsAdapter(getContext(), this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        networkRequestsRecylcerview.setLayoutManager(linearLayoutManager);
        networkRequestsRecylcerview.setAdapter(networkRequestsAdapter);

        networkRequestsRecylcerview.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (networkRequestList != null && networkRequestList.size() >= pageSize) {
                            fetchMemberNetworkRequests(start, pageSize, true);
                        }
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        fetchMemberNetworkRequests(0, pageSize, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (ListUtils.isNotEmpty(networkRequestList)) {
            loadAdapter(networkRequestList);
            if (!hasPageInitialized) {
                hasPageInitialized = true;
                if (NetworkUtils.isConnected(getContext())) {
                    fetchMemberNetworkRequests(0, pageSize, false);
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }
        } else {
            if (NetworkUtils.isConnected(getContext())) {
                fetchMemberNetworkRequests(0, pageSize, false);
            } else {
                networkRequestsAdapter.clearItems();
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        }

        registerReceiver(getContext(), refreshNetworkRequestsReceiver, new IntentFilter(Constants.REFRESH_NETWORK_REQUESTS_INTENT_FILTER));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (pushNotificationReceived && !hasNotificationBeenShown) {
            switchFragment(NetworkRequestFragment.newInstance(getArguments()));
            hasNotificationBeenShown = true;
        }
        pushNotificationReceived = false;
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.request_processing_label);
    }

    private void fetchMemberNetworkRequests(int start, int pageSize, boolean inLoadMoreMode) {
        if (!inLoadMoreMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }

        getHttpService().fetchMemberNetworkRequests(String.valueOf(user.getId()), buildParameterMap(start, pageSize), buildFetchNetworkRequestsCallback(inLoadMoreMode));
    }

    private Map<String, String> buildParameterMap(int start, int pageSize) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
        return requestMap;
    }

    private void updateNetworkList(List<Network> networkListParam) {
        if (networkRequestList == null) {
            networkRequestList = new ArrayList<Network>();
        }
        if (networkListParam != null && networkListParam.size() > 0) {
            networkRequestList.addAll(networkListParam);
        }
    }

    private void updateNetworkListInCache(List<Network> networkList) {
        playNetworkDatabase.networkDao().deleteCachedRequests();
        for (Network network : networkList) {
            network.setMode(Constants.REQUEST_MODE);
        }
        playNetworkDatabase.networkDao().insertAll(ListUtils.convertNetworkListToArray(networkList));
    }

    private void loadAdapter(List<Network> networkList) {
        networkRequestsAdapter.addAll(networkList);
    }

    private Callback<NetworkResponse> buildFetchNetworkRequestsCallback(final boolean inLoadMoreMode) {
        return new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                NetworkResponse networkResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                isLoading = false;
                if (inLoadMoreMode && isVisible()) {
                    networkRequestsAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    List<Network> networkListParam = networkResponse.getNetwork();

                    if (networkListParam == null || networkListParam.size() == 0 || networkListParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (networkRequestList != null) {
                            networkRequestList.clear();
                        }
                        networkRequestsAdapter.clearItems();
                        updateNetworkListInCache(networkListParam);
                    }

                    updateNetworkList(networkListParam);
                    if (isAdded() && isVisible()) {
                        loadAdapter(networkListParam);
                    }

                    if (!hasListEnded) {
                        networkRequestsAdapter.addLoadingFooter();
                    }
                } else {
                    if (isVisible()) {
                        loadAdapter(networkRequestList);
                    }
                }
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    networkRequestsAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1) {
            Network network = networkRequestList.get(position);
            switch (view.getId()) {
                default:
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.NETWORK, network);
                    bundle.putParcelable(Constants.USER, user);
                    bundle.putInt(Constants.POSITION, position);
                    switchFragment(NetworkRequestFragment.newInstance(bundle));
                    break;
            }
        }
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            start = currentPage * pageSize;
            fetchMemberNetworkRequests(start, pageSize, true);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(getContext(), refreshNetworkRequestsReceiver);
        PlayNetworkDatabase.destroyDatabaseInstance();
        super.onDestroy();
    }


    private BroadcastReceiver refreshNetworkRequestsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (NetworkUtils.isConnected(getContext())) {
                fetchMemberNetworkRequests(0, pageSize, false);
            }
        }
    };
}
