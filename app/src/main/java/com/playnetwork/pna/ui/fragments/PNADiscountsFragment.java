package com.playnetwork.pna.ui.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.ApiClientCallbacks;
import com.playnetwork.pna.data.api.responses.OfferResponse;
import com.playnetwork.pna.data.api.responses.SubscriptionAmountResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.data.models.Offer;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.ProfileActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.PNADiscountsAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.AutoRefreshUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.CurrencyUtils;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PNADiscountsFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, PNAInterfaces.PaginationAdapterCallback, ApiClientCallbacks.SubscriptionAmountListener {
    private User user;
    private PNADiscountsAdapter pnaDiscountsAdapter;
    private CustomRecyclerView discountsRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private View mEmptyView;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private boolean hasPageInitialized = false;
    private int start = 0, pageSize = 10;
    private int currentPage = start;
    private Double subscriptionAmount = 0.00;
    private Device device;

    private List<Offer> offerList;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new PNADiscountsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public PNADiscountsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        device = playNetworkDatabase.deviceDao().getDevice();
        offerList = playNetworkDatabase.offerDao().getAll();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pna_discounts, container, false);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.pna_discounts_toolbar_label));

        discountsRecyclerView = view.findViewById(R.id.discounts_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);

        if (device == null || device.getSubscriptionAmount() == null || device.getSubscriptionAmount() < 1) {
            AutoRefreshUtils.fetchSubscriptionAmount(getHttpService(), this);
        } else {
            subscriptionAmount = device.getSubscriptionAmount();
        }

        mEmptyViewLabel.setText(R.string.offers_empty_label);

        discountsRecyclerView.setEmptyView(mEmptyView);
        pnaDiscountsAdapter = new PNADiscountsAdapter(getContext(), this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        discountsRecyclerView.setLayoutManager(linearLayoutManager);
        discountsRecyclerView.setAdapter(pnaDiscountsAdapter);

        discountsRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (offerList != null && offerList.size() >= pageSize) {
                            fetchDiscountOffers(start, pageSize, true);
                        }
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        fetchDiscountOffers(0, pageSize, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (offerList != null && offerList.size() > 0) {
            loadAdapter(offerList);
            if (!hasPageInitialized) {
                hasPageInitialized = true;
                if (NetworkUtils.isConnected(getContext())) {
                    fetchDiscountOffers(0, pageSize, false);
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }
        } else {
            if (NetworkUtils.isConnected(getContext())) {
                fetchDiscountOffers(0, pageSize, false);
            } else {
                pnaDiscountsAdapter.clearItems();
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        String message = getString(R.string.request_processing_label);
        switch (view.getId()) {

        }
    }

    private void fetchDiscountOffers(int start, int pageSize, boolean inLoadMoreMode) {
        if (!inLoadMoreMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }

        getHttpService().fetchDiscountOffers(buildParameterMap(start, pageSize), buildFetchDiscountOffersCallback(inLoadMoreMode));
    }

    private Map<String, String> buildParameterMap(int start, int pageSize) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.STATUS, "active");
        requestMap.put(Constants.MEMBER_ID_PARAM, String.valueOf(user.getId()));
        return requestMap;
    }

    private void updateOfferList(List<Offer> offerListParam) {
        if (offerList == null) {
            offerList = new ArrayList<Offer>();
        }
        if (offerListParam != null && offerListParam.size() > 0) {
            offerList.addAll(offerListParam);
        }
    }

    private void updateOfferListInCache(List<Offer> offerList) {
        playNetworkDatabase.offerDao().deleteCachedOffers();
        playNetworkDatabase.offerDao().insertAll(ListUtils.convertOfferListToArray(offerList));
    }

    private void loadAdapter(List<Offer> offerList) {
        pnaDiscountsAdapter.addAll(offerList);
    }

    private Callback<OfferResponse> buildFetchDiscountOffersCallback(final boolean inLoadMoreMode) {
        return new Callback<OfferResponse>() {
            @Override
            public void onResponse(Call<OfferResponse> call, Response<OfferResponse> response) {
                OfferResponse offerResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                isLoading = false;
                if (inLoadMoreMode && isVisible()) {
                    pnaDiscountsAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    List<Offer> offerListParam = null;

                    if (offerResponse != null) {
                        offerListParam = offerResponse.getOffers();
                    }

                    if (offerListParam == null || offerListParam.size() == 0 || offerListParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (offerList != null) {
                            offerList.clear();
                        }
                        pnaDiscountsAdapter.clearItems();
                        updateOfferListInCache(offerListParam);
                    }

                    updateOfferList(offerListParam);

                    if (isAdded() && isVisible()) {
                        loadAdapter(offerListParam);
                    }

                    if (!hasListEnded) {
                        pnaDiscountsAdapter.addLoadingFooter();
                    }
                }
            }

            @Override
            public void onFailure(Call<OfferResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    pnaDiscountsAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1 && offerList.size() > 0) {
            Offer offer = offerList.get(position);
            switch (view.getId()) {
                default:
                    String offerLevel = offer.getOfferLevel();
                    if ((TextUtils.isEmpty(offerLevel) || offerLevel.equalsIgnoreCase(Constants.REGULAR_OFFER)) || user.getMembershipType().equalsIgnoreCase(Constants.PREMIUM_OFFER)) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.USER, user);
                        bundle.putParcelable(Constants.OFFER, offer);
                        bundle.putInt(Constants.POSITION, position);
                        switchFragment(OfferDetailFragment.newInstance(bundle));
                    } else {
                        showPremiumMembershipPrompt(getActivity(), offer);
                    }
                    break;
            }
        }
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            start = currentPage * pageSize;
            fetchDiscountOffers(start, pageSize, true);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(getContext(), refreshNetworkRequestsReceiver);
        PlayNetworkDatabase.destroyDatabaseInstance();
        super.onDestroy();
    }


    private BroadcastReceiver refreshNetworkRequestsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (NetworkUtils.isConnected(getContext())) {
                fetchDiscountOffers(0, pageSize, false);
            }
        }
    };

    private void showPremiumMembershipPrompt(final Activity activity, Offer offer) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(getString(R.string.become_premium_member_label));
        String formattedAmount = CurrencyUtils.formatToCurrencyWithSymbol(subscriptionAmount, Constants.CURRENCY_SYMBOL);
        builder.setMessage(getString(R.string.premium_membership_offer_prompt_content, offer.getTitle(), offer.getBusinessName(), formattedAmount, getString(R.string.default_subscription_tenor)));
        builder.setCancelable(true);
        builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.USER, user);
                bundle.putString(Constants.VIEW_TYPE, Constants.PREMIUM_SUBSCRIPTION_VIEW_TAG);
                bundle.putDouble(Constants.SUBSCRIPTION_AMOUNT, subscriptionAmount);
                intent.putExtras(bundle);
                startActivityForResult(intent, Constants.PREMIUM_SUBSCRIPTION_REQUEST_CODE);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onSubscriptionAmountFetched(SubscriptionAmountResponse subscriptionAmountResponse) {
        if (subscriptionAmountResponse != null && subscriptionAmountResponse.getAmount() > 0) {
            subscriptionAmount = subscriptionAmountResponse.getAmount();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.PREMIUM_SUBSCRIPTION_REQUEST_CODE) {
            user = playNetworkDatabase.userDao().findLastLoggedInUser();
        }
    }
}
