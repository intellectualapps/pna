package com.playnetwork.pna.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.NetworkResponse;
import com.playnetwork.pna.data.api.responses.SlimMemberResponse;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMemberFragment extends BaseFragment implements View.OnClickListener {

    private TextView memberFullName;
    private TextView memberJobTitle;
    private TextView memberIndustryLocation;
    private ImageView memberPhoto;
    private Button addToNetworkButton;
    private int position;
    private int memberId, networkId;
    private User user, member;

    public static Fragment newInstance(Bundle args) {
        AddMemberFragment frag = new AddMemberFragment();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(Constants.MEMBER)) {
            member = getArguments().getParcelable(Constants.MEMBER);
            position = getArguments().getInt(Constants.POSITION, 0);
            memberId = member.getId();
        }

        if (getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_member_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.add_member_toolbar_label));
        memberFullName = view.findViewById(R.id.member_full_name);
        memberJobTitle = view.findViewById(R.id.member_job_title);
        addToNetworkButton = view.findViewById(R.id.add_to_network_button);
        memberIndustryLocation = view.findViewById(R.id.member_industry_location);
        memberPhoto = view.findViewById(R.id.member_photo);

        loadMemberData(member);
        addToNetworkButton.setOnClickListener(this);
        fetchMemberSlimProfile();
    }

    private void loadMemberData(User member) {
        if (member != null) {
            memberFullName.setText(String.format("%s %s", TextUtils.isEmpty(member.getFname()) ? "" : member.getFname(), TextUtils.isEmpty(member.getLname()) ? "" : member.getLname()));
            memberJobTitle.setText(member.getJobTitle());

            if (!TextUtils.isEmpty(member.getFullName())) {
                memberFullName.setText(member.getFullName());
            }

            if (!TextUtils.isEmpty(member.getState())) {
                memberIndustryLocation.setText(String.format("%s | %s", member.getCompany(), member.getState()));
            } else {
                memberIndustryLocation.setText(TextUtils.isEmpty(member.getCompany()) ? "" : member.getCompany());
            }

            if (!TextUtils.isEmpty(member.getPic())) {
                String photoUrl = member.getPic().contains("http") ? member.getPic() : member.getUrl() + "/" + member.getPic();
                Picasso.with(getContext()).load(photoUrl).placeholder(R.color.colorGray).into(memberPhoto);
            } else {
                Picasso.with(getContext()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(memberPhoto);
            }
        }
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.request_processing_label);
        switch (v.getId()) {
            case R.id.add_to_network_button:
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    showLoadingIndicator(message);
                    String memberIds = String.valueOf(memberId);
                    Map<String, String> requestMap = new HashMap<String, String>();
                    requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
                    requestMap.put(Constants.MEMBER_IDS, memberIds);
                    getHttpService().addMembersToNetwork(requestMap, buildAddToNetworkCallbackCallback());
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
        }
    }

    private void fetchMemberSlimProfile() {
        if (NetworkUtils.isConnected(getContext())) {
            getHttpService().fetchSlimMemberProfile(String.valueOf(memberId), buildFetchMemberSlimProfileCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private Callback<SlimMemberResponse> buildFetchMemberSlimProfileCallback() {
        return new Callback<SlimMemberResponse>() {
            @Override
            public void onResponse(Call<SlimMemberResponse> call, Response<SlimMemberResponse> response) {
                SlimMemberResponse slimMemberResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (slimMemberResponse != null) {
                        User memberParam = new User();
                        memberParam.setFullName(slimMemberResponse.getFullName());
                        memberParam.setJobTitle(slimMemberResponse.getJobTitle());
                        memberParam.setCompany(slimMemberResponse.getCompany());
                        memberParam.setPic(slimMemberResponse.getProfilePhoto());
                        memberParam.setIndustry(slimMemberResponse.getIndustry());
                        memberParam.setState(slimMemberResponse.getState());
                        member = memberParam;
                        if (isVisible()) {
                            loadMemberData(member);
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.fetch_member_profile_error));
                    }
                }
            }

            @Override
            public void onFailure(Call<SlimMemberResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<NetworkResponse> buildAddToNetworkCallbackCallback() {
        return new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                NetworkResponse networkResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    showSuccessPopupMessage(getString(R.string.network_invitation_sent_label));
                    Intent intent = new Intent();
                    intent.setAction(Constants.UPDATE_MEMBER_LIST_INTENT_FILTER);
                    intent.putExtra(Constants.POSITION, position);
                    if (getContext() != null) {
                        getContext().sendBroadcast(intent);
                    }
                    closeFragment();
                }
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }
}
