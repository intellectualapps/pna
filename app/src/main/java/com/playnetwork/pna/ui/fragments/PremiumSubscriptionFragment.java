package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.enums.PaymentType;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.CurrencyUtils;


public class PremiumSubscriptionFragment extends BaseFragment implements View.OnClickListener {
    private User user;
    private Button proceedButton;
    private TextView subscriptionAmountView;
    private EditText memberFullNameInput, memberEmailAddressInput;
    private Double subscriptionAmount;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new PremiumSubscriptionFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public PremiumSubscriptionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
            subscriptionAmount = getArguments().getDouble(Constants.SUBSCRIPTION_AMOUNT, 0.00);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_premium_subscription, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.premium_subscription_label));
        proceedButton = view.findViewById(R.id.proceed_button);
        subscriptionAmountView = view.findViewById(R.id.subscription_amount);
        memberEmailAddressInput = view.findViewById(R.id.member_email_address);
        memberFullNameInput = view.findViewById(R.id.member_full_name);

        String formattedAmount = CurrencyUtils.formatToCurrencyWithSymbol(subscriptionAmount, Constants.CURRENCY_SYMBOL);
        subscriptionAmountView.setText(formattedAmount);
        proceedButton.setOnClickListener(this);

        loadMemberData();
    }

    private void loadMemberData() {
        memberEmailAddressInput.setText(user.getEmail());
        memberFullNameInput.setText(user.getFlname());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.proceed_button:
                if (validateTextInput(memberFullNameInput) && validateEmailInput(memberEmailAddressInput)) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.USER, user);
                    bundle.putString(Constants.FULL_NAME, memberFullNameInput.getText().toString().trim());
                    bundle.putString(Constants.EMAIL_ADDRESS, memberEmailAddressInput.getText().toString().trim());
                    bundle.putDouble(Constants.AMOUNT, subscriptionAmount);
                    bundle.putString(Constants.PAYMENT_TYPE, PaymentType.PREMIUM_SUBSCRIPTION.name());
                    bundle.putString(Constants.CONFIRMATION_CONTENT, getString(R.string.payment_confirmation_for_premium_subscription, user.getFlname()));
                    switchFragment(PaymentConfirmationFragment.newInstance(bundle));
                }
                break;
        }
    }
}
