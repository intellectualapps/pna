package com.playnetwork.pna.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.ChatSessionsResponse;
import com.playnetwork.pna.data.api.responses.MemberResponse;
import com.playnetwork.pna.data.models.ChatSession;
import com.playnetwork.pna.data.models.Experience;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.ProfilePicturesAdapter;
import com.playnetwork.pna.ui.adapters.UserExperienceAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.CustomTabUrlUtil;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemberProfileFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, Toolbar.OnMenuItemClickListener {

    private TextView memberFullName;
    private TextView memberJobTitle;
    private TextView memberIndustryLocation;
    private TextView memberExperienceSummary;
    private TextView memberSkillsSummary;
    private TextView memberLocationView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CustomRecyclerView profilePhotosRecyclerView, userExperienceRecyclerView;
    private TextView mEmptyViewLabel;
    private View mEmptyView;
    private ImageView memberPhoto;
    private Network network;
    private int position;
    private int memberId, networkId;
    private Button removeFromNetworkButton;
    private User user, member;
    private ProfilePicturesAdapter profilePicturesAdapter;
    private UserExperienceAdapter userExperienceAdapter;
    private List<String> profilePictureList;
    private List<Experience> userExperienceList;
    private ImageView facebookButton, twitterButton, instagramButton, websiteButton;
    private MenuItem editProfileMenuItem, chatMenuItem;

    public static Fragment newInstance(Bundle args) {
        MemberProfileFragment frag = new MemberProfileFragment();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(Constants.NETWORK)) {
            network = getArguments().getParcelable(Constants.NETWORK);
            position = getArguments().getInt(Constants.POSITION, 0);
            memberId = Integer.parseInt(network.getMemberId());
        }

        if (getArguments() != null && getArguments().containsKey(Constants.MEMBER)) {
            member = getArguments().getParcelable(Constants.MEMBER);
            position = getArguments().getInt(Constants.POSITION, 0);
            memberId = member.getId();
        }

        if (getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }
        profilePictureList = new ArrayList<String>();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.member_full_profile_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.member_profile_menu, menu);
        chatMenuItem = menu.findItem(R.id.action_chat);
        editProfileMenuItem = menu.findItem(R.id.action_edit_profile);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        checkIfUserViewingSelfProfile();
    }

    private void checkIfUserViewingSelfProfile() {
        if (memberId > 0 && (memberId == user.getId())) {
            editProfileMenuItem.setVisible(true);
            chatMenuItem.setVisible(false);
        } else {
            editProfileMenuItem.setVisible(false);
            chatMenuItem.setVisible(true);
        }
    }

    private void initializeToolbar(final Activity activity) {
        if (activity != null) {
            Toolbar toolbar = ((BaseActivity) activity).initializeToolbar(getString(R.string.member_details_toolbar_label));
            ((BaseActivity) activity).setSupportActionBar(toolbar);
            toolbar.setOnMenuItemClickListener(this);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) activity).navigateUp();
                }
            });
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        initializeToolbar(getActivity());
        memberFullName = view.findViewById(R.id.member_full_name);
        memberJobTitle = view.findViewById(R.id.member_job_title);
        memberLocationView = view.findViewById(R.id.member_location);
        memberIndustryLocation = view.findViewById(R.id.member_industry_location);
        memberExperienceSummary = view.findViewById(R.id.experience_summary);
        memberSkillsSummary = view.findViewById(R.id.member_skills_summary);
        memberPhoto = view.findViewById(R.id.member_photo);
        removeFromNetworkButton = view.findViewById(R.id.remove_from_network_button);
        profilePhotosRecyclerView = view.findViewById(R.id.profile_photos_recyclerview);
        userExperienceRecyclerView = view.findViewById(R.id.user_experience_recyclerview);
        mEmptyView = view.findViewById(R.id.profile_photos_empty_placeholder);
        facebookButton = view.findViewById(R.id.facebook_button);
        instagramButton = view.findViewById(R.id.instagram_button);
        twitterButton = view.findViewById(R.id.twitter_button);
        websiteButton = view.findViewById(R.id.website_button);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchMemberData();
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        profilePhotosRecyclerView.setEmptyView(mEmptyView);
        profilePicturesAdapter = new ProfilePicturesAdapter(getContext(), this);
        profilePhotosRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        profilePhotosRecyclerView.setAdapter(profilePicturesAdapter);
        profilePicturesAdapter.setItems(profilePictureList);

        userExperienceAdapter = new UserExperienceAdapter(getContext(), this);
        userExperienceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        userExperienceRecyclerView.setAdapter(userExperienceAdapter);
        userExperienceAdapter.setItems(userExperienceList);

        removeFromNetworkButton.setOnClickListener(this);
        facebookButton.setOnClickListener(this);
        instagramButton.setOnClickListener(this);
        websiteButton.setOnClickListener(this);
        twitterButton.setOnClickListener(this);
        if (network != null && !TextUtils.isEmpty(network.getMemberId())) {
            loadNetworkData(network);
        } else {
            loadMemberData(member);
        }
        fetchMemberData();
    }

    private void loadNetworkData(Network network) {
        if (network != null) {
            if (!TextUtils.isEmpty(network.getMemberId())) {
                memberId = Integer.parseInt(network.getMemberId());
            }
            memberFullName.setText(String.format("%s %s", network.getFirstName(), network.getLastName()));
            memberJobTitle.setText(network.getJobTitle());
            memberIndustryLocation.setText(String.format("%s | %s", network.getCompany(), network.getState()));
            if (!TextUtils.isEmpty(network.getProfilePhotoUrl())) {
                Picasso.with(getContext()).load(network.getProfilePhotoUrl()).placeholder(R.color.colorGray).into(memberPhoto);
            } else {
                Picasso.with(getContext()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(memberPhoto);
            }
        }
    }

    private void loadMemberData(User member) {
        if (member != null) {
            memberId = member.getId();
            memberFullName.setText(String.format("%s %s", member.getFname(), member.getLname()));
            memberJobTitle.setText(member.getJobTitle());
            if (!TextUtils.isEmpty(member.getState())) {
                memberIndustryLocation.setText(String.format("%s | %s", member.getCompany(), member.getState()));
            } else {
                memberIndustryLocation.setText(member.getCompany());
            }

            if (!TextUtils.isEmpty(member.getSkills())) {
                String skills = member.getSkills();
                skills = skills.replaceAll("\\s*,\\s*", "\n\n");
                memberSkillsSummary.setText(skills);
            }

            if (!ListUtils.isNotEmpty(member.getExperience())) {
                List<Experience> experienceList = member.getExperience();
                userExperienceAdapter.setItems(experienceList);
                memberExperienceSummary.setVisibility(View.GONE);
            } else {
                memberExperienceSummary.setVisibility(View.VISIBLE);
                memberExperienceSummary.setText(getString(R.string.no_experience_label));
            }

            if (!TextUtils.isEmpty(member.getProfilePics())) {
                String profilePics = member.getProfilePics();
                profilePictureList = Arrays.asList(profilePics.split("\\s*,\\s*"));
                profilePicturesAdapter.setItems(profilePictureList);
            }

            if (!TextUtils.isEmpty(member.getFacebook())) {
                facebookButton.setVisibility(View.VISIBLE);
            }

            String memberLocation = buildMemberLocation(member, PlayNetworkDatabase.getPlayNetworkDatabase());
            memberLocationView.setText(memberLocation);

            if (!TextUtils.isEmpty(member.getTwitter())) {
                twitterButton.setVisibility(View.VISIBLE);
            }

            if (!TextUtils.isEmpty(member.getInstagram())) {
                instagramButton.setVisibility(View.VISIBLE);
            }

            if (!TextUtils.isEmpty(member.getWebsite())) {
                websiteButton.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(member.getPic())) {
                String profilePictureUrl = member.getPic().contains("http") ? member.getPic() : member.getUrl().concat("/").concat(member.getPic()).replace("///", "//");
                Picasso.with(getContext()).load(profilePictureUrl).placeholder(R.color.colorGray).into(memberPhoto);
            } else {
                Picasso.with(getContext()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(memberPhoto);
            }
        }
    }

    private String buildMemberLocation(User user, PlayNetworkDatabase playNetworkDatabase) {
        String state = user.getState();
        String country = ListUtils.getCountryNameForCountryCode(user.getCountry(), playNetworkDatabase.countryDao().getAllCountries());
        String format = !TextUtils.isEmpty(state) && !TextUtils.isEmpty(country) ? "%s, %s" : "%s";
        format = "%s";
        return String.format(format, country);
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {
        if (position > -1) {
            String profilePhotoUrl = profilePictureList.get(position);
            showErrorPopupMessage("View photo pending");
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_chat:
                if (NetworkUtils.isNetworkConnected(getContext())) {
                    showLoadingIndicator(getString(R.string.chat_loading_message));
                    Map<String, String> map = new HashMap<String, String>();
                    map.put(Constants.MEMBER_ID_PARAM, String.valueOf(user.getId()));
                    map.put(Constants.OTHER_MEMBER_ID_PARAM, String.valueOf(member == null ? memberId : member.getId()));
                    getHttpService().initializeChatSession(map, buildInitializeChatCallback());
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
            case R.id.action_edit_profile:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.USER, user);
                switchFragment(UserProfileFragment.newInstance(bundle));
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.remove_from_network_button:
                break;
            case R.id.instagram_button:
                String instagramUsername = member.getInstagram().replace("@", "");
                CustomTabUrlUtil.launchInstagramProfile(getActivity(), instagramUsername);
                break;
            case R.id.twitter_button:
                String twitterUsername = member.getTwitter().replace("@", "");
                CustomTabUrlUtil.launchTwitterProfile(getActivity(), twitterUsername);
                break;
            case R.id.facebook_button:
                String facebookUsername = member.getFacebook().replace("@", "");
                CustomTabUrlUtil.launchFacebookProfile(getActivity(), facebookUsername);
                break;
            case R.id.website_button:
                String websiteUrl = member.getWebsite();
                CustomTabUrlUtil.launchWebsite(getActivity(), websiteUrl);
                break;
        }
    }

    private void fetchMemberData() {
        if (memberId > 0) {
            if (NetworkUtils.isConnected(getContext())) {
                animateSwipeRefreshLayout(swipeRefreshLayout);
                getHttpService().fetchMemberProfile(String.valueOf(user.getId()), String.valueOf(memberId), buildFetchMemberProfileCallback());
            } else {
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        }
    }

    private Callback<MemberResponse> buildFetchMemberProfileCallback() {
        return new Callback<MemberResponse>() {
            @Override
            public void onResponse(Call<MemberResponse> call, Response<MemberResponse> response) {
                MemberResponse memberResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                if (ApiUtils.isSuccessResponse(response)) {
                    if (memberResponse != null) {
                        member = memberResponse.getUser();
                        if (isVisible()) {
                            loadMemberData(member);
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.fetch_member_profile_error));
                    }
                }
            }

            @Override
            public void onFailure(Call<MemberResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<ChatSessionsResponse> buildInitializeChatCallback() {
        return new Callback<ChatSessionsResponse>() {
            @Override
            public void onResponse(Call<ChatSessionsResponse> call, Response<ChatSessionsResponse> response) {
                ChatSessionsResponse chatSessionsResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                if (ApiUtils.isSuccessResponse(response)) {
                    if (chatSessionsResponse != null && isVisible()) {
                        ChatSession chatSession = chatSessionsResponse.getChatSession();

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.USER, user);
                        bundle.putParcelable(Constants.CHAT_SESSION, chatSession);
                        bundle.putString(Constants.CHAT_ID, chatSession.getChatId());
                        bundle.putParcelable(Constants.OTHER_MEMBER, member);
                        bundle.putParcelable(Constants.REQUESTING_MEMBER, user);
                        switchFragment(MessagesFragment.newInstance(bundle));
                    } else {
                        showErrorPopupMessage(getString(R.string.chat_initialization_error));
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatSessionsResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
            }
        };
    }
}
