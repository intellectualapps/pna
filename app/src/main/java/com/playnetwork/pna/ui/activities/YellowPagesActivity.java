package com.playnetwork.pna.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.playnetwork.pna.R;
import com.playnetwork.pna.ui.fragments.YellowPageDetailFragment;
import com.playnetwork.pna.ui.fragments.YellowPagesFragment;
import com.playnetwork.pna.utils.Constants;

public class YellowPagesActivity extends BaseActivity {
    private Bundle fragmentBundle;
    private String viewType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yellow_pages);
        toolbar = findViewById(R.id.toolbar);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);

            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    default:
                    case Constants.YELLOW_PAGES_VIEW_TAG:
                        frag = YellowPagesFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.YELLOW_PAGE_DETAIL_VIEW_TAG:
                        frag = YellowPageDetailFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = YellowPagesFragment.newInstance(null);
            }

            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
