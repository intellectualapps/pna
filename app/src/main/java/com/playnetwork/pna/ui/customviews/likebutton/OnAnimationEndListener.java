package com.playnetwork.pna.ui.customviews.likebutton;

public interface OnAnimationEndListener {
    void onAnimationEnd(LikeButton likeButton);
}
