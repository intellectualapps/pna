package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.Transformers;

public class PaymentStatusFragment extends BaseFragment implements View.OnClickListener {
    private User user;
    private Event event;
    private Button closeButton;
    private String memberFullName, memberEmailAddress, membershipType;
    private TextView memberFullNameView, memberEmailAddressView, membershipTypeView, membershipTypeLabelView;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new PaymentStatusFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public PaymentStatusFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
            if (getArguments().containsKey(Constants.EVENT)) {
                event = getArguments().getParcelable(Constants.EVENT);
            }
            memberFullName = getArguments().getString(Constants.FULL_NAME, "");
            memberEmailAddress = getArguments().getString(Constants.EMAIL_ADDRESS, "");
            membershipType = getArguments().getString(Constants.MEMBERSHIP_TYPE, "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_status, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.payment_status_label));

        memberEmailAddressView = view.findViewById(R.id.member_email_address);
        memberFullNameView = view.findViewById(R.id.member_full_name);
        membershipTypeView = view.findViewById(R.id.membership_status);
        membershipTypeLabelView = view.findViewById(R.id.membership_status_label);

        closeButton = view.findViewById(R.id.close_button);
        closeButton.setOnClickListener(this);

        loadUserData();
    }

    private void loadUserData() {
        if (TextUtils.isEmpty(memberEmailAddress)) {
            memberEmailAddress = user.getEmail();
        }

        if (TextUtils.isEmpty(memberFullName)) {
            memberFullName = user.getFlname();
        }

        if (TextUtils.isEmpty(membershipType)) {
            membershipType = Transformers.capitalizeTag(user.getMembershipType());
        }

        memberFullNameView.setText(memberFullName);
        memberEmailAddressView.setText(memberEmailAddress);
        membershipTypeView.setText(membershipType);
        if (event != null) {
            membershipTypeView.setVisibility(View.GONE);
            membershipTypeLabelView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_button:
                if (getActivity() != null) {
                    getActivity().finish();
                }
                break;
        }
    }
}
