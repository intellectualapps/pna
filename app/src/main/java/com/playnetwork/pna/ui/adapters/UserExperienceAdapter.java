package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.Experience;
import com.playnetwork.pna.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class UserExperienceAdapter extends BaseRecyclerAdapter {

    private List<Experience> experienceList;
    private ClickListener clickListener;

    public UserExperienceAdapter(Context context, ClickListener clickListener) {
        super(context);
        this.clickListener = clickListener;
        this.experienceList = new ArrayList<Experience>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        view = mInflater.inflate(R.layout.experience_item_layout, parent, false);
        viewHolder = new UserExperienceAdapter.ViewHolder(view, this.clickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Experience experience = experienceList.get(position);
        final UserExperienceAdapter.ViewHolder viewHolder = (UserExperienceAdapter.ViewHolder) holder;
        if(experience != null){
            viewHolder.companyName.setText(experience.getEmployer());
            viewHolder.position.setText(experience.getPosition());
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(experienceList) ? 0 : experienceList.size();
    }

    /*
    Helpers
    */

    public void setItems(List<Experience> items) {
        this.experienceList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (experienceList != null) {
            this.experienceList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Experience experience) {
        if (experience != null) {
            this.experienceList.add(experience);
            notifyDataSetChanged();
        }
    }


    public void removeItem(Experience experience) {
        int position = experienceList.indexOf(experience);
        if (position > -1) {
            experienceList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeItem(int position) {
        if (position > -1) {
            experienceList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Experience getItem(int position) {
        return experienceList.get(position);
    }


    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView companyName, position;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            companyName =  itemView.findViewById(R.id.company_name);
            position =  itemView.findViewById(R.id.position);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }
}