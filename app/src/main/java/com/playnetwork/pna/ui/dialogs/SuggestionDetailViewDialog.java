package com.playnetwork.pna.ui.dialogs;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.utils.Constants;
import com.squareup.picasso.Picasso;

public class SuggestionDetailViewDialog extends BaseDialogFragment implements View.OnClickListener {

    private TextView suggestionFullName;
    private TextView suggestionJobTitle;
    private TextView suggestionIndustryLocation;
    private ImageView suggestionPhoto, closeDialogButton;
    private User suggestion;
    private int position;
    private boolean isSelected;
    private Button selectSuggestionButton;
    private PNAInterfaces.SuggestionSelectionListener listener;

    public static DialogFragment getInstance(Bundle args) {
        SuggestionDetailViewDialog frag = new SuggestionDetailViewDialog();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);

        if (getArguments() != null && getArguments().containsKey(Constants.SUGGESTION)) {
            suggestion = getArguments().getParcelable(Constants.SUGGESTION);
            position = getArguments().getInt(Constants.POSITION, 0);
            isSelected = getArguments().getBoolean(Constants.IS_SELECTED, false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        View view = inflater.inflate(R.layout.network_suggestion_item_full_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        suggestionFullName = view.findViewById(R.id.suggestion_full_name);
        suggestionJobTitle = view.findViewById(R.id.suggestion_job_title);
        suggestionIndustryLocation = view.findViewById(R.id.suggestion_industry_location);
        suggestionPhoto = view.findViewById(R.id.suggestion_photo);
        closeDialogButton = view.findViewById(R.id.close_dialog);
        selectSuggestionButton = view.findViewById(R.id.select_suggestion_button);

        selectSuggestionButton.setOnClickListener(this);
        closeDialogButton.setOnClickListener(this);
        loadSuggestionData(suggestion);
    }

    private void loadSuggestionData(User suggestion) {
        if (suggestion != null) {
            suggestionFullName.setText(String.format("%s %s", suggestion.getFname(), suggestion.getLname()));
            suggestionJobTitle.setText(suggestion.getJobTitle());
            suggestionIndustryLocation.setText(suggestion.getCompany());
            selectSuggestionButton.setText(isSelected ? getString(R.string.remove_selection) : getString(R.string.select_suggestion_label));
            if (!TextUtils.isEmpty(suggestion.getPic()) && !TextUtils.isEmpty(suggestion.getUrl())) {
                String photoUrl = suggestion.getUrl() + "/" + suggestion.getPic();
                Picasso.with(getContext()).load(photoUrl).placeholder(R.color.colorGray).into(suggestionPhoto);

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.select_suggestion_button:
                if (listener != null) {
                    listener.onSuggestionSelected(position, suggestion.getId());
                }
                dismiss();
                break;
            case R.id.close_dialog:
                dismiss();
                break;
        }
    }

    public void setSuggestionSelectedListener(PNAInterfaces.SuggestionSelectionListener suggestionSelectedListener) {
        this.listener = suggestionSelectedListener;
    }
}
