package com.playnetwork.pna.ui.fragments;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.CommentResponse;
import com.playnetwork.pna.data.api.responses.FeedCommentResponse;
import com.playnetwork.pna.data.api.responses.LikeFeedResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Comment;
import com.playnetwork.pna.data.models.Feed;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.ProfileActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.CommentsAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.ui.customviews.likebutton.LikeButton;
import com.playnetwork.pna.ui.customviews.likebutton.OnLikeListener;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedDetailsFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, PNAInterfaces.PaginationAdapterCallback, TextWatcher, OnLikeListener {
    private User user;
    private CommentsAdapter commentsAdapter;
    private CustomRecyclerView commentsRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private TextView authorFullNameView, feedDateView, feedContentView, feedDescriptionView, feedLikesCountView;
    private ImageView authorPhotoView, feedContentBanner, likeButton;
    private View mEmptyView;
    private Toolbar toolbar;
    private EditText commentInput;
    private ImageButton saveCommentButton;
    private ProgressBar saveCommentProgressBar;
    private PlayNetworkDatabase playNetworkDatabase;
    private Feed feed;
    private LikeButton favouriteButton;

    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private int start = 0, pageSize = 10;
    private int currentPage = start;
    private String feedId;
    private String email;

    private List<Comment> commentList;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new FeedDetailsFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public FeedDetailsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        commentList = new ArrayList<Comment>();

        if (getArguments() != null && getArguments().containsKey(Constants.FEED)) {
            feed = getArguments().getParcelable(Constants.FEED);
        } else {
            showErrorPopupMessage(getString(R.string.generic_error_message));
            closeFragment();
        }

        feedId = String.valueOf(feed.getFeedId());
        email = user.getEmail();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_details, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.feed_details_toolbar_label));
        commentsRecyclerView = (CustomRecyclerView) view.findViewById(R.id.comments_recyclerview);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        authorFullNameView = view.findViewById(R.id.author_full_name);
        feedDateView = view.findViewById(R.id.feed_date);
        feedContentView = view.findViewById(R.id.feed_content_view);
        feedDescriptionView = view.findViewById(R.id.feed_description);
        feedLikesCountView = view.findViewById(R.id.feed_likes_count);
        favouriteButton = view.findViewById(R.id.favourite_button);

        commentInput = view.findViewById(R.id.compose_comment_input);
        saveCommentButton = view.findViewById(R.id.save_comment_button);
        saveCommentProgressBar = view.findViewById(R.id.save_comment_progress_bar);

        authorPhotoView = view.findViewById(R.id.author_photo_thumbnail);
        feedContentBanner = view.findViewById(R.id.feed_content_banner);
        likeButton = view.findViewById(R.id.like_button);

        likeButton.setOnClickListener(this);
        authorFullNameView.setOnClickListener(this);
        authorPhotoView.setOnClickListener(this);
        feedContentBanner.setOnClickListener(this);
        saveCommentButton.setOnClickListener(this);
        favouriteButton.setOnLikeListener(this);

        commentInput.addTextChangedListener(this);

        mEmptyView = view.findViewById(R.id.empty_view);
        commentsRecyclerView.setEmptyView(mEmptyView);
        commentsAdapter = new CommentsAdapter(getContext(), this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        commentsRecyclerView.setLayoutManager(linearLayoutManager);
        commentsRecyclerView.setAdapter(commentsAdapter);
        commentsRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (commentList != null && commentList.size() >= pageSize) {
                            fetchComments(feed.getFeedId(), start, pageSize, true, false);
                        }
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getContext())) {
                        fetchComments(feed.getFeedId(), 0, pageSize, false, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        animateSwipeRefreshLayout(swipeRefreshLayout);
        loadFeedData(this.feed, true);
        if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            fetchComments(feed.getFeedId(), start, pageSize, false, false);
        } else {
            commentsAdapter.setItems(commentList);
        }
    }

    private void loadFeedData(Feed feed, boolean playAnimation) {
        if (feed != null) {
            feedDateView.setText(DateUtils.timeSincePosted(feed.getFeedDate()));
            feedDescriptionView.setText(feed.getDescription());
            authorFullNameView.setText(feed.getAuthor());
            if (feed.getLikes() > 0) {
                feedLikesCountView.setText(String.valueOf(feed.getLikes()));
                adjustLikeButtonIcon(feed.isHasUserLiked());
                favouriteButton.setLiked(feed.isHasUserLiked());
                if (feed.isHasUserLiked() && playAnimation) {
                    favouriteButton.playAnimation();
                }
            } else {
                feedLikesCountView.setText("");
                adjustLikeButtonIcon(false);
                favouriteButton.setLiked(false);
            }

            Picasso.with(getContext()).load(feed.getFeedAuthorPicture()).placeholder(R.color.colorGray).into(authorPhotoView);

            if (!TextUtils.isEmpty(feed.getFeedAuthorPicture())) {
                Picasso.with(getContext()).load(feed.getFeedAuthorPicture()).placeholder(R.color.colorGray).into(authorPhotoView);
            } else {
                Picasso.with(getContext()).load(R.drawable.avatar).placeholder(R.color.colorGray).into(authorPhotoView);
            }

            switch (feed.getType()) {
                case "pic":
                    feedContentBanner.setVisibility(View.VISIBLE);
                    feedContentView.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(feed.getContent())) {
                        Picasso.with(getContext()).load(feed.getContent()).placeholder(R.color.colorGray).into(feedContentBanner);
                    }
                    break;
                case "share":
                    feedContentBanner.setVisibility(View.GONE);
                    feedContentView.setVisibility(View.VISIBLE);
                    feedContentView.setText(feed.getContent());

                    switch (feed.getShareType()) {
                        default:
                        case "text":
                            feedContentBanner.setVisibility(View.GONE);
                            feedContentView.setVisibility(View.VISIBLE);
                            feedContentView.setText(feed.getContent());
                            break;
                        case "picture":
                            feedContentBanner.setVisibility(View.VISIBLE);
                            feedContentView.setVisibility(View.GONE);
                            Picasso.with(getContext()).load(feed.getContent()).placeholder(R.color.colorGray).into(feedContentBanner);
                            break;
                        case "both":
                            feedContentBanner.setVisibility(View.VISIBLE);
                            feedContentView.setVisibility(View.VISIBLE);
                            Picasso.with(getContext()).load(feed.getContent()).placeholder(R.color.colorGray).into(feedContentBanner);
                            feedContentView.setText(feed.getAltContent());
                            break;
                    }
                    break;
                default:
                    feedContentBanner.setVisibility(View.GONE);
                    feedContentView.setVisibility(View.VISIBLE);
                    feedContentView.setText(feed.getContent());
                    break;
            }
        }
    }

    private void fetchComments(int feedId, int start, int pageSize, boolean inLoadMoreMode, boolean scrollDownToEnd) {
        if (!inLoadMoreMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }
        getHttpService().fetchFeedComments(String.valueOf(feedId), buildParameterMap(start, pageSize, user), buildFetchCommentsCallback(inLoadMoreMode, scrollDownToEnd));
    }

    private void adjustSendButtonState(boolean stateFlag) {
        if (getContext() != null) {
            if (stateFlag) {
                saveCommentButton.setEnabled(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    saveCommentButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorDarkOrange), PorterDuff.Mode.SRC_IN);
                } else {
                    Drawable wrapDrawable = DrawableCompat.wrap(saveCommentButton.getDrawable());
                    DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(getContext(), R.color.colorDarkOrange));
                    saveCommentButton.setImageDrawable(DrawableCompat.unwrap(wrapDrawable));
                }
            } else {
                saveCommentButton.setEnabled(false);
                if (getContext() != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        saveCommentButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorDark), PorterDuff.Mode.SRC_IN);
                    } else {
                        Drawable wrapDrawable = DrawableCompat.wrap(saveCommentButton.getDrawable());
                        DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(getContext(), R.color.colorDark));
                        saveCommentButton.setImageDrawable(DrawableCompat.unwrap(wrapDrawable));
                    }
                }
            }
        }
    }

    private void showProgressBar(boolean showProgressBar) {
        if (isVisible()) {
            saveCommentProgressBar.setVisibility(showProgressBar ? View.VISIBLE : View.GONE);
            saveCommentButton.setVisibility(showProgressBar ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.like_button:
                if (NetworkUtils.isConnected(getContext())) {
                    adjustLikeButtonIcon(!feed.isHasUserLiked());
                    getHttpService().updateFeedLikeStatus(feedId, email, buildLikeFeedCallback());
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
            case R.id.save_comment_button:
                if (NetworkUtils.isConnected(getContext())) {
                    if (commentInput.getText().toString().trim().length() > 0) {
                        this.showProgressBar(true);
                        final String comment = commentInput.getText().toString().trim();
                        Map<String, String> requestMap = new HashMap<String, String>();
                        requestMap.put(Constants.EMAIL_ADDRESS, email);
                        requestMap.put(Constants.COMMENT, comment);
                        getHttpService().createComment(feedId, requestMap, buildCreateCommentCallback());
                    } else {
                        showErrorPopupMessage(getString(R.string.blank_message_label));
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
            case R.id.author_photo_thumbnail:
            case R.id.author_full_name:
                openProfileViewFromFeed(feed);
                break;
        }
    }

    private void adjustLikeButtonIcon(boolean stateFlag) {
        likeButton.setImageDrawable(ContextCompat.getDrawable(getPlayNetworkApplicationContext(), stateFlag ? R.drawable.ic_liked_state : R.drawable.ic_unliked_state));
    }

    private void updateFeedItem(Feed feed) {
        feed.setLikes(feed.isHasUserLiked() ? feed.getLikes() - 1 : feed.getLikes() + 1);
        feed.setHasUserLiked(!feed.isHasUserLiked());
        loadFeedData(feed, true);
    }

    private void updateFeedList(List<Comment> commentListParam) {
        if (commentList == null) {
            commentList = new ArrayList<Comment>();
        }
        if (commentListParam != null && commentListParam.size() > 0) {
            commentList.addAll(commentListParam);
        }
    }

    private void loadAdapter(List<Comment> commentList, boolean scrollDownToEnd) {
        commentsAdapter.addAll(commentList);
        if (scrollDownToEnd) {
            commentsRecyclerView.smoothScrollToPosition(this.commentList.size() + 1);
        }
    }

    private Callback<CommentResponse> buildCreateCommentCallback() {
        return new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, Response<CommentResponse> response) {
                CommentResponse commentResponse = response.body();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showProgressBar(false);

                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    commentInput.setText(null);
                    fetchComments(feed.getFeedId(), commentList.size(), pageSize, true, true);
                }
            }

            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
                showProgressBar(false);
            }
        };
    }

    private Callback<LikeFeedResponse> buildLikeFeedCallback() {
        return new Callback<LikeFeedResponse>() {
            @Override
            public void onResponse(Call<LikeFeedResponse> call, Response<LikeFeedResponse> response) {
                LikeFeedResponse likeFeedResponse = response.body();
                endSwipeRefreshLayout(swipeRefreshLayout);

                if (ApiUtils.isSuccessResponse(response)) {
                    feed.setHasUserLiked(likeFeedResponse.isHasUserLiked());
                    feed.setLikes(likeFeedResponse.getLikes());
                    if (isVisible()) {
                        loadFeedData(feed, false);
                    }
                } else {
                    favouriteButton.setLiked(feed.isHasUserLiked());
                    if (isVisible()) {
                        adjustLikeButtonIcon(feed.isHasUserLiked());
                    }
                }
            }

            @Override
            public void onFailure(Call<LikeFeedResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
                adjustLikeButtonIcon(feed.isHasUserLiked());
            }
        };
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1) {
            Comment comment = commentList.get(position);
            switch (view.getId()) {
                default:
                    break;
                case R.id.comment_author_full_name:
                case R.id.comment_author_photo:
                    openProfileViewFromComment(comment, position);
                    break;
            }
        }
    }

    private void openProfileViewFromComment(Comment comment, int position) {
        Network network = new Network();
        String[] fullName = comment.getAuthor().split(" ");
        network.setFirstName(fullName[0]);
        network.setLastName(fullName[1]);
        network.setMemberId(String.valueOf(comment.getMemberId()));
        network.setProfilePhotoUrl(comment.getCommentAuthorPicture());

        User member = new User();
        member.setId(comment.getMemberId());
        member.setFname(fullName[0]);
        member.setLname(fullName[1]);
        member.setPic(comment.getCommentAuthorPicture());
        launchProfileIntent(user, member, network, comment.getNetworkState());
    }

    private void openProfileViewFromFeed(Feed feed) {
        Network network = new Network();
        String[] fullName = feed.getAuthor().split(" ");
        network.setFirstName(fullName[0]);
        network.setLastName(fullName[1]);
        network.setMemberId(String.valueOf(feed.getMemberId()));
        network.setProfilePhotoUrl(feed.getFeedAuthorPicture());

        User member = new User();
        member.setId(feed.getMemberId());
        member.setFname(fullName[0]);
        member.setLname(fullName[1]);
        member.setPic(feed.getFeedAuthorPicture());
        launchProfileIntent(user, member, network, feed.getNetworkState());
    }

    private void launchProfileIntent(User user, User member, Network network, String networkState) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.USER, user);
        bundle.putParcelable(Constants.NETWORK, network);
        bundle.putParcelable(Constants.MEMBER, member);
        Intent intent = new Intent(getContext(), ProfileActivity.class);

        if (!TextUtils.isEmpty(networkState)) {
            switch (networkState) {
                case Constants.PENDING_NETWORK_STATE:
                    bundle.putString(Constants.VIEW_TYPE, Constants.PENDING_PROFILE_VIEW_TAG);
                    break;
                case Constants.ACCEPTED_NETWORK_STATE:
                    bundle.putString(Constants.VIEW_TYPE, Constants.MEMBER_PROFILE_VIEW_TAG);
                    break;
                case Constants.NONE_NETWORK_STATE:
                default:
                    bundle.putString(Constants.VIEW_TYPE, Constants.ADD_MEMBER_VIEW_TAG);
                    break;
            }
        } else {
            bundle.putString(Constants.VIEW_TYPE, Constants.ADD_MEMBER_VIEW_TAG);
        }

        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            start = currentPage * pageSize;
            fetchComments(feed.getFeedId(), start, pageSize, true, false);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length() > 0 && !TextUtils.isEmpty(charSequence)) {
            adjustSendButtonState(true);
        } else {
            adjustSendButtonState(false);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void liked(LikeButton likeButton) {
        if (NetworkUtils.isConnected(getContext())) {
            updateFeedItem(feed);
            getHttpService().updateFeedLikeStatus(feedId, email, buildLikeFeedCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void unLiked(LikeButton likeButton) {
        if (NetworkUtils.isConnected(getContext())) {
            updateFeedItem(feed);
            getHttpService().updateFeedLikeStatus(feedId, email, buildLikeFeedCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private Callback<FeedCommentResponse> buildFetchCommentsCallback(final boolean inLoadMoreMode, final boolean scrollDownToEnd) {
        return new Callback<FeedCommentResponse>() {
            @Override
            public void onResponse(Call<FeedCommentResponse> call, Response<FeedCommentResponse> response) {
                FeedCommentResponse feedCommentResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                isLoading = false;
                if (inLoadMoreMode && isVisible()) {
                    commentsAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && isVisible()) {
                    List<Comment> commentListParam = feedCommentResponse.getFeedComments();

                    if (commentListParam == null || commentListParam.size() == 0 || commentListParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (commentList != null) {
                            commentList.clear();
                        }
                        commentsAdapter.clearItems();
                    }

                    updateFeedList(commentListParam);

                    loadAdapter(commentListParam, scrollDownToEnd);

                    if (!hasListEnded) {
                        commentsAdapter.addLoadingFooter();
                    }
                }
            }

            @Override
            public void onFailure(Call<FeedCommentResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    commentsAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }
}
