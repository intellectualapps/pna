package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.ApiModule;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.NetworkUtils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupFragment extends BaseFragment implements View.OnClickListener {
    private User user;
    private EditText emailAddressInput, passwordInput, firstNameInput, lastNameInput, phoneNumberInput;
    private Button continueButton, loginButton;
    private Toolbar toolbar;
    private PlayNetworkDatabase playNetworkDatabase;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new SignupFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public SignupFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        emailAddressInput = view.findViewById(R.id.email_address);
        firstNameInput = view.findViewById(R.id.first_name);
        lastNameInput = view.findViewById(R.id.last_name);
        phoneNumberInput = view.findViewById(R.id.phone_number);
        passwordInput = view.findViewById(R.id.password);
        continueButton = view.findViewById(R.id.continue_button);
        loginButton = view.findViewById(R.id.login_button);
        continueButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.sign_up_progress_label);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.continue_button:
                if (validateFields(new EditText[]{firstNameInput, lastNameInput}) && validateEmailInput(emailAddressInput) && validatePhoneNumber(phoneNumberInput) && validateTextInput(passwordInput)) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        getHttpService().createAccount(getUserInput(), buildAuthenticateCallback(getUserInput().get(Constants.PASSWORD)));
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.login_button:
                replaceFragment(LoginFragment.newInstance(null));
                break;
        }
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String password = passwordInput.getText().toString().trim();
        String firstName = firstNameInput.getText().toString().trim();
        String lastName = lastNameInput.getText().toString().trim();
        String phoneNumber = phoneNumberInput.getText().toString().trim();
        String emailAddress = emailAddressInput.getText().toString().trim();
        map.put(Constants.FIRST_NAME, firstName);
        map.put(Constants.LAST_NAME, lastName);
        map.put(Constants.PHONE_NUMBER, phoneNumber);
        map.put(Constants.PASSWORD, password);
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        map.put(Constants.AUTH_TYPE, Constants.EMAIL_AUTH_TYPE);
        return map;
    }

    @NonNull
    @org.jetbrains.annotations.Contract(pure = true)
    private Callback<AuthenticationResponse> buildAuthenticateCallback(final String password) {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    HttpService.resetApiService();
                    ApiModule.resetApiClient();
                    clearUserData(playNetworkDatabase);
                    saveUserData(playNetworkDatabase, authenticationResponse, password, null);
                    if (isVisible()) {
                        replaceFragmentWithoutBackStack(CreateProfileFragment.newInstance(null));
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }
}
