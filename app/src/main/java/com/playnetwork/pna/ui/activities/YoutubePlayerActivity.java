package com.playnetwork.pna.ui.activities;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.playnetwork.pna.BuildConfig;
import com.playnetwork.pna.R;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ToastUtil;


public class YoutubePlayerActivity extends YouTubeFailureRecoveryActivity implements View.OnClickListener, YouTubePlayer.OnFullscreenListener {
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private static final int PORTRAIT_ORIENTATION = Build.VERSION.SDK_INT < 9
            ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            : ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;

    private LinearLayout baseLayout;
    private YouTubePlayerView playerView;
    private YouTubePlayer player;
    private String videoId;
    private boolean fullscreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.youtube_player_layout);
        baseLayout = findViewById(R.id.layout);
        playerView = findViewById(R.id.player);
        playerView.initialize(BuildConfig.YOUTUBE_API_KEY, this);
        if (getIntent() != null && getIntent().getExtras().containsKey(Constants.VIDEO_ID)) {
            videoId = getIntent().getStringExtra(Constants.VIDEO_ID);
        } else {
            ToastUtil.showErrorToast(this, getString(R.string.error_player));
            finish();
        }
        doLayout();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        this.player = player;
        player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
        player.setOnFullscreenListener(this);
        if (!wasRestored) {
            player.cueVideo(videoId);
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return playerView;
    }

    @Override
    public void onClick(View v) {
        player.setFullscreen(!fullscreen);
    }

    private void doLayout() {
        LinearLayout.LayoutParams playerParams =
                (LinearLayout.LayoutParams) playerView.getLayoutParams();
        if (fullscreen) {
            playerParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
            playerParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        }
    }

    @Override
    public void onFullscreen(boolean isFullscreen) {
        fullscreen = isFullscreen;
        doLayout();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        doLayout();
    }
}
