package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NetworkSuggestionsAdapter extends MultiSelectableBaseAdapter {

    private List<User> suggestionList;
    private ClickListener clickListener;
    private final int SUGGESTION = 0, MY_NETWORK = 2;
    private int adapterMode = 1;

    public NetworkSuggestionsAdapter(Context context, ClickListener clickListener, PNAInterfaces.PaginationAdapterCallback paginationAdapterCallback, int adapterMode) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.adapterMode = adapterMode;
        this.suggestionList = new ArrayList<User>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;

        switch (viewType) {
            default:
            case SUGGESTION:
                view = mInflater.inflate(R.layout.network_suggestion_item_layout, parent, false);
                viewHolder = new NetworkSuggestionsAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        User user = suggestionList.get(position);
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            default:
                final NetworkSuggestionsAdapter.ViewHolder viewHolder = (NetworkSuggestionsAdapter.ViewHolder) holder;
                if (user != null) {
                    if (isSelected(position)) {
                        viewHolder.selectSuggestion.setVisibility(View.VISIBLE);
                    } else {
                        viewHolder.selectSuggestion.setVisibility(View.GONE);
                    }
                    viewHolder.suggestionFullName.setText(String.format("%s %s", user.getFname(), user.getLname()));
                    viewHolder.suggestionJobTitle.setText(user.getJobTitle());
                    if (user.getPic() != null && user.getUrl() != null) {
                        String photoUrl = user.getUrl() + "/" + user.getPic();
                        Picasso.with(context).load(photoUrl).placeholder(R.color.colorGray).into(viewHolder.suggestionProfilePhoto);
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(suggestionList) ? 0 : suggestionList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == suggestionList.size() - 1 && isLoadingAdded) {
            return LOADING;
        }

        return (adapterMode == Constants.MY_NETWORK_MODE) ? MY_NETWORK : SUGGESTION;
    }

    /*
    Helpers
    */

    public void setItems(List<User> items) {
        this.suggestionList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (suggestionList != null) {
            this.suggestionList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(User user) {
        if (user != null) {
            this.suggestionList.add(user);
            notifyDataSetChanged();
        }
    }

    public void add(User suggestion) {
        suggestionList.add(suggestion);
        notifyItemInserted(suggestionList.size() - 1);
    }

    public void addAll(List<User> suggestionList) {
        if (suggestionList != null && suggestionList.size() > 0) {
            for (User suggestion : suggestionList) {
                add(suggestion);
            }
        }
        notifyDataSetChanged();
    }

    public void remove(User suggestion) {
        int position = suggestionList.indexOf(suggestion);
        if (position > -1) {
            suggestionList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private User getItem(int position) {
        return suggestionList.get(position);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new User());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = suggestionList.size() - 1;
        if (position > -1) {
            User suggestion = getItem(position);

            if (suggestion != null) {
                if (suggestion.getId() < 1) {
                    suggestionList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView suggestionFullName, suggestionJobTitle;
        ImageView suggestionProfilePhoto;
        ImageView selectSuggestion;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            suggestionFullName = (TextView) itemView.findViewById(R.id.suggestion_full_name);
            suggestionJobTitle = (TextView) itemView.findViewById(R.id.suggestion_job_title);
            suggestionProfilePhoto = (ImageView) itemView.findViewById(R.id.suggestion_photo);
            selectSuggestion = (ImageView) itemView.findViewById(R.id.select_suggestion);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            selectSuggestion.setOnClickListener(this);
            suggestionFullName.setOnClickListener(this);
            suggestionJobTitle.setOnClickListener(this);
            suggestionProfilePhoto.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(suggestionList.size() - 1);
        if (errorMsg != null) this.errorMsg = errorMsg;
    }
}