package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.YellowPage;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class YellowPagesAdapter extends BaseRecyclerAdapter {
    private List<YellowPage> yellowPagesList;
    private ClickListener clickListener;
    private final int YELLOW_PAGE = 2;
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    public YellowPagesAdapter(Context context, ClickListener clickListener, PNAInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.yellowPagesList = new ArrayList<YellowPage>();
        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .endConfig()
                .round();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;

        switch (viewType) {
            default:
            case YELLOW_PAGE:
                view = mInflater.inflate(R.layout.yellow_page_item_layout, parent, false);
                viewHolder = new YellowPagesAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            default:
                final YellowPagesAdapter.ViewHolder viewHolder = (YellowPagesAdapter.ViewHolder) holder;
                YellowPage yellowPage = yellowPagesList.get(position);
                if (yellowPage != null && yellowPage.getId() > 0) {
                    viewHolder.businessNameView.setText(stripHtml(yellowPage.getBusinessName()));
                    viewHolder.businessAddressView.setText(yellowPage.getBusinessAddress());
                    viewHolder.businessCategoryView.setText(yellowPage.getBusinessCategory());

                    if (!TextUtils.isEmpty(yellowPage.getBusinessLogo())) {
                        Picasso.with(context).load(yellowPage.getBusinessLogo()).placeholder(R.color.colorGray).into(viewHolder.businessLogoView);
                    } else {
                        Picasso.with(context).load(R.drawable.play_africa_logo).placeholder(R.color.colorGray).into(viewHolder.businessLogoView);
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(yellowPagesList) ? 0 : yellowPagesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        YellowPage yellowPage = yellowPagesList.get(position);
        if (position == yellowPagesList.size() - 1 && isLoadingAdded && (yellowPage != null && TextUtils.isEmpty(yellowPage.getBusinessName()))) {
            return LOADING;
        }

        return YELLOW_PAGE;
    }

    /*
    Helpers
    */

    public void setItems(List<YellowPage> items) {
        this.yellowPagesList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (yellowPagesList != null) {
            this.yellowPagesList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(YellowPage yellowPage) {
        if (yellowPage != null) {
            this.yellowPagesList.add(yellowPage);
            notifyDataSetChanged();
        }
    }

    public void add(YellowPage yellowPage) {
        if (yellowPagesList == null) {
            yellowPagesList = new ArrayList<YellowPage>();
        }
        yellowPagesList.add(yellowPage);
        notifyItemInserted(yellowPagesList.size() - 1);
    }

    public void addAll(List<YellowPage> yellowPagesList) {
        if (ListUtils.isNotEmpty(yellowPagesList)) {
            for (YellowPage yellowPage : yellowPagesList) {
                add(yellowPage);
            }
        }
        notifyDataSetChanged();
    }

    public void remove(YellowPage yellowPage) {
        int position = yellowPagesList.indexOf(yellowPage);
        if (position > -1) {
            yellowPagesList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(int position) {
        if (position > -1) {
            yellowPagesList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private YellowPage getItem(int position) {
        return yellowPagesList.get(position);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new YellowPage());
    }

    public List<YellowPage> getYellowPagesList() {
        return yellowPagesList;
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = yellowPagesList.size() - 1;
        if (position > -1) {
            YellowPage yellowPage = getItem(position);

            if (yellowPage != null) {
                if (TextUtils.isEmpty(yellowPage.getBusinessName()) && yellowPage.getId() < 1) {
                    yellowPagesList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView businessNameView, businessAddressView, businessCategoryView;
        ImageView businessLogoView;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            businessNameView = itemView.findViewById(R.id.business_name);
            businessAddressView = itemView.findViewById(R.id.business_address);
            businessCategoryView = itemView.findViewById(R.id.business_category);
            businessLogoView = itemView.findViewById(R.id.business_logo);
            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);

            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(yellowPagesList.size() - 1);
        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    private String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }
}