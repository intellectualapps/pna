package com.playnetwork.pna.ui.customviews;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Custom ImageView so that it is the optimal height for a movie poster
 */
public class CustomGridItemImageView extends AppCompatImageView {
    private static final float POSTER_WIDTH_RATIO_TO_HEIGHT = 3f/3;

    public CustomGridItemImageView(Context context) {
        super(context);
    }

    public CustomGridItemImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomGridItemImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomGridItemImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        //The height should be 33% longer than the width.
        float newMeasuredHeight = getMeasuredWidth() / POSTER_WIDTH_RATIO_TO_HEIGHT;
        setMeasuredDimension(getMeasuredWidth(), (int)newMeasuredHeight);
    }
}
