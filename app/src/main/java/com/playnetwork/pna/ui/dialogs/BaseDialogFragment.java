package com.playnetwork.pna.ui.dialogs;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.AppUtils;
import com.playnetwork.pna.utils.ToastUtil;
import com.playnetwork.pna.utils.Validator;


public class BaseDialogFragment extends DialogFragment {
    private ProgressDialog progressDialog;
    public static String ARG_ARGS = "args";
    private HttpService httpService;
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 103;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
    }

    public boolean validateFields(EditText[] views) {
        return Validator.validateInputViewsNotEmpty(views);
    }

    public HttpService getHttpService() {
        if (httpService == null) {
            httpService = new HttpService();
        }

        return httpService;
    }

    public void showLoadingIndicator(String message) {

        try {
            if (isVisible() && getActivity() != null) {
                ((BaseActivity) getActivity()).showLoadingIndicator(message);
            }
        } catch (Exception ignored) {
        }
    }

    public void hideLoadingIndicator() {
        try {
            if (isVisible() && getActivity() != null) {
                ((BaseActivity) getActivity()).hideLoadingIndicator();
            }
        } catch (Exception ignored) {
        }
    }

    public void showKeyboard() {
        if (getActivity() != null) {
            View view = getActivity().getCurrentFocus();
            if (view != null && view instanceof EditText) {
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputManager != null) {
                    inputManager.showSoftInput(view, 0);
                }
            }
        }
    }

    public boolean checkFileAccessPermission(final Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(getString(R.string.permission_request_dialog_title));
                    builder.setMessage(getString(R.string.storage_permission_rationale));
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel, null);
                    builder.show();

                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            STORAGE_PERMISSION_REQUEST_CODE);
                }

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public void hideKeyboard() {
        if (getActivity() != null) {
            AppUtils.hideKeyboard(getActivity());
        }
    }

    public void closeFragment() {
        if (getActivity() != null) {
            hideKeyboard();
            dismiss();
        }
    }

    public void showSuccessPopupMessage(Object message) {
        if (isAdded() && getActivity() != null) {
            ToastUtil.showSuccessToast(getActivity(), String.valueOf(message));
        }
    }

    public void showErrorPopupMessage(Object message) {
        if (isAdded() && getActivity() != null) {
            ToastUtil.showErrorToast(getActivity(), String.valueOf(message));
        }
    }

    public boolean validateCardInputView(EditText editText, String errorMessage) {
        return Validator.validateCardInputNotEmpty(editText, errorMessage);
    }

    public boolean validateCVV(EditText view, String errorMessage) {
        return Validator.validateCVVNumber(view, errorMessage);
    }

    public boolean validateExpiryDateInput(EditText view, String errorMessage) {
        return Validator.validateExpiryDateInput(view, errorMessage);
    }
}
