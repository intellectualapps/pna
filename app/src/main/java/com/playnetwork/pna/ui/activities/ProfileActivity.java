package com.playnetwork.pna.ui.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;

import com.playnetwork.pna.R;
import com.playnetwork.pna.ui.dialogs.MediaPickerDialog;
import com.playnetwork.pna.ui.fragments.AddMemberFragment;
import com.playnetwork.pna.ui.fragments.MediaPickerBaseFragment;
import com.playnetwork.pna.ui.fragments.MemberProfileFragment;
import com.playnetwork.pna.ui.fragments.MemberSearchDetailFragment;
import com.playnetwork.pna.ui.fragments.MessagesFragment;
import com.playnetwork.pna.ui.fragments.PaymentConfirmationFragment;
import com.playnetwork.pna.ui.fragments.PendingMemberProfileFragment;
import com.playnetwork.pna.ui.fragments.PremiumSubscriptionFragment;
import com.playnetwork.pna.ui.fragments.UserProfileFragment;
import com.playnetwork.pna.utils.Constants;

public class ProfileActivity extends BaseActivity implements MediaPickerDialog.MediaPickedListener {
    private Bundle fragmentBundle;
    private String viewType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    default:
                    case Constants.MEMBER_PROFILE_VIEW_TAG:
                        frag = MemberProfileFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.MESSAGES_VIEW_TAG:
                        frag = MessagesFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.USER_PROFILE_VIEW_TAG:
                        frag = UserProfileFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.PENDING_PROFILE_VIEW_TAG:
                        frag = PendingMemberProfileFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.PREMIUM_SUBSCRIPTION_VIEW_TAG:
                        frag = PremiumSubscriptionFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.PAYMENT_CONFIRMATION_VIEW_TAG:
                        frag = PaymentConfirmationFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.SEARCH_DETAIL_VIEW_TAG:
                        frag = MemberSearchDetailFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.ADD_MEMBER_VIEW_TAG:
                        frag = AddMemberFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = MemberProfileFragment.newInstance(null);
            }

            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onMediaItemPicked(Uri fileUri, String filePath, int mediaType, String fragmentTag) {
        Fragment frag = findActiveFragmentByTag(fragmentTag, getSupportFragmentManager());
        if (frag != null && frag instanceof MediaPickerBaseFragment) {
            ((MediaPickerBaseFragment) frag).onMediaPickerSuccess(fileUri, filePath, mediaType, fragmentTag);
        }
    }

    @Override
    public void onCancel(String message, String fragmentTag) {
        Fragment frag = findActiveFragmentByTag(fragmentTag, getSupportFragmentManager());
        if (frag != null && frag instanceof MediaPickerBaseFragment) {
            ((MediaPickerBaseFragment) frag).onMediaPickerError(message, fragmentTag);
        }
    }
}
