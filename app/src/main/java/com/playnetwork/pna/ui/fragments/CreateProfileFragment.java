package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.api.responses.LookupStatesResponse;
import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.State;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.SpinnerArrayAdapter;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreateProfileFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private User user;
    private EditText companyNameInput, jobTitleInput;
    private Button createProfileButton;
    private Spinner industrySpinner, countrySpinner, stateSpinner;
    private Toolbar toolbar;
    private List<Country> countries;
    private List<Industry> industries;
    private List<State> states;
    private PlayNetworkDatabase playNetworkDatabase;

    private SpinnerArrayAdapter statesAdapter, countryAdapter, industryAdapter;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new CreateProfileFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public CreateProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();

        industries = PlayNetworkDatabase.getPlayNetworkDatabase().industryDao().getAllIndustries();
        countries = PlayNetworkDatabase.getPlayNetworkDatabase().countryDao().getAllCountries();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_profile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        jobTitleInput = view.findViewById(R.id.job_title);
        companyNameInput = view.findViewById(R.id.company_name);
        industrySpinner = view.findViewById(R.id.industry_spinner);
        countrySpinner = view.findViewById(R.id.country_spinner);
        stateSpinner = view.findViewById(R.id.state_spinner);
        createProfileButton = view.findViewById(R.id.create_profile_button);
        createProfileButton.setOnClickListener(this);

        statesAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());
        countryAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());
        industryAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());

        industryAdapter.addAll(getIndustryList(industries).toArray(new String[getIndustryList(industries).size()]));
        countryAdapter.addAll(getCountryList(countries).toArray(new String[getCountryList(countries).size()]));

        stateSpinner.setAdapter(statesAdapter);
        countrySpinner.setAdapter(countryAdapter);
        industrySpinner.setAdapter(industryAdapter);

        countrySpinner.setOnItemSelectedListener(this);
        industrySpinner.setOnItemSelectedListener(this);
        stateSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View view) {
        String message = getString(R.string.create_profile_progress_label);
        switch (view.getId()) {
            case R.id.create_profile_button:
                if (validateFields(new EditText[]{jobTitleInput, companyNameInput}) && validateSpinnerControls(new Spinner[]{industrySpinner, countrySpinner, stateSpinner})) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        getHttpService().createMemberProfile(getUserInput(), buildUpdateProfileCallback());
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                }
                break;
        }
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String jobTitle = jobTitleInput.getText().toString().trim();
        String company = companyNameInput.getText().toString().trim();
        String industryId = String.valueOf(industries.get(industrySpinner.getSelectedItemPosition() - 1).getId());
        String countryCode = String.valueOf(countries.get(countrySpinner.getSelectedItemPosition() - 1).getSortName());
        String state = states.get(stateSpinner.getSelectedItemPosition() - 1).getName();
        map.put(Constants.EMAIL_ADDRESS, user.getEmail());
        map.put(Constants.JOB_TITLE, jobTitle);
        map.put(Constants.COMPANY, company);
        map.put(Constants.INDUSTRY, industryId);
        map.put(Constants.COUNTRY_CODE, countryCode);
        map.put(Constants.STATE, state);
        return map;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.country_spinner:
                if (position > 0) {
                    Country country = countries.get(position - 1);
                    statesAdapter.clear();
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        getStatesForCountry(String.valueOf(country.getId()));
                    } else {
                        showErrorPopupMessage(getPlayNetworkResources().getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.industry_spinner:
                break;
            case R.id.state_spinner:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void getStatesForCountry(String countryId) {
        getHttpService().fetchStates(countryId, buildFetchStatesCallback());
    }

    private Callback<LookupStatesResponse> buildFetchStatesCallback() {
        return new Callback<LookupStatesResponse>() {
            @Override
            public void onResponse(Call<LookupStatesResponse> call, Response<LookupStatesResponse> response) {
                LookupStatesResponse lookupStatesResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (lookupStatesResponse != null) {
                        states = lookupStatesResponse.getStates();
                    }

                    if (ListUtils.isNotEmpty(states) && isVisible()) {
                        statesAdapter.clear();
                        statesAdapter.addAll(getStateList(states));
                        statesAdapter.notifyDataSetChanged();
                    } else {
                        showErrorPopupMessage(getString(R.string.empty_states_label));
                    }
                }
            }

            @Override
            public void onFailure(Call<LookupStatesResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<AuthenticationResponse> buildUpdateProfileCallback() {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    saveUserData(playNetworkDatabase, authenticationResponse, null, user.getEmail());
                    if (getActivity() != null && isVisible()) {
                        ((BaseActivity) getActivity()).showMainActivity(getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }
}
