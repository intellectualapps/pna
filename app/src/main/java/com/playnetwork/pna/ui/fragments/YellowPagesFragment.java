package com.playnetwork.pna.ui.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.ApiClientCallbacks;
import com.playnetwork.pna.data.api.responses.LookupStatesResponse;
import com.playnetwork.pna.data.api.responses.YellowPageResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.State;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.data.models.YellowPage;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.YellowPagesAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.ui.dialogs.FilterBottomSheetDialogFragment;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.AutoRefreshUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YellowPagesFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, PNAInterfaces.PaginationAdapterCallback, SearchView.OnQueryTextListener, SearchView.OnCloseListener, MenuItem.OnActionExpandListener, View.OnClickListener, PNAInterfaces.FilterSelectionListener, ApiClientCallbacks.LookupStatesListener {
    private User user;
    private YellowPagesAdapter yellowPagesAdapter;
    private CustomRecyclerView yellowPagesRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton, filterButton;
    private View mEmptyView;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private boolean hasPageInitialized = false;
    private int start = 0, pageSize = 10;
    private int currentPage = start;
    private MenuItem searchMenuItem;
    private String searchTerm = null;
    private SearchView searchView;
    private boolean inSearchMode = false, inFilterMode;
    private boolean isSearchRunning = false;
    private List<Integer> stateIds, industryIds;

    private List<YellowPage> yellowPagesList, masterYellowPageList;
    private List<State> stateList;
    private List<Country> countryList;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new YellowPagesFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public YellowPagesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        yellowPagesList = playNetworkDatabase.yellowPageDao().getAll();
        stateList = playNetworkDatabase.stateDao().getAllStates();
        countryList = playNetworkDatabase.countryDao().getAllCountries();
        masterYellowPageList = yellowPagesList;
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_yellow_pages, container, false);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.yellow_pages_search_menu, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchMenuItem.setOnActionExpandListener(this);
        if (searchMenuItem == null) {
            return;
        }
        if (getActivity() != null) {
            SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) searchMenuItem.getActionView();
            try {
                SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
                searchAutoComplete.setHintTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
                searchAutoComplete.setCursorVisible(true);
                searchAutoComplete.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                mCursorDrawableRes.setAccessible(true);
                mCursorDrawableRes.set(searchAutoComplete, R.drawable.search_cursor);

                if (searchManager != null) {
                    searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
                }
                searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
                searchView.setQueryHint(getString(R.string.action_search_label));
                searchView.setIconified(false);
                searchView.setSubmitButtonEnabled(true);
                searchView.setOnQueryTextListener(this);
                searchView.setOnCloseListener(this);
            } catch (Exception ignored) {
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (searchView != null) {
            searchView.clearFocus();
        }
    }

    private void initializeToolbar(final Activity activity) {
        if (activity != null) {
            Toolbar toolbar = ((BaseActivity) activity).initializeToolbar(getString(R.string.yellow_pages_toolbar_label));
            ((BaseActivity) activity).setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) activity).navigateUp();
                }
            });
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        initializeToolbar(getActivity());

        yellowPagesRecyclerView = view.findViewById(R.id.yellow_pages_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        filterButton = view.findViewById(R.id.filter_button);
        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mEmptyViewLabel.setText(R.string.yellow_pages_empty_label);

        filterButton.setOnClickListener(this);

        yellowPagesRecyclerView.setEmptyView(mEmptyView);
        yellowPagesAdapter = new YellowPagesAdapter(getContext(), this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        yellowPagesRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        yellowPagesRecyclerView.setLayoutManager(linearLayoutManager);
        yellowPagesRecyclerView.setAdapter(yellowPagesAdapter);

        yellowPagesRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (yellowPagesList != null && yellowPagesList.size() >= pageSize) {
                            if (inSearchMode) {
                                performSearch(start, pageSize, searchTerm, true);
                            } else {
                                fetchYellowPages(start, pageSize, true, false);
                            }
                        }
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    inSearchMode = false;
                    inFilterMode = false;
                    if (searchView != null) {
                        searchMenuItem.collapseActionView();
                    }
                    clearSelectionFilters();
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        fetchYellowPages(0, pageSize, false, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (ListUtils.isNotEmpty(yellowPagesList)) {
            loadAdapter(yellowPagesList);
            if (!hasPageInitialized) {
                hasPageInitialized = true;
                if (NetworkUtils.isConnected(getContext())) {
                    fetchYellowPages(0, pageSize, false, false);
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }
        } else {
            if (NetworkUtils.isConnected(getContext())) {
                fetchYellowPages(0, pageSize, false, false);
            } else {
                yellowPagesAdapter.clearItems();
                showErrorPopupMessage(getString(R.string.network_connection_label));
            }
        }

        if (ListUtils.isEmpty(stateList)) {
            if (NetworkUtils.isConnected(getContext()) && user != null && !TextUtils.isEmpty(user.getCountry())) {
                AutoRefreshUtils.fetchStates(getHttpService(), ListUtils.getCountryIdForCountryCode(user.getCountry(), countryList), this);
            }
        }
    }

    private void fetchYellowPages(int start, int pageSize, boolean inLoadMoreMode, boolean inFilterMode) {
        mEmptyViewLabel.setText(inFilterMode ? R.string.no_records_matching_filter : R.string.yellow_pages_empty_label);
        if (!inLoadMoreMode || inFilterMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }

        getHttpService().fetchYellowPages(buildParameterMap(start, pageSize), inFilterMode, buildFetchYellowPagesCallback(inLoadMoreMode, inFilterMode));
    }

    private void performSearch(int start, int pageSize, String searchTerm, boolean inLoadMoreMode) {
        mEmptyViewLabel.setText(inFilterMode ? R.string.no_records_matching_filter : R.string.yellow_pages_search_empty_label);
        if (NetworkUtils.isConnected(getContext())) {
            isSearchRunning = true;
            if (!inLoadMoreMode) {
                mEmptyView.setVisibility(View.GONE);
                animateSwipeRefreshLayout(swipeRefreshLayout);
                hasListEnded = false;
                currentPage = 0;
            }

            getHttpService().searchYellowPages(searchTerm, buildParameterMap(start, pageSize), buildSearchYellowPagesCallback(inLoadMoreMode));
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private Map<String, String> buildParameterMap(int start, int pageSize) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.MEMBER_ID_PARAM, String.valueOf(user.getId()));
        if (ListUtils.isNotEmpty(stateIds)) {
            requestMap.put(Constants.STATES, android.text.TextUtils.join(",", stateIds));
        }
        if (ListUtils.isNotEmpty(industryIds)) {
            requestMap.put(Constants.CATEGORIES, android.text.TextUtils.join(",", industryIds));
        }
        return requestMap;
    }

    private void updateYellowPageList(List<YellowPage> yellowPagesParam) {
        if (yellowPagesList == null) {
            yellowPagesList = new ArrayList<YellowPage>();
        }
        if (yellowPagesParam != null && yellowPagesParam.size() > 0) {
            yellowPagesList.addAll(yellowPagesParam);
        }
    }

    private void updateYellowPageListInCache(List<YellowPage> yellowPagesList) {
        playNetworkDatabase.yellowPageDao().deleteCachedYellowPages();
        playNetworkDatabase.yellowPageDao().insertAll(ListUtils.convertYellowPageListToArray(yellowPagesList));
    }

    private void loadAdapter(List<YellowPage> yellowPagesList) {
        hideKeyboard();
        yellowPagesAdapter.addAll(yellowPagesList);
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1) {
            YellowPage yellowPage = yellowPagesAdapter.getYellowPagesList().get(position);
            switch (view.getId()) {
                default:
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.USER, user);
                    bundle.putParcelable(Constants.YELLOW_PAGE, yellowPage);
                    bundle.putInt(Constants.POSITION, position);
                    switchFragment(YellowPageDetailFragment.newInstance(bundle));
                    break;
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query != null && query.trim().length() > 0) {
            if (query.trim().length() > 0) {
                searchTerm = query;
                performSearch(0, pageSize, searchTerm, false);
            } else {
                showErrorPopupMessage(getString(R.string.query_short_error));
            }
        } else {
            yellowPagesAdapter.setItems(masterYellowPageList);
            clearSelectionFilters();
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String query) {
        return false;
    }

    @Override
    public boolean onClose() {
        yellowPagesAdapter.setItems(masterYellowPageList);
        return false;
    }

    @Override
    public void loadMoreItems() {
        start = currentPage * pageSize;
        if (inSearchMode) {
            performSearch(start, pageSize, searchTerm, true);
        } else {
            fetchYellowPages(start, pageSize, true, false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }

    private Callback<YellowPageResponse> buildFetchYellowPagesCallback(final boolean inLoadMoreMode, final boolean inFilterMode) {
        return new Callback<YellowPageResponse>() {
            @Override
            public void onResponse(Call<YellowPageResponse> call, Response<YellowPageResponse> response) {
                YellowPageResponse yellowPageResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                isSearchRunning = false;
                inSearchMode = false;
                isLoading = false;
                if (inLoadMoreMode && isVisible()) {
                    yellowPagesAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && yellowPageResponse != null && isVisible()) {
                    List<YellowPage> yellowPagesListParam = null;
                    yellowPagesListParam = yellowPageResponse.getYellowPages();

                    if (yellowPagesListParam == null || yellowPagesListParam.size() == 0 || yellowPagesListParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (yellowPagesList != null) {
                            yellowPagesList.clear();
                        }
                        yellowPagesAdapter.clearItems();
                        if (!inFilterMode) {
                            masterYellowPageList = yellowPagesListParam;
                            updateYellowPageListInCache(yellowPagesListParam);
                        }
                    }
                    killSwipeRefreshView();

                    updateYellowPageList(yellowPagesListParam);
                    if (isAdded() && isVisible()) {
                        loadAdapter(yellowPagesListParam);
                    }

                    if (!hasListEnded) {
                        yellowPagesAdapter.addLoadingFooter();
                    }
                } else {
                    if (isVisible()) {
                        loadAdapter(masterYellowPageList);
                    }
                }
            }

            @Override
            public void onFailure(Call<YellowPageResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    yellowPagesAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    private void killSwipeRefreshView() {
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    private Callback<YellowPageResponse> buildSearchYellowPagesCallback(final boolean inLoadMoreMode) {
        return new Callback<YellowPageResponse>() {
            @Override
            public void onResponse(Call<YellowPageResponse> call, Response<YellowPageResponse> response) {
                YellowPageResponse yellowPageResponse = response.body();
                hideLoadingIndicator();
                isSearchRunning = false;
                inSearchMode = true;
                isLoading = false;
                endSwipeRefreshLayout(swipeRefreshLayout);
                if (inLoadMoreMode) {
                    yellowPagesAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && yellowPageResponse != null) {
                    List<YellowPage> searchResultsParam = yellowPageResponse.getYellowPages();

                    if (searchResultsParam == null || searchResultsParam.size() == 0 || searchResultsParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (yellowPagesList != null) {
                            yellowPagesList.clear();
                        }
                        yellowPagesAdapter.clearItems();
                    }

                    updateYellowPageList(searchResultsParam);
                    if (isAdded() && isVisible()) {
                        loadAdapter(searchResultsParam);
                    }

                    if (hasListEnded) {
                        yellowPagesAdapter.removeLoadingFooter();
                    }
                } else {
                    loadAdapter(yellowPagesList);
                }
            }

            @Override
            public void onFailure(Call<YellowPageResponse> call, Throwable t) {
                isSearchRunning = false;
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    yellowPagesAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        if (inSearchMode) {
            if (yellowPagesList != null) {
                yellowPagesList.clear();
            }
            yellowPagesAdapter.clearItems();
            updateYellowPageList(masterYellowPageList);
            loadAdapter(masterYellowPageList);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_button:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.USER, user);
                bundle.putSerializable(Constants.STATES, (Serializable) stateList);
                bundle.putSerializable(Constants.STATE_IDS, (Serializable) stateIds);
                bundle.putSerializable(Constants.INDUSTRY_IDS, (Serializable) industryIds);
                FilterBottomSheetDialogFragment filterBottomSheetFragment = new FilterBottomSheetDialogFragment();
                filterBottomSheetFragment.setArguments(bundle);
                filterBottomSheetFragment.setFilterSelectionListener(this);
                filterBottomSheetFragment.show(getSupportFragmentManager(), filterBottomSheetFragment.getTag());
                break;
        }
    }

    @Override
    public void onSelectionComplete(int mode) {

    }

    private void clearSelectionFilters() {
        if (ListUtils.isNotEmpty(stateIds)) {
            stateIds.clear();
        }

        if (ListUtils.isNotEmpty(industryIds)) {
            industryIds.clear();
        }
    }

    @Override
    public void onSelectionComplete(List<Integer> stateIds, List<Integer> industryIds) {
        this.stateIds = stateIds;
        this.industryIds = industryIds;
        inFilterMode = ListUtils.isNotEmpty(stateIds) || ListUtils.isNotEmpty(industryIds);
        if (NetworkUtils.isConnected(getContext())) {
            fetchYellowPages(0, pageSize, false, inFilterMode);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public void onStatesFetched(LookupStatesResponse lookupStatesResponse) {
        if (lookupStatesResponse != null && ListUtils.isNotEmpty(lookupStatesResponse.getStates())) {
            this.stateList = lookupStatesResponse.getStates();
        }
    }
}
