package com.playnetwork.pna.ui.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.BuildConfig;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.YellowPageResponse;
import com.playnetwork.pna.data.models.CustomField;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.data.models.YellowPage;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.CustomFieldListAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YellowPageDetailFragment extends BaseFragment implements View.OnClickListener {

    private TextView businessNameView;
    private Button callBusinessButton;
    private ImageView businessLogoView, businessAddressBannerView;
    private YellowPage yellowPage;
    private CustomRecyclerView customFieldsRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CustomFieldListAdapter customFieldListAdapter;
    private List<CustomField> customFieldList;
    private User user;
    private int position;
    private boolean hasStaticBannerBeenLoaded = false;
    private boolean isLoadingStaticBanner = false;
    private boolean hasCallPermissionBeenGranted = false;

    public static Fragment newInstance(Bundle args) {
        YellowPageDetailFragment frag = new YellowPageDetailFragment();

        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null && getArguments().containsKey(Constants.YELLOW_PAGE)) {
            yellowPage = getArguments().getParcelable(Constants.YELLOW_PAGE);
            position = getArguments().getInt(Constants.POSITION, -1);
        }

        if (getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_yellow_page_detail, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.business_details_toolbar_label));
        businessNameView = view.findViewById(R.id.business_name);
        businessLogoView = view.findViewById(R.id.business_logo);
        businessAddressBannerView = view.findViewById(R.id.business_address_banner);
        callBusinessButton = view.findViewById(R.id.call_button);
        customFieldsRecyclerView = view.findViewById(R.id.custom_fields_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        fetchYellowPageData(yellowPage.getId());
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        customFieldListAdapter = new CustomFieldListAdapter(getContext(), null, false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        customFieldsRecyclerView.setLayoutManager(linearLayoutManager);
        customFieldsRecyclerView.setAdapter(customFieldListAdapter);

        callBusinessButton.setOnClickListener(this);
        loadYellowPageData(yellowPage);
        fetchYellowPageData(yellowPage.getId());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (hasCallPermissionBeenGranted) {
            callBusinessPhoneNumber();
            hasCallPermissionBeenGranted = false;
        }
    }

    private void loadYellowPageData(YellowPage yellowPage) {
        if (yellowPage != null && isAdded() && isVisible()) {
            businessNameView.setText(stripHtml(yellowPage.getBusinessName()));
            if (!TextUtils.isEmpty(yellowPage.getBusinessLogo())) {
                Picasso.with(getContext()).load(yellowPage.getBusinessLogo()).placeholder(R.color.colorGray).into(businessLogoView);
            } else {
                Picasso.with(getContext()).load(R.drawable.play_africa_logo).placeholder(R.color.colorGray).into(businessLogoView);
            }

            callBusinessButton.setText(getString(R.string.call_label_placeholder, yellowPage.getBusinessPhone()));
            customFieldListAdapter.setItems(getFieldDetails(yellowPage));
            callBusinessButton.setVisibility(!TextUtils.isEmpty(yellowPage.getBusinessPhone()) ? View.VISIBLE : View.GONE);
            if (NetworkUtils.isNetworkConnected(getContext())) {
                if (!TextUtils.isEmpty(yellowPage.getBusinessAddress()) && NetworkUtils.isConnected(getContext()) && !hasStaticBannerBeenLoaded && !isLoadingStaticBanner) {
                    isLoadingStaticBanner = true;
                    StaticMapDownloaderAsyncTask staticMapDownloaderAsyncTask = new StaticMapDownloaderAsyncTask(yellowPage, "1000x200");
                    staticMapDownloaderAsyncTask.execute();
                }
            } else {
                loadStaticBanner(null, ContextCompat.getDrawable(getContext(), R.drawable.staticmap));
            }
        }
    }

    private void fetchYellowPageData(int listingId) {
        if (NetworkUtils.isConnected(getContext())) {
            animateSwipeRefreshLayout(swipeRefreshLayout);
            getHttpService().fetchYellowPageById(String.valueOf(listingId), buildFetchYellowPageByIdCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private List<CustomField> getFieldDetails(YellowPage yellowPage) {
        List<CustomField> list = new ArrayList<CustomField>();
        if (!TextUtils.isEmpty(yellowPage.getBusinessAddress())) {
            list.add(new CustomField(yellowPage.getBusinessAddress(), false));
        }
        if (!TextUtils.isEmpty(yellowPage.getBusinessCategory())) {
            list.add(new CustomField(yellowPage.getBusinessCategory(), false));
        }
        if (!TextUtils.isEmpty(yellowPage.getBusinessEmail())) {
            list.add(new CustomField(yellowPage.getBusinessEmail(), true));
        }
        if (!TextUtils.isEmpty(yellowPage.getBusinessWebsite())) {
            list.add(new CustomField(yellowPage.getBusinessWebsite(), true));
        }
        if (!TextUtils.isEmpty(yellowPage.getBusinessDetails())) {
            list.add(new CustomField(yellowPage.getBusinessDetails(), false));
        }
        return list;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.call_button:
                if (checkCallPermission(getActivity())) {
                    callBusinessPhoneNumber();
                }
                break;
        }
    }

    private void callBusinessPhoneNumber() {
        String phoneNumber = yellowPage.getBusinessPhone();
        String formattedPhoneNumber = "tel:".concat(phoneNumber);
        Intent intent = new Intent(Intent.ACTION_CALL);

        intent.setData(Uri.parse(formattedPhoneNumber));
        startActivity(intent);
    }

    private Callback<YellowPageResponse> buildFetchYellowPageByIdCallback() {
        return new Callback<YellowPageResponse>() {
            @Override
            public void onResponse(Call<YellowPageResponse> call, Response<YellowPageResponse> response) {
                YellowPageResponse yellowPageResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);

                if (ApiUtils.isSuccessResponse(response) && yellowPageResponse != null) {
                    yellowPage = yellowPageResponse.getYellowPage();
                    if (isVisible()) {
                        loadYellowPageData(yellowPage);
                    }
                }
            }

            @Override
            public void onFailure(Call<YellowPageResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CALL_PERMISSION_REQUEST_CODE:
                hasCallPermissionRequestBeenMade = true;
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            hasCallPermissionBeenGranted = false;
                            checkCallPermission(getActivity());
                            return;
                        }
                        hasCallPermissionBeenGranted = true;
                    }
                } else {
                    checkCallPermission(getActivity());
                }
                break;
        }
    }

    private class StaticMapDownloaderAsyncTask extends AsyncTask<Void, Void, Bitmap> {
        private YellowPage yellowPage;
        private String dimension = "1000x400";
        private String FORMATTED_URL;
        private String BUSINESS_ADDRESS_STATIC_MAP_API_URL = "https://maps.googleapis.com/maps/api/staticmap?center=BUSINESS_ADDRESS&zoom=15&size=DIMENSION&key=API_KEY&markers=color:blue|size:mid|BUSINESS_ADDRESS";
        private String BUSINESS_STATE_STATIC_MAP_API_URL = "https://maps.googleapis.com/maps/api/staticmap?center=BUSINESS_ADDRESS&zoom=7&size=DIMENSION&key=API_KEY";

        private StaticMapDownloaderAsyncTask(YellowPage yellowPage, String dimension) {
            this.yellowPage = yellowPage;
            this.dimension = dimension;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FORMATTED_URL = BUSINESS_ADDRESS_STATIC_MAP_API_URL.replace("BUSINESS_ADDRESS", yellowPage.getBusinessAddress()).replace("DIMENSION", dimension).replace("API_KEY", BuildConfig.GOOGLE_STATIC_MAP_API_KEY);
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            try {
                URL url = new URL(FORMATTED_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();

                if (!TextUtils.isEmpty(connection.getHeaderField("X-Staticmap-API-Warning"))) {
                    FORMATTED_URL = BUSINESS_STATE_STATIC_MAP_API_URL.replace("BUSINESS_ADDRESS", yellowPage.getBusinessState()).replace("DIMENSION", dimension).replace("API_KEY", BuildConfig.GOOGLE_STATIC_MAP_API_KEY);
                    url = new URL(FORMATTED_URL);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    input = connection.getInputStream();
                }

                Bitmap downloadedBitmap = BitmapFactory.decodeStream(input);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                downloadedBitmap.compress(Bitmap.CompressFormat.PNG, 80, out);
                return BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            loadStaticBanner(bitmap, null);
        }
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    private void loadStaticBanner(Bitmap bitmap, Drawable drawable) {
        hasStaticBannerBeenLoaded = true;
        if (isAdded() && isVisible()) {
            if (bitmap != null) {
                businessAddressBannerView.setImageBitmap(bitmap);
            } else {
                businessAddressBannerView.setImageDrawable(drawable);
            }

            ScaleAnimation scale = new ScaleAnimation(0.7f, 1, 0.7f, 1, ScaleAnimation.RELATIVE_TO_SELF, .75f, ScaleAnimation.RELATIVE_TO_SELF, .75f);
            scale.setDuration(700);
            businessAddressBannerView.startAnimation(scale);
        }
        isLoadingStaticBanner = false;
    }
}
