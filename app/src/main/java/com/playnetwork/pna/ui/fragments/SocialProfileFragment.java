package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.api.responses.LookupStatesResponse;
import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.State;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.SpinnerArrayAdapter;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;

import org.parceler.apache.commons.collections.MapUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SocialProfileFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private EditText companyNameInput, jobTitleInput, emailAddressInput, firstNameInput, lastNameInput, phoneNumberInput;
    private Button createProfileButton;
    private Spinner industrySpinner, countrySpinner, stateSpinner;
    private List<Country> countries;
    private List<Industry> industries;
    private List<State> states;
    private PlayNetworkDatabase playNetworkDatabase;
    private Map<String, String> profileData;
    private User user;

    private SpinnerArrayAdapter statesAdapter, countryAdapter, industryAdapter;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new SocialProfileFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public SocialProfileFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        profileData = new HashMap<String, String>();
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.PROFILE_DATA)) {
                profileData = (Map<String, String>) getArguments().getSerializable(Constants.PROFILE_DATA);
            }
        }

        industries = PlayNetworkDatabase.getPlayNetworkDatabase().industryDao().getAllIndustries();
        countries = PlayNetworkDatabase.getPlayNetworkDatabase().countryDao().getAllCountries();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_social_profile, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).getToolbar().setVisibility(View.GONE);
        emailAddressInput = view.findViewById(R.id.email_address);
        firstNameInput = view.findViewById(R.id.first_name);
        lastNameInput = view.findViewById(R.id.last_name);
        phoneNumberInput = view.findViewById(R.id.phone_number);
        jobTitleInput = view.findViewById(R.id.job_title);
        companyNameInput = view.findViewById(R.id.company_name);
        industrySpinner = view.findViewById(R.id.industry_spinner);
        countrySpinner = view.findViewById(R.id.country_spinner);
        stateSpinner = view.findViewById(R.id.state_spinner);
        createProfileButton = view.findViewById(R.id.create_profile_button);

        createProfileButton.setOnClickListener(this);

        statesAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());
        countryAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());
        industryAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());

        industryAdapter.addAll(getIndustryList(industries).toArray(new String[getIndustryList(industries).size()]));
        countryAdapter.addAll(getCountryList(countries).toArray(new String[getCountryList(countries).size()]));
        statesAdapter.addAll(getStateList(null).toArray(new String[getStateList(null).size()]));

        stateSpinner.setAdapter(statesAdapter);
        countrySpinner.setAdapter(countryAdapter);
        industrySpinner.setAdapter(industryAdapter);

        countrySpinner.setOnItemSelectedListener(this);
        industrySpinner.setOnItemSelectedListener(this);
        stateSpinner.setOnItemSelectedListener(this);

        if (MapUtils.isNotEmpty(profileData)) {
            loadProfileData(profileData);
        } else {
            loadUserData(user);
        }
    }

    private void loadUserData(User user) {
        if (user != null) {
            firstNameInput.setText(user.getFname());
            lastNameInput.setText(user.getLname());
            phoneNumberInput.setText(user.getPhone());
            emailAddressInput.setText(user.getEmail());
            jobTitleInput.setText(user.getJobTitle());
            companyNameInput.setText(user.getCompany());
        }
    }

    private void loadProfileData(Map<String, String> profileData) {
        if (profileData != null && profileData.size() > 0) {
            if (profileData.containsKey(Constants.FIRST_NAME)) {
                firstNameInput.setText(profileData.get(Constants.FIRST_NAME));
            }

            if (profileData.containsKey(Constants.LAST_NAME)) {
                lastNameInput.setText(profileData.get(Constants.LAST_NAME));
            }

            if (profileData.containsKey(Constants.EMAIL_ADDRESS)) {
                emailAddressInput.setText(profileData.get(Constants.EMAIL_ADDRESS));
            }

            if (profileData.containsKey(Constants.PHONE_NUMBER)) {
                phoneNumberInput.setText(profileData.get(Constants.PHONE_NUMBER));
            }
        }
    }

    @Override
    public void onClick(View view) {
        String message = getString(R.string.create_profile_progress_label);
        switch (view.getId()) {
            case R.id.create_profile_button:
                if (validateFields(new EditText[]{firstNameInput, lastNameInput}) && validateEmailInput(emailAddressInput) && validatePhoneNumber(phoneNumberInput) && validateFields(new EditText[]{jobTitleInput, companyNameInput}) && validateSpinnerControls(new Spinner[]{industrySpinner, countrySpinner, stateSpinner})) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        getHttpService().updateSocialAccount(getUserInput(), buildUpdateSocialProfileCallback());
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                }
                break;
        }
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String jobTitle = jobTitleInput.getText().toString().trim();
        String company = companyNameInput.getText().toString().trim();
        String industryId = String.valueOf(industries.get(industrySpinner.getSelectedItemPosition() - 1).getId());
        String countryCode = String.valueOf(countries.get(countrySpinner.getSelectedItemPosition() - 1).getSortName());
        String state = states.get(stateSpinner.getSelectedItemPosition() - 1).getName();
        String firstName = firstNameInput.getText().toString().trim();
        String lastName = lastNameInput.getText().toString().trim();
        String phoneNumber = phoneNumberInput.getText().toString().trim();
        String emailAddress = emailAddressInput.getText().toString().trim();

        map.put(Constants.MEMBER_ID_PARAM, String.valueOf(user.getId()));
        map.put(Constants.FIRST_NAME, firstName);
        map.put(Constants.LAST_NAME, lastName);
        map.put(Constants.PHONE_NUMBER, phoneNumber);
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        map.put(Constants.JOB_TITLE, jobTitle);
        map.put(Constants.COMPANY, company);
        map.put(Constants.INDUSTRY, industryId);
        map.put(Constants.COUNTRY_CODE, countryCode);
        map.put(Constants.STATE, state);
        return map;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.country_spinner:
                if (position > 0) {
                    Country country = countries.get(position - 1);
                    statesAdapter.clear();
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        getStatesForCountry(String.valueOf(country.getId()));
                    } else {
                        showErrorPopupMessage(getPlayNetworkResources().getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.industry_spinner:
                break;
            case R.id.state_spinner:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void getStatesForCountry(String countryId) {
        getHttpService().fetchStates(countryId, buildFetchStatesCallback());
    }

    private Callback<LookupStatesResponse> buildFetchStatesCallback() {
        return new Callback<LookupStatesResponse>() {
            @Override
            public void onResponse(Call<LookupStatesResponse> call, Response<LookupStatesResponse> response) {
                LookupStatesResponse lookupStatesResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (lookupStatesResponse != null) {
                        states = lookupStatesResponse.getStates();
                    }

                    if (ListUtils.isNotEmpty(states) && isVisible()) {
                        statesAdapter.clear();
                        statesAdapter.addAll(getStateList(states));
                        statesAdapter.notifyDataSetChanged();
                    } else {
                        showErrorPopupMessage(getString(R.string.empty_states_label));
                    }
                }
            }

            @Override
            public void onFailure(Call<LookupStatesResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<AuthenticationResponse> buildUpdateSocialProfileCallback() {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    saveUserData(playNetworkDatabase, authenticationResponse, null, user.getEmail());
                    if (getActivity() != null && isVisible()) {
                        ((BaseActivity) getActivity()).showMainActivity(getActivity());
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }
}
