package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.MemberPhoto;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.ProfileActivity;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MemberPhotosAdapter extends BaseRecyclerAdapter {

    private List<MemberPhoto> memberPhotoList;
    private User user;

    public MemberPhotosAdapter(Context context, User user) {
        super(context);
        this.user = user;
        this.memberPhotoList = new ArrayList<MemberPhoto>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;
        view = mInflater.inflate(R.layout.member_photo_item_layout, parent, false);
        viewHolder = new MemberPhotosAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final MemberPhotosAdapter.ViewHolder viewHolder = (MemberPhotosAdapter.ViewHolder) holder;
        final MemberPhoto memberPhoto = memberPhotoList.get(position);
        if (memberPhoto != null) {
            final User member = new User();
            member.setId(memberPhoto.getMemberId());
            member.setPic(memberPhoto.getImageUrl());
            Picasso.with(context).load(memberPhoto.getImageUrl()).into(viewHolder.memberPhoto);
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    launchProfileIntent(user, member, memberPhoto.getNetworkState());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(memberPhotoList) ? 0 : memberPhotoList.size();
    }

    public void setItems(List<MemberPhoto> items) {
        this.memberPhotoList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (memberPhotoList != null) {
            this.memberPhotoList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(MemberPhoto memberPhoto) {
        if (!TextUtils.isEmpty(memberPhoto.getImageUrl())) {
            this.memberPhotoList.add(memberPhoto);
            notifyDataSetChanged();
        }
    }

    public void removeItem(MemberPhoto memberPhoto) {
        int position = memberPhotoList.indexOf(memberPhoto);
        if (position > -1) {
            memberPhotoList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private MemberPhoto getItem(int position) {
        return memberPhotoList.get(position);
    }

    private void launchProfileIntent(User user, User member, String networkState) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.USER, user);
        bundle.putParcelable(Constants.MEMBER, member);
        Intent intent = new Intent(context, ProfileActivity.class);

        if (!TextUtils.isEmpty(networkState)) {
            switch (networkState) {
                case Constants.PENDING_NETWORK_STATE:
                    bundle.putString(Constants.VIEW_TYPE, Constants.PENDING_PROFILE_VIEW_TAG);
                    break;
                case Constants.ACCEPTED_NETWORK_STATE:
                    bundle.putString(Constants.VIEW_TYPE, Constants.MEMBER_PROFILE_VIEW_TAG);
                    break;
                case Constants.NONE_NETWORK_STATE:
                default:
                    bundle.putString(Constants.VIEW_TYPE, Constants.ADD_MEMBER_VIEW_TAG);
                    break;
            }
        } else {
            bundle.putString(Constants.VIEW_TYPE, Constants.ADD_MEMBER_VIEW_TAG);
        }
        if (user.getId() == member.getId()) {
            bundle.putString(Constants.VIEW_TYPE, Constants.USER_PROFILE_VIEW_TAG);
        }
        intent.putExtras(bundle);
        context.startActivity(intent);
    }


    private static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView memberPhoto;
        private ProgressBar progressBar;

        ClickListener clickListener;

        ViewHolder(View itemView) {
            super(itemView);
            memberPhoto = itemView.findViewById(R.id.member_profile_photo);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }
    }
}