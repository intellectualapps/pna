package com.playnetwork.pna.ui.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.room.daos.DeviceDao;
import com.playnetwork.pna.room.daos.EventDao;
import com.playnetwork.pna.room.daos.FeedDao;
import com.playnetwork.pna.room.daos.NetworkDao;
import com.playnetwork.pna.room.daos.OfferDao;
import com.playnetwork.pna.room.daos.YellowPageDao;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.ToastUtil;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;


public class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = BaseActivity.class.getSimpleName();
    private List<WeakReference<Fragment>> mFragList = new ArrayList<WeakReference<Fragment>>();
    private HttpService httpService;
    public Toolbar toolbar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public Toolbar initializeToolbar(String title) {
        if (title == null || title.length() < 2) {
            getToolbar().setVisibility(View.GONE);
            return getToolbar();
        }

        getToolbar().setVisibility(View.VISIBLE);
        getToolbar().setTitle(title);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateUp();
            }
        });
        getToolbar().getMenu().clear();
        return getToolbar();
    }

    public void showErrorPopupMessage(Object message) {
        ToastUtil.showErrorToast(this, (String) message);
    }

    public void showSuccessPopupMessage(Object message) {
        ToastUtil.showSuccessToast(this, (String) message);
    }

    public void showLoadingIndicator(String message) {

        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
            }

            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        } catch (Exception ignored) {
        }
    }

    public void hideLoadingIndicator() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        } catch (Exception ignored) {
        }
    }

    public HttpService getHttpService() {
        if (httpService == null) {
            httpService = new HttpService();
        }

        return httpService;
    }

    public void navigateUp() {
        int backstack = getSupportFragmentManager().getBackStackEntryCount();
        if (backstack > 0) {
            //just pop
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    public void showMainActivity(Activity activity) {
        User user = PlayNetworkDatabase.getPlayNetworkDatabase().userDao().findLastLoggedInUser();

        if (user == null || activity == null || activity.isFinishing()) {
            return;
        }

        Intent intent = null;
        if (user.getHasCreatedProfile()) {
            intent = new Intent(activity, user.getHasViewedNetworkSuggestions() ? MainActivity.class : NetworkActivity.class);
            intent.putExtra(Constants.VIEW_TYPE, Constants.NETWORK_SUGGESTIONS_VIEW_TAG);
        } else {
            intent = new Intent(activity, TempActivity.class);
            if (TextUtils.isEmpty(user.getFname()) || TextUtils.isEmpty(user.getLname())) {
                intent.putExtra(Constants.VIEW_TYPE, Constants.SOCIAL_PROFILE_VIEW_TAG);
            } else {
                intent.putExtra(Constants.VIEW_TYPE, Constants.CREATE_PROFILE_VIEW_TAG);
            }
        }

        intent.putExtra(Constants.USER, user);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void animateSwipeRefreshLayout(final SwipeRefreshLayout swipeRefreshLayout) {
        swipeRefreshLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                swipeRefreshLayout
                        .getViewTreeObserver()
                        .removeOnGlobalLayoutListener(this);
                swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    public void endSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void unregisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        try {
            context.unregisterReceiver(broadcastReceiver);
        } catch (Exception ignored) {
        }
    }

    public Fragment findActiveFragmentByTag(String fragmentTag, FragmentManager fragmentManager) {
        List<Fragment> activeFragments = getActiveFragments();
        if (fragmentManager != null && activeFragments != null && activeFragments.size() > 0) {
            for (Fragment fragment : activeFragments) {
                if (fragment != null && !TextUtils.isEmpty(fragment.getTag()) && fragment.getTag().contains(fragmentTag)) {
                    return fragment;
                }
            }
        }
        return fragmentManager != null ? fragmentManager.findFragmentByTag(fragmentTag) : null;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        mFragList.add(new WeakReference<Fragment>(fragment));
    }

    public List<Fragment> getActiveFragments() {
        ArrayList<Fragment> ret = new ArrayList<Fragment>();
        for (WeakReference<Fragment> ref : mFragList) {
            Fragment f = ref.get();
            if (f != null) {
                if (f.isVisible()) {
                    ret.add(f);
                }
            }
        }
        return ret;
    }

    private Context getPlayNetworkApplicationContext() {
        return PlayNetworkApplication.getAppInstance().getApplicationContext();
    }

    private BroadcastReceiver showChatNotificationPopupReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra(Constants.TITLE);
            String message = intent.getStringExtra(Constants.MESSAGE);
            ToastUtil.showToast(BaseActivity.this, message);
            unregisterReceiver(BaseActivity.this, showChatNotificationPopupReceiver);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(BaseActivity.this, showChatNotificationPopupReceiver);
        PlayNetworkDatabase.destroyDatabaseInstance();
    }

    @Override
    public void onClick(View view) {

    }

    public void launchFragmentDialog(DialogFragment dialogFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null && dialogFragment != null) {
            dialogFragment.show(fragmentManager, dialogFragment.getClass().getSimpleName());
        }
    }

    public void launchFragmentDialogWithBackStack(DialogFragment dialogFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null && dialogFragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.addToBackStack(dialogFragment.getClass().getName());
            fragmentTransaction.commitAllowingStateLoss();
            dialogFragment.show(fragmentManager, dialogFragment.getClass().getSimpleName());
        }
    }

    public String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getPlayNetworkApplicationContext().getString(R.string.error_msg_unknown);
        if (!NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            errorMsg = getResources().getString(R.string.network_connection_label);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }
        return errorMsg;
    }

    public void sendBroadcastIntent(String intentAction) {
        Intent intent = new Intent();
        intent.setAction(intentAction);
        sendBroadcast(intent);
    }

    public void clearCachedData(PlayNetworkDatabase playNetworkDatabase) {
        NetworkDao networkDao = playNetworkDatabase.networkDao();
        OfferDao offerDao = playNetworkDatabase.offerDao();
        EventDao eventDao = playNetworkDatabase.eventDao();
        DeviceDao deviceDao = playNetworkDatabase.deviceDao();
        FeedDao feedDao = playNetworkDatabase.feedDao();
        YellowPageDao yellowPageDao = playNetworkDatabase.yellowPageDao();
        feedDao.deleteCachedFeed();
        Device device = deviceDao.getDevice();
        device.setCachedPNADaily(null);
        device.setAuthToken(null);
        playNetworkDatabase.deviceDao().insertDevice(device);
        offerDao.deleteCachedOffers();
        eventDao.deleteCachedEvents();
        networkDao.deleteCachedNetworks();
        networkDao.deleteCachedRequests();
        yellowPageDao.deleteCachedYellowPages();
    }
}
