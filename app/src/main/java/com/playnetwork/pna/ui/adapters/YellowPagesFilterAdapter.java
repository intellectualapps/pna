package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.FilterRecord;
import com.playnetwork.pna.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class YellowPagesFilterAdapter extends MultiSelectableBaseAdapter {

    private List<FilterRecord> filterRecords;
    private ClickListener clickListener;
    public final static int STATE_MODE = 1, INDUSTRY_MODE = 2;
    private int adapterMode = -1;

    public YellowPagesFilterAdapter(Context context, ClickListener clickListener, int adapterMode) {
        super(context);
        this.clickListener = clickListener;
        this.adapterMode = adapterMode;
        this.filterRecords = new ArrayList<FilterRecord>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view = mInflater.inflate(R.layout.filter_selection_item_layout, parent, false);
        viewHolder = new ViewHolder(view, clickListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FilterRecord filterRecord = filterRecords.get(position);

        final YellowPagesFilterAdapter.ViewHolder viewHolder = (YellowPagesFilterAdapter.ViewHolder) holder;
        if (filterRecord != null) {
            if (isSelected(position)) {
                viewHolder.entryValue.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryBlueWithOpacity));
            } else {
                viewHolder.entryValue.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
            }
            viewHolder.entryValue.setText(filterRecord.getValue());
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(filterRecords) ? 0 : filterRecords.size();
    }

    /*
    Helpers
    */

    public void setItems(List<FilterRecord> items) {
        this.filterRecords = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (filterRecords != null) {
            this.filterRecords.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(FilterRecord filterRecord) {
        if (filterRecord != null) {
            this.filterRecords.add(filterRecord);
            notifyDataSetChanged();
        }
    }

    public void add(FilterRecord filterRecord) {
        filterRecords.add(filterRecord);
        notifyItemInserted(filterRecords.size() - 1);
    }

    public void addAll(List<FilterRecord> filterRecords) {
        if (filterRecords != null && filterRecords.size() > 0) {
            for (FilterRecord filterRecord : filterRecords) {
                add(filterRecord);
            }
        }
        notifyDataSetChanged();
    }

    public void remove(FilterRecord filterRecord) {
        int position = filterRecords.indexOf(filterRecord);
        if (position > -1) {
            filterRecords.remove(position);
            notifyItemRemoved(position);
        }
    }

    private FilterRecord getItem(int position) {
        return filterRecords.get(position);
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView entryValue;
        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            entryValue = (TextView) itemView.findViewById(R.id.entry_value);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }
}