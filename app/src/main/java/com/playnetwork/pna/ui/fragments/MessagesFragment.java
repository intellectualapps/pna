package com.playnetwork.pna.ui.fragments;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.DefaultResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.ChatSession;
import com.playnetwork.pna.data.models.Message;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.MessagesAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.utils.CommonUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.DateUtils;
import com.playnetwork.pna.utils.FirebaseUtils;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesFragment extends BaseFragment implements View.OnClickListener, BaseRecyclerAdapter.ClickListener, ChildEventListener, ValueEventListener, TextWatcher, PNAInterfaces.SessionInitializationListener, PNAInterfaces.NotifyOfflineUserListener {
    private static final String TAG = MessagesFragment.class.getSimpleName();
    private User requestingParticipant, otherParticipant, user;
    private ChatSession chatSession;
    private MessagesAdapter mAdapter;
    private CustomRecyclerView messagesRecyclerView;
    private EditText composeMessageInput;
    private ImageButton sendMessageButton;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mEmptyView;
    private String chatId;
    private String requestingParticipantUsername, otherParticipantUsername;
    private List<Message> messages;
    private DatabaseReference chatSessionMessagesDatabaseReference;
    private boolean isPartnerOnline = false;
    public static int INITIAL_MESSAGE_LOAD_COUNT = 50;
    private boolean isConnectionEnded = false;
    private boolean isUserInitiatedScroll;
    private String currentDate = null;
    private long totalMessageCount = 0;
    private boolean loading = false, hasListEnded;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new MessagesFragment();
        if (args == null) {
            args = new Bundle();
        }

        frag.setArguments(args);
        return frag;
    }

    public MessagesFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
                requestingParticipantUsername = String.valueOf(user.getId());
            }
        }

        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.REQUESTING_MEMBER)) {
                requestingParticipant = getArguments().getParcelable(Constants.REQUESTING_MEMBER);
                if (requestingParticipant != null && !TextUtils.isEmpty(requestingParticipant.getEmail())) {
                    requestingParticipantUsername = String.valueOf(requestingParticipant.getId());
                }
            }

            if (getArguments().containsKey(Constants.OTHER_MEMBER)) {
                otherParticipant = getArguments().getParcelable(Constants.OTHER_MEMBER);
                if (otherParticipant != null && !TextUtils.isEmpty(otherParticipant.getEmail())) {
                    otherParticipantUsername = String.valueOf(otherParticipant.getId());
                }
            }

            if (getArguments().containsKey(Constants.CHAT_ID)) {
                chatId = getArguments().getString(Constants.CHAT_ID);
            }

            if (getArguments().containsKey(Constants.CHAT_SESSION)) {
                chatSession = getArguments().getParcelable(Constants.CHAT_SESSION);
            }
        }

        if (TextUtils.isEmpty(chatId)) {
            showErrorPopupMessage(getString(R.string.chat_setup_error));
            closeFragment();
        }

        messages = new ArrayList<Message>();
        chatSessionMessagesDatabaseReference = FirebaseUtils.getChatSessionMessagesDatabaseReference(chatId);
        setHasOptionsMenu(true);
        hideKeyboard();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.chat_label_toolbar));
        composeMessageInput = view.findViewById(R.id.compose_text_input);
        sendMessageButton = view.findViewById(R.id.send_chat_button);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mEmptyView = view.findViewById(R.id.empty_view);
        messagesRecyclerView = view.findViewById(R.id.messages_recyclerview);

        composeMessageInput.addTextChangedListener(this);
        sendMessageButton.setOnClickListener(this);

        mAdapter = new MessagesAdapter(getContext(), this, requestingParticipantUsername);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        messagesRecyclerView.setLayoutManager(linearLayoutManager);
        messagesRecyclerView.setAdapter(mAdapter);
        messagesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        messagesRecyclerView.setEmptyView(mEmptyView);
        messagesRecyclerView.setHasFixedSize(true);
        messagesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int state) {
                super.onScrollStateChanged(recyclerView, state);
                if (state == RecyclerView.SCROLL_STATE_DRAGGING) {
                    isUserInitiatedScroll = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int topRowVerticalPosition = (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

                if (messages.size() >= INITIAL_MESSAGE_LOAD_COUNT && !loading && !hasListEnded && dy < 0 && linearLayoutManager.findFirstVisibleItemPosition() < 5 && messages.size() < (totalMessageCount == 0 ? 1000 : totalMessageCount) && isUserInitiatedScroll) {
                    messagesRecyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            loading = true;
                            fetchMoreMessages();
                        }
                    });
                }
            }
        });

        messagesRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    messagesRecyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (linearLayoutManager.findLastVisibleItemPosition() < 10) {
                                messagesRecyclerView.scrollToPosition(messages.size() - 1);
                            }
                        }
                    }, 100);
                }
            }
        });

        mSwipeRefreshLayout.setColorSchemeColors(
                getActivity().getResources().getColor(R.color.orange),
                getActivity().getResources().getColor(R.color.green),
                getActivity().getResources().getColor(R.color.blue)
        );

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    loadConversations();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (!TextUtils.isEmpty(chatId)) {
            FirebaseUtils.setupOrLoadChatSession(chatId, requestingParticipantUsername, otherParticipantUsername, this);
        }

        initializeData();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateOnlineStatus(chatId, requestingParticipantUsername, true);
    }

    @Override
    public void onPause() {
        super.onPause();
        updateOnlineStatus(chatId, requestingParticipantUsername, false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        updateOnlineStatus(chatId, requestingParticipantUsername, false);
        removeListeners();
    }

    private void removeListeners() {
        if (chatSessionMessagesDatabaseReference != null) {
            chatSessionMessagesDatabaseReference.removeEventListener((ChildEventListener) this);
            chatSessionMessagesDatabaseReference.removeEventListener((ValueEventListener) this);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.chat_session_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_block:
                Bundle blockArgs = new Bundle();
                blockArgs.putString(Constants.CHAT_ID, chatId);
                break;
            case R.id.action_report:
                Bundle reportArgs = new Bundle();
                reportArgs.putString(Constants.CHAT_ID, chatId);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void initializeData() {
        animateSwipeRefreshLayout(mSwipeRefreshLayout);
        getTotalMessageCount();
        loadConversations();
    }

    private void changeLoadingState(final boolean addLoader) {
        if (addLoader) {
            mAdapter.addLoadingFooter(MessagesAdapter.class, mAdapter);
        } else {
            mAdapter.removeLoadingFooter(MessagesAdapter.class, mAdapter);
        }
    }

    private String getTopmostMessageId(List<Message> messages) {
        for (Message message : messages) {
            if (!TextUtils.isEmpty(message.getId()) && !TextUtils.isEmpty(message.getText()) && !TextUtils.isEmpty(message.getAuthor())) {
                return message.getId();
            }
        }

        return null;
    }

    private void fetchMoreMessages() {
        isUserInitiatedScroll = false;
        changeLoadingState(true);

        String topMostMessageId = getTopmostMessageId(messages);

        if (TextUtils.isEmpty(topMostMessageId)) {
            loading = false;
            return;
        }
        Query messagesQuery = chatSessionMessagesDatabaseReference.orderByKey().endAt(topMostMessageId).limitToLast(INITIAL_MESSAGE_LOAD_COUNT);
        ValueEventListener messagesValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot != null && dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            List<Message> loadedMessages = new ArrayList<Message>();
                            for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                                Message message = FirebaseUtils.getMessageFromMap((Map<String, Object>) messageSnapshot.getValue());
                                if (!messages.contains(message)) {
                                    loadedMessages.add(message);
                                }
                            }
                            loadedMessages = insertDateSeparators(loadedMessages);

                            changeLoadingState(false);
                            messages.addAll(0, loadedMessages);

                            hasListEnded = !ListUtils.isNotEmpty(loadedMessages) && loadedMessages.size() < INITIAL_MESSAGE_LOAD_COUNT;
                            mAdapter.notifyItemRangeInserted(0, loadedMessages.size());
                            loading = false;
                        }
                    }, 1000);
                } else {
                    loading = false;
                    changeLoadingState(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                loading = false;
                isUserInitiatedScroll = false;
                changeLoadingState(false);
            }
        };

        if (NetworkUtils.isConnected(getContext())) {
            messagesQuery.addListenerForSingleValueEvent(messagesValueEventListener);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.send_chat_button:
                if (NetworkUtils.isConnected(getContext())) {
                    if (composeMessageInput.getText().toString().trim().length() > 0) {
                        final String messageToSend = composeMessageInput.getText().toString().trim();
                        uploadMessage(messageToSend);
                    } else {
                        CommonUtils.displayShortToastMessage(getString(R.string.blank_message_label));
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
        }
    }

    @Override
    public void onClick(View v, int position, boolean isLongClick) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0 && !TextUtils.isEmpty(s)) {
            adjustSendButtonState(true);
        } else {
            adjustSendButtonState(false);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void loadConversationAdapter(ArrayList<Message> messages) {
        endSwipeRefreshLayout(mSwipeRefreshLayout);
        mAdapter.setItems(messages);
    }

    private List<Message> insertDateSeparators(List<Message> messagesParam) {
        List<Message> consolidatedMessageList = new ArrayList<Message>();
        for (Message message : messagesParam) {
            consolidatedMessageList.add(message);
            String date = DateUtils.getSimpleDateFormat(message.getTime(), "dd MMM yyyy");

            if (currentDate == null || !currentDate.equalsIgnoreCase(date)) {
                currentDate = date;
                Message newMessage = new Message();
                newMessage.setTime(message.getTime());
                newMessage.setId(date);
                if (messages.contains(newMessage)) {
                    int index = messages.indexOf(newMessage);
                    messages.remove(index);
                    mAdapter.notifyItemRemoved(index);
                }
                consolidatedMessageList.add(consolidatedMessageList.size() - 1, newMessage);
            }
        }
        return consolidatedMessageList;
    }

    private void adjustSendButtonState(boolean stateFlag) {
        if (getContext() != null) {
            if (stateFlag) {
                sendMessageButton.setEnabled(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sendMessageButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorDarkOrange), PorterDuff.Mode.SRC_IN);
                } else {
                    Drawable wrapDrawable = DrawableCompat.wrap(sendMessageButton.getDrawable());
                    DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(getContext(), R.color.colorDarkOrange));
                    sendMessageButton.setImageDrawable(DrawableCompat.unwrap(wrapDrawable));
                }
            } else {
                sendMessageButton.setEnabled(false);
                if (getContext() != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        sendMessageButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorDark), PorterDuff.Mode.SRC_IN);
                    } else {
                        Drawable wrapDrawable = DrawableCompat.wrap(sendMessageButton.getDrawable());
                        DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(getContext(), R.color.colorDark));
                        sendMessageButton.setImageDrawable(DrawableCompat.unwrap(wrapDrawable));
                    }
                }
            }
        }
    }

    private void uploadMessage(String messageToSend) {
        if (!TextUtils.isEmpty(messageToSend)) {
            adjustSendButtonState(false);
            Map<String, Object> newMessageMap = new HashMap<String, Object>();
            Calendar calendar = GregorianCalendar.getInstance();
            long timestamp = calendar.getTimeInMillis();

            newMessageMap.put(Constants.MESSAGE_TEXT_KEY, messageToSend);
            newMessageMap.put(Constants.MESSAGE_AUTHOR_KEY, requestingParticipantUsername);
            newMessageMap.put(Constants.MESSAGE_TIME_KEY, timestamp);

            final String autoGeneratedMessageKey = chatSessionMessagesDatabaseReference.push().getKey();
            DatabaseReference newMessageReference = chatSessionMessagesDatabaseReference.child(autoGeneratedMessageKey);
            newMessageMap.put(Constants.MESSAGE_ID_KEY, autoGeneratedMessageKey);

            newMessageReference.setValue(newMessageMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                }
            });
            composeMessageInput.setText(null);

            if (!isPartnerOnline && !TextUtils.isEmpty(otherParticipantUsername)) {
                Map<String, String> notificationMap = new HashMap<String, String>();
                notificationMap.put(Constants.MESSAGE_NOTIFICATION_ID, chatId);
                notificationMap.put(Constants.OFFLINE_USERNAME, otherParticipantUsername);
                notificationMap.put(Constants.ONLINE_USERNAME, requestingParticipantUsername);
                notificationMap.put(Constants.PLATFORM, Constants.ANDROID_PLATFORM);
            }

        } else {
            CommonUtils.displayShortToastMessage(getString(R.string.blank_message_label));
        }
    }

    private void loadConversations() {
        if (chatSessionMessagesDatabaseReference == null) {
            chatSessionMessagesDatabaseReference = FirebaseUtils.getChatSessionMessagesDatabaseReference(chatId);
        }

        removeListeners();
        chatSessionMessagesDatabaseReference.orderByKey().limitToLast(INITIAL_MESSAGE_LOAD_COUNT).addListenerForSingleValueEvent(this);
    }

    private void getTotalMessageCount() {
        if (chatSessionMessagesDatabaseReference == null) {
            chatSessionMessagesDatabaseReference = FirebaseUtils.getChatSessionMessagesDatabaseReference(chatId);
        }

        chatSessionMessagesDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                totalMessageCount = dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        endSwipeRefreshLayout(mSwipeRefreshLayout);

        ArrayList<Message> loadedMessages = new ArrayList<Message>();
        if (dataSnapshot != null && dataSnapshot.exists() && dataSnapshot.getValue() != null) {
            if (messages != null) {
                messages.clear();
            }
            mAdapter.clearItems();

            if (messages == null) {
                messages = new ArrayList<Message>();
            }

            for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                Message message = FirebaseUtils.getMessageFromMap((Map<String, Object>) messageSnapshot.getValue());
                if (!messages.contains(message)) {
                    loadedMessages.add(message);
                }
            }

            messages.addAll(0, loadedMessages);
            messages = insertDateSeparators(messages);
            mAdapter.setItems(messages);
            isUserInitiatedScroll = false;
            messagesRecyclerView.smoothScrollToPosition(messages.size() - 1);

            loading = false;
        } else {
            mAdapter.setItems(messages);
            loading = false;
        }

        if (ListUtils.isNotEmpty(messages)) {
            chatSessionMessagesDatabaseReference.orderByKey().startAt(messages.get(messages.size() - 1).getId()).addChildEventListener(this);
        } else {
            chatSessionMessagesDatabaseReference.orderByKey().addChildEventListener(this);
        }
        chatSessionMessagesDatabaseReference.removeEventListener((ValueEventListener) this);
    }

    @Override
    public void onChildAdded(final DataSnapshot dataSnapshot, String s) {
        if (dataSnapshot.exists() && dataSnapshot.hasChildren()) {
            new Runnable() {
                @Override
                public void run() {
                    endSwipeRefreshLayout(mSwipeRefreshLayout);
                    Message message = FirebaseUtils.getMessageFromMap((Map<String, Object>) dataSnapshot.getValue());
                    if (!messages.contains(message)) {
                        List<Message> loadedMessages = new ArrayList<Message>();
                        loadedMessages.add(message);
                        loadedMessages = insertDateSeparators(loadedMessages);
                        messages.addAll(loadedMessages);
                        mAdapter.notifyItemRangeInserted(messages.size() - loadedMessages.size(), loadedMessages.size());
                        messagesRecyclerView.smoothScrollToPosition(messages.size() - 1);
                    }
                }
            }.run();
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
    }

    private void updateOnlineStatus(String messageId, String username, boolean status) {
        if (NetworkUtils.isConnected(getContext())) {
            DatabaseReference chatSessionsDatabaseReference = FirebaseUtils.getChatSessionsDatabaseReference().child(messageId);
            chatSessionsDatabaseReference.child(Constants.CHAT_SESSION_PARTICIPANTS_KEY).child(username).setValue(status, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                }
            });
        }
    }

    private String parseRecentMessage(List<Message> recentMessages) {

        StringBuilder parsedMessages = new StringBuilder();
        for (Message message : recentMessages) {
            parsedMessages.append(message.getAuthor());
            parsedMessages.append(" ");
            parsedMessages.append(DateUtils.getSimpleDateFormat(message.getTime(), "dd MMMM, yyyy"));
            parsedMessages.append(Constants.HTML_LINE_BREAK);
            parsedMessages.append(message.getText());
            parsedMessages.append(Constants.HTML_LINE_BREAK);
            parsedMessages.append(Constants.HTML_LINE_BREAK);
        }

        return parsedMessages.toString();
    }

    @Override
    public void onSessionInitialized(ChatSession chatSession) {
        Map<String, Object> map = chatSession.getParticipantData();
        if (map != null && map.size() > 0) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String username = entry.getKey();
                Boolean isUserOnline = false;

                if (entry.getValue().getClass() == Boolean.class) {
                    isUserOnline = (Boolean) entry.getValue();
                } else {
                    FirebaseUtils.getChatSessionsParticipantsDatabaseReference(chatId).child(entry.getKey()).removeValue();
                    username = (String) entry.getValue();
                    isUserOnline = false;
                }

                if (!user.getEmail().equalsIgnoreCase(username)) {
                    otherParticipantUsername = username;
                    isPartnerOnline = isUserOnline;
                } else {
                    requestingParticipantUsername = username;
                }
            }
        }

        FirebaseUtils.getChatSessionsParticipantsDatabaseReference(chatId).child(otherParticipantUsername).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists() && dataSnapshot.getValue() != null) {
                    isPartnerOnline = (Boolean) dataSnapshot.getValue();
                } else {
                    isPartnerOnline = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        updateOnlineStatus(chatId, requestingParticipantUsername, true);
    }

    @Override
    public void onNotificationSent(DefaultResponse defaultResponse) {

    }
}
