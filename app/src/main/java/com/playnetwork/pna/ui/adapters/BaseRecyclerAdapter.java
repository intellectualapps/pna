package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Message;

/**
 *
 */
public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    static Context context;
    LayoutInflater mInflater;
    public boolean isLoadingAdded = false;
    public boolean retryPageLoad = false;
    public PNAInterfaces.PaginationAdapterCallback callback;
    public String errorMsg;
    public final int LOADING = 1;
    private int position = -1;

    public BaseRecyclerAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public BaseRecyclerAdapter() {

    }

    public interface ClickListener {
        public void onClick(View v, int position, boolean isLongClick);
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ProgressBar mProgressBar;
        public ImageButton mRetryBtn;
        public TextView mErrorTxt;
        public LinearLayout mErrorLayout;

        public LoadingViewHolder(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    hideRetry();
                    callback.loadMoreItems();
                    break;
            }
        }
    }

    public void initializeLoadingViewHolder(RecyclerView.ViewHolder holder) {
        LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
        if (retryPageLoad) {
            loadingViewHolder.mErrorLayout.setVisibility(View.VISIBLE);
            loadingViewHolder.mProgressBar.setVisibility(View.GONE);
            loadingViewHolder.mErrorTxt.setText(
                    errorMsg != null ?
                            errorMsg :
                            context.getString(R.string.error_msg_unknown));

        } else {
            loadingViewHolder.mErrorLayout.setVisibility(View.GONE);
            loadingViewHolder.mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideRetry() {
        retryPageLoad = false;
        notifyItemChanged(getItemCount());
        this.errorMsg = null;
    }

    private String getClassName(String className) {
        if (className.contains(".")) {
            String[] splitName = className.split("\\.");
            return splitName[splitName.length - 1];
        }

        return className;
    }

    public void addLoadingFooter(Class<?> clazz, Object adapter) {
        isLoadingAdded = true;
        switch (getClassName(clazz.getName())) {
            case "MessagesAdapter":
                MessagesAdapter messagesAdapter = (MessagesAdapter) adapter;
                messagesAdapter.add(new Message());
                break;
        }
    }

    public void removeLoadingFooter(Class<?> clazz, Object adapter) {
        isLoadingAdded = false;
        switch (getClassName(clazz.getName())) {
            case "MessagesAdapter":
                MessagesAdapter messagesAdapter = (MessagesAdapter) adapter;
                position = 0;

                Message message = messagesAdapter.getMessages().get(position);
                if (message != null && TextUtils.isEmpty(message.getId())) {
                    messagesAdapter.getMessages().remove(position);
                    messagesAdapter.notifyItemRemoved(position);
                }
                break;
        }
    }
}
