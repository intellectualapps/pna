package com.playnetwork.pna.ui.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.responses.NetworkResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.ProfileActivity;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.NetworkSearchAdapter;
import com.playnetwork.pna.ui.adapters.SpinnerArrayAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.ui.dialogs.FilterBottomSheetDialogFragment;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.playnetwork.pna.utils.PaginationScrollListener;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkSearchFragment extends BaseFragment implements BaseRecyclerAdapter.ClickListener, PNAInterfaces.PaginationAdapterCallback, Toolbar.OnMenuItemClickListener, SearchView.OnQueryTextListener, SearchView.OnCloseListener, View.OnClickListener, AdapterView.OnItemSelectedListener {
    private User user;
    private NetworkSearchAdapter networkSearchAdapter;
    private CustomRecyclerView networkSearchRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private Spinner industrySpinner;
    private View mEmptyView;
    private PlayNetworkDatabase playNetworkDatabase;
    private boolean isLoading = false;
    private boolean hasListEnded = false;
    private int start = 0, pageSize = 10;
    private int currentPage = start;
    private SearchView searchView;
    private MenuItem searchMenuItem;
    private String searchTerm = null;
    private String memberId = null;
    private Toolbar toolbar;
    private boolean inSearchMode = false, inFilterMode;
    private boolean isSearchRunning = false;
    private List<Industry> industries;

    private List<User> networkSearchResults;
    private SpinnerArrayAdapter industryAdapter;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new NetworkSearchFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public NetworkSearchFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        if (getArguments() != null && getArguments().containsKey(Constants.USER)) {
            user = getArguments().getParcelable(Constants.USER);
            if (user != null) {
                memberId = String.valueOf(user.getId());
            }
        }
        setHasOptionsMenu(true);
        industries = PlayNetworkDatabase.getPlayNetworkDatabase().industryDao().getAllIndustries();
        registerReceiver(getContext(), updateMemberListReceiver, new IntentFilter(Constants.UPDATE_MEMBER_LIST_INTENT_FILTER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_network_search, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (searchView != null) {
            searchView.clearFocus();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.network_search_menu, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        if (searchMenuItem == null) {
            return;
        }
        if (getActivity() != null) {
            SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) searchMenuItem.getActionView();
            try {
                SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
                searchAutoComplete.setHintTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
                searchAutoComplete.setCursorVisible(true);
                searchAutoComplete.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));
                Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
                mCursorDrawableRes.setAccessible(true);
                mCursorDrawableRes.set(searchAutoComplete, R.drawable.search_cursor);

                if (searchManager != null) {
                    searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
                }
                searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
                searchView.setQueryHint(getString(R.string.action_search_label));
                searchView.setIconified(false);
                searchView.setSubmitButtonEnabled(true);
                searchView.setOnQueryTextListener(this);
                searchView.setOnCloseListener(this);
            } catch (Exception ignored) {
            }
        }
    }

    private void initializeToolbar(final Activity activity) {
        if (activity != null) {
            toolbar = ((BaseActivity) activity).initializeToolbar(getString(R.string.member_search_toolbar_label));
            ((BaseActivity) activity).setSupportActionBar(toolbar);
            toolbar.setOnMenuItemClickListener(this);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) activity).navigateUp();
                }
            });
        }
    }

    private void init(View view, Bundle savedInstanceState) {
        initializeToolbar(getActivity());
        networkSearchRecyclerView = view.findViewById(R.id.network_search_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        industrySpinner = view.findViewById(R.id.industry_spinner);

        mEmptyView = view.findViewById(R.id.empty_view);
        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);

        mEmptyViewLabel.setText(R.string.network_suggestions_empty_label);

        networkSearchRecyclerView.setEmptyView(mEmptyView);
        networkSearchAdapter = new NetworkSearchAdapter(getContext(), this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        networkSearchRecyclerView.setLayoutManager(linearLayoutManager);
        networkSearchRecyclerView.setAdapter(networkSearchAdapter);

        industryAdapter = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_item, new ArrayList<String>());
        industryAdapter.addAll(getIndustryList(industries).toArray(new String[getIndustryList(industries).size()]));

        industrySpinner.setAdapter(industryAdapter);
        industrySpinner.setOnItemSelectedListener(this);

        networkSearchRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                    if (!hasListEnded) {
                        isLoading = true;
                        currentPage += 1;
                        start = currentPage * pageSize;
                        if (networkSearchResults != null && networkSearchResults.size() >= pageSize) {
                            if (inSearchMode) {
                                performSearch(start, pageSize, memberId, searchTerm, true);
                            } else {
                                fetchNetworkSuggestions(start, pageSize, true);
                            }
                        }
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
            }

            @Override
            public boolean hasListEnded() {
                return hasListEnded;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.green),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.blue),
                ContextCompat.getColor(getPlayNetworkApplicationContext(), R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    industrySpinner.setSelection(0);
                    inSearchMode = false;
                    inFilterMode = false;
                    if (searchView != null) {
                        searchMenuItem.collapseActionView();
                    }
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        fetchNetworkSuggestions(0, pageSize, false);
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (NetworkUtils.isConnected(getContext())) {
            animateSwipeRefreshLayout(swipeRefreshLayout);
            fetchNetworkSuggestions(0, pageSize, false);
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Bundle bundle = new Bundle();
        switch (item.getItemId()) {
            default:
                return false;
        }
    }

    private void fetchNetworkSuggestions(int start, int pageSize, boolean inLoadMoreMode) {
        if (!inLoadMoreMode || inFilterMode) {
            hasListEnded = false;
            currentPage = 0;
            animateSwipeRefreshLayout(swipeRefreshLayout);
        }

        getHttpService().fetchNetworkSuggestions(buildParameterMap(start, pageSize), buildSuggestionsCallback(inLoadMoreMode));
    }

    private void performSearch(int start, int pageSize, String memberId, String searchTerm, boolean inLoadMoreMode) {
        if (NetworkUtils.isConnected(getContext())) {
            isSearchRunning = true;
            if (!inLoadMoreMode) {
                showLoadingIndicator(getString(R.string.search_request_message));
                //animateSwipeRefreshLayout(swipeRefreshLayout);
                hasListEnded = false;
                currentPage = 0;
            }

            getHttpService().fetchSearchResults(memberId, searchTerm, buildParameterMap(start, pageSize), buildSearchNetworkCallback(inLoadMoreMode));
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private Map<String, String> buildParameterMap(int start, int pageSize) {
        Map<String, String> requestMap = new HashMap<String, String>();
        requestMap.put(Constants.START, String.valueOf(start));
        requestMap.put(Constants.PAGE_SIZE, String.valueOf(pageSize));
        requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
        return requestMap;
    }

    private void updateSearchList(List<User> searchResultsParam) {
        if (networkSearchResults == null) {
            networkSearchResults = new ArrayList<User>();
        }
        if (searchResultsParam != null && searchResultsParam.size() > 0) {
            networkSearchResults.addAll(searchResultsParam);
        }
    }

    private void loadAdapter(List<User> searchResults) {
        if (isAdded() && isVisible()) {
            hideKeyboard();
            networkSearchAdapter.addAll(searchResults);
        }
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1) {
            User suggestion = networkSearchAdapter.getUserList().get(position);
            switch (view.getId()) {
                case R.id.add_to_network_button:
                    if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
                        String memberIds = String.valueOf(suggestion.getId());
                        Map<String, String> requestMap = new HashMap<String, String>();
                        requestMap.put(Constants.EMAIL_ADDRESS, user.getEmail());
                        requestMap.put(Constants.MEMBER_IDS, memberIds);
                        getHttpService().addMembersToNetwork(requestMap, buildAddToNetworkCallbackCallback(position));
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                    break;
                default:
                    Intent intent = new Intent(getContext(), ProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Constants.USER, user);
                    bundle.putParcelable(Constants.MEMBER, suggestion);
                    bundle.putInt(Constants.POSITION, position);
                    if (!TextUtils.isEmpty(suggestion.getNetworkState())) {
                        switch (suggestion.getNetworkState()) {
                            case Constants.PENDING_NETWORK_STATE:
                                bundle.putString(Constants.VIEW_TYPE, Constants.PENDING_PROFILE_VIEW_TAG);
                                break;
                            case Constants.ACCEPTED_NETWORK_STATE:
                                bundle.putString(Constants.VIEW_TYPE, Constants.MEMBER_PROFILE_VIEW_TAG);
                                break;
                            case Constants.NONE_NETWORK_STATE:
                            default:
                                bundle.putString(Constants.VIEW_TYPE, Constants.SEARCH_DETAIL_VIEW_TAG);
                                break;
                        }
                    } else {
                        bundle.putString(Constants.VIEW_TYPE, Constants.SEARCH_DETAIL_VIEW_TAG);
                    }
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
        unregisterReceiver(getContext(), updateMemberListReceiver);
    }

    @Override
    public void loadMoreItems() {
        if (NetworkUtils.isConnected(getPlayNetworkApplicationContext())) {
            start = currentPage * pageSize;
            if (networkSearchResults != null && networkSearchResults.size() >= pageSize) {
                if (inSearchMode) {
                    performSearch(start, pageSize, memberId, searchTerm, true);
                } else {
                    fetchNetworkSuggestions(start, pageSize, true);
                }
            }
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query != null && query.trim().length() > 0) {
            if (query.trim().length() > 2) {
                inSearchMode = true;
                searchTerm = query;
                performSearch(0, pageSize, memberId, searchTerm, false);
            } else {
                showErrorPopupMessage(getString(R.string.query_short_error));
            }
        } else {
            networkSearchAdapter.setItems(networkSearchResults);
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(final String query) {
        return false;
    }

    @Override
    public boolean onClose() {
        inSearchMode = false;
        networkSearchAdapter.setItems(networkSearchResults);
        return false;
    }

    private BroadcastReceiver updateMemberListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int position = intent.getIntExtra(Constants.POSITION, -1);
            if (position > -1) {
                networkSearchAdapter.remove(position);
                networkSearchResults.remove(position);
            }
        }
    };

    private Callback<NetworkResponse> buildAddToNetworkCallbackCallback(final int position) {
        return new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                NetworkResponse networkResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    if (position > -1 && isVisible()) {
                        networkSearchAdapter.remove(position);
                        networkSearchResults.remove(position);
                    }
                }
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<NetworkResponse> buildSuggestionsCallback(final boolean inLoadMoreMode) {
        return new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                NetworkResponse networkResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                isLoading = false;
                if (inLoadMoreMode) {
                    networkSearchAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && networkResponse != null && isVisible()) {
                    List<User> searchResultsParam = networkResponse.getNetworkSuggestions();

                    if (searchResultsParam == null || searchResultsParam.size() == 0 || searchResultsParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (networkSearchResults != null) {
                            networkSearchResults.clear();
                        }
                        networkSearchAdapter.clearItems();
                    }

                    updateSearchList(searchResultsParam);
                    loadAdapter(searchResultsParam);

                    if (!hasListEnded) {
                        networkSearchAdapter.addLoadingFooter();
                    }
                } else {
                    loadAdapter(networkSearchResults);
                }
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    networkSearchAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    private Callback<NetworkResponse> buildSearchNetworkCallback(final boolean inLoadMoreMode) {
        return new Callback<NetworkResponse>() {
            @Override
            public void onResponse(Call<NetworkResponse> call, Response<NetworkResponse> response) {
                NetworkResponse networkResponse = response.body();
                hideLoadingIndicator();
                isSearchRunning = false;
                inSearchMode = true;
                isLoading = false;
                endSwipeRefreshLayout(swipeRefreshLayout);
                if (inLoadMoreMode) {
                    networkSearchAdapter.removeLoadingFooter();
                }

                if (ApiUtils.isSuccessResponse(response) && networkResponse != null && isVisible()) {
                    List<User> searchResultsParam = networkResponse.getMembers();

                    if (searchResultsParam == null || searchResultsParam.size() == 0 || searchResultsParam.size() < pageSize) {
                        hasListEnded = true;
                    }

                    if (!inLoadMoreMode) {
                        if (networkSearchResults != null) {
                            networkSearchResults.clear();
                        }
                        networkSearchAdapter.clearItems();
                    }

                    updateSearchList(searchResultsParam);
                    loadAdapter(searchResultsParam);
                    endSwipeRefreshLayout(swipeRefreshLayout);
                } else {
                    loadAdapter(networkSearchResults);
                }
            }

            @Override
            public void onFailure(Call<NetworkResponse> call, Throwable t) {
                isSearchRunning = false;
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
                if (inLoadMoreMode) {
                    networkSearchAdapter.showRetry(true, fetchErrorMessage(t));
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    private void dismissSearchView() {
        if (searchView != null) {
            searchView.clearFocus();
            searchMenuItem.collapseActionView();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.industry_spinner:
                if (position > 0) {
                    inSearchMode = true;
                    isSearchRunning = true;
                    Industry industry = industries.get(position - 1);
                    searchTerm = String.valueOf(industry.getId());
                    dismissSearchView();
                    performSearch(0, pageSize, memberId, searchTerm, false);
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
