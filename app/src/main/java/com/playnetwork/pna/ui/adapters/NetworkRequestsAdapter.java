package com.playnetwork.pna.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.utils.ListUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class NetworkRequestsAdapter extends BaseRecyclerAdapter {

    private List<Network> networkList;
    private ClickListener clickListener;
    private final int NETWORK_REQUEST = 0;
    private int adapterMode = 1;

    public NetworkRequestsAdapter(Context context, ClickListener clickListener, PNAInterfaces.PaginationAdapterCallback paginationAdapterCallback) {
        super(context);
        this.clickListener = clickListener;
        this.callback = paginationAdapterCallback;
        this.networkList = new ArrayList<Network>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view;

        switch (viewType) {
            default:
            case NETWORK_REQUEST:
                view = mInflater.inflate(R.layout.network_request_item_layout, parent, false);
                viewHolder = new NetworkRequestsAdapter.ViewHolder(view, this.clickListener);
                break;
            case LOADING:
                view = mInflater.inflate(R.layout.item_progress_layout, parent, false);
                viewHolder = new LoadingViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Network network = networkList.get(position);
        switch (getItemViewType(position)) {
            case LOADING:
                initializeLoadingViewHolder(holder);
                break;
            default:
                final NetworkRequestsAdapter.ViewHolder viewHolder = (NetworkRequestsAdapter.ViewHolder) holder;
                if (network != null) {

                    viewHolder.memberFullName.setText(String.format("%s %s", network.getFirstName(), network.getLastName()));
                    viewHolder.memberJobTitle.setText(network.getJobTitle());
                    if (network.getProfilePhotoUrl() != null) {
                        Picasso.with(context).load(network.getProfilePhotoUrl()).placeholder(R.color.colorGray).into(viewHolder.memberProfilePhoto);
                    }
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return ListUtils.isEmpty(networkList) ? 0 : networkList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == networkList.size() - 1 && isLoadingAdded) {
            return LOADING;
        }

        return NETWORK_REQUEST;
    }

    /*
    Helpers
    */

    public void setItems(List<Network> items) {
        this.networkList = items;
        notifyDataSetChanged();
    }

    public void clearItems() {
        if (networkList != null) {
            this.networkList.clear();
        }
        notifyDataSetChanged();
    }

    public void addItem(Network network) {
        if (network != null) {
            this.networkList.add(network);
            notifyDataSetChanged();
        }
    }

    public void add(Network network) {
        networkList.add(network);
        notifyItemInserted(networkList.size() - 1);
    }

    public void addAll(List<Network> networkList) {
        if (networkList != null && networkList.size() > 0) {
            for (Network network : networkList) {
                add(network);
            }
        }
        notifyDataSetChanged();
    }

    public void remove(Network network) {
        int position = networkList.indexOf(network);
        if (position > -1) {
            networkList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private Network getItem(int position) {
        return networkList.get(position);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Network());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = networkList.size() - 1;
        if (position > -1) {
            Network network = getItem(position);

            if (network != null) {
                if (TextUtils.isEmpty(network.getMemberId())) {
                    networkList.remove(position);
                    notifyItemRemoved(position);
                }
            }
        }
    }

    private static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView memberFullName, memberJobTitle;
        ImageView memberProfilePhoto;

        ClickListener clickListener;

        ViewHolder(View itemView, ClickListener clickListener) {
            super(itemView);
            memberFullName = (TextView) itemView.findViewById(R.id.member_full_name);
            memberJobTitle = (TextView) itemView.findViewById(R.id.member_job_title);
            memberProfilePhoto = (ImageView) itemView.findViewById(R.id.member_profile_photo);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            memberFullName.setOnClickListener(this);
            memberJobTitle.setOnClickListener(this);
            memberProfilePhoto.setOnClickListener(this);
            this.clickListener = clickListener;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, false);
        }

        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            clickListener.onClick(view, position, true);
            return false;
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(networkList.size() - 1);
        if (errorMsg != null) this.errorMsg = errorMsg;
    }
}