package com.playnetwork.pna.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.enums.PaymentType;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.EventConfirmation;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.ui.activities.ProfileActivity;
import com.playnetwork.pna.ui.dialogs.PaymentCheckoutDialog;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.CurrencyUtils;


public class EventCheckoutFragment extends BaseFragment implements View.OnClickListener, NumberPicker.OnValueChangeListener {
    private User user;
    private Button payButton;
    private Double totalAmount, unitPrice;
    private TextView eventTitleView, selectedPurchaseOptionView, purchasePriceView, memberFullNameView, memberEmailAddressView;
    private NumberPicker quantityPicker;
    private EventConfirmation eventConfirmation;
    private PaymentType paymentType;
    private PNAInterfaces.EventPaymentCompletionListener eventPaymentCompletionListener;

    public static Fragment newInstance(Bundle args) {
        Fragment frag = new EventCheckoutFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public EventCheckoutFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey(Constants.USER)) {
                user = getArguments().getParcelable(Constants.USER);
            }
            if (getArguments().containsKey(Constants.EVENT_CONFIRMATION)) {
                eventConfirmation = getArguments().getParcelable(Constants.EVENT_CONFIRMATION);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_checkout, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        ((BaseActivity) getActivity()).initializeToolbar(getString(R.string.event_purchase_confirmation));
        payButton = view.findViewById(R.id.pay_button);
        memberEmailAddressView = view.findViewById(R.id.member_email_address);
        memberFullNameView = view.findViewById(R.id.member_full_name);
        quantityPicker = view.findViewById(R.id.quantity_picker);
        eventTitleView = view.findViewById(R.id.event_title);
        selectedPurchaseOptionView = view.findViewById(R.id.purchase_option);
        purchasePriceView = view.findViewById(R.id.purchase_price);

        quantityPicker.setMinValue(1);
        quantityPicker.setMaxValue(50);
        quantityPicker.setOnValueChangedListener(this);

        payButton.setOnClickListener(this);

        loadUserData();
    }

    private void loadUserData() {
        if (user != null) {
            if (!TextUtils.isEmpty(user.getFlname())) {
                memberFullNameView.setText(user.getFlname());
            }

            if (!TextUtils.isEmpty(user.getEmail())) {
                memberEmailAddressView.setText(user.getEmail());
            }
        }

        if (eventConfirmation != null) {
            unitPrice = eventConfirmation.getPrice();
            if (!TextUtils.isEmpty(eventConfirmation.getSelectedPurchaseOption())) {
                paymentType = PaymentType.fromString(eventConfirmation.getSelectedPurchaseOption());
                if (paymentType != null) {
                    selectedPurchaseOptionView.setText(paymentType.getValue());
                }
            }
            eventTitleView.setText(eventConfirmation.getEventName());
            updateTotalAmount(1);
        }
    }

    private void updateTotalAmount(int quantity) {
        totalAmount = unitPrice * quantity;
        String formattedAmount = CurrencyUtils.formatToCurrencyWithSymbol(totalAmount, Constants.CURRENCY_SYMBOL);
        purchasePriceView.setText(formattedAmount);
        eventConfirmation.setQuantity(quantity);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pay_button:
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.USER, user);
                bundle.putParcelable(Constants.EVENT_CONFIRMATION, eventConfirmation);
                bundle.putString(Constants.FULL_NAME, user.getFlname());
                bundle.putString(Constants.EMAIL_ADDRESS, user.getEmail());
                bundle.putString(Constants.PAYMENT_TYPE, paymentType.name());
                bundle.putString(Constants.VIEW_TYPE, Constants.PAYMENT_CONFIRMATION_VIEW_TAG);
                bundle.putDouble(Constants.AMOUNT, totalAmount);
                bundle.putString(Constants.CONFIRMATION_CONTENT, getString(R.string.payment_confirmation_for_events, quantityPicker.getValue(), paymentType.getValue().concat(quantityPicker.getValue() > 1 ? "s" : ""), eventConfirmation.getEventName()));
                Intent intent = new Intent(getContext(), ProfileActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, Constants.EVENT_PAYMENT_REQUEST_CODE);
                break;
        }
    }

    public void setPaymentCompletedListener(PNAInterfaces.EventPaymentCompletionListener eventPaymentCompletionListener) {
        this.eventPaymentCompletionListener = eventPaymentCompletionListener;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.EVENT_PAYMENT_REQUEST_CODE) {
            if (eventPaymentCompletionListener != null) {
                eventPaymentCompletionListener.onPaymentCompleted();
            }
            closeFragment();
        }
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        updateTotalAmount(newVal);
    }
}
