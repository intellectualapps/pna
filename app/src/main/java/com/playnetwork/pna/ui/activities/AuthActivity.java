package com.playnetwork.pna.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.fragments.CreateProfileFragment;
import com.playnetwork.pna.ui.fragments.ForgotPasswordFragment;
import com.playnetwork.pna.ui.fragments.LoginFragment;
import com.playnetwork.pna.ui.fragments.SignupFragment;
import com.playnetwork.pna.ui.fragments.SocialProfileFragment;
import com.playnetwork.pna.utils.Constants;

public class AuthActivity extends BaseActivity {
    private Bundle fragmentBundle;
    private String viewType;
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        toolbar = findViewById(R.id.toolbar);

        getToolbar().setVisibility(View.GONE);

        if (getIntent().getExtras() != null) {
            viewType = getIntent().getExtras().getString(Constants.VIEW_TYPE);
            fragmentBundle = getIntent().getExtras();
        }

        if (savedInstanceState == null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            Fragment frag = null;
            if (viewType != null) {
                switch (viewType) {
                    case Constants.LOGIN_VIEW_TAG:
                        frag = LoginFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.SIGN_UP_VIEW_TAG:
                        frag = SignupFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.FORGOT_PASSWORD_VIEW_TAG:
                        frag = ForgotPasswordFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.CREATE_PROFILE_VIEW_TAG:
                        frag = CreateProfileFragment.newInstance(fragmentBundle);
                        break;
                    case Constants.SOCIAL_PROFILE_VIEW_TAG:
                        frag = SocialProfileFragment.newInstance(fragmentBundle);
                        break;
                    default:
                        frag = LoginFragment.newInstance(fragmentBundle);
                        break;
                }
            } else {
                frag = LoginFragment.newInstance(null);
            }

            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getSimpleName());
            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(LoginFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }
}
