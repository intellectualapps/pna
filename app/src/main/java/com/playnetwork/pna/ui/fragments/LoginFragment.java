package com.playnetwork.pna.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.ApiModule;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.api.responses.AuthenticationResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.BaseActivity;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.NetworkUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends BaseFragment implements View.OnClickListener, PNAInterfaces.SocialAuthenticationListener {
    private User user;
    private EditText emailAddressInput, passwordInput;
    private Button loginButton, signUpButton, resetPasswordButton;
    private View facebookLoginView;
    private Toolbar toolbar;
    private PlayNetworkDatabase playNetworkDatabase;


    public static Fragment newInstance(Bundle args) {
        Fragment frag = new LoginFragment();
        if (args == null) {
            args = new Bundle();
        }
        frag.setArguments(args);
        return frag;
    }

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mSnackBarView = view;
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view, savedInstanceState);
    }

    private void init(View view, Bundle savedInstanceState) {
        resetPasswordButton = view.findViewById(R.id.forgot_password_button);
        emailAddressInput = view.findViewById(R.id.email_address);
        passwordInput = view.findViewById(R.id.password);
        loginButton = view.findViewById(R.id.login_button);
        signUpButton = view.findViewById(R.id.signup_button);
        facebookLoginView = view.findViewById(R.id.facebook_login_button);

        resetPasswordButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        facebookLoginView.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        loadSavedLoginData();
    }

    @Override
    public void onClick(View v) {
        String message = getString(R.string.login_progress_label);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.login_button:
                if (validateEmailInput(emailAddressInput) && validateTextInput(passwordInput)) {
                    if (NetworkUtils.isConnected(getContext())) {
                        hideKeyboard();
                        showLoadingIndicator(message);
                        getHttpService().authenticateUser(getUserInput(), buildAuthenticateCallback(getUserInput().get(Constants.PASSWORD), getUserInput().get(Constants.EMAIL_ADDRESS)));
                    } else {
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                }
                break;
            case R.id.signup_button:
                replaceFragment(SignupFragment.newInstance(null));
                break;
            case R.id.forgot_password_button:
                replaceFragment(ForgotPasswordFragment.newInstance(null));
                break;
            case R.id.facebook_login_button:
                manageFacebookAuthentication(this);
                break;
        }
    }

    public Map<String, String> getUserInput() {
        Map<String, String> map = new HashMap<String, String>();
        String password = passwordInput.getText().toString().trim();
        String emailAddress = emailAddressInput.getText().toString().trim();
        map.put(Constants.PASSWORD, password);
        map.put(Constants.EMAIL_ADDRESS, emailAddress);
        map.put(Constants.AUTH_TYPE, Constants.EMAIL_AUTH_TYPE);
        return map;
    }

    @Override
    public void onUserAuthenticated(Map<String, String> profileData) {
        Map<String, String> requestMap = new HashMap<String, String>();
        if (profileData.containsKey(Constants.EMAIL_ADDRESS) && !TextUtils.isEmpty(profileData.get(Constants.EMAIL_ADDRESS))) {
            requestMap.put(Constants.ID, profileData.get(Constants.EMAIL_ADDRESS));
            requestMap.put(Constants.SOCIAL_AUTH_TYPE, Constants.SOCIAL_AUTH_EMAIL_TYPE);
            completeSocialAuthentication(requestMap, profileData);
            return;
        } else {
            if (profileData.containsKey(Constants.PHONE_NUMBER) && !TextUtils.isEmpty(profileData.get(Constants.PHONE_NUMBER))) {
                requestMap.put(Constants.ID, profileData.get(Constants.PHONE_NUMBER));
                requestMap.put(Constants.SOCIAL_AUTH_TYPE, Constants.SOCIAL_AUTH_PHONE_TYPE);
                completeSocialAuthentication(requestMap, profileData);
                return;
            }
        }

        hideLoadingIndicator();
        showErrorPopupMessage(getString(R.string.social_authentication_error));
    }

    private void completeSocialAuthentication(Map<String, String> requestMap, Map<String, String> profileData) {
        if (NetworkUtils.isConnected(getContext())) {
            String message = getString(R.string.login_progress_label);
            hideKeyboard();
            showLoadingIndicator(message);
            getHttpService().authenticateSocialAccount(requestMap, buildSocialAuthenticationCallback(requestMap.get(Constants.ID), profileData));
        } else {
            hideLoadingIndicator();
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void loadSavedLoginData() {
        User user = PlayNetworkDatabase.getPlayNetworkDatabase().userDao().findLastLoggedInUser();
        if (user != null && !TextUtils.isEmpty(user.getEmail()) && !TextUtils.isEmpty(user.getPassword())) {
            emailAddressInput.setText(user.getEmail());
            passwordInput.setText(user.getPassword());
        }
    }

    private Callback<AuthenticationResponse> buildAuthenticateCallback(final String password, final String emailAddress) {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response)) {
                    HttpService.resetApiService();
                    ApiModule.resetApiClient();
                    User user = saveUserData(playNetworkDatabase, authenticationResponse, password, emailAddress);
                    if (user.getHasCreatedProfile()) {
                        if (getActivity() != null && isVisible()) {
                            ((BaseActivity) getActivity()).showMainActivity(getActivity());
                        }
                    } else {
                        if (isVisible()) {
                            replaceFragmentWithoutBackStack(CreateProfileFragment.newInstance(null));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    private Callback<AuthenticationResponse> buildSocialAuthenticationCallback(final String emailAddress, final Map<String, String> profileData) {
        return new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                AuthenticationResponse authenticationResponse = response.body();
                hideLoadingIndicator();
                if (ApiUtils.isSuccessResponse(response) && authenticationResponse != null) {
                    HttpService.resetApiService();
                    ApiModule.resetApiClient();
                    User user = saveUserData(playNetworkDatabase, authenticationResponse, null, emailAddress);
                    if (user.getHasCreatedProfile()) {
                        if (getActivity() != null && isVisible()) {
                            ((BaseActivity) getActivity()).showMainActivity(getActivity());
                        }
                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Constants.PROFILE_DATA, (Serializable) profileData);
                        bundle.putParcelable(Constants.USER, user);
                        if (isVisible()) {
                            replaceFragmentWithoutBackStack(SocialProfileFragment.newInstance(bundle));
                        }
                    }
                } else {
                    showErrorPopupMessage(getString(R.string.social_authentication_error));
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                hideLoadingIndicator();
                showErrorPopupMessage(t.getMessage());
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }
}
