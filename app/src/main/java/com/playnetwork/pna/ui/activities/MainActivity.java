package com.playnetwork.pna.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.playnetwork.pna.BuildConfig;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.ApiModule;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.api.responses.HomeScreenResponse;
import com.playnetwork.pna.data.interfaces.PNAInterfaces;
import com.playnetwork.pna.data.models.HomeScreenActiveMembers;
import com.playnetwork.pna.data.models.HomeScreenEvent;
import com.playnetwork.pna.data.models.HomeScreenItem;
import com.playnetwork.pna.data.models.HomeScreenOffer;
import com.playnetwork.pna.data.models.HomeScreenPNADaily;
import com.playnetwork.pna.data.models.HomeScreenVideo;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.adapters.BaseRecyclerAdapter;
import com.playnetwork.pna.ui.adapters.HomeScreenAdapter;
import com.playnetwork.pna.ui.customviews.CustomRecyclerView;
import com.playnetwork.pna.ui.customviews.CustomTypefaceSpan;
import com.playnetwork.pna.ui.dialogs.PNADailyDialog;
import com.playnetwork.pna.utils.ApiUtils;
import com.playnetwork.pna.utils.Constants;
import com.playnetwork.pna.utils.FCMTokenUtils;
import com.playnetwork.pna.utils.FontUtils;
import com.playnetwork.pna.utils.ListUtils;
import com.playnetwork.pna.utils.NetworkUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, BaseRecyclerAdapter.ClickListener, View.OnClickListener, PNAInterfaces.UserProfileUpdateListener {
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private static final int DRAWER_CLOSER_INTERVAL = 250;

    private static final int REQ_START_STANDALONE_PLAYER = 1;
    private static final int REQ_RESOLVE_SERVICE_MISSING = 2;

    private HomeScreenAdapter homeScreenAdapter;
    private CustomRecyclerView homeScreenItemsRecyclerview;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView mEmptyViewLabel, mRetryButton;
    private View mEmptyView;

    private List<HomeScreenItem> homeScreenItems = new ArrayList<HomeScreenItem>();
    private User user;
    private PlayNetworkDatabase playNetworkDatabase;
    private TextView memberFullNameView;
    private TextView memberOccupationView;
    private TextView memberLocationView;
    private ImageView memberProfileImageView;
    private boolean hasPageInitialized = false;
    public static PNAInterfaces.UserProfileUpdateListener userProfileUpdateListener;

    private boolean pushNotificationReceived = false, hasNotificationBeenShown = false;
    private String notificationViewType = "default", memberId, memberFullName, networkId, notificationContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        hasPageInitialized = true;

        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        user = playNetworkDatabase.userDao().findLastLoggedInUser();
        homeScreenItems = playNetworkDatabase.homeScreenItemDao().getAll();

        userProfileUpdateListener = this;

        updateIntent(getIntent());

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
        memberFullNameView = headerLayout.findViewById(R.id.member_full_name);
        memberOccupationView = headerLayout.findViewById(R.id.member_occupation);
        memberLocationView = headerLayout.findViewById(R.id.member_location);
        memberProfileImageView = headerLayout.findViewById(R.id.member_profile_photo);

        memberFullNameView.setOnClickListener(this);
        memberLocationView.setOnClickListener(this);
        memberOccupationView.setOnClickListener(this);
        memberProfileImageView.setOnClickListener(this);

        loadUserData(user);
        styleNavigationMenu(navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        homeScreenItemsRecyclerview = findViewById(R.id.home_screen_recycler_view);
        mEmptyView = findViewById(R.id.empty_view);

        mEmptyViewLabel = mEmptyView.findViewById(R.id.empty_view_label);
        mRetryButton = mEmptyView.findViewById(R.id.retry_button);

        mEmptyViewLabel.setText(getString(R.string.no_home_screen_items));

        homeScreenAdapter = new HomeScreenAdapter(this, this, user);
        mRetryButton.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        homeScreenItemsRecyclerview.setAdapter(homeScreenAdapter);
        homeScreenItemsRecyclerview.setEmptyView(mEmptyView);
        homeScreenItemsRecyclerview.setLayoutManager(linearLayoutManager);
        homeScreenItemsRecyclerview.setItemAnimator(new DefaultItemAnimator());
        homeScreenItemsRecyclerview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(this, R.color.green),
                ContextCompat.getColor(this, R.color.blue),
                ContextCompat.getColor(this, R.color.colorGold)
        );

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    if (NetworkUtils.isConnected(MainActivity.this)) {
                        fetchHomeScreenData();
                    } else {
                        endSwipeRefreshLayout(swipeRefreshLayout);
                        showErrorPopupMessage(getString(R.string.network_connection_label));
                    }
                } catch (Exception e) {
                    endSwipeRefreshLayout(swipeRefreshLayout);
                    e.printStackTrace();
                }
            }
        });
        if (ListUtils.isNotEmpty(homeScreenItems)) {
            homeScreenAdapter.setItems(homeScreenItems);
        }

        new FCMTokenUtils().execute();
        fetchHomeScreenData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (pushNotificationReceived && !TextUtils.isEmpty(notificationViewType)) {
            if (!hasNotificationBeenShown) {
                resolveNotificationView();
            }
        }

        if (homeScreenItems != null && homeScreenItems.size() > 0) {
            homeScreenAdapter.notifyDataSetChanged();
        }
    }

    private void updateIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            pushNotificationReceived = intent.getExtras().getBoolean(Constants.NOTIFICATION_FLAG);
            memberId = intent.getExtras().getString(Constants.NOTIFICATION_MEMBER_ID);
            networkId = intent.getExtras().getString(Constants.NOTIFICATION_NETWORK_ID);
            memberFullName = intent.getExtras().getString(Constants.NOTIFICATION_MEMBER_FULL_NAME);
            notificationViewType = intent.getExtras().getString(Constants.NOTIFICATION_VIEW_TAG);
            notificationContent = intent.getExtras().getString(Constants.NOTIFICATION_CONTENT);
        }
    }

    private void resolveNotificationView() {
        Intent intent = null;
        switch (notificationViewType) {
            case Constants.DECLINE_DIALOG_VIEW_TAG:
                showRequestDeclinedDialog(this, notificationContent);
                break;
            case Constants.MEMBER_PROFILE_VIEW_TAG:
                Network network = new Network();
                String[] fullName = memberFullName.split(" ");
                network.setFirstName(fullName[0]);
                network.setLastName(fullName[1]);
                network.setMemberId(memberId);
                intent = new Intent(MainActivity.this, ProfileActivity.class);
                intent.putExtra(Constants.VIEW_TYPE, Constants.MEMBER_PROFILE_VIEW_TAG);
                intent.putExtra(Constants.NETWORK, network);
                intent.putExtra(Constants.NOTIFICATION_FLAG, true);
                intent.putExtra(Constants.USER, user);
                startActivity(intent);
                break;
            case Constants.NETWORK_REQUEST_VIEW_TAG:
                intent = new Intent(MainActivity.this, NetworkActivity.class);
                intent.putExtra(Constants.MEMBER_ID, memberId);
                intent.putExtra(Constants.FULL_NAME, memberFullName);
                intent.putExtra(Constants.NETWORK_ID, networkId);
                intent.putExtra(Constants.NOTIFICATION_FLAG, true);
                intent.putExtra(Constants.USER, user);
                intent.putExtra(Constants.VIEW_TYPE, Constants.NETWORK_REQUESTS_VIEW_TAG);
                startActivity(intent);
                break;
        }

        hasNotificationBeenShown = true;
        notificationViewType = null;
        pushNotificationReceived = false;
        getIntent().putExtra(Constants.NOTIFICATION_FLAG, false);
    }

    private void manageEmptyViewState(boolean showRetryButton, String emptyViewLabel) {
        mEmptyViewLabel.setText(emptyViewLabel);
        mRetryButton.setVisibility(showRetryButton ? View.VISIBLE : View.GONE);
    }

    private void styleNavigationMenu(NavigationView navigationView) {
        Menu navigationMenu = navigationView.getMenu();
        for (int i = 0; i < navigationMenu.size(); i++) {
            MenuItem menuItem = navigationMenu.getItem(i);
            SubMenu subNavigationMenu = menuItem.getSubMenu();
            if (subNavigationMenu != null && subNavigationMenu.size() > 0) {
                for (int j = 0; j < subNavigationMenu.size(); j++) {
                    MenuItem subMenuItem = subNavigationMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(menuItem);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface typeface = FontUtils.selectTypeface(this, FontUtils.STYLE_REGULAR);
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan(typeface), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void showRequestDeclinedDialog(Context context, final String notificationMessage) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.dialog_request_declined, null);

        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();

        if (alertDialog != null && alertDialog.getWindow() != null) {
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        final TextView mDialogTitle = dialogView.findViewById(R.id.dialog_title);
        final TextView mDialogContent = dialogView.findViewById(R.id.dialog_content);
        final Button mDismissDialog = dialogView.findViewById(R.id.dismiss_button);

        if (notificationMessage != null) {
            mDialogContent.setText(notificationMessage);
        }

        mDismissDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        if (alertDialog != null) {
            alertDialog.show();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        item.setChecked(true);
        drawer.closeDrawer(GravityCompat.START);

        switch (id) {
            case R.id.nav_feed:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, FeedActivity.class);
                        intent.putExtra(Constants.USER, user);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.FEED_LIST_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_profile:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                        intent.putExtra(Constants.USER, user);
                        intent.putExtra(Constants.MEMBER, user);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.MEMBER_PROFILE_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_network:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, NetworkActivity.class);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.MY_NETWORK_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_pna_discounts:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, DiscountsActivity.class);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.DISCOUNT_OFFERS_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_events:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, EventsActivity.class);
                        intent.putExtra(Constants.USER, user);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.EVENTS_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_yellow_pages:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, YellowPagesActivity.class);
                        intent.putExtra(Constants.USER, user);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.YELLOW_PAGES_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_pna_daily:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        launchFragmentDialog(PNADailyDialog.getInstance(null));
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.nav_logout:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        signUserOut();
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
        }
        return true;
    }

    private void loadUserData(User user) {
        if (user != null) {
            memberFullNameView.setText(user.getFlname());
            memberOccupationView.setText(user.getJobTitle());
            if (!TextUtils.isEmpty(user.getPic())) {
                String profilePictureUrl = user.getPic().contains("http") ? user.getPic() : user.getUrl().concat("/").concat(user.getPic()).replace("///", "//");
                Picasso.with(this).load(profilePictureUrl).placeholder(R.color.colorGray).into(memberProfileImageView);
            } else {
                Picasso.with(this).load(R.drawable.avatar).placeholder(R.color.colorGray).into(memberProfileImageView);
            }
        }
    }

    private String buildMemberLocation(User user) {
        String state = user.getState();
        String country = ListUtils.getCountryNameForCountryCode(user.getCountry(), playNetworkDatabase.countryDao().getAllCountries());
        String format = !TextUtils.isEmpty(state) && !TextUtils.isEmpty(country) ? "%s, %s" : "%s";
        return String.format(format, state, country);
    }

    private void fetchHomeScreenData() {
        if (NetworkUtils.isConnected(MainActivity.this)) {
            animateSwipeRefreshLayout(swipeRefreshLayout);
            getHttpService().fetchHomeScreenData(String.valueOf(user.getId()), buildHomeScreenDataCallback());
        } else {
            showErrorPopupMessage(getString(R.string.network_connection_label));
        }
    }

    private void signUserOut() {
        HttpService.resetApiService();
        ApiModule.resetApiClient();

        Intent intent = new Intent(MainActivity.this, TempActivity.class);
        user.setLoggedIn(false);
        user.setTimestamp(System.currentTimeMillis());
        playNetworkDatabase.userDao().insertUser(user);
        clearCachedData(playNetworkDatabase);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.member_full_name:
            case R.id.member_occupation:
            case R.id.member_profile_photo:
                drawer.closeDrawer(GravityCompat.START);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                        intent.putExtra(Constants.USER, user);
                        intent.putExtra(Constants.MEMBER, user);
                        intent.putExtra(Constants.VIEW_TYPE, Constants.MEMBER_PROFILE_VIEW_TAG);
                        startActivity(intent);
                    }
                }, DRAWER_CLOSER_INTERVAL);
                break;
            case R.id.retry_button:
                if (NetworkUtils.isConnected(this)) {
                    mEmptyView.setVisibility(View.GONE);
                    manageEmptyViewState(false, getString(R.string.no_feed_label));
                    animateSwipeRefreshLayout(swipeRefreshLayout);

                } else {
                    showErrorPopupMessage(getString(R.string.network_connection_label));
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PlayNetworkDatabase.destroyDatabaseInstance();
    }

    @Override
    public void onProfileUpdated(User user) {
        if (user != null) {
            this.user = user;
            loadUserData(user);
            if (getIntent() != null && getIntent().getExtras() != null) {
                getIntent().getExtras().putParcelable(Constants.USER, user);
            }
        }
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        if (position > -1) {
            HomeScreenItem homeScreenItem = homeScreenAdapter.getHomeScreenItems().get(position);
            if (homeScreenItem instanceof HomeScreenPNADaily) {
                launchFragmentDialog(PNADailyDialog.getInstance(null));
            } else if (homeScreenItem instanceof HomeScreenEvent) {
                Intent intent = new Intent(MainActivity.this, EventsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.VIEW_TYPE, Constants.EVENT_DETAIL_VIEW_TAG);
                bundle.putParcelable(Constants.USER, user);
                bundle.putString(Constants.EVENT_ID, ((HomeScreenEvent) homeScreenItem).getId());
                intent.putExtras(bundle);
                startActivity(intent);
            } else if (homeScreenItem instanceof HomeScreenOffer) {
                Intent intent = new Intent(MainActivity.this, DiscountsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.VIEW_TYPE, Constants.OFFER_DETAIL_VIEW_TAG);
                bundle.putParcelable(Constants.USER, user);
                bundle.putString(Constants.OFFER_ID, ((HomeScreenOffer) homeScreenItem).getId());
                intent.putExtras(bundle);
                startActivity(intent);
            } else if (homeScreenItem instanceof HomeScreenVideo) {
                HomeScreenVideo homeScreenVideo = (HomeScreenVideo) homeScreenItem;
                String videoId = homeScreenVideo.getVideoUrl().split("=")[1];
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(
                        this, BuildConfig.YOUTUBE_API_KEY, videoId, 0, true, false);
                if (intent != null) {
                    if (canResolveIntent(intent)) {
                        startActivityForResult(intent, REQ_START_STANDALONE_PLAYER);
                    } else {
                        YouTubeInitializationResult.SERVICE_MISSING
                                .getErrorDialog(this, REQ_RESOLVE_SERVICE_MISSING).show();
                    }
                }
            }
        }
    }

    private boolean canResolveIntent(Intent intent) {
        List<ResolveInfo> resolveInfo = getPackageManager().queryIntentActivities(intent, 0);
        return resolveInfo != null && !resolveInfo.isEmpty();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_START_STANDALONE_PLAYER && resultCode != RESULT_OK) {
            YouTubeInitializationResult errorReason =
                    YouTubeStandalonePlayer.getReturnedInitializationResult(data);
            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(this, 0).show();
            } else {
                String errorMessage =
                        String.format(getString(R.string.error_player), errorReason.toString());
                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
            }
        }
    }

    private List<HomeScreenItem> buildHomeScreenItems(HomeScreenResponse homeScreenResponse) {
        List<HomeScreenItem> homeScreenItems = new ArrayList<HomeScreenItem>();
        homeScreenItems.add(homeScreenResponse.getHomeScreenPNADaily());
        homeScreenItems.add(homeScreenResponse.getHomeScreenEvent());
        homeScreenItems.add(homeScreenResponse.getHomeScreenOffer());
        HomeScreenActiveMembers homeScreenActiveMembers = new HomeScreenActiveMembers(homeScreenResponse.getMemberPhotos());
        homeScreenActiveMembers.setTitle(getString(R.string.homescreen_members_title));
        homeScreenItems.add(homeScreenActiveMembers);
        homeScreenItems.add(homeScreenResponse.getHomeScreenVideo());
        return homeScreenItems;
    }

    private Callback<HomeScreenResponse> buildHomeScreenDataCallback() {
        return new Callback<HomeScreenResponse>() {
            @Override
            public void onResponse(Call<HomeScreenResponse> call, Response<HomeScreenResponse> response) {
                HomeScreenResponse homeScreenResponse = response.body();
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                if (ApiUtils.isSuccessResponse(response) && homeScreenResponse != null) {
                    List<HomeScreenItem> homeScreenItems = buildHomeScreenItems(homeScreenResponse);
                    playNetworkDatabase.homeScreenItemDao().deleteCachedHomeScreenItems();
                    playNetworkDatabase.homeScreenItemDao().insertAll(ListUtils.convertHomeScreenItemListToArray(homeScreenItems));
                    homeScreenAdapter.setItems(homeScreenItems);
                }
            }

            @Override
            public void onFailure(Call<HomeScreenResponse> call, Throwable t) {
                hideLoadingIndicator();
                endSwipeRefreshLayout(swipeRefreshLayout);
                showErrorPopupMessage(t.getMessage());
            }
        };
    }
}
