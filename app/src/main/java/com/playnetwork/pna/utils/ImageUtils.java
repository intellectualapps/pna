package com.playnetwork.pna.utils;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.playnetwork.pna.R;
import com.squareup.picasso.Picasso;

public class ImageUtils {

    public static String TAG = ImageUtils.class.getSimpleName();

    public static void loadImageUrl(final ImageView imageView, final Context context, String imagePath) {
        try {
            Picasso.with(context).load(imagePath).placeholder(R.color.colorGray).into(imageView);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void loadImageUri(final ImageView imageView, final Context context, Uri imagePath) {
        try {
            Picasso.with(context).load(imagePath).placeholder(R.color.colorGray).into(imageView);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
