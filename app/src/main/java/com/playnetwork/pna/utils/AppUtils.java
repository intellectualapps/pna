package com.playnetwork.pna.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.room.daos.DeviceDao;

public class AppUtils {
    public static String getAppVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException nnfe) {
            nnfe.printStackTrace();
            return "";
        }
    }

    public static boolean isServiceRunning(Context context, Class serviceClass) {
        ActivityManager manager =
                (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String getFCMToken(PlayNetworkDatabase playNetworkDatabase) {
        String token = null;
        DeviceDao deviceDao = playNetworkDatabase.deviceDao();
        token = FirebaseInstanceId.getInstance() != null && !TextUtils.isEmpty(FirebaseInstanceId.getInstance().getToken())
                ? FirebaseInstanceId.getInstance().getToken()
                : (deviceDao != null && deviceDao.getDevice() != null && !TextUtils.isEmpty(deviceDao.getDevice().getToken())) ? deviceDao.getDevice().getToken() : null;

        return token;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }
}
