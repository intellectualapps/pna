/**
 *
 */
package com.playnetwork.pna.utils;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v7.app.AlertDialog;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.data.models.CalendarEvent;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.room.daos.CalendarEventDao;
import com.playnetwork.pna.utils.calendar.CalendarCursor;

import java.util.TimeZone;

public class CalendarUtils {
    public static void saveEventToCalendar(Event event) {
        Context context = PlayNetworkApplication.getAppInstance().getApplicationContext();
        if (context == null || event == null) {
            return;
        }

        try {
            Uri eventsUri;
            Uri reminderUri;
            Cursor cursor;

            if (android.os.Build.VERSION.SDK_INT <= 7) {
                eventsUri = Uri.parse("content://calendar/events");
                reminderUri = Uri.parse("content://calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);

            } else if (android.os.Build.VERSION.SDK_INT <= 14) {
                eventsUri = Uri.parse("content://com.android.calendar/events");
                reminderUri = Uri.parse("content://com.android.calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);

            } else {
                eventsUri = Uri.parse("content://com.android.calendar/events");
                reminderUri = Uri.parse("content://com.android.calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "calendar_displayName"}, null, null, null);

            }

            int[] calendarId = getCalendarIds(cursor);

            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.CALENDAR_ID, calendarId[0]);
            values.put(CalendarContract.Events.TITLE, event.getTitle());
            values.put(CalendarContract.Events.DESCRIPTION, event.getInfo());
            values.put(CalendarContract.Events.EVENT_LOCATION, event.getAddress());
            values.put(CalendarContract.Events.DTSTART, DateUtils.getEventStartDate(event.getDate(), event.getTime()));
            values.put(CalendarContract.Events.DTEND, DateUtils.getEventEndDate(event.getDate()));
            values.put(CalendarContract.Events.STATUS, 1);
            values.put(CalendarContract.Events.HAS_ALARM, 1);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());

            Uri newEvent = context.getContentResolver().insert(eventsUri, values);
            if (newEvent != null) {
                ContentValues reminders = new ContentValues();
                long calendarEventId = Long.parseLong(newEvent.getLastPathSegment());
                reminders.put(CalendarContract.Reminders.EVENT_ID, calendarEventId);
                reminders.put(CalendarContract.Reminders.METHOD, CalendarContract.Reminders.METHOD_ALERT);
                reminders.put(CalendarContract.Reminders.MINUTES, 10);
                context.getContentResolver().insert(reminderUri, reminders);
                sendCompletionBroadcast(context, Constants.EVENT_ADDED, calendarEventId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            sendCompletionBroadcast(context, Constants.EVENT_ERROR, -1);
        }
    }

    public static void updateEvent(Event event) {
        Context context = PlayNetworkApplication.getAppInstance().getApplicationContext();
        if (context == null || event == null) {
            return;
        }

        try {
            Uri eventsUri;
            Uri reminderUri;
            Cursor cursor;

            if (android.os.Build.VERSION.SDK_INT <= 7) {
                eventsUri = Uri.parse("content://calendar/events");
                reminderUri = Uri.parse("content://calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);

            } else if (android.os.Build.VERSION.SDK_INT <= 14) {
                eventsUri = Uri.parse("content://com.android.calendar/events");
                reminderUri = Uri.parse("content://com.android.calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);

            } else {
                eventsUri = Uri.parse("content://com.android.calendar/events");
                reminderUri = Uri.parse("content://com.android.calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "calendar_displayName"}, null, null, null);
            }

            int[] calendarId = getCalendarIds(cursor);
            ContentValues values = new ContentValues();

            values.put(CalendarContract.Events.CALENDAR_ID, calendarId[0]);
            values.put(CalendarContract.Events.TITLE, event.getTitle());
            values.put(CalendarContract.Events.DESCRIPTION, event.getInfo());
            values.put(CalendarContract.Events.EVENT_LOCATION, event.getAddress());
            values.put(CalendarContract.Events.DTSTART, DateUtils.getEventStartDate(event.getDate(), event.getTime()));
            values.put(CalendarContract.Events.DTEND, DateUtils.getEventEndDate(event.getDate()));
            values.put(CalendarContract.Events.STATUS, 1);
            values.put(CalendarContract.Events.HAS_ALARM, 1);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, TimeZone.getDefault().getID());

            Uri uri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, event.getCalendarEventId());
            int rows = context.getContentResolver().update(uri, values, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteEvent(Event event) {
        Context context = PlayNetworkApplication.getAppInstance().getApplicationContext();
        if (context == null || event == null) {
            return;
        }

        try {
            Uri eventsUri;
            Uri reminderUri;
            Cursor cursor;

            if (android.os.Build.VERSION.SDK_INT <= 7) {
                eventsUri = Uri.parse("content://calendar/events");
                reminderUri = Uri.parse("content://calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);

            } else if (android.os.Build.VERSION.SDK_INT <= 14) {
                eventsUri = Uri.parse("content://com.android.calendar/events");
                reminderUri = Uri.parse("content://com.android.calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);

            } else {
                eventsUri = Uri.parse("content://com.android.calendar/events");
                reminderUri = Uri.parse("content://com.android.calendar/reminders");
                cursor = context.getContentResolver().query(
                        Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "calendar_displayName"}, null, null, null);
            }

            Uri deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, event.getCalendarEventId());
            int rows = context.getContentResolver().delete(deleteUri, null, null);
            sendCompletionBroadcast(context, Constants.EVENT_DELETED, event.getCalendarEventId());
            deleteCalendarEventInDatabase(event);
        } catch (Exception e) {
            e.printStackTrace();
            sendCompletionBroadcast(context, Constants.EVENT_ERROR, -1);
        }
    }

    private static void deleteCalendarEventInDatabase(Event event) {
        PlayNetworkDatabase playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        CalendarEventDao calendarEventDao = playNetworkDatabase.calendarEventDao();
        CalendarEvent calendarEvent = calendarEventDao.getEventWithEventId(event.getId());
        calendarEventDao.deleteCalendarEvent(calendarEvent);
        playNetworkDatabase = null;
    }

    private static void sendCompletionBroadcast(Context context, String action, long eventId) {
        Intent intent = new Intent();
        intent.setAction(Constants.CALENDAR_EVENT_INTENT_FILTER);
        intent.putExtra(Constants.ACTION, action);
        if (eventId > 0) {
            intent.putExtra(Constants.EVENT_ID, eventId);
        }
        if (context != null) {
            context.sendBroadcast(intent);
        }
    }

    private static Cursor getEventCursor(Context context) {
        Uri eventsUri;
        Uri reminderUri;
        Cursor cursor;

        if (android.os.Build.VERSION.SDK_INT <= 7) {
            eventsUri = Uri.parse("content://calendar/events");
            reminderUri = Uri.parse("content://calendar/reminders");
            cursor = context.getContentResolver().query(
                    Uri.parse("content://calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);

        } else if (android.os.Build.VERSION.SDK_INT <= 14) {
            eventsUri = Uri.parse("content://com.android.calendar/events");
            reminderUri = Uri.parse("content://com.android.calendar/reminders");
            cursor = context.getContentResolver().query(
                    Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "displayName"}, null, null, null);

        } else {
            eventsUri = Uri.parse("content://com.android.calendar/events");
            reminderUri = Uri.parse("content://com.android.calendar/reminders");
            cursor = context.getContentResolver().query(
                    Uri.parse("content://com.android.calendar/calendars"), new String[]{"_id", "calendar_displayName"}, null, null, null);

        }

        return cursor;
    }

    private void showCalendarPicker(Context context, CalendarCursor calendarCursor) {
        new AlertDialog.Builder(context)
                .setCursor(calendarCursor, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // changeCalendar(which);
                    }
                }, CalendarContract.Calendars.CALENDAR_DISPLAY_NAME)
                .setNegativeButton(android.R.string.cancel, null)
                .create()
                .show();
    }

    private static int[] getCalendarIds(Cursor cursor) {
        if (cursor != null) {
            String[] calendarNames = new String[cursor.getCount()];
            int[] calendarId = new int[cursor.getCount()];

            cursor.moveToFirst();
            for (int i = 0; i < calendarId.length; i++) {
                int col1 = cursor.getInt(0);
                String col2 = cursor.getString(1);

                calendarId[i] = col1;
                calendarNames[i] = col2;
                cursor.moveToNext();
            }

            return calendarId;
        }
        return null;
    }

    private static String[] getCalendarNames(Cursor cursor) {
        if (cursor != null) {
            String[] calendarNames = new String[cursor.getCount()];
            int[] calendarId = new int[cursor.getCount()];
            for (int i = 0; i < calendarNames.length; i++) {
                calendarId[i] = cursor.getInt(0);
                calendarNames[i] = cursor.getString(1);
                cursor.moveToNext();
            }

            return calendarNames;
        }
        return null;
    }
}
