package com.playnetwork.pna.utils;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.playnetwork.pna.data.models.User;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class DebugUtils {
    public static String TAG = DebugUtils.class.getName();

    public static void debugPropertiesFile(Properties properties) {
        if (properties != null && !properties.isEmpty()) {
            Enumeration<?> e = properties.propertyNames();
            while (e.hasMoreElements()) {
                String key = (String) e.nextElement();
                String value = properties.getProperty(key);
                System.out.println("Key : " + key + ", Value : " + value + "\n\n");
            }
        } else {
            System.out.println("Null");
        }
    }

    public static void debugBundleExtras(Bundle bundle) {

        if (bundle != null) {
            for (String key : bundle.keySet()) {
                Object value = bundle.get(key);
                Log.d("Bundle values", String.format("%s %s (%s)", key, value.toString(), value.getClass().getName()));
            }
        } else {
            Log.e("Bundle values", "Null");
        }
    }

    public static void debugPreferences(SharedPreferences sharedPreferences) {
        Map<String, ?> keys = sharedPreferences.getAll();

        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    public static int debugObject(Object obj) {
        ArrayList<String> classNames = new ArrayList<String>();
        classNames.add("participant");
        try {
            if (obj != null) {
                for (Field field : obj.getClass().getDeclaredFields()) {

                    field.setAccessible(true); // if you want to modify private fields
                    try {
                        System.out.println(field.getName()
                                + " - " + field.getType()
                                + " - " + field.get(obj));

                        if (classNames.contains(field.getName())) {
                            System.out.println("Class: " + field.getName());
                            Object subObject = field.get(obj);

                            if (subObject != null)
                                debugObject(subObject);
                            else
                                System.out.println(field.getName() + " is null");
                        }
                    } catch (IllegalAccessException e) {
                        e.getMessage();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void debugStringList(ArrayList<String> list) {
        if (list != null && list.size() > 0) {
            for (String value : list) {
                System.out.println(value);
            }
        } else {
            System.out.println("Empty list");
        }
    }

    public static void debugUserList(List<User> list) {
        if (list != null && list.size() > 0) {
            for (User value : list) {
                Log.w(TAG, value.getId() + "|" + value.getEmail());
            }
        } else {
            Log.e(TAG, "Empty list");
        }
    }

    public static void debugObjectArrayList(ArrayList<Object> list) {
        if (list != null && list.size() > 0) {
            for (Object value : list) {
                System.out.println(value.toString());
            }
        } else {
            System.out.println("Empty list");
        }
    }

    public static void debugFirebaseMap(Map<String, Object> map) {
        if (map != null && map.size() > 0) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if (entry.getValue() != null) {
                    Log.w(entry.getKey(), entry.getValue().toString());
                } else {
                    Log.e(entry.getKey(), "Null");
                }
            }
        } else {
            Log.e("Map Error", "No data");
        }
    }

    public static void debugDisplayMap(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (entry.getValue() != null) {
                    Log.w(entry.getKey(), String.valueOf(entry.getValue()));
                } else {
                    Log.e(entry.getKey(), "Null");
                }
            }
        } else {
            Log.e("Map Error", "No data");
        }
    }

    public static void debugDisplayMapKeys(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                Log.e(entry.getKey(), "Null");
            }
        } else {
            Log.e("Map Error", "No data");
        }
    }

    public static void debugDisplayMapValues(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                if (entry.getValue() != null) {
                    Log.w(entry.getKey(), entry.getValue());
                }
            }
        } else {
            Log.e("Map Error", "No data");
        }
    }
}
