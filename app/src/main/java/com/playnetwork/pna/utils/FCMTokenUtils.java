package com.playnetwork.pna.utils;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.api.responses.DeviceResponse;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FCMTokenUtils extends AsyncTask<Void, Void, Void> {
    private PlayNetworkDatabase playNetworkDatabase;

    public FCMTokenUtils() {
        playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        User user = playNetworkDatabase.userDao().findLastLoggedInUser();
        final Device device = playNetworkDatabase.deviceDao().getDevice();

        String refreshedToken = AppUtils.getFCMToken(playNetworkDatabase);
        if (user != null && !TextUtils.isEmpty(user.getEmail()) && !TextUtils.isEmpty(refreshedToken) && device != null) {

            Map<String, String> map = new HashMap<String, String>();
            map.put(Constants.EMAIL_ADDRESS, user.getEmail());
            map.put(Constants.FCM_TOKEN, refreshedToken);
            map.put(Constants.PLATFORM, device.getAppPlatform());
            map.put(Constants.DEVICE_ID, device.getDeviceId());

            Callback<DeviceResponse> callback = new Callback<DeviceResponse>() {
                @Override
                public void onResponse(Call<DeviceResponse> call, Response<DeviceResponse> response) {
                    DeviceResponse deviceResponse = response.body();
                    if (ApiUtils.isSuccessResponse(response)) {
                        if (deviceResponse != null && deviceResponse.getDevice() != null) {
                            Device deviceParam = deviceResponse.getDevice();
                            device.setToken(deviceParam.getToken());
                            playNetworkDatabase.deviceDao().insertDevice(device);
                        }
                    }
                }

                @Override
                public void onFailure(Call<DeviceResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            };

            HttpService httpService = new HttpService();
            if (NetworkUtils.isConnected(PlayNetworkApplication.getAppInstance().getApplicationContext())) {
                httpService.storeDeviceToken(map, callback);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        playNetworkDatabase = null;
        PlayNetworkDatabase.destroyDatabaseInstance();
    }
}

