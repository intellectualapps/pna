package com.playnetwork.pna.utils;

import android.os.Build;

import com.playnetwork.pna.BuildConfig;
import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.room.PlayNetworkDatabase;

import java.util.concurrent.Executor;


public class AutoRefreshExecutor implements Executor {
    private static String TAG = AutoRefreshExecutor.class.getName();
    private static final String APP_PLATFORM = "1";
    private HttpService httpService;

    @Override
    public void execute(Runnable command) {
        try {
            initializeDeviceMetaData();
            if (NetworkUtils.isConnected(PlayNetworkApplication.getAppInstance().getApplicationContext())) {
                AutoRefreshUtils.fetchIndustries(getHttpService(), null);
                AutoRefreshUtils.fetchCountries(getHttpService(), null);
                AutoRefreshUtils.fetchSubscriptionAmount(getHttpService(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeDeviceMetaData() {
        DeviceUuidFactory deviceIdFactory = new DeviceUuidFactory(PlayNetworkApplication.getAppInstance().getApplicationContext());
        Device device = new Device();
        device.setDeviceId(deviceIdFactory.getStringUUID());
        device.setAppPlatform(Constants.ANDROID_PLATFORM);
        device.setAppVersion(BuildConfig.VERSION_NAME);
        device.setBuildType(BuildConfig.BUILD_TYPE);
        device.setApplicationId(BuildConfig.APPLICATION_ID);
        device.setOSVersion(String.valueOf(Build.VERSION.SDK_INT));
        device.setDeviceModel(Build.MODEL);
        PlayNetworkDatabase.getPlayNetworkDatabase().deviceDao().insertDevice(device);
    }

    private HttpService getHttpService() {
        if (httpService == null) {
            httpService = new HttpService();
        }

        return httpService;
    }
}
