package com.playnetwork.pna.utils;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class StringUtils {

    /**
     * Gets whether or not a string is null, empty or "null"
     *
     * @param str string to check
     * @return true if the string is null, empty or "null"
     */
    public static boolean isEmpty(String str) {
        return TextUtils.isEmpty(str) || str.equalsIgnoreCase("null");
    }

    /**
     * This method nullifies a string.
     *
     * @param value string to nullify
     * @return the input string if the string is not empty, null otherwise
     */
    public static String nullify(String value) {
        if (isEmpty(value)) {
            return null;
        }
        return value;
    }

    public static String userToString(User user) {
        Gson gson = new Gson();
        return gson.toJson(user, User.class);
    }

    public static User userFromString(String sUser) {
        Gson gson = new Gson();
        User user = null;
        try {
            user = gson.fromJson(sUser, User.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public static String industriesToString(List<Industry> industries) {
        Gson gson = new Gson();
        try {
            return gson.toJson(industries);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Industry> industriesFromString(String sIndustries) {
        Gson gson = new Gson();
        ArrayList<Industry> industries = null;
        try {
            industries = gson.fromJson(sIndustries, new TypeToken<List<Industry>>() {
            }.getType());
        } catch (Exception e) {
            return null;
        }
        return industries;
    }

    private static int indexOf(String string, char character, int start, boolean reverseOrder) {
        if (isEmpty(string)) return -1;
        char[] chars = string.toCharArray();

        if (start < 0 || start > string.length()) return -1;

        if (reverseOrder) {
            for (int i = start; i > -1; i--) {
                if (character == chars[i]) return i;
            }
        } else {
            for (int i = 0; i < start; i--) {
                if (character == chars[i]) return i;
            }
        }
        return -1;
    }

    public static int indexOf(String string, char character, int start) {
        return indexOf(string, character, start, false);
    }
}
