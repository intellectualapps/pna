package com.playnetwork.pna.utils;

import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.data.models.Feed;
import com.playnetwork.pna.data.models.HomeScreenItem;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.Offer;
import com.playnetwork.pna.data.models.State;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.data.models.YellowPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ListUtils {
    public static boolean isEmpty(Collection items) {
        return items == null || items.size() < 1;
    }

    public static boolean isNotEmpty(Collection items) {
        return items != null && items.size() > 0;
    }

    public static Industry[] convertIndustryListToArray(List<Industry> industries) {
        return industries.toArray(new Industry[industries.size()]);
    }

    public static Country[] convertCountryListToArray(List<Country> countries) {
        return countries.toArray(new Country[countries.size()]);
    }

    public static State[] convertStateListToArray(List<State> states) {
        return states.toArray(new State[states.size()]);
    }

    public static User[] convertUserListToArray(List<User> users) {
        return users.toArray(new User[users.size()]);
    }

    public static Network[] convertNetworkListToArray(List<Network> networkList) {
        return networkList.toArray(new Network[networkList.size()]);
    }

    public static Offer[] convertOfferListToArray(List<Offer> offerList) {
        return offerList.toArray(new Offer[offerList.size()]);
    }

    public static Event[] convertEventListToArray(List<Event> eventList) {
        return eventList.toArray(new Event[eventList.size()]);
    }

    public static YellowPage[] convertYellowPageListToArray(List<YellowPage> yellowPageList) {
        return yellowPageList.toArray(new YellowPage[yellowPageList.size()]);
    }

    public static Feed[] convertFeedListToArray(List<Feed> feedList) {
        return feedList.toArray(new Feed[feedList.size()]);
    }

    public static HomeScreenItem[] convertHomeScreenItemListToArray(List<HomeScreenItem> homeScreenItems) {
        return homeScreenItems.toArray(new HomeScreenItem[homeScreenItems.size()]);
    }

    public static Collection<? extends String> getIndustryLabels(List<Industry> industries) {
        List<String> list = new ArrayList<String>();
        for (Industry industry : industries) {
            list.add(industry.getIndustry());
        }

        return list;
    }

    public static Collection<? extends String> getCountryLabels(List<Country> countries) {
        List<String> list = new ArrayList<String>();
        for (Country country : countries) {
            list.add(country.getName());
        }

        return list;
    }

    public static Collection<? extends String> getStateLabels(List<State> states) {
        List<String> list = new ArrayList<String>();
        for (State state : states) {
            list.add(state.getName());
        }

        return list;
    }

    public static String getCountryIdForCountryCode(String countryCode, List<Country> countryList) {
        if (ListUtils.isNotEmpty(countryList)) {
            for (Country country : countryList) {
                if (country.getSortName().equalsIgnoreCase(countryCode)) {
                    return String.valueOf(country.getId());
                }
            }
        }

        return String.valueOf(-1);
    }

    public static String getCountryNameForCountryCode(String countryCode, List<Country> countryList) {
        if (ListUtils.isNotEmpty(countryList)) {
            for (Country country : countryList) {
                if (country.getSortName().equalsIgnoreCase(countryCode)) {
                    return country.getName();
                }
            }
        }

        return null;
    }
}


