package com.playnetwork.pna.utils;

public class Constants {
    public static final String USER = "user";
    public static final String EMAIL_ADDRESS = "email";
    public static final String JOB_TITLE = "job-title";
    public static final String COMPANY = "company";
    public static final String INDUSTRY = "industry";
    public static final String COUNTRY_CODE = "country-code";
    public static final String STATE = "state";
    public static final String ADDRESS = "address";
    public static final String INSTAGRAM = "instagram";
    public static final String TWITTER = "twitter";
    public static final String FACEBOOK = "facebook";
    public static final String PLATFORM = "platform";
    public static final String WEBSITE = "website";
    public static final String FIRST_NAME = "first-name";
    public static final String LAST_NAME = "last-name";
    public static final String FULL_NAME = "full-name";
    public static final String FACEBOOK_ACCESS_TOKEN = "fb_token";
    public static final String FACEBOOK_ID = "fb_id";
    public static final String PHONE_NUMBER = "phone-number";
    public static final String PASSWORD = "password";
    public static final String ID = "id";
    public static final String TEXT = "text";
    public static final String PIC_URL = "pic-url";
    public static final String AUTH_TYPE = "auth-type";
    public static final String SOCIAL_AUTH_TYPE = "type";
    public static final String SOCIAL_AUTH_EMAIL_TYPE = "email";
    public static final String SOCIAL_AUTH_PHONE_TYPE = "phone";
    public static final String EMAIL_AUTH_TYPE = "email";
    public static final String FACEBOOK_AUTH_TYPE = "facebook";
    public static final String PROFILE_PHOTO = "profile-photo";
    public static final String PHOTO_URL = "photo-url";
    public static final String PAGE_SIZE = "page-size";
    public static final String START = "start";
    public static final String FEED = "feed";
    public static final String SUGGESTION = "suggestion";
    public static final String NETWORK = "network";
    public static final String MEMBER = "member";
    public static final String OFFER = "offer";
    public static final String EVENT = "event";
    public static final String YELLOW_PAGE = "yellow-page";
    public static final String POSITION = "position";
    public static final String CALENDAR_EVENT_ID = "calendar_event_id";
    public static final String HAS_RSVP = "has-rsvp";
    public static final String IS_SELECTED = "is_selected";
    public static final String COMMENT = "comment";
    public static final String MEMBER_IDS = "member-ids";
    public static final String STATUS = "status";
    public static final String MEMBER_ID = "memberId";
    public static final String MEMBER_ID_PARAM = "member-id";
    public static final String STATES = "states";
    public static final String STATE_IDS = "state-ids";
    public static final String INDUSTRY_IDS = "industry-ids";
    public static final String CATEGORIES = "categories";
    public static final String OTHER_MEMBER_ID_PARAM = "other-member-id";
    public static final String TOKEN_PARAM = "token";
    public static final String NETWORK_ID = "networkId";
    public static final String SUBSCRIPTION_AMOUNT = "subscription-amount";
    public static final String AMOUNT = "amount";
    public static final String REGULAR_OFFER = "Regular";
    public static final String PREMIUM_OFFER = "Premium";
    public static final String MEMBERSHIP_TYPE = "membership-type";
    public static final String BUSINESS_NAME = "Business Name:";
    public static final String DISCOUNT_TYPE = "Discount Type:";
    public static final String OFFER_LEVEL = "Level:";
    public static final String OFFER_ENDS = "Ends:";
    public static final String OFFER_REDEEMED = "Redeemed:";
    public static final String OFFER_REDEEMED_AT = "Redeemed at:";
    public static final String REQUEST_MODE = "REQUEST";
    public static final String NETWORK_MODE = "NETWORK";
    public static final String ACCEPTED_NETWORK_STATE = "accepted";
    public static final String PENDING_NETWORK_STATE = "pending";
    public static final String NONE_NETWORK_STATE = "none";
    public static final String ACTION = "action";
    public static final String EVENT_ID = "event-id";
    public static final String EVENT_CONFIRMATION = "event-confirmation";
    public static final String OFFER_ID = "offer-id";
    public static final String PROFILE_DATA = "profile-data";
    public static final String OFFLINE_USERNAME = "offline-username";
    public static final String ONLINE_USERNAME = "online-username";
    public static final String MESSAGE_NOTIFICATION_ID = "message-id";
    public static final String REFRESH_FLAG = "refresh-flag";
    public static final String UPLOADED_IMAGE_URL = "image-url";
    public static final String SKILLS = "skills";
    public static final String EMPLOYER = "employer";
    public static final String LOCATION = "location";
    public static final String START_DATE = "start-date";
    public static final String PERIOD = "period";
    public static final String END_DATE = "end-date";
    public static final String VIDEO_ID = "video-id";
    public static final String CONFIRMATION_CONTENT = "confirmation-content";
    public static final String PAYMENT_TYPE = "payment-type";
    public static final String TICKET_TYPE = "ticket-type";
    public static final String TABLE_TYPE = "table-type";
    public static final String QUANTITY = "quantity";

    public static final String EVENT_VENUE = "Venue:";
    public static final String EVENT_ADDRESS = "Address:";
    public static final String EVENT_DATE = "Date:";
    public static final String EVENT_TIME = "Time:";
    public static final String EVENT_DRESS_CODE = "Dress Code:";
    public static final String EVENT_INFO = "Info:";
    public static final String REGULAR_EVENT_TICKET = "Regular Ticket:";
    public static final String VIP_EVENT_TICKET = "VIP Ticket:";
    public static final String REGULAR_EVENT_TABLE = "Regular Table:";
    public static final String VIP_EVENT_TABLE = "VIP Table:";

    public static final String EVENT_ERROR = "EVENT_ERROR";
    public static final String EVENT_ADDED = "EVENT_ADDED";
    public static final String EVENT_DELETED = "EVENT_DELETED";

    public static final String CLOUDINARY_UPLOADER_INTENT_FILTER = "cloudinary-image-uploader";
    public static final String STATIC_MAP_INTENT_FILTER = "static-map-receiver";
    public static final String REFRESH_FEED_INTENT_FILTER = "refresh-feed-receiver";
    public static final String DISMISS_NOTIFICATION_INTENT_FILTER = "dismiss-notification-receiver";
    public static final String REFRESH_NETWORK_REQUESTS_INTENT_FILTER = "refresh-network-requests-receiver";
    public static final String UPDATE_MEMBER_LIST_INTENT_FILTER = "update-member-list-receiver";
    public static final String BACK_KEY_PRESSED_INTENT_FILTER = "back-key-pressed-receiver";
    public static final String CALENDAR_EVENT_INTENT_FILTER = "calendar-event-receiver";
    public static final String UPDATE_EVENTS_INTENT_FILTER = "update-events-receiver";

    public static final int NETWORK_SUGGESTION_MODE = 100;
    public static final int MY_NETWORK_MODE = 200;
    public static int POPUP_TIMEOUT = 20000;
    public static final int MIN_CARD_LENGTH = 16;
    public static final int MAX_CARD_LENGTH = 20;
    public static final String CURRENCY_SYMBOL = "₦";
    public static final String PASSWORD_VALIDATION_REGEX = "^[ A-Za-z0-9_@.\\/#&+-]{6,}$";
    public static final int EVENT_PAYMENT_REQUEST_CODE = 101;
    public static final int PREMIUM_SUBSCRIPTION_REQUEST_CODE = 102;
    /*
    Push Notification Keys
     */

    public static final String PUSH_PHOTO_URL = "photoUrl";
    public static final String VIEW_TYPE = "viewType";


    /*
    Chat Related
     */
    public static final String CHAT_SESSIONS = "chat-sessions";
    public static final String CHAT_SESSION = "chat_session";
    public static final String CHAT_SESSION_MESSAGES_KEY = "messages";
    public static final String CHAT_SESSION_PARTICIPANTS_KEY = "participants";
    public static final String CHAT_ID = "id";
    public static final String REQUESTING_MEMBER = "requesting-member";
    public static final String OTHER_MEMBER = "other-member";
    public static final String MESSAGE_NOTIFICATION = "offline-user";
    public static final String HTML_LINE_BREAK = "<br/>";

    public static final String MESSAGE_ID_KEY = "id";
    public static final String MESSAGE_AUTHOR_KEY = "author";
    public static final String MESSAGE_TEXT_KEY = "text";
    public static final String MESSAGE_TIME_KEY = "time";


    /*
    Cloud Messaging
     */
    public static final String NOTIFICATION_TITLE = "notificationTitle";
    public static final String NOTIFICATION_TYPE = "notificationType";
    public static final String NOTIFICATION_CONTENT = "notificationContent";
    public static final String NOTIFICATION_MEMBER_ID = "memberId";
    public static final String NOTIFICATION_NETWORK_ID = "networkId";
    public static final String NOTIFICATION_MEMBER_FULL_NAME = "memberName";
    public static final String NOTIFICATION_MESSAGE = "notification-message";
    public static final String NOTIFICATION_USER_PARAM = "user-param";
    public static final String NOTIFICATION_NETWORK_REQUEST_TYPE = "network_invitation";
    public static final String NOTIFICATION_REQUEST_DECLINED_TYPE = "request_declined";
    public static final String NOTIFICATION_REQUEST_ACCEPTED_TYPE = "request_accepted";
    public static final String MESSAGE = "message";
    public static final String TITLE = "title";
    public static final String NOTIFICATION_FLAG = "NOTIFICATION_FLAG";
    public static final String NOTIFICATION_VIEW_TAG = "NOTIFICATION_VIEW_TAG";
    public static final String NOTIFICATION_POPUP_LISTENER = "com.playnetwork.pna.notification";

    public static final String FCM_TOKEN = "token";
    public static final String DEVICE_ID = "uuid";
    public static final String ANDROID_PLATFORM = "android";

    /*
    View Tags
     */
    public static final String LOGIN_VIEW_TAG = "LOGIN_VIEW";
    public static final String SIGN_UP_VIEW_TAG = "SIGN_UP_VIEW";
    public static final String FORGOT_PASSWORD_VIEW_TAG = "FORGOT_PASSWORD_VIEW";
    public static final String CREATE_PROFILE_VIEW_TAG = "CREATE_PROFILE_VIEW";
    public static final String SOCIAL_PROFILE_VIEW_TAG = "SOCIAL_PROFILE_VIEW";
    public static final String MY_NETWORK_VIEW_TAG = "MY_NETWORK_VIEW";
    public static final String PROFILE_VIEW_TAG = "PROFILE_VIEW";
    public static final String MESSAGES_VIEW_TAG = "MESSAGES_VIEW";
    public static final String PAYMENT_CONFIRMATION_VIEW_TAG = "PAYMENT_CONFIRMATION_VIEW";
    public static final String USER_PROFILE_VIEW_TAG = "USER_PROFILE_VIEW";
    public static final String PREMIUM_SUBSCRIPTION_VIEW_TAG = "PREMIUM_SUBSCRIPTION_VIEW";
    public static final String MEMBER_PROFILE_VIEW_TAG = "MEMBER_PROFILE_VIEW";
    public static final String PENDING_PROFILE_VIEW_TAG = "PENDING_PROFILE_VIEW";
    public static final String SEARCH_DETAIL_VIEW_TAG = "SEARCH_DETAIL_VIEW";
    public static final String ADD_MEMBER_VIEW_TAG = "ADD_MEMBER_VIEW";
    public static final String NETWORK_REQUESTS_VIEW_TAG = "NETWORK_REQUESTS_VIEW";
    public static final String NETWORK_REQUEST_VIEW_TAG = "NETWORK_REQUEST_VIEW";
    public static final String NETWORK_SUGGESTIONS_VIEW_TAG = "NETWORK_SUGGESTIONS_VIEW";
    public static final String DECLINE_DIALOG_VIEW_TAG = "DECLINE_DIALOG_VIEW";
    public static final String DISCOUNT_OFFERS_VIEW_TAG = "DISCOUNT_OFFERS_VIEW";
    public static final String OFFER_DETAIL_VIEW_TAG = "OFFER_DETAIL_VIEW";
    public static final String QR_CODE_SCANNER_VIEW_TAG = "QR_CODE_SCANNER_VIEW";
    public static final String FEED_DETAILS_VIEW_TAG = "FEED_DETAILS_VIEW";
    public static final String FEED_LIST_VIEW_TAG = "FEED_LIST_VIEW";
    public static final String CREATE_POST_VIEW_TAG = "CREATE_POST_VIEW";
    public static final String EVENTS_VIEW_TAG = "EVENTS_VIEW";
    public static final String YELLOW_PAGES_VIEW_TAG = "YELLOW_PAGES_VIEW";
    public static final String YELLOW_PAGES_SEARCH_VIEW_TAG = "YELLOW_PAGES_SEARCH_VIEW";
    public static final String EVENT_DETAIL_VIEW_TAG = "EVENT_DETAIL_VIEW";
    public static final String YELLOW_PAGE_DETAIL_VIEW_TAG = "YELLOW_PAGE_DETAIL_VIEW";
}