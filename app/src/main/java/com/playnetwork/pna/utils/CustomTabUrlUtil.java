package com.playnetwork.pna.utils;

import android.content.Context;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.R;

public class CustomTabUrlUtil {

    public static void launchTwitterProfile(Context context, String username) {
        Uri uri = Uri.parse(String.format("https://twitter.com/%s", username));
        launchCustomTab(context, uri);
    }

    public static void launchFacebookProfile(Context context, String username) {
        Uri uri = Uri.parse(String.format("https://facebook.com/%s", username));
        launchCustomTab(context, uri);
    }

    public static void launchInstagramProfile(Context context, String username) {
        Uri uri = Uri.parse(String.format("https://instagram.com/%s", username));
        launchCustomTab(context, uri);
    }

    public static void launchWebsite(Context context, String websiteUrl) {
        if (!websiteUrl.contains("http")) {
            websiteUrl = "http://".concat(websiteUrl);
        }
        Uri uri = Uri.parse(websiteUrl);
        launchCustomTab(context, uri);
    }

    private static Context getAppContext() {
        return PlayNetworkApplication.getAppInstance().getApplicationContext();
    }

    public static void launchWebPage(String url) {
        Uri uri = Uri.parse(url);
        launchCustomTab(getAppContext(), uri);
    }

    private static void launchCustomTab(Context context, Uri uri) {
        try {
            CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
            intentBuilder.setToolbarColor(ContextCompat.getColor(context, R.color.colorAccent));
            intentBuilder.addDefaultShareMenuItem();
            intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
            intentBuilder.setStartAnimations(context, R.anim.enter_from_right, R.anim.exit_to_left);
            intentBuilder.setExitAnimations(context, R.anim.enter_from_left,
                    R.anim.exit_to_right);

            CustomTabsIntent customTabsIntent = intentBuilder.build();
            // customTabsIntent.intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            customTabsIntent.launchUrl(context, uri);
        } catch (Exception e) {
            e.printStackTrace();
            //This catches malformed URL exceptions
        }
    }
}
