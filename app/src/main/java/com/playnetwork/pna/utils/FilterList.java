package com.playnetwork.pna.utils;

import com.playnetwork.pna.data.interfaces.PNAInterfaces;

import java.util.ArrayList;
import java.util.List;

public class FilterList<E> {
    public <T> List filterList(List<T> originalList, PNAInterfaces.Filter filter, E text) {
        List<T> filterList = new ArrayList<T>();
        for (T object : originalList) {
            if (filter.isMatched(object, text)) {
                filterList.add(object);
            } else {
                continue;
            }
        }
        return filterList;
    }
}
