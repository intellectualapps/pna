package com.playnetwork.pna.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 */
public class DateUtils {


    public static String getSimpleDateFormat(long timestamp, String simpleDateFormat) {
        Date dateObject = new Date(timestamp);

        DateFormat df = new SimpleDateFormat(simpleDateFormat, Locale.ENGLISH);
        String formattedString = df.format(dateObject);

        return formattedString;
    }

    public static String getSimpleDateFormat(String sDate, String simpleDateFormat) {
        SimpleDateFormat originalDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String formattedString = null;
        try {
            DateFormat outputDateFormat = new SimpleDateFormat(simpleDateFormat, Locale.ENGLISH);
            Date dateObject = originalDateFormat.parse(sDate);
            formattedString = outputDateFormat.format(dateObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedString;
    }

    public static long getEventEndDate(String eventDate) {
        SimpleDateFormat originalDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        long eventStartDate = -1;
        try {
            Date dateObject = originalDateFormat.parse(eventDate);
            eventStartDate = getEndOfDay(dateObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eventStartDate;
    }

    public static long getEventStartDate(String eventDate, String eventTime) {
        SimpleDateFormat originalDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.ENGLISH);
        long eventStartDate = -1;
        try {
            Date dateObject = originalDateFormat.parse(eventDate.concat(" ").concat(eventTime));
            eventStartDate = dateObject.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eventStartDate;
    }

    private static long getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis();
    }

    private static long getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static String getOrdinalDate(long timestamp, String simpleDateFormat) {
        Date dateObject = new Date(timestamp);


        DateFormat df = new SimpleDateFormat(simpleDateFormat, Locale.getDefault());
        int date = Integer.parseInt(df.format(dateObject));

        return getPosition(date);
    }

    public static String getOrdinalDate(String sDate) {
        SimpleDateFormat originalDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat df = new SimpleDateFormat("dd", Locale.getDefault());

        Date dateObject;
        int date;
        try {
            dateObject = originalDateFormat.parse(sDate);
            date = Integer.parseInt(df.format(dateObject));
            return getPosition(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String timeSincePosted(long timestamp) {
        Date date = new Date(timestamp);
        Date currentDate = new Date();
        try {
            long diff = currentDate.getTime() - date.getTime();
            return getInterval(diff);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String timeSincePosted(String sDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            Date date = format.parse(sDate);
            Date currentDate = new Date();
            long diff = currentDate.getTime() - date.getTime();
            return getInterval(diff);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String getInterval(long difference) {
        long daysSince = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
        if (daysSince < 1) {
            long hoursSince = TimeUnit.HOURS.convert(difference, TimeUnit.MILLISECONDS);
            if (hoursSince < 1) {
                long minutesSince = TimeUnit.MINUTES.convert(difference, TimeUnit.MILLISECONDS);
                if (minutesSince < 2) {
                    return "just now";
                }
                return getDisplayText(minutesSince, " minute ago", " minutes ago");
            } else {
                return getDisplayText(hoursSince, " hour ago", " hours ago");
            }
        } else {
            if (daysSince < 7) {
                return getDisplayText(daysSince, " day ago", " days ago");
            } else if (daysSince >= 7 && daysSince < 30) {
                long weekCount = daysSince / 7;
                return getDisplayText(weekCount, " week ago", " weeks ago");
            } else if (daysSince >= 30) {
                long monthCount = daysSince / 30;
                return getDisplayText(monthCount, " month ago", " months ago");
            }
        }
        return null;
    }

    private static String getDisplayText(long time, String singular, String plural) {
        return time + (time == 1 ? singular : plural);
    }

    private static String getPosition(int i) {
        String[] suffixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + suffixes[i % 10];
        }
    }
}
