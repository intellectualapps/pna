package com.playnetwork.pna.utils;

import android.text.TextUtils;
import android.util.Log;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.api.ApiModule;
import com.playnetwork.pna.data.api.responses.DefaultResponse;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.room.PlayNetworkDatabase;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Converter;
import retrofit2.Response;

public class ApiUtils {
    private static final String HEADER_AUTHORIZATION = "Authorization";

    private static String title = "PlayNetwork";
    private static String TAG = ApiUtils.class.getName();

    public static Map<String, String> buildHeaders(Map<String, String> extraHeaders) {
        Map<String, String> headers = new HashMap<String, String>();
        headers.put(HEADER_AUTHORIZATION, "Bearer " + StringUtils.nullify(getDevice().getAuthToken()));

        if (extraHeaders != null) {
            headers.putAll(extraHeaders);
        }
        return headers;
    }

    private static Device getDevice() {
        return PlayNetworkDatabase.getPlayNetworkDatabase().deviceDao().getDevice();
    }

    public static RequestBody getRequestBody(String value) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), value);
        return requestBody;
    }

    public static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            copy.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    private static void updateAuthenticationToken(DefaultResponse response) {
        PlayNetworkDatabase playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
        Device device = playNetworkDatabase.deviceDao().getDevice();
        device.setAuthToken(response.getAuthToken());
        playNetworkDatabase.deviceDao().insertDevice(device);
    }

    public static boolean isSuccessResponse(Response<? extends DefaultResponse> response) {
        String error = PlayNetworkApplication.getApplicationResources().getString(R.string.null_response_label);
        if (response != null) {
            if (response.isSuccessful()) {
                if (response.body() != null && !TextUtils.isEmpty(response.body().getAuthToken())) {
                    updateAuthenticationToken(response.body());
                }
                return true;
            } else {
                Converter<ResponseBody, DefaultResponse> converter =
                        ApiModule.buildRetrofitAdapter(PlayNetworkApplication.getCacheFile(), null)
                                .responseBodyConverter(DefaultResponse.class, new Annotation[0]);
                DefaultResponse defaultResponse = null;
                try {
                    if (response.errorBody() != null) {
                        defaultResponse = converter.convert(response.errorBody());
                    }
                    ToastUtil.showToast((defaultResponse != null && defaultResponse.getMessage() != null ? (String) defaultResponse.getMessage() : error), ToastUtil.ERROR);
                } catch (Exception e) {
                    ToastUtil.showToast(error, ToastUtil.ERROR);
                    Log.e(TAG, e.getMessage());
                }
            }
        } else {
            ToastUtil.showToast(error, ToastUtil.ERROR);
        }

        return false;
    }
}
