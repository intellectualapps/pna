package com.playnetwork.pna.utils;

import android.os.AsyncTask;

import com.playnetwork.pna.data.models.CalendarEvent;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.room.daos.CalendarEventDao;

import java.util.List;

public class CalendarEventsUpdaterTask extends AsyncTask<Void, Void, Void> {
    private PlayNetworkDatabase playNetworkDatabase;
    private List<Event> eventList;

    public CalendarEventsUpdaterTask(List<Event> eventList) {
        this.eventList = eventList;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            for (Event event : eventList) {
                CalendarEvent calendarEvent = new CalendarEvent(event.getId(), 1);
                CalendarEventDao calendarEventDao = playNetworkDatabase.calendarEventDao();
                List<CalendarEvent> calendarEvents = calendarEventDao.getAll();
                if (calendarEvents.contains(calendarEvent)) {
                    if (event.getHasRSVP()) {
                        CalendarUtils.updateEvent(event);
                    } else {
                        CalendarUtils.deleteEvent(event);
                    }
                }
            }
        } catch (Exception ignored) {
            System.out.println(ignored.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        playNetworkDatabase = null;
    }
}
