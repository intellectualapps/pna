package com.playnetwork.pna.utils;


import android.util.Log;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class CurrencyUtils {
    private static final String TAG = CurrencyUtils.class.getSimpleName();

    public CurrencyUtils() {
    }

    public static String formatToCurrencyWithSymbol(Double amount, String currency) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        nf.setMaximumFractionDigits(2);
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        nf.setRoundingMode(RoundingMode.HALF_UP);
        return currency != null && !currency.isEmpty() ? String.format("%s%s", currency, nf.format(amount != null ? amount : 0.0)) : nf.format(amount);
    }

    public static double parseAmount(String amountString) {
        NumberFormat nf = DecimalFormat.getInstance(Locale.getDefault());
        char groupingSeperator = ((DecimalFormat) nf).getDecimalFormatSymbols().getGroupingSeparator();
        amountString = amountString.replaceAll(String.valueOf(groupingSeperator), "");

        try {
            return nf.parse(amountString).doubleValue();
        } catch (ParseException var4) {
            Log.d(TAG, "Exception: " + var4.toString());
            Log.d(TAG, "Returning 0, it seems something happened.");
            return 0.0D;
        }
    }
}
