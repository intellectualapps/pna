package com.playnetwork.pna.utils;

import android.os.AsyncTask;

import com.playnetwork.pna.data.api.ApiClientCallbacks;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.api.responses.LookupCountriesResponse;
import com.playnetwork.pna.data.api.responses.LookupIndustriesResponse;
import com.playnetwork.pna.room.PlayNetworkDatabase;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutoRefreshAsyncTask extends AsyncTask<Void, Void, Void> {
    private HttpService httpService;

    public AutoRefreshAsyncTask(HttpService httpService) {
        this.httpService = httpService;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        fetchIndustries(this.httpService, null);
        fetchCountries(this.httpService, null);
        return null;
    }

    private void fetchIndustries(final HttpService httpService, final ApiClientCallbacks.LookupIndustriesListener lookupIndustriesListener) {
        Callback<LookupIndustriesResponse> callback = new Callback<LookupIndustriesResponse>() {
            @Override
            public void onResponse(Call<LookupIndustriesResponse> call, Response<LookupIndustriesResponse> response) {
                LookupIndustriesResponse lookupIndustriesResponse = response.body();
                if (ApiUtils.isSuccessResponse(response)) {
                    PlayNetworkDatabase.getPlayNetworkDatabase().industryDao().insertAllIndustries(ListUtils.convertIndustryListToArray(lookupIndustriesResponse.getIndustries()));
                    if (lookupIndustriesListener != null) {
                        lookupIndustriesListener.onIndustriesFetched(lookupIndustriesResponse);
                    }
                }
            }

            @Override
            public void onFailure(Call<LookupIndustriesResponse> call, Throwable t) {
                t.printStackTrace();
            }
        };
        httpService.fetchIndustries(callback);
    }

    private void fetchCountries(final HttpService httpService, final ApiClientCallbacks.LookupCountriesListener lookupCountriesListener) {
        Callback<LookupCountriesResponse> callback = new Callback<LookupCountriesResponse>() {
            @Override
            public void onResponse(Call<LookupCountriesResponse> call, Response<LookupCountriesResponse> response) {
                LookupCountriesResponse lookupCountriesResponse = response.body();
                if (ApiUtils.isSuccessResponse(response)) {
                    PlayNetworkDatabase.getPlayNetworkDatabase().countryDao().insertAllCountries(ListUtils.convertCountryListToArray(lookupCountriesResponse.getCountries()));
                    if (lookupCountriesListener != null) {
                        lookupCountriesListener.onCountriesFetched(lookupCountriesResponse);
                    }
                }
            }

            @Override
            public void onFailure(Call<LookupCountriesResponse> call, Throwable t) {
                t.printStackTrace();
            }
        };
        httpService.fetchCountries(callback);
    }
}
