package com.playnetwork.pna.utils;

public class Transformers {

    public static String capitalizeTag(String tag) {
        return Character.toUpperCase(tag.charAt(0)) + tag.substring(1);
    }
}
