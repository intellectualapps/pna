package com.playnetwork.pna.utils;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.data.api.ApiClientCallbacks;
import com.playnetwork.pna.data.api.HttpService;
import com.playnetwork.pna.data.api.responses.LookupCountriesResponse;
import com.playnetwork.pna.data.api.responses.LookupIndustriesResponse;
import com.playnetwork.pna.data.api.responses.LookupStatesResponse;
import com.playnetwork.pna.data.api.responses.PublicKeyResponse;
import com.playnetwork.pna.data.api.responses.SubscriptionAmountResponse;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.room.PlayNetworkDatabase;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutoRefreshUtils {

    public static void fetchIndustries(final HttpService httpService, final ApiClientCallbacks.LookupIndustriesListener lookupIndustriesListener) {
        Callback<LookupIndustriesResponse> callback = new Callback<LookupIndustriesResponse>() {
            @Override
            public void onResponse(Call<LookupIndustriesResponse> call, Response<LookupIndustriesResponse> response) {
                LookupIndustriesResponse lookupIndustriesResponse = response.body();
                if (ApiUtils.isSuccessResponse(response) && lookupIndustriesResponse != null) {
                    PlayNetworkDatabase.getPlayNetworkDatabase().industryDao().insertAllIndustries(ListUtils.convertIndustryListToArray(lookupIndustriesResponse.getIndustries()));
                    if (lookupIndustriesListener != null) {
                        lookupIndustriesListener.onIndustriesFetched(lookupIndustriesResponse);
                    }
                }
            }

            @Override
            public void onFailure(Call<LookupIndustriesResponse> call, Throwable t) {
                t.printStackTrace();
            }
        };
        httpService.fetchIndustries(callback);
    }

    public static void fetchCountries(final HttpService httpService, final ApiClientCallbacks.LookupCountriesListener lookupCountriesListener) {
        Callback<LookupCountriesResponse> callback = new Callback<LookupCountriesResponse>() {
            @Override
            public void onResponse(Call<LookupCountriesResponse> call, Response<LookupCountriesResponse> response) {
                LookupCountriesResponse lookupCountriesResponse = response.body();
                if (ApiUtils.isSuccessResponse(response) && lookupCountriesResponse != null) {
                    PlayNetworkDatabase.getPlayNetworkDatabase().countryDao().insertAllCountries(ListUtils.convertCountryListToArray(lookupCountriesResponse.getCountries()));
                    if (lookupCountriesListener != null) {
                        lookupCountriesListener.onCountriesFetched(lookupCountriesResponse);
                    }
                }
            }

            @Override
            public void onFailure(Call<LookupCountriesResponse> call, Throwable t) {
                t.printStackTrace();
            }
        };
        httpService.fetchCountries(callback);
    }

    public static void fetchStates(final HttpService httpService, String countryId, final ApiClientCallbacks.LookupStatesListener lookupStatesListener) {
        Callback<LookupStatesResponse> callback = new Callback<LookupStatesResponse>() {
            @Override
            public void onResponse(Call<LookupStatesResponse> call, Response<LookupStatesResponse> response) {
                LookupStatesResponse lookupStatesResponse = response.body();
                if (ApiUtils.isSuccessResponse(response) && lookupStatesResponse != null) {
                    PlayNetworkDatabase.getPlayNetworkDatabase().stateDao().insertAllStates(ListUtils.convertStateListToArray(lookupStatesResponse.getStates()));
                    if (lookupStatesListener != null) {
                        lookupStatesListener.onStatesFetched(lookupStatesResponse);
                    }
                }
            }

            @Override
            public void onFailure(Call<LookupStatesResponse> call, Throwable t) {
                t.printStackTrace();
            }
        };
        httpService.fetchStates(countryId, callback);
    }

    public static void fetchSubscriptionAmount(final HttpService httpService, final ApiClientCallbacks.SubscriptionAmountListener subscriptionAmountListener) {
        Callback<SubscriptionAmountResponse> callback = new Callback<SubscriptionAmountResponse>() {
            @Override
            public void onResponse(Call<SubscriptionAmountResponse> call, Response<SubscriptionAmountResponse> response) {
                SubscriptionAmountResponse subscriptionAmountResponse = response.body();
                if (ApiUtils.isSuccessResponse(response)) {
                    PlayNetworkDatabase playNetworkDatabase = PlayNetworkDatabase.getPlayNetworkDatabase();
                    Device device = playNetworkDatabase.deviceDao().getDevice();
                    if (subscriptionAmountResponse != null) {
                        device.setSubscriptionAmount(subscriptionAmountResponse.getAmount());
                    }
                    playNetworkDatabase.deviceDao().insertDevice(device);
                    if (subscriptionAmountListener != null) {
                        subscriptionAmountListener.onSubscriptionAmountFetched(subscriptionAmountResponse);
                    }
                    playNetworkDatabase = null;
                    PlayNetworkDatabase.destroyDatabaseInstance();
                }
            }

            @Override
            public void onFailure(Call<SubscriptionAmountResponse> call, Throwable t) {
                t.printStackTrace();
            }
        };
        if (NetworkUtils.isConnected(PlayNetworkApplication.getAppInstance().getApplicationContext())) {
            httpService.fetchSubscriptionAmount(callback);
        }
    }

    public static void fetchPublicKey(final HttpService httpService, final ApiClientCallbacks.PublicKeyListener publicKeyListener) {
        Callback<PublicKeyResponse> callback = new Callback<PublicKeyResponse>() {
            @Override
            public void onResponse(Call<PublicKeyResponse> call, Response<PublicKeyResponse> response) {
                PublicKeyResponse publicKeyResponse = response.body();
                if (ApiUtils.isSuccessResponse(response)) {

                    if (publicKeyListener != null) {
                        publicKeyListener.onPublicKeyFetched(publicKeyResponse);
                    }
                }
            }

            @Override
            public void onFailure(Call<PublicKeyResponse> call, Throwable t) {
                t.printStackTrace();
            }
        };
        if (NetworkUtils.isConnected(PlayNetworkApplication.getAppInstance().getApplicationContext())) {
            httpService.fetchPublicKey(callback);
        }
    }
}
