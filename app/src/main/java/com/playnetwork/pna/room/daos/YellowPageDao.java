package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.YellowPage;

import java.util.List;

@Dao
public interface YellowPageDao {
    @Query("SELECT * FROM yellowpage ORDER BY businessName ASC LIMIT 10")
    List<YellowPage> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertYellowPage(YellowPage yellowPage);

    @Insert
    Long[] insertAll(YellowPage... yellowPages);

    @Delete
    void deleteAllYellowPages(YellowPage... yellowPages);

    @Query("DELETE FROM yellowpage")
    public void deleteCachedYellowPages();
}
