package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.Industry;

import java.util.List;

@Dao
public interface IndustryDao {
    @Query("SELECT * FROM industry")
    List<Industry> getAllIndustries();

    @Query("SELECT * FROM industry WHERE id IN (:industryIds)")
    List<Industry> loadAllByIds(int[] industryIds);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertAllIndustries(Industry... industries);
}
