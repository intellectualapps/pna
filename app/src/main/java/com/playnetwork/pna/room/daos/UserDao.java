package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.playnetwork.pna.data.models.User;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    List<User> getAll();

    @Query("SELECT * FROM user WHERE id IN (:userIds)")
    List<User> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM user WHERE fname LIKE :fname AND "
            + "lname LIKE :lname LIMIT 1")
    User findByName(String fname, String lname);

    @Query("SELECT * FROM user WHERE email LIKE :email LIMIT 1")
    User findByEmail(String email);

    @Query("SELECT * FROM user ORDER by timestamp DESC LIMIT 1")
    User findLastLoggedInUser();

    @Update
    public int updateUser(User user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertUser(User user);

    @Insert
    Long[] insertAll(User... users);

    @Delete
    void deleteUser(User user);

    @Delete
    void deleteAllUsers(User... users);
}
