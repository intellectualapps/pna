package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.Offer;

import java.util.List;

@Dao
public interface OfferDao {
    @Query("SELECT * FROM offer")
    List<Offer> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertOffer(Offer offer);

    @Insert
    Long[] insertAll(Offer... offers);

    @Delete
    void deleteAllOffers(Offer... offers);

    @Query("DELETE FROM offer")
    public void deleteCachedOffers();
}
