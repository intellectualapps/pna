package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.playnetwork.pna.data.models.CalendarEvent;

import java.util.List;

@Dao
public interface CalendarEventDao {
    @Query("SELECT * FROM calendarevent")
    List<CalendarEvent> getAll();

    @Query("SELECT * FROM calendarevent WHERE eventId = :eventId")
    CalendarEvent getEventWithEventId(String eventId);

    @Query("SELECT * FROM calendarevent WHERE calendarEventId = :calendarEventId")
    CalendarEvent getEventWithCalendarEventId(String calendarEventId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertCalendarEvent(CalendarEvent calendarEvent);

    @Update
    public int updateCalendarEvent(CalendarEvent calendarEvent);

    @Insert
    Long[] insertAll(CalendarEvent... calendarEvents);

    @Delete
    void deleteAllEvents(CalendarEvent... calendarEvents);

    @Query("DELETE FROM calendarevent")
    public void deleteCachedCalendarEvents();

    @Delete
    void deleteCalendarEvent(CalendarEvent calendarEvent);
}
