package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.User;

import java.util.List;

@Dao
public interface CountryDao {
    @Query("SELECT * FROM country")
    List<Country> getAllCountries();

    @Query("SELECT * FROM country WHERE id IN (:countryIds)")
    List<Country> loadAllByIds(int[] countryIds);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertAllCountries(Country... countries);

    @Delete
    void delete(User user);
}
