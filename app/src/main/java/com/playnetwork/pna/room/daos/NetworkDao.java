package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.Network;

import java.util.List;

@Dao
public interface NetworkDao {
    @Query("SELECT * FROM network WHERE mode = 'NETWORK' ORDER BY networkId DESC")
    List<Network> getMemberNetwork();

    @Query("SELECT * FROM network WHERE mode = 'REQUEST' ORDER BY networkId DESC")
    List<Network> getRequests();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertNetwork(Network network);

    @Insert
    Long[] insertAll(Network... networks);

    @Delete
    void deleteAllNetworks(Network... networks);

    @Query("DELETE FROM network WHERE mode = 'NETWORK'")
    public void deleteCachedNetworks();

    @Query("DELETE FROM network WHERE mode = 'REQUEST'")
    public void deleteCachedRequests();
}
