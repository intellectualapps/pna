package com.playnetwork.pna.room;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;

import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.data.models.CalendarEvent;
import com.playnetwork.pna.data.models.Country;
import com.playnetwork.pna.data.models.Device;
import com.playnetwork.pna.data.models.Event;
import com.playnetwork.pna.data.models.Feed;
import com.playnetwork.pna.data.models.HomeScreenItem;
import com.playnetwork.pna.data.models.Industry;
import com.playnetwork.pna.data.models.Network;
import com.playnetwork.pna.data.models.Offer;
import com.playnetwork.pna.data.models.State;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.data.models.YellowPage;
import com.playnetwork.pna.room.daos.CalendarEventDao;
import com.playnetwork.pna.room.daos.CountryDao;
import com.playnetwork.pna.room.daos.DeviceDao;
import com.playnetwork.pna.room.daos.EventDao;
import com.playnetwork.pna.room.daos.FeedDao;
import com.playnetwork.pna.room.daos.HomeScreenItemDao;
import com.playnetwork.pna.room.daos.IndustryDao;
import com.playnetwork.pna.room.daos.NetworkDao;
import com.playnetwork.pna.room.daos.OfferDao;
import com.playnetwork.pna.room.daos.StateDao;
import com.playnetwork.pna.room.daos.UserDao;
import com.playnetwork.pna.room.daos.YellowPageDao;
import com.playnetwork.pna.room.utils.Converters;

@Database(entities = {User.class, Industry.class, Country.class, State.class, Device.class, Feed.class, Network.class, Offer.class, Event.class, YellowPage.class, HomeScreenItem.class, CalendarEvent.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class PlayNetworkDatabase extends RoomDatabase {
    private static PlayNetworkDatabase playNetworkDatabase;

    public abstract UserDao userDao();

    public abstract IndustryDao industryDao();

    public abstract CountryDao countryDao();

    public abstract StateDao stateDao();

    public abstract DeviceDao deviceDao();

    public abstract FeedDao feedDao();

    public abstract NetworkDao networkDao();

    public abstract OfferDao offerDao();

    public abstract EventDao eventDao();

    public abstract YellowPageDao yellowPageDao();

    public abstract HomeScreenItemDao homeScreenItemDao();

    public abstract CalendarEventDao calendarEventDao();

    public static PlayNetworkDatabase getPlayNetworkDatabase() {
        if (playNetworkDatabase == null) {
            playNetworkDatabase = Room.databaseBuilder(PlayNetworkApplication.getAppInstance().getApplicationContext(),
                    PlayNetworkDatabase.class, "play-network-db")
                    .allowMainThreadQueries()
                    //.addMigrations(MIGRATION_1_2)
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return playNetworkDatabase;
    }

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE user "
                    + " ADD COLUMN extraField TEXT");
        }
    };

    public static void destroyDatabaseInstance() {
        playNetworkDatabase = null;
    }
}
