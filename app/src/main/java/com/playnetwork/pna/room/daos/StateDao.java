package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.State;

import java.util.List;

@Dao
public interface StateDao {
    @Query("SELECT * FROM state")
    List<State> getAllStates();

    @Query("SELECT * FROM state WHERE countryId = :countryId")
    List<State> loadAllStatesByCountry(int countryId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertAllStates(State... state);
}
