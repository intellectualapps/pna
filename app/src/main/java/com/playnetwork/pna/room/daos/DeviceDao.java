package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.Device;

import java.util.List;

@Dao
public interface DeviceDao {
    @Query("SELECT * FROM device LIMIT 1")
    Device getDevice();

    @Query("SELECT * FROM device")
    List<Device> getAllDevices();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertDevice(Device device);
}
