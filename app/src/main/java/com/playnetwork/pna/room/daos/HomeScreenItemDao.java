package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.HomeScreenItem;

import java.util.List;

@Dao
public interface HomeScreenItemDao {
    @Query("SELECT * FROM homescreenitem")
    List<HomeScreenItem> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertHomeScreenItem(HomeScreenItem homeScreenItem);

    @Insert
    Long[] insertAll(HomeScreenItem... homeScreenItems);

    @Delete
    void deleteAllHomeScreenItems(HomeScreenItem... homeScreenItems);

    @Query("DELETE FROM homescreenitem")
    public void deleteCachedHomeScreenItems();
}
