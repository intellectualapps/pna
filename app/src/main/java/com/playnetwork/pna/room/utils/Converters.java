package com.playnetwork.pna.room.utils;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.playnetwork.pna.data.models.Experience;
import com.playnetwork.pna.data.models.Table;
import com.playnetwork.pna.data.models.Ticket;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class Converters {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<String> fromString(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<String>>() {
        }.getType();
        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String fromList(List<String> list) {
        return gson.toJson(list);
    }

    @TypeConverter
    public static List<Experience> experienceListFromString(String value) {
        Type listType = new TypeToken<List<Experience>>() {
        }.getType();
        return gson.fromJson(value, listType);
    }

    @TypeConverter
    public static String stringFromExperienceList(List<Experience> list) {
        return gson.toJson(list);
    }

    @TypeConverter
    public static Table tableFromString(String value) {
        Type tableType = new TypeToken<Table>() {
        }.getType();
        return gson.fromJson(value, tableType);
    }

    @TypeConverter
    public static String stringFromTable(Table table) {
        return gson.toJson(table);
    }

    @TypeConverter
    public static Ticket ticketFromString(String value) {
        Type ticketType = new TypeToken<Ticket>() {
        }.getType();
        return gson.fromJson(value, ticketType);
    }

    @TypeConverter
    public static String stringFromTicket(Ticket ticket) {
        return gson.toJson(ticket);
    }
}
