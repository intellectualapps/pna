package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.Event;

import java.util.List;

@Dao
public interface EventDao {
    @Query("SELECT * FROM event")
    List<Event> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insertEvent(Event event);

    @Insert
    Long[] insertAll(Event... events);

    @Delete
    void deleteAllEvents(Event... events);

    @Query("DELETE FROM event")
    public void deleteCachedEvents();
}
