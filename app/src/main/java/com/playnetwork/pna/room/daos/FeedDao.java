package com.playnetwork.pna.room.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.playnetwork.pna.data.models.Feed;

import java.util.List;

@Dao
public interface FeedDao {
    @Query("SELECT * FROM feed ORDER BY feedId DESC LIMIT 10")
    List<Feed> getFeedItems();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertFeedItems(Feed... feeds);

    @Query("DELETE FROM feed")
    public void deleteCachedFeed();
}
