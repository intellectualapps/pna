package com.playnetwork.pna.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.playnetwork.pna.utils.FCMTokenUtils;

public class InstanceIdListenerService extends FirebaseInstanceIdService {
    private static final String TAG = "FCM LISTENER";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        updateFCMToken(refreshedToken);
    }

    private void updateFCMToken(String token) {
        new FCMTokenUtils().execute();
    }
}
