package com.playnetwork.pna.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.playnetwork.pna.PlayNetworkApplication;
import com.playnetwork.pna.R;
import com.playnetwork.pna.data.models.User;
import com.playnetwork.pna.room.PlayNetworkDatabase;
import com.playnetwork.pna.ui.activities.MainActivity;
import com.playnetwork.pna.utils.AppLifecycleHandler;
import com.playnetwork.pna.utils.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FirebaseCloudMessagingListenerService extends FirebaseMessagingService {
    private static final String TAG = "FCM";
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder notificationBuilder;
    Handler handler;
    Map<String, String> bundle = new HashMap<String, String>();
    String notificationCategory = null;
    User user = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> data = remoteMessage.getData();
            this.bundle = data;
            Log.w(TAG, data.toString());
            user = PlayNetworkDatabase.getPlayNetworkDatabase().userDao().findLastLoggedInUser();
            if (user != null && !TextUtils.isEmpty(user.getEmail()) && user.isLoggedIn()) {
                buildNotification(remoteMessage.getData());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void buildNotification(Map<String, String> notificationData) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String channelId = getString(R.string.default_notification_channel_id);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence channelName = "Play Network Africa";
            String channelDescription = "";
            int channelImportance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName,
                    channelImportance);
            mChannel.setDescription(channelDescription);
            mChannel.setLightColor(Color.CYAN);
            mChannel.canShowBadge();
            mChannel.enableLights(true);
            mChannel.enableVibration(true);
            mChannel.setShowBadge(true);
            getNotificationManager().createNotificationChannel(mChannel);
        }

        notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_stat_play_africa_logo)
                .setAutoCancel(true)
                .setLights(Color.parseColor("blue"), 5000, 1000)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{500, 500, 500, 500, 500});

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        String photoUrl = notificationData.get(Constants.PUSH_PHOTO_URL);
        if (!TextUtils.isEmpty(photoUrl)) {
            bitmap = getBitmapFromURL(photoUrl);
        }

        if (!AppLifecycleHandler.isApplicationInForeground() && !AppLifecycleHandler.isApplicationVisible()) {
            displayNotification(bitmap, notificationData, notificationBuilder);
        } else {
            displayNotification(bitmap, notificationData, notificationBuilder);
            sendChatNotificationIntent(notificationData.get(Constants.NOTIFICATION_TITLE), notificationData.get(Constants.NOTIFICATION_CONTENT));
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            Vibrator vibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
            if (vibrator != null) {
                vibrator.vibrate(1000);
            }
        }
    }

    private NotificationManager getNotificationManager() {
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mNotificationManager;
    }

    private void sendChatNotificationIntent(String title, String message) {
        Intent intent = new Intent();
        intent.setAction(Constants.NOTIFICATION_POPUP_LISTENER);
        intent.putExtra(Constants.TITLE, title);
        intent.putExtra(Constants.MESSAGE, message);
        PlayNetworkApplication.getAppInstance().getApplicationContext().sendBroadcast(intent);
    }

    private void displayNotification(Bitmap bitmap, Map<String, String> notificationData, NotificationCompat.Builder notificationBuilder) {
        Random rnd = new Random();
        int notificationId = 100000 + rnd.nextInt(900000);

        Intent intent = new Intent(this, MainActivity.class);
        notificationBuilder.setLargeIcon(bitmap);

        String notificationType = notificationData.get(Constants.NOTIFICATION_TYPE);
        String notificationTitle = notificationData.get(Constants.NOTIFICATION_TITLE);
        String notificationContent = notificationData.get(Constants.NOTIFICATION_CONTENT);
        String memberId = notificationData.get(Constants.NOTIFICATION_MEMBER_ID);
        String memberFullName = notificationData.get(Constants.NOTIFICATION_MEMBER_FULL_NAME);
        String networkId = notificationData.get(Constants.NOTIFICATION_NETWORK_ID);
        String photoUrl = notificationData.get(Constants.PUSH_PHOTO_URL);

        if (networkId == null && memberId == null) {
            return;
        }

        notificationBuilder.setLargeIcon(bitmap);

        notificationBuilder.setContentTitle(notificationTitle);
        notificationBuilder.setContentText(notificationContent);
        notificationBuilder.setWhen(0);
        notificationBuilder.setTicker(getString(R.string.app_name));

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        intent.putExtra(Constants.NOTIFICATION_FLAG, true);
        switch (notificationType) {
            default:
            case Constants.NOTIFICATION_NETWORK_REQUEST_TYPE:
                intent.putExtra(Constants.NOTIFICATION_VIEW_TAG, Constants.NETWORK_REQUEST_VIEW_TAG);
                break;
            case Constants.NOTIFICATION_REQUEST_DECLINED_TYPE:
                intent.putExtra(Constants.NOTIFICATION_VIEW_TAG, Constants.DECLINE_DIALOG_VIEW_TAG);
                break;
            case Constants.NOTIFICATION_REQUEST_ACCEPTED_TYPE:
                intent.putExtra(Constants.NOTIFICATION_VIEW_TAG, Constants.MEMBER_PROFILE_VIEW_TAG);
                break;
        }

        intent.putExtra(Constants.NOTIFICATION_NETWORK_ID, networkId);
        intent.putExtra(Constants.NOTIFICATION_MEMBER_FULL_NAME, memberFullName);
        intent.putExtra(Constants.NOTIFICATION_TYPE, notificationType);
        intent.putExtra(Constants.NOTIFICATION_MEMBER_ID, memberId);
        intent.putExtra(Constants.NOTIFICATION_CONTENT, notificationContent);

        PendingIntent notificationPendingIntent =
                PendingIntent.getActivity(
                        this,
                        (int) System.currentTimeMillis(),
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        notificationBuilder.setContentIntent(notificationPendingIntent);
        getNotificationManager().notify(notificationId, notificationBuilder.build());
    }
}
